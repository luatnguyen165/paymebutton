const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const _ = require('lodash');
const GeneralConstants = require('../../voucher/constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	accountId: {
		type: Number,
		required: true,
	},
	merchantId: {
		type: Number,
		required: true,
	},
	name: {
		type: String,
		required: true
	},
	voucherCode: {
		type: String,
		required: true,
	},
	description: {
		type: String
	},
	amount: {
		type: Number,
		default: 0
	},
	unit: {
		type: String,
		required: true,
		enum: _.values(GeneralConstants.VOUCHER_UNIT)
	},
	quantity: { // tổng số voucher
		type: Number,
		default: 10000
	},
	quantityRemaining: { // số lượng còn lại
		type: Number,
		default: 0
	},
	minAmount: { // thanh toán tối thiểu
		type: Number,
		default: 0
	},
	maxDiscount: { // số tiền tối đa được giảm
		type: Number,
		default: 0
	},
	expiredIn: {
		from: {
			type: Date,
		},
		to: {
			type: Date
		}
	},
	allowMethods: [String],
	isActive: {
		type: Boolean,
		default: true
	}
}, {
	collection: 'Voucher',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
Schema.index({ voucherCode: 1, merchantId: 1 }, { unique: true });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
