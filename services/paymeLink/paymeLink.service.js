/* eslint-disable no-throw-literal */
/* eslint-disable object-shorthand */
const _ = require('lodash');
const Cron = require('moleculer-cron');
const { I18n } = require('i18n');
const path = require('path');
const GeneralConstants = require('./constants/general.constant');
const statusPaymeLink = require('./constants/status');
const typePaymeLink = require('./constants/type');

const DIRNAME = __dirname;

module.exports = {
	name: 'paymeLink',
	mixins: [Cron],
	version: 1,

	hooks: {
		before: {
			// '*': ['checkIsAuthenticated'],
			create: ['checkUserScope'],
			createBulk: ['checkUserScope'],
			update: ['checkUserScope'],
			read: ['checkUserScope'],
			readDetailInternal: ['checkUserScope'],
			sendMail: ['checkUserScope']
		},
		error: {
			'*': function (ctx, err) {
				this.logger.error(`Error occurred when '${ctx.action.name}' action was called`, err);
				return _.pick(err, ['code', 'message']);
			}
		}
	},

	/**
	 * Settings
	 */
	settings: {
		routes: [{
		}]
	},

	dependencies: ['v1.paymeLinkModel', 'v1.paymentRequestModel'],
	crons: [
		{
			name: 'UpdateLinkExpired',
			cronTime: '0/1 * * * *', // every minutes
			onTick: async function () {
				// console.log('UpdateLinkExpired is already started....');
				await this.call('v1.paymeLink.updateExpiredLink');
			},
			runOnInit: () => {
				console.log('UpdateLinkExpired job is created');
			},
			timeZone: 'Asia/Ho_Chi_Minh'
		},
		// {
		// 	name: 'InquiredPaymentRequest',
		// 	cronTime: '0/2 * * * *', // every 5 minutes
		// 	onTick: async function () {
		// 		await this.call('v1.paymeLink.inquiredPaymentRequest');
		// 	},
		// 	runOnInit: () => {
		// 		console.log('InquiredPaymentRequest job is created');
		// 	},
		// 	timeZone: 'Asia/Ho_Chi_Minh'
		// }
	],
	/**
	 * Actions
	 */
	actions: {
		receiveCallbackSendEmailAndSMS: {
			params: {
				linkId: 'string|trim|required',
				sms: {
					$$type: 'object|optional',
					totalSuccess: 'number|optional',
					totalFail: 'number|optional',
				},
				email: {
					$$type: 'object|optional',
					totalSuccess: 'number|optional',
					totalFail: 'number|optional',
				}
			},
			handler: require('./actions/receiveCallbackSendEmailAndSMS.action'),
		},
		create: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/create',
				auth: {
					strategies: ['Default'],
					mode: 'required'
				},
			},
			params: {
				body: {
					$$type: 'object',
					customerDetail: {
						$$type: 'object|optional',
						id: 'number|optional',
						email: 'string|optional|trim',
						phone: 'string|optional|trim',
						fullname: 'string|optional|trim',
						shippingAddress: 'string|optional|trim'
					},
					orderRequiredField: {
						$$type: 'object|optional',
						email: 'boolean|optional|default:false',
						fullname: 'boolean|optional|default:false',
						phone: 'boolean|optional|default:false',
						shippingAddress: 'boolean|optional|default:false'
					},
					description: 'string|required|trim|max:1000',
					referenceId: 'string|optional|trim',
					expiredAt: {
						type: 'enum',
						optional: true,
						values: _.keys(GeneralConstants.LINK_EXPIRED_TIME)
					},
					amount: 'number|positive|required|min:10000|max:9999999999', // validate base CTT
					currency: 'string|optional|default:VND',
					isAcceptedPartialPayment: 'boolean|optional',
					// minPaidPartial: 'number|optional|min:1',
					notify: {
						$$type: 'object|optional',
						email: 'boolean|optional',
						phone: 'boolean|optional'
					},
					note: 'string|optional|trim|max:1000',
					type: {
						type: 'enum',
						optional: true,
						default: typePaymeLink.STANDARD,
						values: _.values(typePaymeLink)
					},
					attachedFiles: {
						type: 'array',
						items: {
							$$type: 'object',
							fileName: 'string|optional',
							url: 'string|optional'
						},
						optional: true,
					}
				},
			},
			scope: {
				name: ['mc.paymeLink.create'],
				condition: 'AND'
			},
			handler: require('./actions/create.action'),
		},
		createBulk: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/create/bulk',
				auth: {
					strategies: ['Default'],
					mode: 'required'
				},
			},
			params: {
				body: {
					$$type: 'object|required',
					data: {
						$$type: 'array|required',
						items: {
							$$type: 'object',
							customerDetail: {
								$$type: 'object|optional',
								id: 'number|optional',
								email: 'string|optional|trim',
								phone: 'string|optional|trim',
								fullname: 'string|optional|trim',
								shippingAddress: 'string|optional|trim'
							},
							orderRequiredField: {
								$$type: 'object|optional',
								email: 'boolean|optional|default:false',
								fullname: 'boolean|optional|default:false',
								phone: 'boolean|optional|default:false',
								shippingAddress: 'boolean|optional|default:false'
							},
							description: 'string|required|trim|max:1000',
							referenceId: 'string|optional|trim',
							expiredAt: {
								type: 'enum',
								optional: true,
								values: _.keys(GeneralConstants.LINK_EXPIRED_TIME)
							},
							amount: 'number|required|min:10000|max:999999999', // validate base CTT
							currency: 'string|optional|default:VND',
							isAcceptedPartialPayment: 'boolean|optional',
							notify: {
								$$type: 'object|optional',
								email: 'boolean|optional',
								phone: 'boolean|optional'
							},
							note: 'string|optional|trim|max:1000',
							type: {
								type: 'enum',
								optional: true,
								default: typePaymeLink.STANDARD,
								values: _.values(typePaymeLink)
							},
							attachedFiles: {
								type: 'array',
								items: {
									$$type: 'object',
									fileName: 'string|optional',
									url: 'string|optional'
								},
								optional: true,
							}
						},
					}
				}
			},
			scope: {
				name: ['mc.paymeLink.create'],
				condition: 'AND'
			},
			handler: require('./actions/createBulk.action'),
		},
		read: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/read',
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			params: {
				body: {
					$$type: 'object',
					filter: {
						$$type: 'object',
						status: [
							{
								type: 'array', items: 'string', optional: true, values: _.values(statusPaymeLink)
							},
							{
								type: 'string', optional: true, values: _.values(statusPaymeLink)
							}
						],
						linkId: 'string|optional|trim',
						referenceId: 'string|optional|trim',
						// accountId: 'number|optional',
						customerDetail: {
							$$type: 'object|optional',
							id: 'number|optional',
							phone: 'string|optional|trim',
							fullname: 'string|optional|trim',
							email: { type: 'email', optional: true },
						},
						note: 'string|optional|min:2|max:256',
						description: 'string|optional',
						type: { type: 'enum', optional: true, values: _.values(typePaymeLink) },
						// $$optional: true,
						from: { type: 'date', convert: true, optional: true },
						to: {
							type: 'date',
							convert: true,
							optional: true

						},

					},
					paging: {
						$$type: 'object',
						start: 'number',
						limit: 'number'
					},
					sort: {
						$$type: 'object'
					}
				}

			},
			scope: {
				name: ['mc.paymeLink.read'],
				condition: 'AND'
			},
			handler: require('./actions/read.action'),
		},

		update: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/link/update/:linkId',
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			params: {
				body: {
					$$type: 'object',
					status: { type: 'enum', optional: true, values: _.values(statusPaymeLink) },
					isAcceptedPartialPayment: 'boolean|optional',
					referenceId: 'string|optional',
					expiredAt: {
						type: 'enum',
						optional: true,
						values: _.keys(GeneralConstants.LINK_EXPIRED_TIME)
					},
					note: 'string|optional|trim|max:1000',
					attachedFiles: {
						type: 'array',
						items: {
							$$type: 'object',
							fileName: 'string|optional',
							url: 'string|optional'
						},
						optional: true
					},
				},
				params: {
					$$type: 'object',
					linkId: 'string',
				},
			},
			scope: {
				name: ['mc.paymeLink.update', 'mc.paymeLink.cancelLink'],
				condition: 'OR'
			},
			handler: require('./actions/update.action'),
		},
		readDetailInternal: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/link/internal/:linkId', // id of record
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			params: {
				params: {
					$$type: 'object',
					linkId: 'string'
				}
			},
			scope: {
				name: ['mc.paymeLink.read'],
				condition: 'AND'
			},
			handler: require('./actions/readDetailInternal.action'),
		},
		// no authentication
		readDetailExternal: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/link/external/:linkId', // id of record
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: {
				params: {
					$$type: 'object',
					linkId: 'string'
				}
			},
			handler: require('./actions/readDetailExternal.action'),
		},
		// no authentication
		createOrder: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/external/create_order',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: {
				body: {
					$$type: 'object',
					linkId: 'string',
					amount: 'number|positive|min:10000|max:999999999',
					username: 'string|optional',
					customerInfo: {
						$$type: 'object',
						id: 'number|optional',
						phone: 'string|trim',
						fullname: 'string|trim|optional',
						email: 'string|trim',
						shippingAddress: 'string|trim|optional'
						// shippingAddress: {
						// 	type: 'object',
						// 	optional: true,
						// 	props: {
						// 		street: 'string|trim',
						// 		ward: {
						// 			$$type: 'object',
						// 			title: 'string|trim|default:null',
						// 			identifyCode: 'string|trim|default:null',
						// 			path: 'string|trim|default:null'
						// 		},
						// 		district: {
						// 			$$type: 'object',
						// 			title: 'string|trim|default:null',
						// 			identifyCode: 'string|trim|default:null',
						// 			path: 'string|trim|default:null'
						// 		},
						// 		city: {
						// 			$$type: 'object',
						// 			title: 'string|trim|default:null',
						// 			identifyCode: 'string|trim|default:null',
						// 			path: 'string|trim|default:null'
						// 		}
						// 	}
						// }
					}
				}
			},
			handler: require('./actions/createOrder.action'),
		},
		// no authentication
		receiveIPN: {
			rest: {
				method: 'POST',
				security: false,
				fullPath: '/widgets/link/external/receiveIPN',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: {

				body: {
					$$type: 'object',
					transaction: 'string|trim',
					partnerTransaction: 'string',
					amount: 'number',
					state: 'string'
				},
			},
			handler: require('./actions/receiveIPN.action'),
		},
		sendMail: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/send_mail',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: {
				body: {
					$$type: 'object',
					linkId: 'string',
					email: 'email|trim'
				},
			},
			scope: {
				name: ['mc.paymeLink.sendEmail'],
				condition: 'AND'
			},
			handler: require('./actions/sendMail.action'),
		},
		sendEmailAndSMS: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/send_email_sms',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: {
				body: {
					$$type: 'object',
					linkId: 'string',
					method: {
						type: 'array',
						items: { type: 'string', enum: ['EMAIL', 'SMS'] },
						min: 1
					},
					chanel: {
						type: 'string',
						enum: ['INDIVIDUAL', 'GROUP']
					},
					target: {
						type: 'array',
						items: { type: 'number' },
						min: 1
					},
				},
			},
			scope: {
				name: ['mc.paymeLink.sendEmailAndSMS'],
				condition: 'AND'
			},
			handler: require('./actions/sendEmailAndSMS.action'),
		},
		updateExpiredLink: {
			handler: require('./actions/updateLinkStatusExpired.action'),
			timeout: 30000
		},
		inquiredPaymentRequest: {
			handler: require('./actions/inquiredPaymentRequest.action')
		},
		tuitionPayLink: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/external/tuition_pay',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: {
				body: {
					$$type: 'object',
					data: 'string|required'
				}
			},
			timeout: 180000, // 3mins
			handler: require('./actions/tuitionpay.action'),
		},
		teslaTuitionPayLink: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/link/external/tesla/tuition_pay',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: {
				body: {
					$$type: 'object',
					data: 'string|required'
				}
			},
			timeout: 180000,
			handler: require('./actions/teslaTuitionpay.action'),
		}
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		// eslint-disable-next-line consistent-return
		async checkUserScope(ctx) {
			this.broker.logger.info(`PaymeLink: role check response: ${JSON.stringify(ctx.action.scope)}, ${ctx.action.scope.name && ctx.action.scope.name.length > 0}`);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				this.broker.logger.info(`PaymeLink: auth info: ${JSON.stringify(auth)}`);
				if (_.get(auth, 'credentials.accountId', null) === null) {
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!'
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
				} catch (error) {
					this.broker.logger.info(`PaymeLink: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(auth.credentials)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`
					};
				}
				if (_.get(merchantInfo, 'id', null) === null) {
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[3] Không tìm thấy thông tin merchant.'
					};
				}
				const accountInfo = {
					accountId: auth.credentials.accountId,
					merchantId: auth.credentials.merchantId || merchantInfo.id
				};
				let mcRole = '';
				if (ctx.action.scope.name.includes('mc.paymeLink.read')) {
					mcRole = 'mc.paymeLink.read';
				} else if (ctx.action.scope.name.includes('mc.paymeLink.create')) {
					mcRole = 'mc.paymeLink.create';
				} else if (ctx.action.scope.name.includes('mc.paymeLink.update')) {
					mcRole = 'mc.paymeLink.update';
				} else if (ctx.action.scope.name.includes('mc.paymeLink.notify')) {
					mcRole = 'mc.paymeLink.notify';
				}
				try {
					const resultCheckRole = await this.broker.call('v1.role.get', { target: accountInfo });
					this.broker.logger.info(`PaymeLink: call v1.role.read response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager', mcRole]).length <= 0) {
						throw {
							code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden'
						};
					}
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager']).length > 0) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
							merchantInfo
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true, // creator
							merchantInfo
						};
					}
				} catch (error) {
					this.broker.logger.warn(`PaymeLink: call v1.role.get response: ${error}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}, error: ${JSON.stringify(error)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`
					};
				}
				if (_.get(auth.credentials, 'merchantId', null) === null) {
					this.broker.logger.warn(`PaymePage: Auth INfo: MCID === null ==>${JSON.stringify(auth.credentials)}`);
					auth.credentials.merchantId = merchantInfo.id;
				}
				// }
				// else {
				// 	let checkData = {};
				// 	try {
				// 		checkData = {
				// 			target: accountInfo,
				// 			scope: ctx.action.scope.name,
				// 			// storeID: 1, // optional
				// 			options: { // optional
				// 				condition: ctx.action.scope.condition // AND & OR, default AND
				// 			}
				// 		};
				// 		const resultCheckRole = await this.broker.call('v1.role.check', checkData);
				// 		this.broker.logger.info(`PaymeLink: call v1.role.check response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)}`);
				// 		if (_.get(resultCheckRole, 'isValid', false) === false) {
				// 			throw {
				// 				code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 				message: 'Forbidden'
				// 			};
				// 		}
				// 		return resultCheckRole;
				// 	} catch (error) {
				// 		this.broker.logger.warn(`PaymeLink: call v1.role.check error: ${error}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)} , error: ${JSON.stringify(error)}`);
				// 		throw {
				// 			code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 			message: `[3]${error.message}`
				// 		};
				// 	}
				// }
			}
		},
		customNanoId: require('./methods/customNanoId.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(DIRNAME, '/locales'),
			defaultLocale: 'vi',
			register: this
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		// this.$i18n.setLocale('vi');
	}
	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
