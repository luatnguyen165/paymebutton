const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: GeneralConstants.ResponseCode.REQUEST_FAILED,
			message: this.__('Thất bại'),
		};
		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';
		this.setLocale(payload.language);

		const {
			filter,
			paging,
			sort = { id: -1 }
		} = payload;
		const auth = _.get(ctx, 'meta.auth', {});
		const query = {};

		// if (_.get(auth, 'credentials.id', false) !== false) {
		// 	query['createdBy.accountId'] = auth.credentials.accountId;
		// }

		if (_.isString(filter.status)) {
			query.status = { $in: [filter.status] };
		}
		if (_.isArray(filter.status)) {
			query.status = { $in: filter.status };
		}

		if (filter.type) {
			query.type = filter.type;
		}

		if (filter.referenceId) {
			query.referenceId = filter.referenceId;
		}

		if (filter.linkId) {
			query.$or = [
				{ linkId: filter.linkId, },
				{ 'shortLinkInfo.slug': filter.linkId }
			];
		}
		if (_.get(filter, 'customerDetail.fullname', false) !== false) {
			query['customerDetail.fullname'] = filter.customerDetail.fullname;
		}
		if (_.get(filter, 'customerDetail.id', false) !== false) {
			query['customerDetail.id'] = filter.customerDetail.id;
		}
		if (_.get(filter, 'customerDetail.email', false) !== false) {
			query['customerDetail.email'] = filter.customerDetail.email;
		}
		if (_.get(filter, 'customerDetail.phone', false) !== false) {
			query['customerDetail.phone'] = filter.customerDetail.phone;
		}

		if (filter.description) {
			query.$text = { $search: filter.description };
		}

		if (_.get(filter, 'from', false) !== false || _.get(filter, 'to', false) !== false) {
			query.createdAt = {};
			if (filter.from) {
				query.createdAt.$gte = new Date(moment(filter.from).startOf('days'));
			}
			if (filter.to) {
				query.createdAt.$lte = new Date(moment(filter.to).endOf('days'));
			}
		}

		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			query['createdBy.merchantId'] = auth.credentials.merchantId || merchantInfo.id;
		} else {
			query['createdBy.accountId'] = auth.credentials.accountId;
		}
		this.broker.logger.info(`Link read filter by====> ${JSON.stringify(query)}`);
		const total = await this.broker.call('v1.paymeLinkModel.count', { query });
		let result = [];
		if (total > 0) {
			try {
				const paymeLinks = await this.broker.call('v1.paymeLinkModel.find', {
					body: {
						query, paging, sort, fields: '-_id linkId referenceId createdAt status type customerDetail description expiredAt currency isAcceptedPartialPayment minPaidPartial notify note amount createdBy paymentLink orderRequiredField shortLinkInfo paymentAt finishedAt attachedFiles isNotified totalSending'
					},
				});
				const paymentRequests = await ctx.broker.call('v1.paymentRequestModel.find', {
					body: {
						query: { referType: 'PAYME_LINK' },
						sort: { id: -1 },
					}
				});
				// #region
				/*
							try {
								const agg = [
									{
										$match: query
									},
									{
										$lookup: {
											from: 'PaymentRequest',
											localField: 'linkId',
											foreignField: 'referId',
											as: 'paymentRequest'
										}
									},
									{
										$unwind: {
											path: '$paymentRequest',
											preserveNullAndEmptyArrays: true,
										}
									},
									// {
									// 	$match: {
									// 		'paymentRequest.referType': 'PAYME_LINK',
									// 	}
									// },
									{
										$addFields: {
											orderId: '$paymentRequest.orderId'
										}
									},
									{ $sort: sort },
									{ $skip: paging.start },
									{ $limit: paging.limit },
									{
										$unset: [
											'_id',
											'paymentRequest'
										]
									}];
								const paymeLinks = await this.broker.call('v1.paymeLinkModel.aggregate', {
									pipeline: agg
								});
								ctx.broker.logger.info(`search link aggregate ===> ${ JSON.stringify(agg) }`);
				*/
				// #endregion
				result = paymeLinks.map((paymeLink) => {
					let fullLink = `${process.env.LINK_URL_SHORT}/${paymeLink.linkId}`;
					let linkIdUuid = _.get(paymeLink, 'linkId', '');
					if (_.get(paymeLink, 'shortLinkInfo.slug', null) !== null) {
						fullLink = `${process.env.LINK_URL_SHORT}/${paymeLink.shortLinkInfo.slug}`;
						linkIdUuid = paymeLink.shortLinkInfo.slug;
					}
					const payment = paymentRequests.length > 0 ? paymentRequests.find((p) => p.referId === paymeLink.linkId) : null;
					let paymentId = null;
					if (_.get(payment, 'status', false) === 'SUCCEEDED') {
						try {
							const ipnResponse = JSON.parse(payment.ipnInfo);
							paymentId = _.get(ipnResponse, 'paymentId', null);
						} catch (error) {
							this.logger.info(`[Parse_error] :>> ${JSON.stringify({ payment: JSON.stringify(payment), error: error.message })}`);
						}
					}
					const l = {
						customerDetail: paymeLink.customerDetail,
						createdBy: paymeLink.createdBy,
						description: paymeLink.description,
						referenceId: paymeLink.referenceId,
						expiredAt: paymeLink.expiredAt,
						status: paymeLink.status,
						paymentLink: fullLink,
						linkId: linkIdUuid, // paymeLink.linkId,
						amount: paymeLink.amount,
						currency: paymeLink.currency,
						isAcceptedPartialPayment: paymeLink.isAcceptedPartialPayment,
						notify: paymeLink.notify,
						note: paymeLink.note,
						type: paymeLink.type,
						orderRequiredField: paymeLink.orderRequiredField,
						shortLink: fullLink, // _.get(paymeLink, 'shortLinkInfo.url', paymeLink.paymentLink),
						createdAt: paymeLink.createdAt,
						orderId: payment ? payment.orderId : null,
						paymentId,
						paymentAt: paymeLink.paymentAt || paymeLink.finishedAt,
						attachedFiles: paymeLink.attachedFiles,
						isNotified: paymeLink.isNotified,
						totalSending: {
							email: {
								success: _.get(paymeLink.totalSending, 'email.success', 0),
								totalSend: _.get(paymeLink.totalSending, 'email.totalSend', 0)
							},
							sms: {
								success: _.get(paymeLink.totalSending, 'sms.success', 0),
								totalSend: _.get(paymeLink.totalSending, 'sms.totalSend', 0)
							}
						}
					};
					return l;
				});
			} catch (e) {
				ctx.broker.logger.warn(e);
				return dataResponse;
			}
		}
		dataResponse.code = GeneralConstants.ResponseCode.REQUEST_SUCCEEDED;
		dataResponse.message = this.__('Thành công!');
		dataResponse.data = {
			total,
			items: result
		};
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
