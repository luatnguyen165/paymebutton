const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const generalConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const auth = _.get(ctx, 'meta.auth', {});
		const dataResponse = {
			code: generalConstant.ResponseCode.REQUEST_FAILED,
			message: this.__('Thất bại'),
		};
		const payload = ctx.params;
		payload.language = payload.language || 'vi';

		this.setLocale(payload.language);

		ctx.broker.logger.warn(`Get link details internal payload: ${JSON.stringify(payload)}`);
		const query = {
			$or: [
				{ linkId: payload.params.linkId, },
				{ 'shortLinkInfo.slug': payload.params.linkId }
			]
		};
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			query['createdBy.merchantId'] = auth.credentials.merchantId || merchantInfo.id;
		} else {
			query['createdBy.accountId'] = auth.credentials.accountId;
		}

		this.broker.logger.info(`Link detail filter by====> ${JSON.stringify(query)}`);
		let paymeLink = [];
		try {
			const agg = [
				{
					$match: query
				},
				{
					$lookup: {
						from: 'PaymentRequest',
						localField: 'linkId',
						foreignField: 'referId',
						as: 'paymentRequest'
					}
				},
				{
					$unwind: {
						path: '$paymentRequest',
						preserveNullAndEmptyArrays: true,
					}
				},
				// {
				// 	$match: {
				// 		'paymentRequest.referType': 'PAYME_LINK',
				// 	}
				// },
				{
					$addFields: {
						orderId: '$paymentRequest.orderId'
					}
				},
				{ $sort: { 'paymentRequest.id': -1 } },
				{ $skip: 0 },
				{ $limit: 1 },
				{
					$unset: [
						'_id',
						'paymentRequest'
					]
				}];
			paymeLink = await this.broker.call('v1.paymeLinkModel.aggregate', {
				pipeline: agg
			});
			ctx.broker.logger.info(`get detail link internal aggregate===>${JSON.stringify(agg)}`);
		} catch (e) {
			dataResponse.message = this.__('Không tìm thấy Link Id!');
			dataResponse.code = generalConstant.ResponseCode.LINKID_NOT_FOUND;
			return dataResponse;
		}
		ctx.broker.logger.info(`get detail link internal===>${JSON.stringify(paymeLink)}`);

		if (!paymeLink || paymeLink.length <= 0) {
			dataResponse.message = this.__('Không tìm thấy Link Id');
			dataResponse.code = generalConstant.ResponseCode.LINKID_NOT_FOUND;
			return dataResponse;
		}
		// eslint-disable-next-line prefer-destructuring
		paymeLink = paymeLink[0];
		let accountCreateLink = {};
		try {
			accountCreateLink = await ctx.broker.call('v1.account.getInformationInternal', {
				accountId: _.get(paymeLink, 'createdBy.accountId', '')
			});
			ctx.broker.logger.info(`PAYME LINK call v1.account.getInformationInternal res ===> ${JSON.stringify(accountCreateLink)} filter: _.get(paymeLink, 'createdBy.accountId', '---*---')}`);
			if (!accountCreateLink || _.get(accountCreateLink, 'data.id', false) === false) {
				return {
					code: generalConstant.ResponseCode.REQUEST_FAILED,
					message: this.__('Không tìm thấy thông tin tài khoản ví')
				};
			}
		} catch (error) {
			ctx.broker.logger.warn(`PAYME LINK Error ===> ${error}`);
			return {
				code: generalConstant.ResponseCode.REQUEST_FAILED,
				message: this.__('Không tìm thấy thông tin tài khoản ví!')
			};
		}

		dataResponse.code = generalConstant.ResponseCode.REQUEST_SUCCEEDED;
		dataResponse.message = this.__('Thành công');
		dataResponse.data = {
			customerDetail: paymeLink.customerDetail,
			createdBy: {
				...paymeLink.createdBy,
				displayname: accountCreateLink.data.fullname,
				phone: accountCreateLink.data.phone,
				email: accountCreateLink.data.email
			},
			description: paymeLink.description,
			referenceId: paymeLink.referenceId,
			expiredAt: paymeLink.expiredAt,
			status: paymeLink.status,
			paymentLink: `${process.env.LINK_URL_SHORT}/${paymeLink.linkId}`, // paymeLink.paymentLink,
			linkId: paymeLink.linkId,
			amount: paymeLink.amount,
			currency: paymeLink.currency,
			isAcceptedPartialPayment: paymeLink.isAcceptedPartialPayment,
			notify: paymeLink.notify,
			note: paymeLink.note,
			type: paymeLink.type,
			orderRequiredField: paymeLink.orderRequiredField,
			shortLink: `${process.env.LINK_URL_SHORT}/${paymeLink.linkId}`, // _.get(paymeLink, 'shortLinkInfo.url', paymeLink.paymentLink),
			createdAt: paymeLink.createdAt,
			orderId: paymeLink.orderId || null,
			paymentAt: paymeLink.paymentAt || paymeLink.finishedAt,
			attachedFiles: paymeLink.attachedFiles,
			isNotified: paymeLink.isNotified,
			totalSending: {
				email: _.get(paymeLink, 'totalSending.email', 0),
				sms: _.get(paymeLink, 'totalSending.sms', 0)
			},
		};
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Button withdraw Error: ${err.message}`, 99);
	}
};
