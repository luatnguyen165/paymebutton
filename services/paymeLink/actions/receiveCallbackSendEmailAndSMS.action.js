/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params;
		ctx.broker.logger.info(`EMAIL/SMS IPN PaymeLink, data: ${JSON.stringify(payload)}`);

		const linkInfo = await ctx.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId: payload.linkId },
				{ 'shortLinkInfo.slug': payload.linkId }
			]
		}]);
		if (!linkInfo) {
			const { code } = GeneralConstants.ResponseCode.RECEIVE_IPN_FAILED;
			return { code, message: this.__('Không tìm thấy linkId') };
		}
		await this.broker.call('v1.paymeLinkModel.updateOne', [
			{
				_id: linkInfo._id,
			},
			{
				$inc: {
					'totalSending.email.success': _.get(payload, 'email.totalSuccess', 0),
					'totalSending.email.totalSend': _.toNumber(_.get(payload, 'email.totalSuccess', 0)) + _.toNumber(_.get(payload, 'email.totalFail', 0)),
					'totalSending.sms.success': _.get(payload, 'sms.totalSuccess', 0),
					'totalSending.sms.totalSend': _.toNumber(_.get(payload, 'sms.totalSuccess', 0)) + _.toNumber(_.get(payload, 'sms.totalFail', 0)),
				}
			}]);

		const { code, message } = GeneralConstants.ResponseCode.RECEIVE_IPN_SUCCEEDED;
		return { code, message: this.__(message) };
	} catch (err) {
		ctx.broker.logger.error(`PaymeLink Receive Callback EMAIL/SMS Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
