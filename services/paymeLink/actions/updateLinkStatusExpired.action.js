const _ = require('lodash');
const Async = require('async');
const AsyncForEach = require('await-async-foreach');
const StatusLink = require('../constants/status');
const PaymentRequestConstant = require('../../paymentRequestModel/constant/paymentRequest.constant');

module.exports = async function (ctx) {
	try {
		let arrLink = [];

		try {
			arrLink = await this.broker.call('v1.paymeLinkModel.findMany', [{
				status: {
					$in: [StatusLink.CREATED, StatusLink.PARTIALLY_PAID]
				},
				$and: [
					{ expiredAt: { $lt: new Date() } },
					{ expiredAt: { $ne: null } }
				]
			}, '-_id linkId status']);
		} catch (e) {
			this.logger.info('[CRON] -> [PAYME_LINK] -> [EXCEPTION] -> ', e);
		}

		// eslint-disable-next-line consistent-return

		if (arrLink.length > 0) {
			await AsyncForEach(arrLink, async (task) => {
				if ([StatusLink.CREATED, StatusLink.PARTIALLY_PAID].includes(task.status)) {
					try {
						await this.broker.call('v1.paymeLinkModel.updateOne', [
							{ linkId: task.linkId },
							{
								status: StatusLink.EXPIRED
							}
						]);

						// hủy payment request nếu có
						const paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [
							{
								referId: task.linkId,
								referType: 'PAYME_LINK',
								status: { $ne: 'SUCCEEDED' }
							}
						], '-_id id');
						if (paymentRequest) {
							await this.broker.call('v1.paymentRequestModel.updateOne', [{
								id: paymentRequest.id,
								status: { $ne: 'SUCCEEDED' }
							}, {
								status: PaymentRequestConstant.STATUS.CANCELED,
								reason: 'Link đã hết hạn'
							}]);
						}
					} catch (error) {
						ctx.broker.logger.warn(`Update expired link error:${error}`);
						return {
							message: error.message
						};
					}
				}
			}, 'parallel', 3);
		}

		return null;
	} catch (e) {
		this.logger.info('[CRON] -> [PAYME_LINK] -> [EXCEPTION] -> ', e);
	}
};
