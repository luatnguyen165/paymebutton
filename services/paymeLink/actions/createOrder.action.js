const _ = require('lodash');
const moment = require('moment');
const GeneralConstants = require('../constants/general.constant');
const StatusLink = require('../constants/status');
const StatusOrder = require('../../paymentRequestModel/constant/paymentRequest.constant');
const StatusOrderLink = require('../constants/statusOrder');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';

		this.setLocale(payload.language);

		ctx.broker.logger.warn(`Create order >>> payload ${JSON.stringify(payload)}`);

		const { linkId } = payload;

		const result = {
			code: GeneralConstants.ResponseCode.CREATE_ORDER_FAILED,
			message: this.__('Thất bại')
		};

		const linkIdExisted = await this.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId },
				{ 'shortLinkInfo.slug': linkId }
			]
		}]);

		if (!linkIdExisted && _.get(linkIdExisted, 'id', false) === false) {
			result.code = GeneralConstants.ResponseCode.LINKID_NOT_FOUND;
			result.message = this.__('Không tìm thấy link id');
			return result;
		}
		if (linkIdExisted.status === StatusLink.PAID) {
			result.code = GeneralConstants.ResponseCode.STATUS_LINK_INVALID;
			result.message = this.__('Link đã thanh toán hoàn tất');
			return result;
		}
		if (linkIdExisted.status !== StatusLink.CREATED && linkIdExisted.status !== StatusLink.PARTIALLY_PAID) {
			result.code = GeneralConstants.ResponseCode.STATUS_LINK_INVALID;
			result.message = this.__('Trạng thái link không hợp lệ');
			return result;
		}
		// // Check link is ordered
		// if ([StatusOrderLink.PROCESSING, StatusOrderLink.SUCCESS].includes(linkIdExisted.statusOrder)) {
		// 	result.code = GeneralConstants.ResponseCode.STATUS_LINK_INVALID;
		// 	result.message = this.__('Link đang chờ thanh toán, không thể tạo đơn hàng mới');
		// 	return result;
		// }
		// Case make full payment
		if (payload.amount !== linkIdExisted.amount) {
			result.code = GeneralConstants.ResponseCode.AMOUNT_INVALID;
			result.message = this.__('Số tiền không hợp lệ');
			return result;
		}

		if (_.get(linkIdExisted, 'orderRequiredField.email', false) === true && _.get(payload, 'customerInfo.email', null) === null) {
			result.code = GeneralConstants.ResponseCode.PARAMS_NOT_ENOUGH;
			result.message = this.__('Vui lòng nhập địa chỉ email');
			return result;
		}
		if (_.get(linkIdExisted, 'orderRequiredField.shippingAddress', false) === true && _.get(payload, 'customerInfo.shippingAddress', null) === null) {
			result.code = GeneralConstants.ResponseCode.PARAMS_NOT_ENOUGH;
			result.message = this.__('Vui lòng nhập địa chỉ');
			return result;
		}

		if (_.get(linkIdExisted, 'orderRequiredField.fullname', false) === true && _.get(payload, 'customerInfo.fullname', null) === null) {
			result.code = GeneralConstants.ResponseCode.PARAMS_NOT_ENOUGH;
			result.message = this.__('Vui lòng nhập đầy đủ họ tên');
			return result;
		}

		if (_.get(linkIdExisted, 'orderRequiredField.phone', false) === true && _.get(payload, 'customerInfo.phone', null) === null) {
			result.code = GeneralConstants.ResponseCode.PARAMS_NOT_ENOUGH;
			result.message = this.__('Vui lòng nhập số điện thoại');
			return result;
		}

		let merchantInfo = null;
		try {
			merchantInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: linkIdExisted.createdBy.merchantId });
			ctx.broker.logger.warn(`Create order get MC INfo-- merchantId:  ${linkIdExisted.createdBy.merchantId},  ${JSON.stringify(merchantInfo)}`);
		} catch (e) {
			result.message = this.__('Không tìm thấy thông tin merchant!');
			result.code = GeneralConstants.ResponseCode.MERCHANT_NOT_FOUND;
			return result;
		}
		if (merchantInfo.code !== 114100) {
			result.message = this.__('Không tìm thấy thông tin merchant');
			result.code = GeneralConstants.ResponseCode.MERCHANT_NOT_FOUND;
			return result;
		}
		merchantInfo = merchantInfo.data;

		let paymentMethod = null;
		if (_.get(payload, 'paymentMethod.methodId', null) !== null) {
			paymentMethod = merchantInfo.paymentMethod.find(
				(method) => method.methodId === payload.paymentMethod.methodId && method.payCode === payload.paymentMethod.payCode
			);
			// ctx.broker.logger.warn(`Get payment method Info ${JSON.stringify(paymentMethod)}`);

			if (!paymentMethod || paymentMethod.isActive === false) {
				result.message = this.__('Phương thức thanh toán không hợp lệ');
				result.code = GeneralConstants.ResponseCode.INVALID_PAYMENT_METHOD;
				return result;
			}
		}

		const customerInfoPayload = [];
		if (_.get(payload, 'customerInfo.id', null) !== null) {
			customerInfoPayload.push({
				label: 'id',
				name: _.camelCase('id'),
				value: _.get(payload.customerInfo, 'id', linkIdExisted.customerDetail.id)
			});
		}
		if (_.get(payload, 'customerInfo.fullname', null) !== null) {
			customerInfoPayload.push({
				label: 'fullname',
				name: _.camelCase('fullname'),
				value: payload.customerInfo.fullname
			});
		}
		if (_.get(payload, 'customerInfo.email', null) !== null) {
			customerInfoPayload.push({
				label: 'email',
				name: _.camelCase('email'),
				value: payload.customerInfo.email
			});
		}
		if (_.get(payload, 'customerInfo.phone', null) !== null) {
			customerInfoPayload.push({
				label: 'phone',
				name: _.camelCase('phone'),
				value: payload.customerInfo.phone
			});
		}
		if (_.get(payload, 'customerInfo.shippingAddress', null) !== null) {
			customerInfoPayload.push({
				label: 'shippingAddress',
				name: _.camelCase('shippingAddress'),
				value: payload.customerInfo.shippingAddress
			});
		}
		let paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [
			{
				referId: linkIdExisted.linkId,
				referType: 'PAYME_LINK',
				status: { $nin: ['SUCCEEDED', 'EXPIRED'] }
			}
		]);
		if (_.get(paymentRequest, 'id', null) !== null) { // đã tạo đơn hàng trước đó => update lại PTTT và expiryAt
			await this.broker.call('v1.paymentRequestModel.updateOne', [
				{ id: paymentRequest.id },
				{
					paymentMethod: paymentMethod || {},
					customerDetails: customerInfoPayload,
					expiryAt: moment(new Date()).add(15, 'minutes').toISOString()
				}
			]);
		} else {
			// chưa tạo đơn hàng lần nào => tạo mới
			const transactionId = await this.customNanoId({ prefix: 'PAYMENT_REQUEST_ID', type: 'string' });

			if (!transactionId) {
				result.code = GeneralConstants.ResponseCode.GET_TRANSACTIONID_FAILED;
				result.message = this.__('Không thể tạo được mã giao dịch');
				return result;
			}
			await this.broker.call('v1.paymeLinkModel.updateOne', [
				{ linkId: linkIdExisted.linkId },
				{ statusOrder: StatusOrderLink.PROCESSING },
			]);
			// pay full
			const paymentRequestData = {
				transactionId,
				referId: linkIdExisted.linkId,
				referType: 'PAYME_LINK',
				amount: payload.amount,
				customerDetails: customerInfoPayload,
				paymentItems: [{
					label: 'amount',
					name: _.camelCase('amount'),
					amount: payload.amount
				}],
				paymentMethod: paymentMethod || {},
				// expiryAt: linkIdExisted.expiredAt === null ? undefined : moment(linkIdExisted.expiredAt).toISOString()
			};
			paymentRequest = await this.broker.call('v1.paymentRequestModel.create', [paymentRequestData]);
		}

		if (_.get(paymentRequest, 'id', null) === null) {
			return result;
		}
		let ipnEmail = '';
		if (_.get(linkIdExisted, 'extraData', null) !== null) {
			const paymelinkData = JSON.parse(linkIdExisted.extraData);
			const aIPNEmail = _.get(paymelinkData, 'extraData.info.contact', ' Email: ').split(' Email: ');
			ipnEmail = _.isArray(aIPNEmail) && aIPNEmail.length >= 2 ? aIPNEmail[1] : '';
		}
		const customerInfoObj = {};
		// eslint-disable-next-line no-restricted-syntax
		for (const val of customerInfoPayload) {
			customerInfoObj[val.name] = val.value;
		}
		const orderData = {
			partnerTransaction: paymentRequest.transactionId,
			referType: 'PAYME_LINK',
			referId: linkIdExisted.linkId,
			accountId: linkIdExisted.createdBy.accountId,
			merchantId: _.get(linkIdExisted, 'createdBy.merchantId', ''),
			amount: payload.amount,
			username: payload.username,
			lang: payload.language,
			// expiryAt: linkIdExisted.expiredAt === null ? undefined : moment(linkIdExisted.expiredAt).toISOString(),
			redirectUrl: _.get(linkIdExisted, 'redirectURL', ''), // redirect if success or fail...
			failedUrl: linkIdExisted.paymentLink, // user cancel payment and rechoose payment method
			ipnUrl: `${process.env.HOST}/widgets/link/external/receiveIPN`,
			description: linkIdExisted.description || linkIdExisted.note || '-',
			note: linkIdExisted.note,
			referData: JSON.stringify({
				orderInfo: {
					title: linkIdExisted.description,
					paymentItems: paymentRequest.paymentItems,
					attachedFiles: linkIdExisted.attachedFiles,
				},
				customerInfo: customerInfoObj,
				voucherInfo: {},
				note: payload.note
			}),
			payMethod: paymentMethod ? paymentMethod.payCode : undefined,
			ipnEmail
		};
		if (linkIdExisted.customerDetail.id) {
			orderData.customerId = linkIdExisted.customerDetail.id;
		} else {
			const authGetCustomer = {
				data: {
					id: linkIdExisted.createdBy.accountId
				}
			};
			const bodyGetCustomer = {
				filter: {}
			};
			if (payload.customerInfo.email) {
				bodyGetCustomer.filter.email = payload.customerInfo.email;
			}
			if (payload.customerInfo.phone) {
				bodyGetCustomer.filter.phone = payload.customerInfo.phone;
			}
			if (payload.customerInfo.fullname) {
				bodyGetCustomer.filter.name = payload.customerInfo.fullname;
			}
			ctx.broker.logger.info(`[LINK] create bodyGetCustomer: ${JSON.stringify(bodyGetCustomer)}`);

			if (Object.keys(bodyGetCustomer.filter).length !== 0) {
				try {
					const customer = await this.broker.call('v1.customer.GetCustomerInfos',
						{
							body: bodyGetCustomer
						},
						{
							meta: {
								auth: authGetCustomer
							}
						});
					ctx.broker.logger.info(`[LINK] call v1.customer.GetCustomerInfos response: ${JSON.stringify(customer)}, params: ${JSON.stringify({ authGetCustomer, bodyGetCustomer })}`);
					if (_.get(customer, 'code', null) !== 1010000) {
						await this.broker.call('v1.paymeLinkModel.updateOne', [
							{ id: linkIdExisted.id },
							{
								statusOrder: StatusOrderLink.NULL
							}
						]);
						result.message = customer.message === '404' ? '[Error] Tạo đơn hàng thất bại.' : customer.message;
						return result;
					}
					if (_.get(customer, 'totalRow', 0) > 0) {
						orderData.customerId = customer.data[0].id;
					}
				} catch (error) {
					ctx.broker.logger.info(`[LINK] call v1.customer.GetCustomerInfos error: ${error.message}, params: ${JSON.stringify({ authGetCustomer, bodyGetCustomer })}`);
					await this.broker.call('v1.paymeLinkModel.updateOne', [
						{ id: linkIdExisted.id },
						{
							statusOrder: StatusOrderLink.NULL
						}
					]);
					result.message = this.__('Tạo đơn hàng thanh toán thất bại.');
					return result;
				}
			}
		}
		ctx.broker.logger.info(`[LINK] Create order input ${JSON.stringify(orderData)}`);

		let createOrder;
		try {
			createOrder = await this.broker.call('v1.order.createWeb', orderData);
			ctx.broker.logger.warn(`Create order response >>> createOrder ${JSON.stringify(createOrder)}`);
		} catch (error) {
			await this.broker.call('v1.paymeLinkModel.updateOne', [
				{ id: linkIdExisted.id },
				{
					statusOrder: StatusOrderLink.NULL
				}
			]);
			result.message = this.__('Tạo đơn hàng thanh toán thất bại');
			ctx.broker.logger.warn(error);
			return result;
		}

		if (_.get(createOrder, 'code', null) !== 105000) { // 105000: code thành công
			await this.broker.call('v1.paymeLinkModel.updateOne', [
				{ id: linkIdExisted.id },
				{
					statusOrder: StatusOrderLink.NULL
				}
			]);
			result.message = createOrder.message === '404' ? '[Error] Tạo đơn hàng thất bại!' : createOrder.message;
			return result;
		}

		const dataUpdate = {
			orderId: createOrder.data.orderId,
			paymentUrl: createOrder.data.url,
			status: StatusOrder.STATUS.PENDING
		};
		await this.broker.call('v1.paymentRequestModel.updateOne', [
			{ id: paymentRequest.id },
			dataUpdate,
		]);

		result.data = {
			url: dataUpdate.paymentUrl,
			transaction: paymentRequest.transactionId
		};

		result.code = GeneralConstants.ResponseCode.CREATE_ORDER_SUCCEEDED;
		result.message = this.__('Thành công!');
		return result;
	} catch (err) {
		ctx.broker.logger.warn(`PaymeLink Create Order Error!! ${err.message}`);
		return {
			code: GeneralConstants.ResponseCode.SYSTEM_ERROR,
			message: 'Internal Server Error'
		};
	}
};
