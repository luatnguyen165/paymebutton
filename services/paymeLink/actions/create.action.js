const _ = require('lodash');
const QRCode = require('qrcode');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const { customAlphabet } = require('nanoid');
const GeneralConstants = require('../constants/general.constant');
const TypeLink = require('../constants/type');
const StatusLink = require('../constants/status');

const createLinkId = async function (ctx) {
	try {
		const result = {
			code: GeneralConstants.ResponseCode.GET_LINKID_FAILED,
			isSucceeded: false,
			message: ctx.service.__('Không thể lấy link id'),
		};
		if (_.isFunction(ctx.customNanoId) === true) {
			result.linkId = await ctx.customNanoId({ prefix: 'PAYME_LINKS_SHORT', type: 'string' });
		} else {
			console.log(`[create.action.js] => customNanoId is NOT FUNCTION:  ${_.isFunction(ctx.customNanoId)}`);
			const nanoId = customAlphabet('0123456789abcdefghiklmnopqrstuwxyz', 8);
			result.linkId = nanoId();
		}

		if (!result.linkId && _.get(result, 'linkId', false) === false) {
			return result;
		}
		result.isSucceeded = true;
		return result;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};

const checkReferenceId = async function (referenceId, accountId, ctx) {
	try {
		const result = {
			code: GeneralConstants.ResponseCode.REFERENCEID_INVALID,
			isSucceeded: false,
			message: ctx.service.__('Mã đơn hàng đã tồn tại')
		};

		const isReferenceIdExisted = await ctx.broker.call('v1.paymeLinkModel.findOne', [{
			referenceId,
			'createdBy.accountId': accountId
		}]);
		if (_.get(isReferenceIdExisted, 'id', false) !== false) {
			return result;
		}

		result.message = ctx.service.__('Thành công');
		result.isSucceeded = true;
		return result;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};

const prepareData = async function (payload, auth, ctx) {
	try {
		const result = {
			code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
			isSucceeded: false,
			message: ctx.service.__('Thất bại')
		};
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		const createdBy = {
			merchantId: merchantInfo.id,
			accountId: auth.credentials.accountId,
			email: _.get(auth, 'data.email', null),
			phone: _.get(auth, 'data.phone', null),
		};
		const expiredDate = payload.expiredAt ? GeneralConstants.LINK_EXPIRED_TIME[payload.expiredAt] : null;
		const dataPrepared = {
			createdBy,
			customerDetail: {
				id: _.get(payload, 'customerDetail.id', null),
				email: _.get(payload, 'customerDetail.email', null),
				fullname: _.get(payload, 'customerDetail.fullname', null),
				phone: _.get(payload, 'customerDetail.phone', null),
				shippingAddress: _.get(payload, 'customerDetail.shippingAddress', null)
			},
			amount: payload.amount,
			description: _.get(payload, 'description', null),
			expiredAt: expiredDate ? moment(new Date()).add(expiredDate.amount, expiredDate.unit).toISOString() : null,
			currency: payload.currency || 'VND',
			isAcceptedPartialPayment: _.get(payload, 'isAcceptedPartialPayment', null),
			minPaidPartial: _.get(payload, 'minPaidPartial', 0),
			notify: {
				email: _.get(payload, 'notify.email', false),
				phone: _.get(payload, 'notify.phone', false)
			},
			note: payload.note || '',
			status: StatusLink.CREATED,
			orderRequiredField: {
				email: _.get(payload, 'orderRequiredField.email', false),
				shippingAddress: _.get(payload, 'orderRequiredField.shippingAddress', false),
				fullname: _.get(payload, 'orderRequiredField.fullname', false),
				phone: _.get(payload, 'orderRequiredField.phone', false)
			},
			attachedFiles: (payload.attachedFiles && payload.attachedFiles[0].url) ? payload.attachedFiles : null
		};

		dataPrepared.type = payload.type || TypeLink.STANDARD;

		if (_.get(payload, 'referenceId', false) !== false) {
			const ResultCheckReferenceId = await checkReferenceId(payload.referenceId, createdBy.accountId, ctx);
			if (ResultCheckReferenceId.isSucceeded === false) {
				result.code = ResultCheckReferenceId.code;
				result.message = ResultCheckReferenceId.message;
				return result;
			}
			dataPrepared.referenceId = payload.referenceId;
		}
		const linkId = await createLinkId(ctx);

		if (_.get(linkId, 'linkId', false) !== false) {
			dataPrepared.linkId = linkId.linkId;
			const linkUrl = process.env.LINK_URL_SHORT || 'https://payme.vn/l';
			dataPrepared.paymentLink = `${linkUrl}/${dataPrepared.linkId}`;
		} else {
			result.message = linkId.message;
			result.code = linkId.code;
			return result;
		}

		result.isSucceeded = true;
		result.message = ctx.service.__('Thành công!');
		result.dataPrepared = dataPrepared;

		return result;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};

module.exports = async function (ctx) {
	try {
		// ctx.broker.logger.info(`ENV setting=${JSON.stringify(process.env)}`);
		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';

		this.setLocale(payload.language);
		const dataResponse = {
			code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
			message: this.__('Tạo link thất bại'),
		};
		const auth = _.get(ctx, 'meta.auth', {});
		ctx.broker.logger.warn(`create link payload: ${JSON.stringify({ payload, auth })}`);
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		if (_.get(merchantInfo, 'id', null) === null) {
			if (_.get(auth.credentials, 'merchantId', null) === null) {
				return {
					code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
					message: this.__('Vui lòng cung cấp thông tin merchant')
				};
			}
		}
		auth.credentials.merchantId = auth.credentials.merchantId || merchantInfo.id;
		_.set(ctx, 'customNanoId', this.customNanoId);
		const dataPrepared = await prepareData(payload, auth, ctx);

		if (dataPrepared.isSucceeded === false) {
			dataResponse.message = dataPrepared.message;
			dataResponse.code = dataPrepared.code;
			return dataResponse;
		}

		let merchantFullInfo = null;

		try {
			merchantFullInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: auth.credentials.merchantId });
		} catch (e) {
			return {
				code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
				message: this.__('Không tìm thấy thông tin merchant!')
			};
		}
		if (merchantFullInfo.code !== 114100) {
			ctx.broker.logger.warn(`send email to MC-- merchantId:  ${auth.credentials.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			return {
				code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
				message: _.get(merchantFullInfo, 'message', 'Không tìm thấy thông tin merchant')
			};
		}
		merchantFullInfo = merchantFullInfo.data;
		let create = null;
		try {
			create = await this.broker.call('v1.paymeLinkModel.create', [dataPrepared.dataPrepared]);
		} catch (e) {
			ctx.broker.logger.warn(e);
			return dataResponse;
		}
		if (_.get(create, 'id', null) === null) {
			return dataResponse;
		}
		// let generateQR;
		// try {
		// 	generateQR = await QRCode.toDataURL(_.get(create, 'shortLinkInfo.url', ''));
		// } catch (error) {
		// 	ctx.broker.logger.warn(`tạo QRCode thất bại + ${error}`);
		// }

		// nếu có setting tự động gửi mail thì gửi cho KH nếu có chọn KH có sẵn lúc tạo link
		if (_.get(merchantFullInfo, 'isAutoEmail', false) === true && _.get(payload, 'customerDetail.id', null) !== null) {
			const dataSendMail = {
				meta: {
					merchantId: auth.credentials.merchantId,
					accountId: auth.credentials.accountId,
					chanel: 'INDIVIDUAL',
					target: [payload.customerDetail.id],
					method: ['EMAIL', 'SMS'],
					service: 'PAYME_LINK',
					extraData: { referId: create.linkId },
					brokerCall: 'v1.paymeLink.receiveCallbackSendEmailAndSMS',
					template: {
						email: 'merchant-payme-link',
						sms: 'default-sms'
					},
					content: {
						mail: {
							subject: 'PayME - Đơn hàng mới',
							content: {
								merchantLogo: _.get(merchantFullInfo, 'logo', ''),
								merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
								orderUrl: create.paymentLink,
								qrImage: `https://t.payme.vn/qr?t=${create.paymentLink}`,
							}
						},
						sms: {
							content: {
								data: `${merchantFullInfo.brandName || merchantFullInfo.fullName} gui quy khach don hang can thanh toan, vui long truy cap ${create.paymentLink} de thanh toan va hoan tat don hang. Xin cam on!`
							}
						}
					}
				}
			};
			try {
				const sendEmailAndSMSRes = await this.broker.call('v1.customer.sendEmailAndSMS', {}, dataSendMail);
				this.broker.logger.info(`PayMELink > Createlink > auto send mail/sms > broker v1.customer.sendEmailAndSMS > ${JSON.stringify({ sendEmailAndSMSRes, dataSendMail })}`);
			} catch (e) {
				ctx.broker.logger.warn(`PayMELink > Createlink > auto send mail/sms > broker v1.customer.sendEmailAndSMS > Error > ${e.message} data: ${JSON.stringify(dataSendMail)} `);
			}
		}
		/**
		// #region  gửi mail cho MC
		try {
			const dataSendMail = {
				meta: {
					merchantId: auth.credentials.merchantId,
					accountId: auth.credentials.accountId,
					chanel: 'CUSTOM',
					target: [
						{
							email: _.get(merchantFullInfo, 'contactInfo.email', ''),
						}
					],
					method: ['EMAIL'],
					service: 'PAYME_LINK',
					extraData: { referId: create.linkId },
					template: {
						email: 'default-merchant-create'
					},
					content: {
						mail: {
							subject: 'PayME - Tạo link thành công',
							content: {
								displayNameOwner: _.get(merchantFullInfo, 'fullName', 'Merchant Owner'),
								displayNameCreate: _.get(auth, 'data.username', 'Khách Hàng'),
								merchantLogo: _.get(merchantFullInfo, 'logo', ''),
								merchantTitle: _.get(merchantFullInfo, 'title', 'Tên Đăng Kí'),
								orderUrl: _.get(create, 'shortLinkInfo.url', ''),
								qrImage: `https://t.payme.vn/qr?t=${_.get(create, 'shortLinkInfo.url', '')}`,
								sendEmailUrl: `${process.env.DASHBOARD_HOST}/payment/paymentlinks/?linkId=${create.linkId}&action=share`
							}
						}
					}
				}
			};
			const sendEmailAndSMSRes = await this.broker.call('v1.customer.sendEmailAndSMS', {}, dataSendMail);
			this.broker.logger.info(`PayMELINK > CreateLink > broker v1.customer.sendEmailAndSMS > ${JSON.stringify({ sendEmailAndSMSRes, dataSendMail })}`);
		} catch (e) {
			ctx.broker.logger.warn(`PayMELINK > CreateLink > broker v1.customer.sendEmailAndSMS > Error > ${e.message} `);
		}
		 */
		// #endregion

		dataResponse.code = GeneralConstants.ResponseCode.CREATE_LINK_SUCCEEDED;
		dataResponse.message = this.__('Tạo link thành công');
		dataResponse.data = {
			customerDetail: create.customerDetail,
			createdBy: create.createdBy,
			description: create.description,
			referenceId: create.referenceId,
			expiredAt: create.expiredAt,
			status: create.status,
			paymentLink: create.paymentLink,
			linkId: create.linkId,
			amount: create.amount,
			currency: create.currency,
			isAcceptedPartialPayment: create.isAcceptedPartialPayment,
			notify: create.notify,
			note: create.note,
			type: create.type,
			orderRequiredField: create.orderRequiredField,
			shortLink: create.paymentLink, // create.shortLinkInfo.url,
			createdAt: create.createdAt,
			attachedFiles: create.attachedFiles,
		};

		return dataResponse;
	} catch (err) {
		ctx.broker.logger.warn(`create link payload error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
