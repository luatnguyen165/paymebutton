const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const StatusLink = require('../constants/status');
const StatusOrderLink = require('../constants/statusOrder');
const PaymentRequestConstant = require('../../paymentRequestModel/constant/paymentRequest.constant');

const checkExistLinkId = async function (linkId, ctx) {
	try {
		const result = {
			code: GeneralConstants.ResponseCode.LINKID_NOT_FOUND,
			isSucceeded: false,
			message: ctx.service.__('Không tìm thấy link Id')
		};

		const isLinkIdExisted = await ctx.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId },
				{ 'shortLinkInfo.slug': linkId }
			]
		}]);

		if (_.get(isLinkIdExisted, 'id', false) === false) {
			return result;
		}

		result.statusOrder = isLinkIdExisted.statusOrder;
		result.expiredAt = isLinkIdExisted.expiredAt;
		result.linkId = isLinkIdExisted.linkId;
		result.isSucceeded = true;
		result.status = isLinkIdExisted.status;
		result.createdBy = isLinkIdExisted.createdBy;

		return result;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};

module.exports = async function (ctx) {
	try {
		const auth = _.get(ctx, 'meta.auth', {});
		try {
			await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			return {
				code: error.code,
				message: error.message
			};
		}
		const dataResponse = {
			code: GeneralConstants.ResponseCode.UPDATE_LINK_FAILED,
			message: this.__('Cập nhật link thất bại')
		};
		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';
		this.setLocale(payload.language);

		if (payload.expiredAt) {
			const expiredDate = payload.expiredAt === 'NONE' ? null : GeneralConstants.LINK_EXPIRED_TIME[payload.expiredAt];
			payload.expiredAt = expiredDate === null ? null : moment(new Date()).add(expiredDate.amount, expiredDate.unit).toISOString();
		}

		const checkLinkId = await checkExistLinkId(ctx.params.params.linkId, ctx);

		if (checkLinkId.isSucceeded === false) {
			dataResponse.message = checkLinkId.message;
			dataResponse.code = checkLinkId.code;
			return dataResponse;
		}
		if (payload.status === StatusLink.CANCELLED) {
			// if (payload.status === StatusLink.CANCELLED && statusOrderLink === StatusOrderLink.PROCESSING) {
			// 	result.code = GeneralConstants.ResponseCode.LINK_PROCESSING;
			// 	result.message = 'Không được hủy link đang có đơn hàng chờ thanh toán';
			// 	return result;
			// }

			// hủy payment request nếu user hủy link
			const paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [
				{
					referId: ctx.params.params.linkId,
					referType: 'PAYME_LINK',
					status: { $ne: 'SUCCEEDED' }
				}
			]);
			if (paymentRequest) {
				await this.broker.call('v1.paymentRequestModel.updateOne', [{
					id: paymentRequest.id,
					status: { $ne: 'SUCCEEDED' }
				}, {
					status: PaymentRequestConstant.STATUS.CANCELED,
					reason: 'Merchant đã hủy link'
				}]);
			}
		}
		if (payload.referenceId) {
			const referenceIdExisted = await this.broker.call('v1.paymeLinkModel.findOne', [{
				referenceId: payload.referenceId,
				'createdBy.accountId': checkLinkId.createdBy.accountId,
				linkId: { $ne: checkLinkId.linkId }
			}]);
			if (_.get(referenceIdExisted, 'id', false) !== false) {
				dataResponse.message = this.__('Mã đơn hàng đã tồn tại');
				return dataResponse;
			}
		}
		try {
			const checkExist = await this.broker.call('v1.paymeLinkModel.updateOne', [
				{
					linkId: ctx.params.params.linkId,
				},
				payload
			]);

			if (!checkExist || checkExist.nModified <= 0) {
				return dataResponse;
			}
		} catch (e) {
			return dataResponse;
		}

		dataResponse.code = GeneralConstants.ResponseCode.UPDATE_LINK_SUCCEEDED;
		dataResponse.message = this.__('Thành công');
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Wallet withdraw Error: ${err.message}`, 99);
	}
};
