/* eslint-disable no-param-reassign */
const _ = require('lodash');
const createLinkPool = require('../service/poolCreateLink');
const GeneralConstants = require('../constants/general.constant');
const TuitionService = require('../service/tuitionService');

module.exports = async function (ctx) {
	try {
		ctx.broker.logger.info(`TUITION PAY info: ${JSON.stringify(ctx.meta)}`);
		if (_.get(ctx.meta, 'security.credentials.accountId', null) === null || _.get(ctx.meta, 'security.credentials.merchantId', null) === null) {
			return {
				code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
				message: 'Vui lòng đăng nhập hoặc xác thực tài khoản'
			};
		}
		let payload = ctx.params.body;
		payload = JSON.parse(Buffer.from(payload.data, 'base64').toString());
		ctx.broker.logger.warn(`TUITION PAY info payload: ${(JSON.stringify(payload))}`);
		let accountInfo = {};
		try {
			accountInfo = await ctx.broker.call('v1.account.getInformationInternal', {
				username: _.get(payload, 'info.merchantAccount', '')
			});
			ctx.broker.logger.info(`TUITION PAY call v1.account.getInformationInternal res ===> ${JSON.stringify(accountInfo)} filter: ${_.get(payload, 'info.merchantAccount', '---')}`);
			if (!accountInfo || _.get(accountInfo, 'data.id', false) === false) {
				return {
					code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
					message: this.__('Không tìm thấy thông tin tài khoản ví')
				};
			}
		} catch (error) {
			ctx.broker.logger.warn(`TUITION PAY Error ===> ${error}`);
			return {
				code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
				message: this.__('Không tìm thấy thông tin tài khoản ví!')
			};
		}
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: ctx.meta.security.credentials.merchantId });
		} catch (e) {
			return {
				code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
				message: this.__('Ghi nhận thất bại. Vui lòng thử lại sau!')
			};
		}
		if (merchantFullInfo.code !== 114100) {
			ctx.broker.logger.warn(`GET MC info by merchantId:  ${ctx.meta.security.credentials.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			return {
				code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
				message: this.__('Ghi nhận thất bại. Vui lòng thử lại sau.')
			};
		}
		merchantFullInfo = merchantFullInfo.data;
		const data = payload.students.map((item) => {
			item.info = payload.info;
			item.accountId = accountInfo.data.id; // 6;
			item.merchantId = ctx.meta.security.credentials.merchantId; // 22;
			item.merchantLogo = _.get(merchantFullInfo, 'logo', '');
			item.broker = this.broker;
			item.mcname = 'tueduc';
			item.mclogo = _.get(merchantFullInfo, 'logo', '');
			item.mcPaymentMethod = _.get(merchantFullInfo, 'paymentMethod', {}); // @dev Get paymentMethod to create paymentRequest with payment method is MANUAL_BANK
			return item;
		});
		ctx.broker.logger.info(`TUITION PAY sendEmailSms ===> ${_.get(payload.info, 'sendEmailSms', null) === 0}, other: ${payload.info.sendEmailSms}`);
		if (_.get(payload.info, 'sendEmailSms', null) === 0) { // không yc gửi mail
			const tuitionService = new TuitionService();
			tuitionService.calcPayment(data);
			const previewData = await tuitionService.previewData(this.broker, payload.students.length);
			ctx.broker.logger.info(`TUITION  tuitionService.previewData :${JSON.stringify(previewData)}`);
			return {
				code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_SUCCESS,
				message: this.__('Ghi nhận thành công. Hệ thống đang xử lý.'),
				data: previewData
			};
		}
		createLinkPool.init(data);
		const countSMS = payload.students.map((v) => _.get(v.studentInfo, 'parentPhone', null)).filter((v) => v !== null).length;
		const countEmail = payload.students.map((v) => _.get(v.studentInfo, 'parentEmail', null)).filter((v) => v !== null).length;
		return {
			code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_SUCCESS,
			message: this.__('Ghi nhận thành công {{countSMS}} sms, {{countEmail}} email. Hệ thống đang xử lý.', { countSMS, countEmail })
		};
	} catch (err) {
		ctx.broker.logger.warn(`TUITION PAY Error!!!!!! ===> ${err}`);
		return {
			code: GeneralConstants.ResponseCode.CALL_TUITION_PAY_FAIL,
			message: this.__('Ghi nhận thất bại. Vui lòng thử lại sau')
		};
	}
};
