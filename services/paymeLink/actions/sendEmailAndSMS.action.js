const QRCode = require('qrcode');
const _ = require('lodash');
const generalConstants = require('../constants/general.constant');
const LinkStatus = require('../constants/status');

function formatCurrency(str = '', separate = ',') {
	const number = str.toString().split('.').join('');
	return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${separate}`);
}
module.exports = async function (ctx) {
	try {
		ctx.broker.logger.info(ctx.params);
		const payload = ctx.params.body;
		this.setLocale(payload.language || 'vi');
		let resMessage = 'Email và SMS';
		if (payload.method.length === 1 && payload.method[0] === 'SMS') resMessage = 'SMS';
		if (payload.method.length === 1 && payload.method[0] === 'EMAIL') resMessage = 'Email';

		const result = {
			code: generalConstants.ResponseCode.SEND_MAIL_FAILED,
			message: this.__(`Gửi ${resMessage} thất bại`)
		};

		const paymeLink = await this.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId: payload.linkId, },
				{ 'shortLinkInfo.slug': payload.linkId }
			]
		}]);

		if (!paymeLink && _.get(paymeLink, 'id', false) === false) {
			result.code = generalConstants.ResponseCode.LINKID_NOT_FOUND;
			result.message = this.__('Không tìm thấy thông tin link id');
			return result;
		}
		if (paymeLink.status === LinkStatus.PAID) {
			result.code = generalConstants.ResponseCode.SEND_MAIL_FAILED;
			result.message = this.__('Link đã hoàn tất');
			return result;
		}

		// if (_.get(paymeLink, 'createdBy.accountId', null) !== _.get(ctx, 'meta.auth.credentials.accountId', null)) {
		// 	result.code = generalConstants.ResponseCode.ACCOUNT_INVALID;
		// 	result.message = this.__('Thông tin tài khoản không hợp lệ');
		// 	return result;
		// }
		const { credentials = {}, data = {} } = _.get(ctx, 'meta.auth', {});
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: credentials.merchantId });
		} catch (e) {
			return {
				code: generalConstants.ResponseCode.SEND_MAIL_FAILED,
				message: this.__('Không tìm thấy thông tin merchant!')
			};
		}
		if (merchantFullInfo.code !== 114100) {
			ctx.broker.logger.warn(`send email to MC-- merchantId:  ${credentials.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			return {
				code: generalConstants.ResponseCode.SEND_MAIL_FAILED,
				message: this.__('Không tìm thấy thông tin merchant')
			};
		}
		merchantFullInfo = merchantFullInfo.data;
		const dataSendMail = {
			meta: {
				merchantId: credentials.merchantId,
				accountId: credentials.accountId,
				chanel: payload.chanel,
				target: payload.target,
				method: payload.method,
				service: 'PAYME_LINK',
				brokerCall: 'v1.paymeLink.receiveCallbackSendEmailAndSMS',
				extraData: { referId: paymeLink.linkId },
				template: {
					email: 'merchant-payme-link'
				},
				content: {
					mail: {
						subject: 'PayME - Yêu cầu thanh toán mới',
						content: {
							merchantLogo: _.get(merchantFullInfo, 'logo', ''),
							merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
							// username: _.get(data, 'username', ''),
							orderUrl: paymeLink.paymentLink,
							qrImage: `https://t.payme.vn/qr?t=${paymeLink.paymentLink}`,
						}
					}
				}
			}
		};
		if (_.includes(payload.method, 'SMS')) {
			dataSendMail.meta.template.sms = 'default-sms';
			dataSendMail.meta.content.sms = {
				content: {
					data: `${merchantFullInfo.brandName || merchantFullInfo.fullName} gui quy khach don hang can thanh toan, vui long truy cap ${paymeLink.paymentLink} de thanh toan va hoan tat don hang. Xin cam on!`
				}
			};
		}
		try {
			const sendEmailAndSMSRes = await this.broker.call('v1.customer.sendEmailAndSMS', {}, dataSendMail);
			this.broker.logger.info(`PayLink > sendEmailAndSMS > broker v1.customer.sendEmailAndSMS > ${JSON.stringify({ sendEmailAndSMSRes, dataSendMail })}`);
		} catch (e) {
			ctx.broker.logger.warn(`PayLink > sendEmailAndSMS > broker v1.customer.sendEmailAndSMS > Error > ${e.message} > dataSendMail: ${JSON.stringify(dataSendMail)}`);
			return result;
		}
		await this.broker.call('v1.paymeLinkModel.updateOne', [
			{ id: paymeLink.id },
			{
				isNotified: {
					email: _.get(paymeLink, 'isNotified.email', false) || payload.method.includes('EMAIL'),
					sms: _.get(paymeLink, 'isNotified.sms', false) || payload.method.includes('SMS')
				}
			}
		]);
		result.code = generalConstants.ResponseCode.SEND_MAIL_SUCCEEDED;
		result.message = this.__(`Đã gửi ${resMessage}`);
		return result;
	} catch (err) {
		ctx.broker.logger.error(`PayMELink > sendEmailAndSMS > Error > ${err.message}`);
		return {
			code: generalConstants.ResponseCode.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
