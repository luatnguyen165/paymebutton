/* eslint-disable no-param-reassign */
const _ = require('lodash');
const moment = require('moment');
const BulkLinkPool = require('../service/bulkLinkService');
const GeneralConstants = require('../constants/general.constant');
const StatusLink = require('../constants/status');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body.data;
		ctx.broker.logger.info(`create Bulk Link payload: ${JSON.stringify(payload)}`);
		const { credentials = {} } = _.get(ctx, 'meta.auth', {});

		const data = payload.map((link) => {
			const expiredDate = link.expiredAt ? GeneralConstants.LINK_EXPIRED_TIME[payload.expiredAt] : null;
			const linkDetail = {
				customNanoId: this.customNanoId,
				broker: ctx.broker,
				merchantId: credentials.merchantId,
				accountId: credentials.accountId,
				customerDetail: {
					id: _.get(link, 'customerDetail.id', null),
					email: _.get(link, 'customerDetail.email', null),
					fullname: _.get(link, 'customerDetail.fullname', null),
					phone: _.get(link, 'customerDetail.phone', null),
					shippingAddress: _.get(link, 'customerDetail.shippingAddress', null)
				},
				amount: link.amount,
				description: _.get(link, 'description', null),
				expiredAt: expiredDate ? moment(new Date()).add(expiredDate.amount, expiredDate.unit).toISOString() : null,
				currency: link.currency || 'VND',
				isAcceptedPartialPayment: _.get(link, 'isAcceptedPartialPayment', null),
				minPaidPartial: _.get(link, 'minPaidPartial', 0),
				notify: {
					email: _.get(link, 'notify.email', false),
					phone: _.get(link, 'notify.phone', false)
				},
				note: link.note || '',
				status: StatusLink.CREATED,
				orderRequiredField: {
					email: _.get(link, 'orderRequiredField.email', false),
					shippingAddress: _.get(link, 'orderRequiredField.shippingAddress', false),
					fullname: _.get(link, 'orderRequiredField.fullname', false),
					phone: _.get(link, 'orderRequiredField.phone', false)
				},
				attachedFiles: (link.attachedFiles.length > 0 && link.attachedFiles[0].url) ? link.attachedFiles : null
			};
			return linkDetail;
		});
		BulkLinkPool.init(data);
		return {
			code: GeneralConstants.ResponseCode.CREATE_LINK_SUCCEEDED,
			message: this.__('Ghi nhận thành công. Hệ thống đang xử lý.')
		};
	} catch (err) {
		ctx.broker.logger.warn(`create Bulk Link Error!!!!!! ===> ${err}`);
		return {
			code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
			message: this.__('Ghi nhận thất bại. Vui lòng thử lại sau')
		};
	}
};
