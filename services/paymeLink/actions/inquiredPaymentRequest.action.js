const _ = require('lodash');
const Async = require('async');
const StatusLink = require('../constants/status');
const StatusOrder = require('../constants/statusOrder');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');
const GeneralConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	// eslint-disable-next-line consistent-return
	const POOL = Async.queue(async (task, callback) => {
		try {
			// ctx.broker.logger.info(`Cron inquiredPaymentRequest >> Data: ${JSON.stringify(task)}`);
			const security = {
				credentials: {
					accountId: _.get(task.paymeLink, 'createdBy.accountId', '') // task.accountId
				}
			};
			const orderQueryResult = await this.broker.call('v1.order.query', {
				body: {
					partnerTransaction: task.transactionId,
				}
			}, { meta: { security } });
			// ctx.broker.logger.info(`Cron inquiredPaymentRequest >> orderQueryResult: ${JSON.stringify({ transactionId: task.transactionId, orderQueryResult })}`);
			if (_.get(orderQueryResult, 'code', 0) === 105002) {
				const queriedOrder = _.get(orderQueryResult, 'data', {});
				const queriedOrderStatus = _.get(queriedOrder, 'state', '');
				// Only 2 IPN state: SUCCEEDED & EXPIRED & CANCELED
				// TODO: nếu có IPN trạng thái tương tự thì tránh update duplicate?
				let updateDataLink = {};
				let updateDataPaymentReq = {};
				if (queriedOrderStatus === PaymentRequestConst.IPN_STATUS.SUCCEEDED) {
					updateDataLink = {
						amountPaid: _.get(queriedOrder, 'amount', 0),
						status: StatusLink.PAID, // TODO: PARTIALLY_PAID?
						statusOrder: StatusOrder.SUCCESS,
						finishedAt: new Date()
					};
					updateDataPaymentReq = {
						status: PaymentRequestConst.STATUS.SUCCEEDED,
						reason: 'Đơn hàng CTT đã thành công'
					};

					// nếu đơn hàng thành công => thu thập thông tin KH
					const customerInfo = {};
					// eslint-disable-next-line array-callback-return
					task.customerDetails.map((v) => {
						customerInfo[v.name] = v.value || undefined;
					});
					const newCustomer = {
						name: customerInfo.fullname,
						email: customerInfo.email,
						number: customerInfo.phone ? customerInfo.phone : undefined,
						location: customerInfo.shippingAddress ? customerInfo.shippingAddress : undefined,
					};
					ctx.broker.logger.info(`[inquired payment request] Create customer in IPN customerInfo: ${JSON.stringify(newCustomer)}`);
					if (Object.values(newCustomer).filter((v) => v === undefined).length !== 4) {
						try {
							const createCustomerRes = await ctx.broker.call(
								'v1.customer.CreateCustomer',
								{ body: newCustomer },
								{
									meta: {
										auth: {
											data: {
												accountId: task.paymeLink.createdBy.accountId
											}
										}
									}
								}
							);
							ctx.broker.logger.info(`[inquired payment request] Create customer in IPN response: ${JSON.stringify(createCustomerRes)}`);
						} catch (error) {
							ctx.broker.logger.info(`[inquired payment request] Create customer in IPN error!: ${error}`);
						}
					}
				} else if (queriedOrderStatus === PaymentRequestConst.IPN_STATUS.EXPIRED) {
					updateDataLink = {
						statusOrder: StatusOrder.NULL
					};
					updateDataPaymentReq = {
						status: PaymentRequestConst.STATUS.EXPIRED,
						finishedAt: new Date(),
						reason: 'Đơn hàng CTT đã hết hạn'
					};
				} else if (queriedOrderStatus === PaymentRequestConst.IPN_STATUS.CANCELED) {
					updateDataLink = { statusOrder: StatusOrder.NULL };
					updateDataPaymentReq = {
						status: PaymentRequestConst.STATUS.CANCELED,
						reason: 'Đơn hàng CTT đã bị hủy',
						finishedAt: new Date()
					};
				}
				try {
					updateDataPaymentReq.crossChecking = true;
					ctx.broker.logger.info(`Cron inquiredPaymentRequest >> updateDataPaymentReq = ${JSON.stringify(updateDataPaymentReq)}`);

					const UpdateRequestPayment = await this.broker.call('v1.paymentRequestModel.updateOne', [
						{
							id: task.id,
							status: PaymentRequestConst.STATUS.PENDING // avoid duplicate
						},
						updateDataPaymentReq
					]);

					if (!UpdateRequestPayment || UpdateRequestPayment.modifiedRow) {
						ctx.broker.logger.warn(`Cron inquiredPaymentRequest >> Quired state = ${queriedOrderStatus} >> Update payment request error: Cant update payment status`);
					}
				} catch (e) {
					ctx.broker.logger.warn(`Cron inquiredPaymentRequest >> Quired state = ${queriedOrderStatus} >> Update payment request error: ${e}`);
				}
				if (!_.isEmpty(updateDataLink)) {
					try {
						const UpdateLink = await this.broker.call('v1.paymeLinkModel.updateOne', [
							{
								linkId: task.paymeLink.linkId,
								status: StatusLink.CREATED, // avoid duplicate
								statusOrder: StatusOrder.PROCESSING // avoid duplicate
							},
							updateDataLink
						]);

						if (!UpdateLink || UpdateLink.modifiedRow) {
							ctx.broker.logger.warn(`Cron inquiredPaymentRequest >> Quired state = ${queriedOrderStatus} >> Update link error: Cant update link`);
						}
					} catch (e) {
						ctx.broker.logger.warn(`Cron inquiredPaymentRequest >> Quired state = ${queriedOrderStatus} >> Update link error: ${e}`);
					}
				} else {
					// ctx.broker.logger.info(`Cron updateLinkStatus Success with queriedOrderStatus = ${queriedOrderStatus}`);
				}
			} else {
				ctx.broker.logger.info('Cron inquiredPaymentRequest CTT response FAILED');
			}
		} catch (error) {
			ctx.broker.logger.warn(`Cron inquiredPaymentRequest error:${error}`);
			return {
				message: error.message
			};
		}
		callback();
	}, 1);
	let pendingOrderLink = [];
	try {
		pendingOrderLink = await this.broker.call('v1.paymentRequestModel.aggregate', {
			pipeline: [
				{
					$match: {
						status: PaymentRequestConst.STATUS.PENDING,
						expiryAt: { $lt: new Date() },
						$or: [{ crossChecking: false }, { crossChecking: { $exists: false } }]
					}
				},
				{
					$lookup: {
						from: 'PaymeLink',
						localField: 'referId',
						foreignField: 'linkId',
						as: 'paymeLink'
					}
				},
				{
					$unwind: '$paymeLink'
				},
				{
					$match: {
						'paymeLink.status': StatusLink.CREATED,
						'paymeLink.statusOrder': StatusOrder.PROCESSING
					}
				},
				{
					$project: {
						_id: 0,
						'paymeLink.linkId': 1,
						'paymeLink.createdBy': 1,
						'paymeLink.statusOrder': 1,
						'paymeLink.status': 1,
						transactionId: 1,
						orderId: 1,
						status: 1,
						id: 1,
						customerDetails: 1
					}
				}
			]
		});
		ctx.broker.logger.info(`Cron inquiredPaymentRequest >> Size = ${JSON.stringify(pendingOrderLink.length)}`);
	} catch (err) {
		ctx.broker.logger.warn(`Cron inquiredPaymentRequest >> Get pending order error: ${err}`);
	}
	if (pendingOrderLink.length > 0) {
		POOL.push(pendingOrderLink);
	}
	POOL.drain = function () {
		console.log('UpdateLinkStatus all items have been processed');
	};
	return null;
};
