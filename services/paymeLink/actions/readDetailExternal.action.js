const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const generalConstant = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: generalConstant.ResponseCode.REQUEST_FAILED,
			message: 'Thất bại',
		};
		const payload = ctx.params;
		payload.language = payload.language || 'vi';

		this.setLocale(payload.language);

		ctx.broker.logger.warn(`LinkDetails External payload ${JSON.stringify(payload)}`);

		let paymeLink;
		try {
			paymeLink = await this.broker.call('v1.paymeLinkModel.findOne', [{
				$or: [
					{ linkId: payload.params.linkId },
					{ 'shortLinkInfo.slug': payload.params.linkId }
				]
			}]);
		} catch (e) {
			dataResponse.message = this.__('Không tìm thấy thông tin link!');
			dataResponse.code = generalConstant.ResponseCode.LINKID_NOT_FOUND;
			return dataResponse;
		}
		if (_.get(paymeLink, 'id', null) === null) {
			dataResponse.message = this.__('Không tìm thấy thông tin link');
			dataResponse.code = generalConstant.ResponseCode.LINKID_NOT_FOUND;
			return dataResponse;
		}
		let merchantInfo = null;
		try {
			merchantInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: paymeLink.createdBy.merchantId });
		} catch (e) {
			dataResponse.message = this.__('Không tìm thấy thông tin merchant!');
			dataResponse.code = generalConstant.ResponseCode.MERCHANT_NOT_FOUND;
			return dataResponse;
		}
		ctx.broker.logger.warn(`LinkDetails External get MC INfo merchantId:  ${paymeLink.createdBy.merchantId},  ${JSON.stringify(merchantInfo)}`);
		if (_.isEmpty(_.get(merchantInfo, 'data', {}))) {
			dataResponse.message = this.__(_.get(merchantInfo, 'message', 'Không tìm thấy thông tin merchant'));
			dataResponse.code = generalConstant.ResponseCode.LINKID_NOT_FOUND;
			return dataResponse;
		}
		if (_.get(merchantInfo, 'data.paymentMethod', null) !== null) {
			delete merchantInfo.data.paymentMethod;
		}
		dataResponse.code = generalConstant.ResponseCode.REQUEST_SUCCEEDED;
		dataResponse.message = this.__('Thành công');
		dataResponse.data = {
			customerDetail: paymeLink.customerDetail,
			createdBy: paymeLink.createdBy,
			description: paymeLink.description,
			referenceId: paymeLink.referenceId,
			expiredAt: paymeLink.expiredAt,
			status: paymeLink.status,
			paymentLink: `${process.env.LINK_URL_SHORT}/${paymeLink.linkId}`,
			linkId: paymeLink.linkId,
			amount: paymeLink.amount,
			currency: paymeLink.currency,
			isAcceptedPartialPayment: paymeLink.isAcceptedPartialPayment,
			notify: paymeLink.notify,
			note: paymeLink.note,
			type: paymeLink.type,
			orderRequiredField: paymeLink.orderRequiredField,
			shortLink: `${process.env.LINK_URL_SHORT}/${paymeLink.linkId}`, // _.get(paymeLink, 'shortLinkInfo.url', paymeLink.paymentLink),
			merchantInfo: merchantInfo.data,
			createdAt: paymeLink.createdAt,
			attachedFiles: paymeLink.attachedFiles,
		};
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Link withdraw Error: ${err.message}`, 99);
	}
};
