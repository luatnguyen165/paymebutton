const _ = require('lodash');
const generalConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const auth = _.get(ctx, 'meta.auth', {});
		try {
			await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			return {
				code: error.code,
				message: error.message
			};
		}
		const { linkId, email, language = 'vi' } = ctx.params.body;

		this.setLocale(language);

		const result = {
			code: generalConstants.ResponseCode.SEND_MAIL_FAILED,
			message: this.__('Gửi mail thất bại')
		};

		const checkExistLinkId = await this.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId },
				{ 'shortLinkInfo.slug': linkId }
			]
		}]);

		if (!checkExistLinkId && _.get(checkExistLinkId, 'id', false) === false) {
			result.code = generalConstants.ResponseCode.LINKID_NOT_FOUND;
			result.message = this.__('Không tìm thấy thông tin link id');
			return result;
		}

		if (_.get(checkExistLinkId, 'createdBy.accountId', null) !== _.get(ctx, 'meta.auth.credentials.accountId', null)) {
			result.code = generalConstants.ResponseCode.ACCOUNT_INVALID;
			result.message = this.__('Thông tin tài khoản không hợp lệ');
			return result;
		}

		const PreparedData = {
			template: 'paymelink',
			data: {
				email: {
					to: email,
					subject: `Send payment link: ${checkExistLinkId.paymentLink}`
				},
				content: {
					ownerCreateLinkEmail: _.get(checkExistLinkId, 'createdBy.email', ''),
					ownerCreateLinkPhone: _.get(checkExistLinkId, 'createdBy.phone', ''),
					paymentFor: _.get(checkExistLinkId, 'description', ''),
					amount: checkExistLinkId.amount,
					paymentLink: checkExistLinkId.paymentLink,
					activeCode: 123456
				}
			}
		};
		try {
			await this.broker.call('sendEmail.sendMail', PreparedData);
		} catch (e) {
			ctx.broker.logger.info(`SendMail sendMailData: ${JSON.stringify(PreparedData)}`);
			ctx.broker.logger.warn(`Service 'sendEmail.sendMail' Error: ${e.message}`);
			return result;
		}

		result.code = generalConstants.ResponseCode.SEND_MAIL_SUCCEEDED;
		result.message = this.__('Gửi mail thành công');
		return result;
	} catch (err) {
		ctx.broker.logger.error(`PaymeLink Send mail Error: ${err.message}`);
		return {
			code: generalConstants.ResponseCode.SYSTEM_ERROR,
			message: 'Internal Server Error'
		};
	}
};
