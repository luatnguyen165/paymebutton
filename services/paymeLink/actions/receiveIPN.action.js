const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const IPNStatus = require('../../paymentRequestModel/constant/paymentRequest.constant');
const linkStatus = require('../constants/status');
const requestPaymentStatus = require('../../paymentRequestModel/constant/paymentRequest.constant');
const statusOrderLink = require('../constants/statusOrder');

module.exports = async function (ctx) {
	try {
		const result = {
			code: GeneralConstants.ResponseCode.RECEIVE_IPN_FAILED,
			message: 'Cập nhật thất bại',
		};

		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';
		this.setLocale(payload.language);

		ctx.broker.logger.info(`Pay link order IPN payload: ${JSON.stringify(payload)}`);

		const query = {
			transactionId: payload.partnerTransaction,
			orderId: payload.transaction,
			referType: 'PAYME_LINK'
		};

		const paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [
			query,
		]);

		if (_.get(paymentRequest, 'id', false) === false) {
			result.code = GeneralConstants.ResponseCode.ORDER_NOT_FOUND;
			result.message = 'Không tìm thấy đơn hàng';
			return result;
		}

		if (paymentRequest.status !== requestPaymentStatus.STATUS.PENDING) {
			result.code = GeneralConstants.ResponseCode.PAYMENT_STATUS_INVALID;
			result.message = 'Trạng thái đơn hàng không hợp lệ';
			return result;
		}
		const checkExistedLinkId = await this.broker.call('v1.paymeLinkModel.findOne', [{
			$or: [
				{ linkId: paymentRequest.referId },
				{ 'shortLinkInfo.slug': paymentRequest.referId }
			]
		}]);

		if (!checkExistedLinkId && _.get(checkExistedLinkId, 'id', false) === false) {
			result.code = GeneralConstants.ResponseCode.LINKID_NOT_FOUND;
			result.message = 'Không tìm thấy link id';
			return result;
		}
		// nếu đơn hàng thành công => thu thập thông tin KH
		if (payload.state === IPNStatus.IPN_STATUS.SUCCEEDED) {
			const customerInfo = {};
			paymentRequest.customerDetails.map((v) => {
				customerInfo[v.name] = v.value || undefined;
			});
			const newCustomer = {
				name: customerInfo.fullname || undefined,
				email: customerInfo.email || undefined,
				number: customerInfo.phone ? customerInfo.phone : undefined,
				location: customerInfo.shippingAddress ? customerInfo.shippingAddress : undefined,
				code: customerInfo.id || _.get(checkExistedLinkId, 'customerDetail.id', undefined)
			};
			ctx.broker.logger.info(`Create customer in IPN customerInfo: ${JSON.stringify(newCustomer)}`);
			if (Object.values(newCustomer).filter((v) => v === undefined).length !== 5) {
				try {
					const createCustomerRes = await ctx.broker.call(
						'v1.customer.CreateCustomer',
						{ body: newCustomer },
						{
							meta: {
								auth: {
									data: {
										accountId: _.get(checkExistedLinkId, 'createdBy.accountId', payload.accountId)
									}
								}
							}
						}
					);
					ctx.broker.logger.info(`Create customer in IPN response: ${JSON.stringify(createCustomerRes)}`);
				} catch (error) {
					ctx.broker.logger.info(`Create customer in IPN error!: ${error}`);
				}
			}
		}
		if (checkExistedLinkId.status !== linkStatus.CREATED && checkExistedLinkId.status !== linkStatus.PARTIALLY_PAID) {
			result.code = GeneralConstants.ResponseCode.STATUS_LINK_INVALID;
			result.message = 'Trạng thái link không hợp lệ.';
			return result;
		}

		// Not include case partially payment
		if (payload.amount > checkExistedLinkId.amount) {
			result.code = GeneralConstants.ResponseCode.AMOUNT_INVALID;
			result.message = 'Số tiền không hợp lệ';
			return result;
		}
		let updateDataLink = {};
		let updateDataPaymentReq = {};
		if (payload.state === IPNStatus.IPN_STATUS.SUCCEEDED) {
			updateDataLink = {
				amountPaid: payload.amount,
				status: linkStatus.PAID,
				statusOrder: statusOrderLink.SUCCESS,
				finishedAt: new Date()
			};
			updateDataPaymentReq = {
				status: requestPaymentStatus.STATUS.SUCCEEDED,
				ipnInfo: JSON.stringify(payload),
				finishedAt: new Date()
			};
		} else if (payload.state === IPNStatus.IPN_STATUS.EXPIRED) {
			updateDataLink = { statusOrder: statusOrderLink.NULL };
			updateDataPaymentReq = {
				status: requestPaymentStatus.STATUS.EXPIRED,
				ipnInfo: JSON.stringify(payload),
				finishedAt: new Date()
			};
		} else if (payload.state === IPNStatus.IPN_STATUS.CANCELED) {
			updateDataLink = {
				statusOrder: statusOrderLink.NULL
			};
			updateDataPaymentReq = {
				status: requestPaymentStatus.STATUS.CANCELED,
				finishedAt: new Date()
			};
		}
		updateDataLink.customerDetail = {
			...checkExistedLinkId.customerDetail,
			email: _.get(paymentRequest.customerDetails.find((v) => v.name === 'email'), 'value', ''),
			fullname: _.get(paymentRequest.customerDetails.find((v) => v.name === 'fullname'), 'value', ''),
			phone: _.get(paymentRequest.customerDetails.find((v) => v.name === 'phone'), 'value', ''),
			shippingAddress: _.get(paymentRequest.customerDetails.find((v) => v.name === 'shippingAddress'), 'value', ''),
		};
		if (!_.isEmpty(updateDataLink) && !_.isEmpty(updateDataPaymentReq)) {
			try {
				const UpdateLink = await this.broker.call('v1.paymeLinkModel.updateOne', [{
					linkId: checkExistedLinkId.linkId,
					status: linkStatus.CREATED, // avoid duplicate
					statusOrder: statusOrderLink.PROCESSING // avoid duplicate
				}, updateDataLink]);

				if (!UpdateLink || UpdateLink.modifiedRow) {
					ctx.broker.logger.warn(`IPN updateLinkStatus >> Quired state = ${payload.state} >> Update link error: Cant update link`);
				}
			} catch (e) {
				ctx.broker.logger.warn(`IPN updateLinkStatus >> Quired state = ${payload.state} >> Update link error: ${e}`);
			}
			try {
				const UpdateRequestPayment = await this.broker.call('v1.paymentRequestModel.updateOne', [
					{
						id: paymentRequest.id,
						status: requestPaymentStatus.STATUS.PENDING // avoid duplicate
					},
					updateDataPaymentReq
				]);

				if (!UpdateRequestPayment || UpdateRequestPayment.modifiedRow) {
					ctx.broker.logger.warn(`IPN updateLinkStatus >> Quired state = ${payload.state} >> Update payment request error: Cant update payment status`);
				}
			} catch (e) {
				ctx.broker.logger.warn(`IPN updateLinkStatus >> Quired state = ${payload.state} >> Update payment request error: ${e}`);
			}
		} else {
			ctx.broker.logger.warn(`Ghi nhận IPN!!  statusReceived = ${payload.status}`);
		}
		// else if (payload.state === IPNStatus.IPN_STATUS.CANCELED_SUCCEEDED) { // huỷ thanh toán (hoàn tiền 100%)
		// 	try {
		// 		const UpdateLink = await this.broker.call('v1.paymeLinkModel.updateOne', [{
		// 			linkId: checkExistedLinkId.linkId,
		// 		}, {
		// 			statusOrder: statusOrderLink.NULL
		// 		}]);

		// 		if (!UpdateLink || UpdateLink.modifiedRow) {
		// 			result.message = 'Không thể cập nhật link';
		// 			result.code = GeneralConstants.ResponseCode.UPDATE_LINK_FAILED;

		// 			return result;
		// 			// console.log('update failed !!!', payload);
		// 		}

		// 		const UpdateRequestPayment = await this.broker.call('v1.paymentRequestModel.updateOne', [{
		// 			id: paymentRequest.id
		// 		}, {
		// 			status: requestPaymentStatus.STATUS.CANCELED_SUCCEEDED,
		// 			ipnInfo: JSON.stringify(payload)
		// 		}]);

		// 		if (!UpdateRequestPayment || UpdateRequestPayment.modifiedRow) {
		// 			result.message = 'Không thể cập nhật trạng thái đơn hàng';
		// 			result.code = GeneralConstants.ResponseCode.UPDATE_STATUS_ORDER_FAILED;

		// 			return result;
		// 			// console.log('update failed !!!', payload);
		// 		}
		// 		result.code = GeneralConstants.ResponseCode.CANCELED_SUCCEEDED;
		// 		result.message = 'Hủy thanh toán thành công';
		// 		return result;
		// 	} catch (e) {
		// 		ctx.broker.logger.warn(e);
		// 		result.code = GeneralConstants.ResponseCode.CANCELED_SUCCEEDED;
		// 		result.message = 'Hủy thanh toán thành công';
		// 		return result;
		// 	}
		// }
		// else if (payload.state === IPNStatus.IPN_STATUS.REFUNDED) {
		// 			if (payload.amount === checkExistedLinkId.amount) {
		// 				try {
		// 					const UpdateLink = await this.broker.call('v1.paymeLinkModel.updateOne', [{
		// 						linkId: checkExistedLinkId.linkId,
		// 					}, {
		// 						statusOrder: statusOrderLink.NULL
		// 					}]);

		// 					if (!UpdateLink || UpdateLink.modifiedRow) {
		// 						result.message = 'Không thể cập nhật link';
		// 						result.code = GeneralConstants.ResponseCode.UPDATE_LINK_FAILED;
		// 						return result;
		// 						// console.log('update failed !!!', payload);
		// 					}
		// 					const UpdateRequestPayment = await this.broker.call('v1.paymentRequestModel.updateOne', [{
		// 						id: paymentRequest.id
		// 					}, {
		// 						status: requestPaymentStatus.STATUS.REFUNDED,
		// 						ipnInfo: JSON.stringify(payload)
		// 					}]);

		// 					if (!UpdateRequestPayment || UpdateRequestPayment.modifiedRow) {
		// 						result.message = 'Không thể cập nhật trạng thái đơn hàng';
		// 						result.code = GeneralConstants.ResponseCode.UPDATE_STATUS_ORDER_FAILED;

		// 						return result;
		// 						// console.log('update failed !!!', payload);
		// 					}
		// 				} catch (e) {
		// 					ctx.broker.logger.warn(e);
		// 					result.message = 'Không thể cập nhật trạng thái đơn hàng';
		// 					result.code = GeneralConstants.ResponseCode.UPDATE_STATUS_ORDER_FAILED;
		// 					return result;
		// 				}
		// 			} else {
		// 				ctx.broker.logger.warn('REFUND payload.amount !== Link.amount');
		// 			}
		// 		}

		result.code = GeneralConstants.ResponseCode.RECEIVE_IPN_SUCCEEDED;
		result.message = 'Cập nhật thành công';
		return result;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Button withdraw Error: ${err.message}`, 99);
	}
};
