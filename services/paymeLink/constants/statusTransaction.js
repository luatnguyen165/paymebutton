module.exports = {
    SUCCESS: 'SUCCESS',
    FAIL: 'FAIL',
    CANCELLED: 'CANCELLED',
    PENDING: 'PENDING'
};