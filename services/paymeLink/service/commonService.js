const _ = require('lodash');
const { customAlphabet } = require('nanoid');

const StatusOrderLink = require('../constants/statusOrder');
const StatusOrder = require('../../paymentRequestModel/constant/paymentRequest.constant');

module.exports = {
	async createLink(payload, broker, customNanoId) {
		const returnData = {
			success: false,
			message: 'Thất bại',
			data: null
		};
		if (payload.totalPaymentAmount <= 0 && payload.amount <= 0) {
			broker.logger.info(`CREATE LINK payload:  ${JSON.stringify(payload)}`);
			returnData.message = 'Số tiền thanh toán không hợp lệ';
			return returnData;
		}

		const dataCreate = {
			createdBy: {
				merchantId: payload.merchantId,
				accountId: payload.accountId
			},
			customerDetail: {
				id: _.get(payload, 'customerDetail.id', null),
				email: _.get(payload, 'customerDetail.email', null),
				fullname: _.get(payload, 'customerDetail.fullname', null),
				phone: _.get(payload, 'customerDetail.phone', null),
				shippingAddress: _.get(payload, 'customerDetail.shippingAddress', null)
			},
			amount: payload.totalPaymentAmount || payload.amount,
			description: _.get(payload, 'description', null),
			expiredAt: payload.expiredAt || null,
			currency: payload.currency || 'VND',
			minPaidPartial: _.get(payload, 'minPaidPartial', 0),
			notify: {
				email: _.get(payload, 'notify.email', false),
				phone: _.get(payload, 'notify.phone', false)
			},
			note: payload.note || '',
			status: 'CREATED',
			orderRequiredField: {
				email: _.get(payload, 'orderRequiredField.email', false),
				shippingAddress: _.get(payload, 'orderRequiredField.shippingAddress', false),
				fullname: _.get(payload, 'orderRequiredField.fullname', false),
				phone: _.get(payload, 'orderRequiredField.phone', false)
			},
			type: 'STANDARD',
			extraData: JSON.stringify(payload),
			attachedFiles: payload.attachedFiles || null
		};
		dataCreate.linkId = null;
		try {
			// const transactionId = await this.customNanoId({ prefix: 'PAYMENT_REQUEST_ID', type: 'string' });
			if (_.isFunction(customNanoId) === true) {
				dataCreate.linkId = await customNanoId({ prefix: 'PAYME_LINKS_SHORT', type: 'string' });
			} else {
				broker.logger.info(`commonService.js => customNanoId is NOT FUNCTION:  ${_.isFunction(customNanoId)}`);
				const nanoId = customAlphabet('0123456789abcdefghiklmnopqrstuwxyz', 8);
				dataCreate.linkId = nanoId();
			}
		} catch (error) {
			broker.logger.info(`GET PAYME_LINKS_SHORT FAILED.... ${error}`);
			returnData.message = error.message;
			return returnData;
		}
		if (!dataCreate.linkId) {
			returnData.message = 'Get linkId failed!';
			return returnData;
		}
		const linkUrl = process.env.LINK_URL_SHORT || 'https://payme.vn/l';
		dataCreate.paymentLink = `${linkUrl}/${dataCreate.linkId}`;
		// try {
		// 	dataCreate.shortLinkInfo = await broker.call('v1.shortLink.pick', {
		// 		source: 'PG_LINK',
		// 		forwardTo: dataCreate.paymentLink
		// 	});
		// } catch (error) {
		// 	broker.logger.info(`GET SHORTLINK FAILED.... ${error}`);
		// 	console.log(`GET SHORTLINK FAILED.... ${error}`);
		// 	returnData.message = error.message;
		// 	return returnData;
		// }
		// if (_.get(dataCreate, 'shortLinkInfo.url', null) === null) {
		// 	broker.logger.info(`GET SHORTLINK INFO.... ${JSON.stringify(dataCreate.shortLinkInfo)}`);
		// 	console.log(`GE SHORTLINK INFO.... ${JSON.stringify(dataCreate.shortLinkInfo)}`);
		// 	dataCreate.shortLinkInfo = {
		// 		url: dataCreate.paymentLink,
		// 		slug: '',
		// 		transaction: ''
		// 	};
		// }
		let create = null;
		try {
			create = await broker.call('v1.paymeLinkModel.create', [dataCreate]);
		} catch (error) {
			broker.logger.warn(`paymeLinkModel.create Error! =====> ${error}`);
			returnData.message = error.message;
		}
		returnData.success = true;
		returnData.message = 'Tạo link thành công';
		returnData.data = create;
		return returnData;
	},

	/**
	 * Get customerId from customer info.
	 * If customer info (phone, email) is exist -> return old customerId
	 * Otherwise, create new customer and return this customerId
	 * @param {Object} customerInfo
	 * customerInfo.name: required
	 * customerInfo.email: required
	 * customerInfo.phone: optional
	 */
	async getCustomerId({ customerInfo = {}, accountId = '', broker = {} }) {
		let customerId = null;
		const customerInfoCleanData = {
			name: customerInfo.name,
			email: customerInfo.email,
			number: customerInfo.phone,
		};
		try {
			broker.logger.info(`PayMELink > commomService > getCustomerId > call 'v1.customer.CreateCustomer' with requestBody = ${JSON.stringify(customerInfoCleanData)}}`);
			const createCustomerRes = await broker.call(
				'v1.customer.CreateCustomer',
				{ body: customerInfoCleanData },
				{
					meta: {
						auth: {
							data: { accountId }
						}
					}
				}
			);
			broker.logger.info(`PayMELink > commomService > getCustomerId > call 'v1.customer.CreateCustomer' with responseData = ${JSON.stringify(createCustomerRes)}}`);
			const filterCustomer = {
				phone: customerInfo.phone,
				email: customerInfo.email,
			};
			broker.logger.info(`PayMELink > commomService > getCustomerId > call 'v1.customer.GetCustomerInfos' with filter = ${JSON.stringify(filterCustomer)}}`);
			const customers = await broker.call('v1.customer.GetCustomerInfos',
				{
					body: { filter: filterCustomer }
				},
				{
					meta: {
						auth: {
							data: { id: accountId }
						}
					}
				});
			broker.logger.info(`PayMELink > commomService > getCustomerId > call 'v1.customer.GetCustomerInfos' with responseData = ${JSON.stringify(customers)}}`);
			if (_.get(customers, 'code', null) !== 1010000 || _.get(customers, 'totalRow', null) === 0) {
				return customerId; // fail -> return null
			}
			customerId = customers.data[0].id;
			return customerId;
		} catch (error) {
			broker.logger.error(`PayMELink > commomService > getCustomerId > error ${error}`);
			return customerId;
		}
	},

	/**
	 * @notice createOrder with paymentMethod is MANUAL_BANK. generate virtual account for customer
	 * @param {*} param0
	 * param0.payMELink: payMELink info
	 * param0.mcPaymentMethod: All payment method of merchant
	 * param0.broker: broker
	 */
	async createOrder({ payMELink, mcPaymentMethod = {}, broker }) {
		const returnData = {
			success: false,
			message: 'Thất bại',
			data: null
		};
		const PAY_CODE_MANUAL_BANK = 'MANUAL_BANK';
		try {
			let paymentRequest = await broker.call('v1.paymentRequestModel.findOne', [
				{
					referId: payMELink.linkId,
					referType: 'PAYME_LINK',
					status: { $nin: ['SUCCEEDED', 'EXPIRED'] }
				}, '-_id id transactionId paymentItems'
			]);
			const paymentMethod = mcPaymentMethod.find(
				(method) => method.payCode === PAY_CODE_MANUAL_BANK
			);
			if (!paymentMethod || paymentMethod.isActive === false) {
				returnData.message = 'Phương thức thanh toán không hợp lệ';
				return returnData;
			}
			if (_.get(paymentRequest, 'id', null) !== null) { // đã tạo đơn hàng trước đó => update lại PTTT và expiryAt
				await broker.call('v1.paymentRequestModel.updateOne', [
					{ id: paymentRequest.id },
					{
						paymentMethod: paymentMethod || {},
						customerDetails: payMELink.customerDetails,
						// expiryAt: moment(new Date()).add(15, 'minutes').toISOString()
					}
				]);
			} else {
				// chưa tạo đơn hàng lần nào => tạo mới
				const transactionId = await this.customNanoId({ prefix: 'PAYMENT_REQUEST_ID', type: 'string' });
				if (!transactionId) {
					returnData.message = 'Không thể tạo được mã giao dịch';
					return returnData;
				}
				await broker.call('v1.paymeLinkModel.updateOne', [
					{ linkId: payMELink.linkId },
					{ statusOrder: StatusOrderLink.PROCESSING },
				]);
				// pay full
				const paymentRequestData = {
					transactionId,
					referId: payMELink.linkId,
					referType: 'PAYME_LINK',
					amount: payMELink.amount,
					paymentItems: [{
						label: 'amount',
						name: _.camelCase('amount'),
						amount: payMELink.amount
					}],
					paymentMethod: paymentMethod || {},
				};
				// mapping customer detail from object to array object
				paymentRequestData.customerDetails = Object.entries(payMELink.customerDetail).reduce((newObject, [key, val]) => {
					newObject.push({
						label: key,
						name: _.camelCase(key),
						value: val
					});
					return newObject;
				}, []);
				paymentRequest = await broker.call('v1.paymentRequestModel.create', [paymentRequestData]);
			}
			if (!paymentRequest) {
				returnData.message = 'Khởi tạo đơn hàng không thành công';
				return returnData;
			}
			let ipnEmail = '';
			if (_.get(payMELink, 'extraData', null) !== null) {
				const payMELinkExtraData = JSON.parse(payMELink.extraData);
				const aIPNEmail = _.get(payMELinkExtraData, 'extraData.info.contact', ' Email: ').split(' Email: ');
				ipnEmail = _.isArray(aIPNEmail) && aIPNEmail.length >= 2 ? aIPNEmail[1] : '';
			}
			const orderData = {
				partnerTransaction: paymentRequest.transactionId,
				referType: 'PAYME_LINK',
				referId: payMELink.linkId,
				accountId: payMELink.createdBy.accountId,
				amount: payMELink.amount,
				username: payMELink.username,
				lang: payMELink.language,
				redirectUrl: _.get(payMELink, 'redirectURL', ''), // redirect if success or fail...
				failedUrl: payMELink.paymentLink, // user cancel payment and rechoose payment method
				ipnUrl: `${process.env.HOST}/widgets/link/external/receiveIPN`,
				description: payMELink.description || payMELink.note || '-',
				note: payMELink.note,
				referData: JSON.stringify({
					orderInfo: {
						title: payMELink.description,
						paymentItems: paymentRequest.paymentItems
					},
					customerInfo: payMELink.customerDetail,
					voucherInfo: {},
					note: payMELink.note
				}),
				payMethod: PAY_CODE_MANUAL_BANK, // hardcode. transfer to bank (virtual account)
				ipnEmail,
				customerId: payMELink.customerDetail.id
			};
			broker.logger.info(`PayMELink > commonService > createOrder > call 'v1.order.createWeb' with requestBody = ${JSON.stringify(orderData)}`);
			let createOrder;
			try {
				createOrder = await broker.call('v1.order.createWeb', orderData);
				broker.logger.info(`PayMELink > commonService > createOrder > call 'v1.order.createWeb' with responseData = ${JSON.stringify(createOrder)}`);
			} catch (error) {
				broker.logger.error(`PayMELink > commonService > createOrder > call 'v1.order.createWeb' with error = ${error}`);
				await broker.call('v1.paymeLinkModel.updateOne', [
					{ id: payMELink.id },
					{
						statusOrder: StatusOrderLink.NULL
					}
				]);
				returnData.message = 'Tạo đơn hàng thanh toán thất bại';
				return returnData;
			}

			if (_.get(createOrder, 'code', null) !== 105000) { // 105000: code thành công
				await broker.call('v1.paymeLinkModel.updateOne', [
					{ id: payMELink.id },
					{
						statusOrder: StatusOrderLink.NULL
					}
				]);
				returnData.message = createOrder.message === '404' ? '[Error] Tạo đơn hàng thất bại!' : createOrder.message;
				return returnData;
			}

			const dataUpdate = {
				orderId: createOrder.data.orderId,
				paymentUrl: createOrder.data.url,
				status: StatusOrder.STATUS.PENDING
			};
			await broker.call('v1.paymentRequestModel.updateOne', [
				{ id: paymentRequest.id },
				dataUpdate,
			]);

			returnData.data = {
				orderId: createOrder.data.orderId,
				bankInfo: _.get(createOrder, 'data.bankInfo', {})
			};
			returnData.message = 'Tạo đơn hàng thành công';
			returnData.success = true;
			return returnData;
		} catch (error) {
			broker.logger.error(`PayMELink > commomService > createOrder > error ${error}`);
			return returnData;
		}
	}
};
