/* eslint-disable no-param-reassign */
const _ = require('lodash');
const Async = require('async');
const CommonService = require('./commonService');
const PoolSendMail = require('./poolSendMailTuition');
const TuitionService = require('./tuitionService');
const UtilityHelper = require('../helper/UtilityHelper');

const POOL_CREATE_LINK = Async.queue(async (task, callback) => {
	let payload = _.clone(task);
	delete payload.broker;
	task.broker.logger.info(`PayME Link > POOL_CREATE_LINK> payload = ${JSON.stringify(payload)}`);
	const tuitionService = new TuitionService();
	tuitionService.calcPayment([payload]);
	const { broker } = task;
	payload = await tuitionService.getPaymentDetail(1, broker);
	if (payload.length > 0) payload = payload[0];
	task.broker.logger.info(`PayME Link > POOL_CREATE_LINK > payload = ${JSON.stringify(payload)}`);
	// ========================== GET CUSTOMER ID =============================
	const customerInfo = {
		name: payload.studentInfo.name,
		email: payload.studentInfo.parentEmail,
		phone: payload.studentInfo.parentPhone
	};
	const customerId = await CommonService.getCustomerId({ customerInfo, accountId: task.accountId, broker });
	// ========================= END GET CUSTOMER ID ==========================
	const linkDataCreate = {
		merchantId: task.merchantId,
		accountId: task.accountId,
		customerDetail: {
			id: customerId,
			fullname: payload.studentInfo.name,
			email: payload.studentInfo.parentEmail,
			phone: payload.studentInfo.parentPhone,
			shippingAddress: null
		},
		amount: payload.totalPaymentAmount || 0,
		description: `${payload.info.title} - ${payload.studentInfo.name}` || `Thanh toán học phí - ${payload.studentInfo.name}`,
		extraData: payload
	};
	broker.logger.info(`PayME Link > POOL_CREATE_LINK > linkDataCreate > ${JSON.stringify(linkDataCreate)}`);
	const linkInfo = await CommonService.createLink(linkDataCreate, broker);
	broker.logger.info(`PayME Link > POOL_CREATE_LINK > createLinkRes > ${JSON.stringify(linkInfo)}`);
	if (linkInfo.success === false) {
		broker.logger.info(`PayME Link > POOL_CREATE_LINK error > ${linkInfo.message}`);
	}
	// =============================== CREATE ORDER ===============================
	const createOrderRes = await CommonService.createOrder({ payMELink: linkInfo.data, mcPaymentMethod: task.mcPaymentMethod, broker });
	if (createOrderRes.success === false) {
		broker.logger.info(`PayME Link > POOL_CREATE_LINK > createOrder > error > ${createOrderRes.message}`);
	}

	const aBankAccount = payload.info.bankAccount.split(':');
	const defaultBankAccount = aBankAccount[1].split('-');
	// const customerBankAccount = _.get(createOrderRes, 'data.bankInfo', { fullName: '', number: '', bankName: '' }); // virtual account system generate for customer
	const customerBankAccount = _.get(createOrderRes, 'data.bankInfo', { fullName: defaultBankAccount[1], number: defaultBankAccount[0], bankName: defaultBankAccount[2] , branch: defaultBankAccount[3]}); // virtual account system generate for customer
	// Get branch from school name: format schoolName like: "Mầm non Tuệ Đức | Cơ sở Hà Huy Giáp, Quận 8" => schoolBranch: "Quận 8"
	const schoolBranch = _.split(payload.studentInfo.schoolName, /,\s/).pop().trim();
	const utilityHelper = new UtilityHelper();
	let bankTransferDesc = `${payload.studentInfo.name}_${payload.studentInfo.parentPhone}_${schoolBranch}`; // format Ten Hoc Sinh_So Dien Thoai_Ten Co So
	bankTransferDesc = utilityHelper.removeUnicode(bankTransferDesc).toUpperCase();

	const dataSendMail = {
		mcname: task.mcname,
		mclogo: task.mclogo,
		broker,
		merchantId: task.merchantId,
		accountId: task.accountId,
		studentInfo: {
			studentId: payload.studentInfo.id,
			fullName: payload.studentInfo.name,
			className: payload.studentInfo.className,
			schoolYear: payload.info.scholastic,
			email: payload.studentInfo.parentEmail,
			schoolBranch: payload.studentInfo.schoolName,
			phoneNumber: payload.studentInfo.parentPhone,
			parentName: payload.studentInfo.parentName,
		},
		tuition: {
			items: payload.tuitionInfo,
			total: payload.totalPaymentAmount || 0
		},
		paymentLink: linkInfo.data.paymentLink,
		detail: {
			title: payload.info.title,
			// bankAccount: payload.info.bankAccount,
			bankTransferInfo: {
				fullName: customerBankAccount.fullName,
				number: customerBankAccount.number,
				bankName: customerBankAccount.bankName,
				branch: customerBankAccount.branch,
				description: bankTransferDesc
			},
			submissionDeadline: payload.info.deadline,
			contact: payload.info.contact
		},
	};
	PoolSendMail.init(dataSendMail);
	callback();
}, 1);

POOL_CREATE_LINK.drain = () => {
	console.log('PayME Link > POOL_CREATE_LINK items have been executed!');
};
module.exports = {
	init(data) {
		POOL_CREATE_LINK.push(data);
	}
};
