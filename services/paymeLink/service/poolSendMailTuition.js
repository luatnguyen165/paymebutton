const Async = require('async');
const fs = require('fs');
const moment = require('moment');
const _ = require('lodash');
const ejs = require('ejs');
const path = require('path');
const mkdirp = require('mkdirp');
const generatePDF = require('./generatePdfService');
const UtilityHelper = require('../helper/UtilityHelper');

const POOL_SEND_EMAIL = Async.queue(async (task, callback) => {
	const { broker } = task;

	const payload = _.clone(task);
	delete payload.broker;
	broker.logger.info(`PayME Link > POOL_SEND_EMAIL > payload = ${JSON.stringify(payload)}`);
	const data = {
		logo: payload.mclogo || '',
		studentInfo: payload.studentInfo,
		tuition: payload.tuition,
		detail: payload.detail,
		paymentLink: payload.paymentLink,
		helpers: {
			formatCurrency: (str = '', separate = ',') => {
				const number = str.toString().split('.').join('');
				return number.replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${separate}`);
			}
		}
	};
	const dirname = `${__dirname}/../tuition-files/${moment().format('YYYYMMDD').toString()}`;
	broker.logger.info(`PayME Link > POOL_SEND_MAIL > dirname: ${dirname} make dir info: ${fs.existsSync(dirname)}`);
	const makeDir = mkdirp.sync(dirname);
	const pdfFileName = `thong-bao-hoc-phi-hoc-sinh-${data.studentInfo.studentId}.pdf`;
	// let resultGenFile = null;
	// try {
	// 	resultGenFile = await generatePDF(data, '../template/tuition-tueduc.ejs', `${dirname}/${pdfFileName}`, broker);
	// } catch (error) {
	// 	broker.logger.info(`........****************222****************........\n resultGenFile error ===> ${error}`);
	// 	return;
	// }
	// broker.logger.info(`........****************2****************........\nPOOL_SEND_EMAIL resultGenFile res ===> ${JSON.stringify(resultGenFile)}`);

	const schoolName = (payload.studentInfo.schoolBranch);
	const utilityHelper = new UtilityHelper();
	const paymentLink = payload.paymentLink.replace('https://', 'http://');

	const template = {
		tueduc: {
			sms: `Truong Tue Duc thong bao hoc phi cua ${utilityHelper.removeUnicode(payload.studentInfo.fullName)} la ${data.helpers.formatCurrency(payload.tuition.total, '.')}d, quy PH vui long thanh toan truc tuyen tai ${(paymentLink)}. Tran trong.`,
			email: '../template/tuition-tueduc-mailcontent.ejs',
			method: ['EMAIL', 'SMS']
		},
		tesla: {
			sms: `HT truong Tesla thong bao hoc phi cua ${utilityHelper.removeUnicode(payload.studentInfo.fullName)} la ${data.helpers.formatCurrency(payload.tuition.total, '.')}d, quy khach thanh toan truc tuyen tai ${(paymentLink)}. Tran trong.`,
			email: '../template/tuition-tesla-mailcontent.ejs',
			method: ['EMAIL', 'SMS']
		}
	};

	const smsData = template[`${payload.mcname}`].sms; // `HT truong Tue Duc thong bao hoc phi cua ${utilityHelper.removeUnicode(payload.studentInfo.fullName)} la ${data.helpers.formatCurrency(payload.tuition.total, '.')}d, quy khach thanh toan truc tuyen tai ${(paymentLink)}. Tran trong.`;
	broker.logger.info(`PayME Link > POOL_SEND_MAIL > smsData = ${JSON.stringify({ smsData, schoolName, data })}`);

	// if (_.get(resultGenFile, 'filePath', null) !== null) {
	// const stream = await fs.createReadStream(resultGenFile.filePath);
	const metaData = {
		merchantId: payload.merchantId,
		accountId: payload.accountId,
		target: [
			{
				name: payload.studentInfo.parentName,
				email: payload.studentInfo.email,
				number: payload.studentInfo.phoneNumber
			}
		],
		chanel: 'CUSTOM',
		method: template[`${payload.mcname}`].method,
		service: 'PAYME_LINK',
		template: {
			email: 'default-mail',
			sms: 'default-sms'
		},
		filename: pdfFileName
	};
	broker.logger.info(`PayME Link > POOL_SEND_MAIL > call v1.customer.sendEmailAndSMS with metaData = ${JSON.stringify(metaData)}`);

	try {
		ejs.renderFile(path.join(__dirname, template[`${payload.mcname}`].email), { data }, async (err, htmlData) => {
			if (err) {
				throw err;
			}
			metaData.content = {
				mail: {
					subject: `THÔNG BÁO ${payload.detail.title}`.toUpperCase(),
					content: {
						data: htmlData
					}
				},
				sms: {
					content: {
						data: smsData
					}
				}
			};
			const sendEmailRes = await broker.call('v1.customer.sendEmailAndSMS', {},
				{
					meta: metaData
				});
			broker.logger.info({ sendEmailRes });
		});
	} catch (error) {
		broker.logger.info(`PayME Link > POOL_SEND_MAIL > error > ${error}`);
	}
	// }
	callback();
}, 10);
module.exports = {
	init(data) {
		POOL_SEND_EMAIL.push(data);
	}
};
