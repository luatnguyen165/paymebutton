const path = require('path');
const ejs = require('ejs');
const puppeteer = require('puppeteer');

async function generatePDF(data, template, fileName, broker) {
	broker.logger.info(`.......****************1************** generatePDF input===> ${JSON.stringify({ data, template, fileName })}`);
	const html = await ejs.renderFile(path.join(__dirname, template), { data });
	broker.logger.info(`.......****************2************** generatePDF html===> ${html}`);
	if (!html) {
		broker.logger.info('.......****************2************** generatePDF error html');
		throw new Error('Create html file error!!!');
	}
	const browser = await puppeteer.launch({
		args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]
	});
	broker.logger.info(`.......****************3************** generatePDF browser===> ${browser}`);
	if (!browser) {
		broker.logger.info('.......****************3************** generatePDF error browser');
		throw new Error('browser error!!!');
	}
	const page = await browser.newPage();
	broker.logger.info(`.......****************4************** generatePDF page===> ${page}`);
	if (!page) {
		broker.logger.info('.......****************4************** generatePDF error page');
		throw new Error('new page error!!!');
	}
	const setContent = await page.setContent(html);
	broker.logger.info(`.......****************5************** generatePDF setContent===> ${setContent}`);
	const fileBuffer = await page.pdf({
		path: fileName,
		format: 'A4',
		margin: {
			top: '20px',
			bottom: '40px',
			left: '20px',
			right: '20px'
		}
	});
	if (fileBuffer) {
		broker.logger.info(`.......****************1************** generatePDF success at ${fileName}`);
		// await browser.close();
		return { filePath: fileName };
	}
	throw new Error('Create pdf file error!!!');
}
module.exports = generatePDF;
