/* eslint-disable no-param-reassign */
const _ = require('lodash');
const Async = require('async');
const Decimal = require('decimal.js');
const numeral = require('numeral');

const delay = (ms = 2000) => new Promise((resolve) => {
	setTimeout(() => resolve(true), ms);
});
const regexString = /-\d{1,3}%/gm;
/**
 *
1. // số lượng từ 1-100, và giá khác 0 (volume)
	 quantity >=1 && quantity <= 100 && price <> 0
	 price = price
	 quantity = quantity
	 total = price*quantity
2. // số lượng là % (percentage)
		quantity*100 > -100 && quantity*100 < 0
	price = price + (quantity*price)
	quantity = 1
	total = price + (quantity*price)
3. // số lượng nhỏ hơn -1000 và giá là 0 (amount)
		quantity< -1000 && price = 0
	price = quantity
	quantity = 1
	total = price
4. // số lượng nhỏ hơn -1000 và giá khác 0 (amount)
	quantity < -1000 && price <> 0
	price = quantity
	quantity = 1
	total = price
5. // số lượng là % (percentage)
		quantity*100 > -100 && quantity*100 < 0 && price bằng rỗng
	price = quantity*total
	quantity = 1
	total = quantity*total
 */

class TuitionService {
	constructor() {
		this.maxTry = 1;
		this.tuitions = [];
		this.POOL = Async.queue(async (task, callback) => {
			const payload = _.clone(task);
			delete payload.broker;
			let amount = new Decimal(0);
			let discount = {
				price: 0,
				quantity: 0
			};
			let discountIndex = -1;
			payload.tuitionInfo = _.map(task.tuitionInfo, (tuition, index) => {
				if (_.isString(tuition.discount) && tuition.discount.match(regexString)) {
					tuition.discount = eval(`${tuition.discount.slice(0, -1)}/100`);
				}
				if (_.isString(tuition.quantity) && tuition.quantity.match(regexString)) {
					tuition.quantity = eval(`${tuition.quantity.slice(0, -1)}/100`);
				}
				if (_.get(tuition, 'discount', 0) < 0 && _.toNumber(tuition.quantity) > 0 && _.toNumber(tuition.quantity) <= 100) { // existed discount value
					if (tuition.discount >= -1 && tuition.discount < 0) { // discount by percent
						if (!tuition.price || !tuition.quantity) {
							tuition.paymentAmount = 0;
						} else {
							//  ((quantity*price)*discount)/100
							tuition.paymentAmount = new Decimal(tuition.quantity).mul(tuition.price).mul(tuition.discount).toNumber();
							tuition.title += ` (${tuition.discount * 100}%)`;
						}
					} else { // discount by value
						tuition.paymentAmount = new Decimal(tuition.quantity).mul(tuition.discount).toNumber();
						tuition.title += ` (${numeral(tuition.paymentAmount).format('0,0')}đ)`;
					}
					tuition.price = '';
					tuition.quantity = '';
					tuition.unit = '';
				} else { // not existed discount value
					// eslint-disable-next-line no-lonely-if
					if (tuition.quantity > 0 && tuition.quantity <= 100 && tuition.price !== 0) {
						tuition.paymentAmount = new Decimal(tuition.quantity).mul(tuition.price).toNumber();
					} else if ((tuition.quantity * 100) > -100 && (tuition.quantity * 100) < 0 && _.toNumber(tuition.price) !== 0) {
						tuition.price = new Decimal(tuition.price).add(tuition.quantity * tuition.price).toNumber();
						tuition.quantity = 1;
						tuition.paymentAmount = tuition.price;
					} else if (tuition.quantity < -1000 && tuition.price === 0) {
						tuition.price = tuition.quantity;
						tuition.quantity = 1;
						tuition.paymentAmount = tuition.price;
					} else if (tuition.quantity < -1000 && tuition.price !== 0 && tuition.discount === undefined) {
						if (tuition.price < 0) {
							tuition.price = tuition.quantity;
							tuition.quantity = 1;
							tuition.paymentAmount = tuition.price;
						} else {
							tuition.price = tuition.quantity + tuition.price;
							tuition.quantity = 1;
							tuition.paymentAmount = tuition.price;
						}
					} else if ((tuition.quantity * 100) > -100 && (tuition.quantity * 100) < 0 && _.toNumber(tuition.price) === 0 && tuition.unit === '') {
						discount = _.clone(tuition);
						discountIndex = index;
						tuition.paymentAmount = 0;
					} else if (tuition.quantity > 100 && tuition.discount === undefined) {
						tuition.price = tuition.quantity;
						tuition.quantity = 1;
						tuition.paymentAmount = tuition.price;
					} else {
						tuition.paymentAmount = 0;
					}
					tuition.paymentAmount = Math.ceil(tuition.paymentAmount); // rounding up payment amount each tuition fee
				}
				amount = new Decimal(amount).add(tuition.paymentAmount);
				return tuition;
			});
			const listTuitionsCalculated = payload.tuitionInfo.filter((v) => v.paymentAmount !== 0);
			if (discountIndex >= 0) payload.tuitionInfo.splice(discountIndex, 1);
			// payload.tuitionInfo = payload.tuitionInfo.sort((a, b) => (a.code < b.code ? -1 : (a.code > b.code ? 1 : 0)));
			let discountAmount = 0;
			if (discount.quantity !== 0) {
				discountAmount = Math.ceil(new Decimal(amount).mul(discount.quantity).toNumber());
				listTuitionsCalculated.push({
					title: discount.title,
					unit: '',
					price: '', // new Decimal(amount).mul(discount.quantity).toNumber(),
					quantity: '', // 1,
					code: discount.code,
					paymentAmount: discountAmount
				});
			}
			amount = new Decimal(amount).add(discountAmount); ///  discountAmount < 0
			if (payload.tuitionInfo.length > 0) {
				this.tuitions.push({
					studentInfo: payload.studentInfo,
					tuitionInfo: listTuitionsCalculated,
					info: payload.info,
					totalPaymentAmount: _.toNumber(amount)
				});
			}
			callback();
		}, 1);
	}

	calcPayment(data) {
		this.POOL.push(data);
	}

	async getPaymentDetail(dataLength, broker) {
		// broker.logger.info(`[POOL]  this.maxTry = ${this.maxTry}, dataLength = ${dataLength}, tuitionsLength = ${this.tuitions.length}`);
		if (this.tuitions.length === dataLength || this.maxTry >= 100) { // in 2.5m
			return this.tuitions;
		}
		this.maxTry += 1;
		await delay(1500);
		const a = await this.getPaymentDetail(dataLength, broker);
		return a;
	}

	async previewData(broker, dataLength = 0) {
		let resData = await this.getPaymentDetail(dataLength, broker);
		broker.logger.info(`[POOL]  previewData===>${JSON.stringify(resData)}`);
		if (resData && resData.length > 0) {
			resData = resData.map((student) => {
				let tuitionGroup = _.groupBy(student.tuitionInfo, 'code');
				tuitionGroup = Object.keys(tuitionGroup).map((k) => tuitionGroup[k]); // obj to arr;
				tuitionGroup = tuitionGroup.map((tuitions) => {
					if (tuitions.length > 1) {
						let newTuition = _.clone(tuitions);
						const tuition = newTuition.find((v) => v.discount === undefined);
						newTuition = [{
							...tuition,
							paymentAmount: tuitions.map((t) => t.paymentAmount).reduce((sum, item) => sum + item)
						}];
						return newTuition;
					}
					return tuitions;
				});
				student.tuitionInfo = _.flatMap(tuitionGroup);
				return student;
			});
		}
		return resData;
	}
}
module.exports = TuitionService;
