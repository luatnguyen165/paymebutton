/* eslint-disable no-param-reassign */
const _ = require('lodash');
const Async = require('async');
const Decimal = require('decimal.js');
const moment = require('moment');
const { createLink } = require('./commonService');
const PoolSendMail = require('./poolSendMailTuition');
const TuitionService = require('./tuitionService');

const POOL_CREATE_BULK_LINK = Async.queue(async (task, callback) => {
	const payload = _.clone(task);
	delete payload.broker;
	delete payload.customNanoId;
	const { broker } = task;
	broker.logger.info(`[POOL_CREATE_BULK_LINK] payload===>${JSON.stringify(payload)}`);
	const linkInfo = await createLink(payload, task.broker, task.customNanoId);
	broker.logger.info(`POOL_CREATE_BULK_LINK linkInfo====>${JSON.stringify(linkInfo)}`);
	if (linkInfo.success) {
		// const dataSendMail = {
		// 	broker,
		// 	merchantId: task.merchantId,
		// 	accountId: task.accountId,
		// 	studentInfo: {
		// 		studentId: payload.studentInfo.id,
		// 		fullName: payload.studentInfo.name,
		// 		className: payload.studentInfo.className,
		// 		schoolYear: payload.info.scholastic,
		// 		email: payload.studentInfo.parentEmail,
		// 		schoolBranch: payload.studentInfo.schoolName,
		// 		phoneNumber: payload.studentInfo.parentPhone,
		// 		parentName: payload.studentInfo.parentName,
		// 	},
		// 	tuition: {
		// 		items: payload.tuitionInfo,
		// 		total: payload.totalPaymentAmount || 0
		// 	},
		// 	paymentLink: _.get(linkInfo.data, 'shortLinkInfo.url', linkInfo.data.paymentLink),
		// 	detail: {
		// 		title: payload.info.title,
		// 		bankAccount: payload.info.bankAccount,
		// 		submissionDeadline: payload.info.deadline,
		// 		contact: payload.info.contact
		// 	},
		// };
		// PoolSendMail.init(dataSendMail);
	} else {
		broker.logger.info(`POOL_CREATE_BULK_LINK error!${linkInfo.message}`);
	}
	callback();
}, 10);

POOL_CREATE_BULK_LINK.drain = () => {
	console.log('POOL_CREATE_BULK_LINK items have been executed!');
};
module.exports = {
	init(data) {
		POOL_CREATE_BULK_LINK.push(data);
	}
};
