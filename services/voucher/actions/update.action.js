const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/responseCode.constant');
const GeneralConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		// const auth = {
		// 	credentials: {
		// 		accountId: 1,
		// 		merchantId: 2
		// 	}
		// };
		const auth = _.get(ctx, 'meta.auth', { credentials: {} });

		if (payload.unit === GeneralConstants.VOUCHER_UNIT.PERCENT && payload.amount >= 100) {
			return {
				code: ResponseCode.VOUCHER_AMOUNT_INVALID.code,
				message: this.__(ResponseCode.VOUCHER_AMOUNT_INVALID.message)
			};
		}
		const query = {
			id: _.toNumber(ctx.params.params.id),
		};
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			query.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			query.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo
		}
		const voucherInfo = await this.broker.call('v1.voucherModel.findOne', [query]);
		if (!voucherInfo) {
			return {
				code: ResponseCode.VOUCHER_NOT_FOUND.code,
				message: this.__(ResponseCode.VOUCHER_NOT_FOUND.message)
			};
		}
		if (payload.unit === GeneralConstants.VOUCHER_UNIT.PERCENT && payload.amount >= 100) {
			return {
				code: ResponseCode.VOUCHER_AMOUNT_INVALID.code,
				message: this.__(ResponseCode.VOUCHER_AMOUNT_INVALID.message)
			};
		}
		const used = voucherInfo.quantity - voucherInfo.quantityRemaining;
		const updateData = {
			name: payload.name === undefined ? voucherInfo.name : payload.name,
			description: payload.description === undefined ? voucherInfo.description : payload.description,
			amount: payload.amount === undefined ? voucherInfo.amount : payload.amount,
			quantity: payload.quantity === undefined ? voucherInfo.quantity : payload.quantity,
			minAmount: payload.minAmount === undefined ? voucherInfo.minAmount : payload.minAmount,
			maxDiscount: payload.maxDiscount === undefined ? voucherInfo.maxDiscount : payload.maxDiscount,
			unit: payload.unit === undefined ? voucherInfo.unit : payload.unit,
			quantityRemaining: payload.quantity === undefined ? voucherInfo.quantityRemaining : (payload.quantity - used)
		};
		if (payload.unit === 'VND') {
			updateData.maxDiscount = 0; // vì đã giảm giá theo số tiền
		}
		if (payload.expiredIn) {
			// check time expire from - to
			const fromDate = moment(payload.expiredIn.fromDate).format('YYYY-MM-DD');
			const fromTime = payload.expiredIn.fromTime || '00:00:00';
			const expiredFrom = new Date(momentTz(`${fromDate} ${fromTime}`).tz('UTC'));

			const toDate = moment(payload.expiredIn.toDate).format('YYYY-MM-DD');
			const toTime = payload.expiredIn.toTime || '24:00:00';
			const expiredTo = new Date(momentTz(`${toDate} ${toTime}`).tz('UTC'));

			const now = new Date(momentTz().tz('UTC'));

			if (moment(expiredTo).isBefore(now)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian kết thúc mã giảm giá không hợp lệ')
				};
			}
			if (moment(expiredTo).isBefore(expiredFrom)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian kết thúc mã giảm giá không hợp lệ')
				};
			}
			updateData.expiredIn = {
				from: expiredFrom,
				to: expiredTo
			};
		}
		if (_.isBoolean(payload.isActive)) {
			updateData.isActive = payload.isActive;
		}
		ctx.broker.logger.info(`update voucher ===> ${JSON.stringify({
			payload, updateData, used, quantity: payload.quantity, Oldquantity: voucherInfo.quantity
		})}`);
		if (payload.quantity && _.toInteger(payload.quantity) < used) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Số lượng mã giảm giá không hợp lệ')
			};
		}

		let voucherUpdated = null;
		try {
			voucherUpdated = await this.broker.call('v1.voucherModel.updateOne', [{
				id: voucherInfo.id
			}, updateData]);
		} catch (error) {
			ctx.broker.logger.warn(`update voucher  error ===> ${error}`);
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.ACTION_VOUCHER_FAIL.message)
			};
		}
		if (_.get(voucherUpdated, 'nModified', 0) === 0) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.ACTION_VOUCHER_FAIL.message)
			};
		}
		let { message } = ResponseCode.ACTION_VOUCHER_SUCCEESS;
		if (_.get(payload, 'isActive', null) === true) {
			message = _.toString(ResponseCode.TURN_ON_DISCOUNT_CODE);
		}
		if (_.get(payload, 'isActive', null) === false) {
			message = _.toString(ResponseCode.TURN_OFF_DISCOUNT_CODE);
		}
		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(message)
		};
	} catch (err) {
		ctx.broker.logger.warn(`update voucher error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
