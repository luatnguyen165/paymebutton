const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const {
			filter,
			paging,
			sort = { id: -1 }
		} = payload;
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: filter.pageId },
				{ 'shortLinkInfo.slug': filter.pageId }
			],
			status: 'ACTIVE'
		}]);
		if (!pageInfo) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Không tìm thấy thông tin page')
			};
		}
		const query = { // lấy các voucher có hiệu lực
			isActive: true,
			quantityRemaining: {
				$gt: 0
			},
			'expiredIn.to': {
				$gt: new Date()
			},
			merchantId: pageInfo.creator.merchantId// lấy voucher theo MC của page
			// query.accountId = pageInfo.creator.accountId; // lấy voucher theo account tạo page
		};
		if (filter.voucherCode) {
			query.voucherCode = filter.voucherCode;
		}

		let vouchers = [];
		vouchers = await this.broker.call('v1.voucherModel.find', {
			body: {
				query, paging, sort
			}
		});
		if (vouchers.length > 0) {
			vouchers = _.map(vouchers, (voucherInfo) => {
				const item = {
					name: voucherInfo.name,
					voucherCode: voucherInfo.voucherCode,
					description: voucherInfo.description,
					amount: voucherInfo.amount,
					unit: voucherInfo.unit,
					minAmount: voucherInfo.minAmount,
					maxDiscount: voucherInfo.maxDiscount,
					expiredIn: {
						from: voucherInfo.expiredIn.from,
						to: voucherInfo.expiredIn.to
					},
					allowMethods: voucherInfo.allowMethods
				};
				return item;
			});
		}

		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(ResponseCode.ACTION_VOUCHER_SUCCEESS.message),
			data: {
				items: vouchers
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`Search voucher error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
