const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const {
			filter,
			paging,
			sort = { id: -1 }
		} = payload;
		// const auth = {
		// 	credentials: {
		// 		accountId: 1,
		// 		merchantId: 2
		// 	}
		// };
		const auth = _.get(ctx, 'meta.auth', { credentials: {} });
		const query = {};
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			query.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			query.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}
		if (filter.name) {
			query.name = { $regex: RegExp(filter.name, 'i') };
		}
		if (filter.voucherCode) {
			query.voucherCode = filter.voucherCode;
		}
		if (_.isBoolean(filter.isActive)) {
			query.isActive = filter.isActive;
		}
		if (filter.createdAt) {
			query.createdAt = {
				$gte: filter.createdAt.from,
				$lte: filter.createdAt.to
			};
		}
		if (filter.expiredIn) {
			if (filter.expiredIn.from) {
				query['expiredIn.from'] = { $gte: new Date(filter.expiredIn.from) };
			}
			if (filter.expiredIn.to) {
				query['expiredIn.to'] = { $lte: new Date(filter.expiredIn.to) };
			}
		}
		ctx.broker.logger.info(`Search voucher by: ${JSON.stringify(query)}`);
		const total = await this.broker.call('v1.voucherModel.count', { query });
		let vouchers = [];
		if (total > 0) {
			vouchers = await this.broker.call('v1.voucherModel.find', {
				body: {
					query, paging, sort
				}
			});
			vouchers = _.map(vouchers, (voucherInfo) => {
				const item = {
					id: voucherInfo.id,
					createdAt: voucherInfo.createdAt,
					updatedAt: voucherInfo.updatedAt,
					accountId: voucherInfo.accountId,
					merchantId: voucherInfo.merchantId,
					name: voucherInfo.name,
					voucherCode: voucherInfo.voucherCode,
					description: voucherInfo.description,
					amount: voucherInfo.amount,
					unit: voucherInfo.unit,
					// quantity: voucherInfo.quantity,
					// quantityRemaining: voucherInfo.quantityRemaining,
					// quantityUsed: voucherInfo.quantity - voucherInfo.quantityRemaining,
					minAmount: voucherInfo.minAmount,
					maxDiscount: voucherInfo.maxDiscount,
					expiredIn: {
						from: voucherInfo.expiredIn.from,
						to: voucherInfo.expiredIn.to
					},
					allowMethods: voucherInfo.allowMethods,
					isActive: voucherInfo.isActive,
					isExpired: momentTz(voucherInfo.expiredIn.to).tz('UTC').isBefore(moment().tz('UTC'))
				};
				return item;
			});
		}

		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(ResponseCode.ACTION_VOUCHER_SUCCEESS.message),
			data: {
				total,
				items: vouchers
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`Search voucher error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
