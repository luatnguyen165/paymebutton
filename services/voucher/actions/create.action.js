const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const ResponseCode = require('../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		// const auth = {
		// 	credentials: {
		// 		accountId: 1,
		// 		merchantId: 2
		// 	}
		// };
		const auth = _.get(ctx, 'meta.auth', { credentials: {} });
		try {
			await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			return {
				code: error.code,
				message: this.__(error.message)
			};
		}
		let merchantInfo = null;
		try {
			merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			return {
				code: error.code,
				message: this.__(error.message)
			};
		}
		if (_.get(merchantInfo, 'id', null) === null) {
			if (_.get(auth.credentials, 'merchantId', null) === null) {
				return {
					code: ResponseCode.ACTION_VOUCHER_FAIL.code,
					message: this.__('Không tìm thấy thông tin merchant')
				};
			}
		}
		auth.credentials.merchantId = auth.credentials.merchantId || merchantInfo.id;

		const now = new Date(momentTz().tz('UTC'));
		// check time expire from - to
		const fromDate = moment(payload.expiredIn.fromDate).format('YYYY-MM-DD');
		let fromTime = payload.expiredIn.fromTime || '00:00:00';
		if (moment(fromDate).isSame(momentTz().format('YYYY-MM-DD'))) {
			fromTime = momentTz().add(60, 'seconds').format('HH:mm:ss');
		}
		const expiredFrom = new Date(momentTz(`${fromDate} ${fromTime}`).tz('UTC'));

		const toDate = moment(payload.expiredIn.toDate).format('YYYY-MM-DD');
		const toTime = payload.expiredIn.toTime || '24:00:00';
		const expiredTo = new Date(momentTz(`${toDate} ${toTime}`).tz('UTC'));

		ctx.broker.logger.info(`create voucher ===>: ${JSON.stringify({ expiredFrom, expiredTo, now })}`);
		if (moment(expiredFrom).isBefore(now)) {
			return {
				code: ResponseCode.EXPIRED_TIME_INVALID.code,
				message: this.__('Thời gian bắt đầu mã giảm giá không hợp lệ')
			};
		}
		if (moment(expiredTo).isBefore(expiredFrom)) {
			return {
				code: ResponseCode.EXPIRED_TIME_INVALID.code,
				message: this.__('Thời gian kết thúc mã giảm giá không hợp lệ')
			};
		}
		if (payload.unit === GeneralConstants.VOUCHER_UNIT.PERCENT && payload.amount >= 100) {
			return {
				code: ResponseCode.VOUCHER_AMOUNT_INVALID.code,
				message: this.__(ResponseCode.VOUCHER_AMOUNT_INVALID.message)
			};
		}
		// if (/^([a-zA-Z0-9]{5,10})$/.test(payload.voucherCode)) {
		// 	return {
		// 		code: ResponseCode.VOUCHER_CODE_INVALID.code,
		// 		message: this.__(ResponseCode.VOUCHER_CODE_INVALID.message)
		// 	};
		// }
		// check vc code trùng theo MC
		const checkVoucherExitsed = await this.broker.call('v1.voucherModel.findOne', [{
			voucherCode: payload.voucherCode,
			merchantId: auth.credentials.merchantId
		}]);
		if (checkVoucherExitsed) {
			return {
				code: ResponseCode.VOUCHER_CODE_EXISTED.code,
				message: this.__(ResponseCode.VOUCHER_CODE_EXISTED.message)
			};
		}

		const voucherData = {
			accountId: auth.credentials.accountId,
			merchantId: auth.credentials.merchantId,
			name: payload.name,
			voucherCode: payload.voucherCode,
			description: payload.description,
			amount: payload.amount,
			unit: payload.unit,
			quantity: payload.quantity,
			quantityRemaining: payload.quantity,
			minAmount: payload.minAmount || 0,
			maxDiscount: payload.maxDiscount || 0,
			expiredIn: {
				from: expiredFrom,
				to: expiredTo
			},
			allowMethods: [],
			isActive: true
		};
		let voucher = null;
		try {
			voucher = await this.broker.call('v1.voucherModel.create', [voucherData]);
		} catch (error) {
			ctx.broker.logger.info(`create voucher error =====>: ${error.message}`);
			ctx.broker.logger.error(error);
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.ACTION_VOUCHER_FAIL.message)
			};
		}
		ctx.broker.logger.info(`create voucher =====>: ${JSON.stringify(voucher)}`);

		if (_.get(voucher, 'id', null) === null) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.ACTION_VOUCHER_FAIL.message)
			};
		}
		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(ResponseCode.ACTION_VOUCHER_SUCCEESS.message),
			data: {
				voucherCode: voucher.voucherCode,
				id: voucher.id,
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`create voucher error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
