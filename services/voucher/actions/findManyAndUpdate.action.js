const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/responseCode.constant');

/**
 * @dev ctx.params.body with format [{voucherCode: 'ABC', dataUpdate: { expiredIn: { fromDate: YYYY-MM-DD, toDate: YYYY-MM-DD } } }, ...]
 * @param {*} ctx
 */
module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const auth = _.get(ctx, 'meta.auth', { credentials: {} });
		let merchantInfo = {};
		try {
			merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			ctx.broker.logger.error(`Voucher > updateMany > call 'v1.kycAccount.checkKYC' with accountId = ${auth.credentials.accountId} and error = ${error}`);
		}
		if (!merchantInfo.id || !auth.credentials.merchantId) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Không tìm thấy thông tin merchant')
			};
		}
		auth.credentials.merchantId = auth.credentials.merchantId || merchantInfo.id;

		// format dataUpdate
		const listVoucherAndDataUpdate = []; // new list voucher data with expire in formatted to DateTime
		payload.forEach((voucherData) => {
			const { dataUpdate = {} } = voucherData;
			const rawExpiredIn = _.get(dataUpdate, 'expiredIn', {});

			const fromDate = moment(rawExpiredIn.fromDate).format('YYYY-MM-DD');
			let fromTime = rawExpiredIn.fromTime || '00:00:00';
			if (moment(fromDate).isSame(momentTz().format('YYYY-MM-DD'))) {
				fromTime = momentTz().add(60, 'seconds').format('HH:mm:ss');
			}
			const expiredFrom = new Date(momentTz(`${fromDate} ${fromTime}`).tz('UTC'));

			const toDate = moment(rawExpiredIn.toDate).format('YYYY-MM-DD');
			const toTime = rawExpiredIn.toTime || '24:00:00';
			const expiredTo = new Date(momentTz(`${toDate} ${toTime}`).tz('UTC'));

			listVoucherAndDataUpdate.push({
				voucherCode: voucherData.voucherCode,
				dataUpdate: {
					expiredIn: {
						from: expiredFrom,
						to: expiredTo
					}
				}
			});
		});

		// check time expire from - to and check conflict unit and amount or not
		const now = new Date(momentTz().tz('UTC'));
		listVoucherAndDataUpdate.forEach(({ dataUpdate }) => {
			if (moment(dataUpdate.expiredIn.from).isBefore(now)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian bắt đầu mã giảm giá không hợp lệ')
				};
			}
			if (moment(dataUpdate.expiredIn.to).isBefore(dataUpdate.expiredIn.from)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian kết thúc mã giảm giá không hợp lệ')
				};
			}
		});

		/** prepare list voucher data to update */
		const promises = [];
		let vouchers = [];
		try {
			listVoucherAndDataUpdate.forEach((voucher) => {
				const pUpdateVoucher = this.broker.call('v1.voucherModel.findOneAndUpdate', [{
					merchantId: auth.credentials.merchantId,
					voucherCode: voucher.voucherCode
				}, {
					expiredIn: voucher.dataUpdate.expiredIn,
				}, {
					new: true
				}]);
				promises.push(pUpdateVoucher);
			});

			vouchers = await Promise.all(promises);
		} catch (error) {
			ctx.broker.logger.info(`Voucher > updateMany > error : ${error} and vouchers = ${JSON.stringify(vouchers)}`);
			return {
				code: ResponseCode.UPDATE_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.UPDATE_VOUCHER_FAIL.message)
			};
		}
		return {
			code: ResponseCode.UPDATE_VOUCHER_SUCCESS.code,
			message: this.__(ResponseCode.UPDATE_VOUCHER_SUCCESS.message),
			data: {
				items: vouchers
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`Voucher > updateMany > error: ${err}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
