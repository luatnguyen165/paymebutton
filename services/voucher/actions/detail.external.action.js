const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const ResponseCode = require('../constants/responseCode.constant');

// lấy thông tin voucher có thể sử dụng
module.exports = async function (ctx) {
	try {
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: ctx.params.body.pageId },
				{ 'shortLinkInfo.slug': ctx.params.body.pageId }
			],
			status: 'ACTIVE'
		}]);
		if (!pageInfo) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Không tìm thấy thông tin page')
			};
		}
		const filter = {
			voucherCode: ctx.params.params.voucherCode,
			isActive: true,
			merchantId: pageInfo.creator.merchantId // lấy voucher theo MC của page
			// accountId: pageInfo.creator.accountId, // lấy voucher theo account tạo page
			// quantityRemaining: {
			// 	$gt: 0
			// },
			// 'expiredIn.to': {
			// 	$gt: new Date()
			// },
		};
		ctx.broker.logger.info(`get detail voucher filter ===> ${JSON.stringify(filter)}`);
		const voucherInfo = await this.broker.call('v1.voucherModel.findOne', [filter]);
		if (!voucherInfo) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Mã giảm giá không hợp lệ')
			};
		}
		if (voucherInfo.quantityRemaining <= 0) {
			const { code, message } = ResponseCode.VOUCHER_ENDOFUSED;
			return { code, message: this.__(message) };
		}
		if (moment(voucherInfo.expiredIn.to).isBefore(new Date())) {
			const { code, message } = ResponseCode.VOUCHER_EXPIRED;
			return { code, message: this.__(message) };
		}
		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(ResponseCode.ACTION_VOUCHER_SUCCEESS.message),
			data: {
				id: voucherInfo.id,
				name: voucherInfo.name,
				voucherCode: voucherInfo.voucherCode,
				description: voucherInfo.description,
				amount: voucherInfo.amount,
				unit: voucherInfo.unit,
				minAmount: voucherInfo.minAmount,
				maxDiscount: voucherInfo.maxDiscount,
				expiredIn: {
					from: voucherInfo.expiredIn.from,
					to: voucherInfo.expiredIn.to
				},
				allowMethods: voucherInfo.allowMethods,
				isActive: voucherInfo.isActive,
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`get detail voucher error: ${JSON.stringify(err.message)}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
