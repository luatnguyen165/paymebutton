const _ = require('lodash');
const momentTz = require('moment-timezone');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const ResponseCode = require('../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const rawListVoucher = ctx.params.body;
		const auth = _.get(ctx, 'meta.auth', { credentials: {} });
		let merchantInfo = {};
		try {
			merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
		} catch (error) {
			ctx.broker.logger.error(`Voucher > createMany > call 'v1.kycAccount.checkKYC' with accountId = ${auth.credentials.accountId} and error = ${error}`);
		}
		if (!merchantInfo.id || !auth.credentials.merchantId) {
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__('Không tìm thấy thông tin merchant')
			};
		}
		auth.credentials.merchantId = auth.credentials.merchantId || merchantInfo.id;

		// prepare data
		const listVoucherCode = [];
		const listVoucherData = []; // new list voucher data with expire in formatted to DateTime
		rawListVoucher.forEach((voucherData) => {
			const rawExpiredIn = _.get(voucherData, 'expiredIn', {});

			const fromDate = moment(rawExpiredIn.fromDate).format('YYYY-MM-DD');
			let fromTime = rawExpiredIn.fromTime || '00:00:00';
			if (moment(fromDate).isSame(momentTz().format('YYYY-MM-DD'))) {
				fromTime = momentTz().add(60, 'seconds').format('HH:mm:ss');
			}
			const expiredFrom = new Date(momentTz(`${fromDate} ${fromTime}`).tz('UTC'));

			const toDate = moment(rawExpiredIn.toDate).format('YYYY-MM-DD');
			const toTime = rawExpiredIn.toTime || '24:00:00';
			const expiredTo = new Date(momentTz(`${toDate} ${toTime}`).tz('UTC'));

			listVoucherData.push({
				...voucherData,
				expiredIn: {
					from: expiredFrom,
					to: expiredTo
				}
			});

			if (voucherData.unit === GeneralConstants.VOUCHER_UNIT.PERCENT && voucherData.amount >= 100) {
				return {
					code: ResponseCode.VOUCHER_AMOUNT_INVALID.code,
					message: this.__(ResponseCode.VOUCHER_AMOUNT_INVALID.message)
				};
			}

			listVoucherCode.push(voucherData.voucherCode); // push `voucherCode` into array to check
		});

		if (listVoucherCode.length !== _.uniq(listVoucherCode).length) {
			return {
				code: ResponseCode.DUPLICATE_VOUCHER_CODE.code,
				message: this.__(ResponseCode.DUPLICATE_VOUCHER_CODE.message)
			};
		}

		// check time expire from - to and check conflict unit and amount or not
		const now = new Date(momentTz().tz('UTC'));
		listVoucherData.forEach((voucherData) => {
			if (moment(voucherData.expiredIn.from).isBefore(now)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian bắt đầu mã giảm giá không hợp lệ')
				};
			}
			if (moment(voucherData.expiredIn.to).isBefore(voucherData.expiredIn.from)) {
				return {
					code: ResponseCode.EXPIRED_TIME_INVALID.code,
					message: this.__('Thời gian kết thúc mã giảm giá không hợp lệ')
				};
			}
			if (voucherData.unit === GeneralConstants.VOUCHER_UNIT.PERCENT && rawListVoucher.amount >= 100) {
				return {
					code: ResponseCode.VOUCHER_AMOUNT_INVALID.code,
					message: this.__(ResponseCode.VOUCHER_AMOUNT_INVALID.message)
				};
			}
		});

		const checkVoucherExisted = await this.broker.call('v1.voucherModel.findOne', [{
			voucherCode: { $in: listVoucherCode },
			merchantId: auth.credentials.merchantId
		}]);
		if (checkVoucherExisted) {
			return {
				code: ResponseCode.VOUCHER_CODE_EXISTED.code,
				message: this.__(ResponseCode.VOUCHER_CODE_EXISTED.message)
			};
		}

		/** prepare list voucher data to create */
		// @dev require use `create`. Because `insertMany` not compatible with `mongoose-auto-increment`
		const promises = [];
		let vouchers = [];
		try {
			listVoucherData.forEach((voucher) => {
				const pCreateVoucher = this.broker.call('v1.voucherModel.create', [{
					accountId: auth.credentials.accountId,
					merchantId: auth.credentials.merchantId,
					name: voucher.name,
					voucherCode: voucher.voucherCode,
					description: voucher.description,
					amount: voucher.amount,
					unit: voucher.unit,
					quantity: voucher.quantity,
					quantityRemaining: voucher.quantity,
					minAmount: voucher.minAmount || 0,
					maxDiscount: voucher.maxDiscount || 0,
					expiredIn: voucher.expiredIn,
					allowMethods: [],
					isActive: true
				}]);
				promises.push(pCreateVoucher);
			});

			vouchers = await Promise.all(promises);
		} catch (error) {
			ctx.broker.logger.info(`Voucher > createMany > error : ${error} and vouchers = ${JSON.stringify(vouchers)}`);
			return {
				code: ResponseCode.ACTION_VOUCHER_FAIL.code,
				message: this.__(ResponseCode.ACTION_VOUCHER_FAIL.message)
			};
		}
		return {
			code: ResponseCode.ACTION_VOUCHER_SUCCEESS.code,
			message: this.__(ResponseCode.ACTION_VOUCHER_SUCCEESS.message),
			data: {
				items: vouchers
			}
		};
	} catch (err) {
		ctx.broker.logger.warn(`Voucher > createMany > error: ${err}`);
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Error: ${err.message}`, 99);
	}
};
