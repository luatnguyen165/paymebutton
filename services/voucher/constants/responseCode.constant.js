module.exports = {
	ACTION_VOUCHER_SUCCEESS: {
		code: 134100,
		message: 'Tạo mã giảm giá thành công'
	},
	ACTION_VOUCHER_FAIL: {
		code: 134101,
		message: 'Tạo mã giảm giá thất bại'
	},
	VOUCHER_CODE_EXISTED: {
		code: 134102,
		message: 'Mã giảm giá đã tồn tại. Vui lòng chọn mã khác.'
	},
	EXPIRED_TIME_INVALID: {
		code: 134103,
		message: 'Thời gian không hợp lệ'
	},
	VOUCHER_CODE_INVALID: {
		code: 134104,
		message: 'Mã giảm giá không hợp lệ'
	},
	VOUCHER_NOT_FOUND: {
		code: 134105,
		message: 'Không tìm thấy mã giảm giá'
	},
	VOUCHER_AMOUNT_INVALID: {
		code: 134106,
		message: 'Amount không hợp lệ'
	},
	VOUCHER_ENDOFUSED: {
		code: 134107,
		message: 'Voucher đã hết lượt sử dụng'
	},
	VOUCHER_EXPIRED: {
		code: 134108,
		message: 'Voucher đã hết hạn sử dụng'
	},
	DUPLICATE_VOUCHER_CODE: {
		code: 134109,
		message: 'Mã giảm giá bị trùng lặp'
	},
	UPDATE_VOUCHER_SUCCESS: {
		code: 134110,
		message: 'Cập nhật mã giảm giá thành công'
	},
	UPDATE_VOUCHER_FAIL: {
		code: 134111,
		message: 'Cập nhật mã giảm giá thất bại'
	},
	TURN_ON_DISCOUNT_CODE: 134200,
	TURN_OFF_DISCOUNT_CODE: 134210,
};
