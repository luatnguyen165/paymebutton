/* eslint-disable no-throw-literal */
const _ = require('lodash');
const path = require('path');
const { I18n } = require('i18n');
const { MoleculerError } = require('moleculer').Errors;
const VoucherSchema = require('./schema/voucher.schema');
const GeneralConstants = require('./constants/general.constant');

const DIRNAME = __dirname;

module.exports = {
	name: 'voucher',

	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	hooks: {
		before: {
			'*': [
				function setLanguage(ctx) {
					const language = _.get(ctx.params, 'body.language', '') || _.get(ctx.params, 'language', '') || 'vi';
					this.setLocale(language);
				}
			],
			create: ['checkUserScope'],
			update: ['checkUserScope'],
			read: ['checkUserScope'],
		},
		error: {
			'*': function (ctx, err) {
				return _.pick(err, ['message', 'code', 'data']);
			}
		}
	},

	dependencies: ['v1.voucherModel'],

	/**
	 * Actions
	 */
	actions: {
		create: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/voucher',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: VoucherSchema.create,
			handler: require('./actions/create.action'),
			scope: {
				name: ['mc.voucher.create'],
				condition: 'AND'
			},
		},
		update: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/voucher/:id',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: VoucherSchema.update,
			handler: require('./actions/update.action'),
			scope: {
				name: ['mc.voucher.update'],
				condition: 'AND'
			},
		},
		read: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/vouchers/search',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: VoucherSchema.read,
			handler: require('./actions/read.action'),
			scope: {
				name: ['mc.voucher.read', 'mc.paymePage.read'],
				condition: 'AND'
			},
		},
		externalSearch: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/vouchers/external/search',
				auth: false
			},
			params: {
				body: {
					$$type: 'object',
					filter: {
						$$type: 'object',
						voucherCode: 'string|trim|optional',
						pageId: 'string|trim|required',
					},
					paging: {
						$$type: 'object',
						start: 'number|integer|min:0|default:0',
						limit: 'number|integer|min:0'
					},
					sort: {
						$$type: 'object',
						createdAt: { type: 'enum', default: -1, values: [1, -1] }
					}
				},
			},
			handler: require('./actions/read.external.action'),
		},
		detail: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/voucher/:voucherCode',
				auth: false
			},
			params: VoucherSchema.externalDetail,
			handler: require('./actions/detail.external.action'),
		},
		createMany: {
			params: VoucherSchema.createMany,
			handler: require('./actions/createMany.action'),
			scope: {
				name: ['mc.voucher.create'],
				condition: 'AND'
			},
		},
		findManyAndUpdate: {
			params: VoucherSchema.findManyAndUpdate,
			handler: require('./actions/findManyAndUpdate.action'),
			scope: {
				name: ['mc.voucher.update'],
				condition: 'AND'
			},
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		// eslint-disable-next-line consistent-return
		async checkUserScope(ctx) {
			this.broker.logger.info(`Voucher: role check response: ${JSON.stringify(ctx.action.scope)}, ${ctx.action.scope.name && ctx.action.scope.name.length > 0}`);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				this.broker.logger.info(`Voucher: auth info: ${JSON.stringify(auth)}`);
				if (_.get(auth, 'credentials.accountId', null) === null) {
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!'
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: auth.credentials.accountId });
				} catch (error) {
					this.broker.logger.info(`Voucher: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(auth.credentials)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`
					};
				}
				const accountInfo = {
					accountId: auth.credentials.accountId,
					merchantId: auth.credentials.merchantId || merchantInfo.id
				};
				let mcRole = [];
				if (ctx.action.scope.name.includes('mc.voucher.read')) {
					mcRole = ['mc.voucher.read', 'mc.paymePage.read'];
				} else if (ctx.action.scope.name.includes('mc.voucher.create')) {
					mcRole = ['mc.voucher.create', 'mc.paymePage.create'];
				} else if (ctx.action.scope.name.includes('mc.voucher.update')) {
					mcRole = ['mc.voucher.update', 'mc.paymePage.update'];
				}
				try {
					const resultCheckRole = await this.broker.call('v1.role.get', { target: accountInfo });
					this.broker.logger.info(`Voucher: call v1.role.read response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager', ...mcRole]).length <= 0) {
						throw {
							code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden'
						};
					}
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager']).length > 0) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true // creator
						};
					}
				} catch (error) {
					this.broker.logger.warn(`Voucher: call v1.role.get response: ${error.message}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}, error: ${JSON.stringify(error)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`
					};
				}
				// }
				// else {
				// 	let checkData = {};
				// 	try {
				// 		checkData = {
				// 			target: accountInfo,
				// 			scope: ctx.action.scope.name,
				// 			// storeID: 1, // optional
				// 			options: { // optional
				// 				condition: ctx.action.scope.condition // AND & OR, default AND
				// 			}
				// 		};
				// 		const resultCheckRole = await this.broker.call('v1.role.check', checkData);
				// 		this.broker.logger.info(`Voucher: call v1.role.check response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)}`);
				// 		if (_.get(resultCheckRole, 'isValid', false) === false) {
				// 			throw {
				// 				code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 				message: 'Forbidden'
				// 			};
				// 		}
				// 		return resultCheckRole;
				// 	} catch (error) {
				// 		this.broker.logger.warn(`Voucher: call v1.role.check error: ${error}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)} , error: ${JSON.stringify(error)}`);
				// 		throw {
				// 			code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 			message: `[3]${error.message}`
				// 		};
				// 	}
				// }
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(DIRNAME, '/locales'),
			defaultLocale: 'vi',
			register: this
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		//
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
