const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');

module.exports = {
	create: {
		// $$strict: true,
		body: {
			$$type: 'object',
			name: {
				type: 'string',
				required: true,
			},
			voucherCode: {
				type: 'string',
				required: true,
				uppercase: true,
				pattern: /^([a-zA-Z0-9]{4,10})$/,
				min: 4
			},
			description: {
				type: 'string',
				optional: true,
			},
			unit: {
				type: 'enum',
				values: _.values(GeneralConstants.VOUCHER_UNIT),
				required: true,
			},
			amount: {
				type: 'number',
				required: true,
				min: 1
			},
			quantity: {
				type: 'number',
				// required: true,
				min: 1,
				default: 1000000
			},
			minAmount: {
				type: 'number',
				default: 0
			},
			maxDiscount: {
				type: 'number',
				default: 0
			},
			expiredIn: {
				type: 'object',
				required: true,
				props: {
					fromDate: {
						type: 'string',
						required: true,
					},
					fromTime: {
						type: 'string',
						optional: true
					},
					toDate: {
						type: 'string',
						required: true
					},
					toTime: {
						type: 'string',
						optional: true
					},
				}
			},
		},
	},
	update: {
		query: { $$type: 'object' },
		params: {
			$$type: 'object',
			id: 'string|numeric:true|trim',
		},
		body: {
			$$type: 'object',
			name: {
				type: 'string',
				optional: true,
			},
			description: {
				type: 'string',
				optional: true,
			},
			quantity: {
				type: 'number',
				optional: true,
				min: 1
			},
			amount: {
				type: 'number',
				optional: true,
			},
			minAmount: {
				type: 'number',
				optional: true,
			},
			maxDiscount: {
				type: 'number',
				optional: true,
			},
			isActive: {
				type: 'boolean',
				optional: true,
			},
			unit: {
				type: 'string',
				optional: true,
			},
			expiredIn: {
				type: 'object',
				optional: true,
				props: {
					fromDate: {
						type: 'string',
						required: true,
					},
					fromTime: {
						type: 'string',
						optional: true
					},
					toDate: {
						type: 'string',
						required: true
					},
					toTime: {
						type: 'string',
						optional: true
					},
				}
			},
		},
	},
	read: {
		body: {
			$$type: 'object',
			filter: {
				$$type: 'object',
				name: 'string|trim|optional',
				voucherCode: 'string|trim|optional',
				isActive: { type: 'boolean', optional: true },
				createdAt: {
					$$type: 'object|optional',
					from: { type: 'date', convert: true, optional: true },
					to: { type: 'date', convert: true, optional: true }
				},
				expiredIn: {
					$$type: 'object|optional',
					from: { type: 'date', convert: true, optional: true },
					to: { type: 'date', convert: true, optional: true }
				},
			},
			paging: {
				$$type: 'object',
				start: 'number|integer|min:0|default:0',
				limit: 'number|integer|min:0'
			},
			sort: {
				$$type: 'object',
				createdAt: { type: 'enum', default: -1, values: [1, -1] }
			}
		},
	},
	externalSearch: {
		body: {
			$$type: 'object',
			filter: {
				$$type: 'object',
				voucherCode: 'string|trim|optional',
				pageId: 'string|trim|required',
			},
			paging: {
				$$type: 'object',
				start: 'number|integer|min:0|default:0',
				limit: 'number|integer|min:0'
			},
			sort: {
				$$type: 'object',
				createdAt: { type: 'enum', default: -1, values: [1, -1] }
			}
		},
	},
	externalDetail: {
		params: {
			$$type: 'object',
			voucherCode: 'string|trim',
		},
		body: {
			$$type: 'object',
			pageId: 'string|trim|required',
		},
	},
	createMany: {
		body: {
			type: 'array',
			items: {
				type: 'object',
				props: {
					name: {
						type: 'string',
						required: true,
					},
					voucherCode: {
						type: 'string',
						required: true,
						uppercase: true,
						pattern: /^([a-zA-Z0-9]{4,10})$/,
						min: 4
					},
					description: {
						type: 'string',
						optional: true,
					},
					unit: {
						type: 'enum',
						values: _.values(GeneralConstants.VOUCHER_UNIT),
						required: true,
					},
					amount: {
						type: 'number',
						required: true,
						min: 1
					},
					quantity: {
						type: 'number',
						// required: true,
						min: 1,
						default: 1000000
					},
					minAmount: {
						type: 'number',
						default: 0
					},
					maxDiscount: {
						type: 'number',
						default: 0
					},
					expiredIn: {
						type: 'object',
						required: true,
						props: {
							fromDate: {
								type: 'string',
								required: true,
							},
							fromTime: {
								type: 'string',
								optional: true
							},
							toDate: {
								type: 'string',
								required: true
							},
							toTime: {
								type: 'string',
								optional: true
							},
						}
					},
				},
			},
		}
	},
	// only allow update expiredIn of voucher
	findManyAndUpdate: {
		body: {
			type: 'array',
			items: {
				type: 'object',
				props: {
					voucherCode: {
						type: 'string'
					},
					dataUpdate: {
						type: 'object',
						props: {
							expiredIn: {
								type: 'object',
								required: true,
								props: {
									fromDate: {
										type: 'string',
										required: true,
									},
									fromTime: {
										type: 'string',
										optional: true
									},
									toDate: {
										type: 'string',
										required: true
									},
									toTime: {
										type: 'string',
										optional: true
									},
								}
							}
						}
					},
				},
			},
		}
	},
};
