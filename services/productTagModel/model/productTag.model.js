const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose);
const Schema = mongoose.Schema({
	merchantId: {
		type: Number,
		require: true
	},
	accountId: {
		type: Number,
		require: true
	},
	title: {
		type: String,
		required: true
	}
}, {
	collection: 'ProductTag',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
