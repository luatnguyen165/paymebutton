const _ = require('lodash');

const SERVICE_CODE = '122';
const ACTION_CODE = {
	CREATE_TAG: '7',
	READ_TAG: '8',
	REMOVE_TAG: '9'
};

module.exports = { // 00, 01: success, 02-99: fail
	CREATE_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_TAG}00`),
			key: 'CREATE_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_TAG}02`),
			key: 'CREATE_TAG_FAIL',
			message: 'Tạo tag thất bại'
		},
	},
	READ_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ_TAG}00`),
			key: 'READ_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ_TAG}02`),
			key: 'READ_TAG_FAIL',
			message: 'Lấy danh sách tag thất bại'
		},
	},
	REMOVE_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}00`),
			key: 'REMOVE_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}02`),
			key: 'REMOVE_TAG_FAIL',
			message: 'Xóa tag thất bại'
		},
		MISS_TAGID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}03`),
			key: 'REMOVE_TAG_MISS_PAGEID',
			message: 'Vui lòng cung cấp tagId'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}04`),
			key: 'REMOVE_TAG_NOT_FOUND',
			message: 'Không tìm thấy thông tin tag'
		}
	},

	replaceMsg: (str, replacements) => {
		let newStr = str;
		for (let i = 0; i < replacements.length; i += 1) {
			const replacement = replacements[i];
			newStr = _.replace(newStr, `{{${i}}}`, replacement);
		}
		return newStr;
	},
	SCOPE_CODE: {
		CHECK_ROLE_SUCCESS: 131000,
		ROLE_NOT_FOUND: 131001,
		SCOPE_NOT_FOUND: 131002
	}
};
