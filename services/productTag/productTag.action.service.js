/* eslint-disable object-shorthand */
/* eslint-disable no-throw-literal */
const _ = require('lodash');
const path = require('path');
const { I18n } = require('i18n');
const { MoleculerError } = require('moleculer').Errors;
const productTagSchema = require('./schema/productTag.schema');
const GeneralConstants = require('./constants/general.constant');
const ResponseConstants = require('./constants/response.constant');

const DIRNAME = __dirname;

module.exports = {
	name: 'productTag',
	mixins: [],
	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	hooks: {
		before: {
			create: ['checkUserScope'],
			remove: ['checkUserScope'],
			read: ['checkUserScope'],
			// Assign it to a specified action or all actions (*) in service.
			'*': [
				// async function isValidScope(ctx) {
				// 	const mode = _.get(ctx, 'action.rest.auth.mode', '');
				// 	const scope = _.get(ctx, 'action.scope', null);
				// 	if ((mode === 'required' || mode === 'optional') && scope !== null) {
				// 		const accountInfo = {
				// 			accountId: _.get(ctx, 'meta.auth.credentials.accountId', ''),
				// 			merchantId: _.get(ctx, 'meta.auth.credentials.merchantId', ''),
				// 			// storeId: '' // TODO
				// 		};
				// 		const scopeInfo = {
				// 			name: scope.name || [],
				// 			condition: scope.condition || GeneralConstants.SCOPE.CONDITION.AND
				// 		};
				// 		const checkScopeResult = await this.checkScope(accountInfo, scopeInfo);
				// 		if (checkScopeResult.code !== GeneralConstants.SCOPE.CODE.CHECK_ROLE_SUCCESS) {
				// 			const errMsg = checkScopeResult.message || 'Tài khoản không được phép thực hiện thao tác này';
				// 			throw new MoleculerError(errMsg, checkScopeResult.code);
				// 		}
				// 	}
				// },

				function setLanguage(ctx) {
					const language = _.get(ctx.params, 'body.language', '') || _.get(ctx.params, 'query.language', '') || GeneralConstants.LANGUAGE.VI;
					ctx.service.setLocale(language);
				}

			]
		},
		error: {
			'*': function (ctx, err) {
				return _.pick(err, ['message', 'code', 'data']);
			}
		}
	},

	dependencies: ['v1.paymePageModel', 'v1.productTagModel'],
	crons: [],
	/**
	 * Actions
	 */
	actions: {
		create: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/tag',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: productTagSchema.create,
			scope: {
				name: ['mc.paymePage.create'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/createTag.action'),
		},
		read: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/tag/list',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: productTagSchema.read,
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/listTag.action'),
		},
		remove: {
			rest: {
				method: 'DELETE',
				fullPath: '/widgets/tag/:tagId',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: productTagSchema.remove,
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/removeTag.action'),
		},
		readExternal: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/tag/external/list',
				auth: false
			},
			params: productTagSchema.readExternal,
			handler: require('./actions/listTag.external.action'),
		},
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		// eslint-disable-next-line consistent-return
		async checkUserScope(ctx) {
			this.broker.logger.info(`PaymePage:TAG role check response: ${JSON.stringify(ctx.action.scope)}, ${ctx.action.scope.name && ctx.action.scope.name.length > 0}`);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				this.broker.logger.info(`PaymePage:TAG auth info: ${JSON.stringify(auth)}`);
				if (_.get(auth, 'credentials.accountId', null) === null) {
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!'
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: _.toNumber(auth.credentials.accountId) });
				} catch (error) {
					this.broker.logger.info(`PaymePage: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(auth.credentials)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`
					};
				}
				const accountInfo = {
					accountId: _.toNumber(auth.credentials.accountId),
					merchantId: auth.credentials.merchantId || merchantInfo.id
				};
				let mcRole = '';
				if (ctx.action.scope.name.includes('mc.paymePage.read')) {
					mcRole = 'mc.paymePage.read';
				} else if (ctx.action.scope.name.includes('mc.paymePage.create')) {
					mcRole = 'mc.paymePage.create';
				} else if (ctx.action.scope.name.includes('mc.paymePage.update')) {
					mcRole = 'mc.paymePage.update';
				} else if (ctx.action.scope.name.includes('mc.paymePage.notify')) {
					mcRole = 'mc.paymePage.notify';
				}
				try {
					const resultCheckRole = await this.broker.call('v1.role.get', { target: accountInfo });
					this.broker.logger.info(`PaymePage:TAG call v1.role.read response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager', mcRole]).length <= 0) {
						throw {
							code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden'
						};
					}
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager']).length > 0) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
							merchantInfo
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true, // creator
							merchantInfo
						};
					}
				} catch (error) {
					this.broker.logger.warn(`PaymePage:TAG call v1.role.get response: ${error.message}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}, error: ${JSON.stringify(error)}`);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`
					};
				}
				// }
				// else {
				// 	let checkData = {};
				// 	try {
				// 		checkData = {
				// 			target: accountInfo,
				// 			scope: ctx.action.scope.name,
				// 			// storeID: 1, // optional
				// 			options: { // optional
				// 				condition: ctx.action.scope.condition // AND & OR, default AND
				// 			}
				// 		};
				// 		const resultCheckRole = await this.broker.call('v1.role.check', checkData);
				// 		this.broker.logger.info(`PaymePage:TAG call v1.role.check response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)}`);
				// 		if (_.get(resultCheckRole, 'isValid', false) === false) {
				// 			throw {
				// 				code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 				message: 'Forbidden'
				// 			};
				// 		}
				// 		return resultCheckRole;
				// 	} catch (error) {
				// 		this.broker.logger.warn(`PaymePage:TAG call v1.role.check error: ${error}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)} , error: ${JSON.stringify(error)}`);
				// 		throw {
				// 			code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 			message: `[3]${error.message}`
				// 		};
				// 	}
				// }
			}
		},
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(DIRNAME, '/locales'),
			defaultLocale: 'vi',
			register: this
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		//
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
