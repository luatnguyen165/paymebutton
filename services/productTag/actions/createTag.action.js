const _ = require('lodash');
const ResponseConstant = require('../constants/response.constant');
const GeneralConstants = require('../constants/general.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		ctx.broker.logger.info(`PaymePage create tag payload: ${JSON.stringify(payload)}`);
		const { credentials = {} } = _.get(ctx, 'meta.auth', {});

		const dataToCreate = {
			merchantId: credentials.merchantId,
			accountId: credentials.accountId,
			title: payload.title,
		};

		const created = await this.broker.call('v1.productTagModel.create', [dataToCreate]);
		if (_.get(created, 'id', null) === null) {
			const { code, message } = ResponseConstant.CREATE_TAG.FAIL;
			return { code, message: this.__(message) };
		}
		const { code, message } = ResponseConstant.CREATE_TAG.SUCCESS;
		return {
			code,
			message: this.__(message),
		};
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Create TAG Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
