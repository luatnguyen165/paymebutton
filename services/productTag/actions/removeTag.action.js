/* eslint-disable no-underscore-dangle */
const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');

module.exports = async function (ctx) {
	try {
		const { tagId } = ctx.params.params;
		if (!tagId) {
			const { code, message } = RespConst.REMOVE_TAG.MISS_TAGID;
			return { code, message: this.__(message) };
		}
		const filter = { id: tagId };
		const auth = _.get(ctx, 'meta.auth', {});
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			filter.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}
		const tagInfo = await ctx.broker.call('v1.productTagModel.findOne', [filter]);
		if (!tagInfo) {
			const { code, message } = RespConst.REMOVE_TAG.NOT_FOUND;
			return { code, message };
		}
		const removeTag = await ctx.broker.call('v1.productTagModel.deleteOne', { query: { id: tagInfo.id } });
		if (removeTag.deletedCount === 0) {
			const { code, message } = RespConst.REMOVE_TAG.FAIL;
			return { code, message };
		}
		const { code, message } = RespConst.REMOVE_TAG.SUCCESS;
		return {
			code,
			message: this.__(message),
		};
	} catch (err) {
		ctx.broker.logger.error(`PaymePage =: removeTag Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
