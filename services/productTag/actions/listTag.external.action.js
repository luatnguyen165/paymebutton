const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const { tagId, title, pageId } = payload.filter;
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId },
				{ 'shortLinkInfo.slug': pageId }
			],
			status: 'ACTIVE'
		}]);
		if (!pageInfo) {
			const { code } = RespConst.READ_TAG.FAIL;
			return { code, message: this.__('Không tìm thấy thông tin page') };
		}
		const filter = {
			...tagId && { tagId },
			...title && { title: RegExp(title, 'i') },
			merchantId: pageInfo.creator.merchantId
		};

		this.broker.logger.info(`Page:tag read filter by > ${JSON.stringify(filter)}`);
		let result = [];
		try {
			let total = 0;
			total = await this.broker.call('v1.productTagModel.count', { query: filter });
			if (total > 0) {
				result = await this.broker.call('v1.productTagModel.find', {
					body: {
						query: filter,
						paging: payload.paging,
						sort: payload.sort,
						fields: '-_id id title merchantId accountId createdAt'
					},
				});
			}
			const { code, message } = RespConst.READ_TAG.SUCCESS;
			return {
				code,
				message: this.__(message),
				data: {
					total,
					items: result
				}
			};
		} catch (e) {
			ctx.broker.logger.warn(e);
			const { code, message } = RespConst.READ_TAG.FAIL;
			return { code, message };
		}
	} catch (err) {
		ctx.broker.logger.error(`PaymePage:Tag Read Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
