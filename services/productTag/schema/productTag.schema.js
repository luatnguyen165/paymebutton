const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');

module.exports = {
	create: {
		body: {
			$$type: 'object',
			title: 'string|required|trim',
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		}
	},
	remove: {
		params: {
			$$type: 'object',
			tagId: 'string|trim'
		},
		query: {
			$$type: 'object',
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		}
	},
	read: {
		body: {
			$$type: 'object',
			filter: {
				$$type: 'object',
				tagId: 'string|trim|optional',
				title: 'string|trim|optional',
			},
			paging: {
				$$type: 'object',
				start: 'number|integer|min:0|default:0',
				limit: 'number|integer|min:0'
			},
			sort: {
				$$type: 'object',
				createdAt: { type: 'enum', default: -1, values: [1, -1] }
			},
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		},
	},
	readExternal: {
		body: {
			$$type: 'object',
			filter: {
				$$type: 'object',
				pageId: 'string|trim|required',
				tagId: 'string|trim|optional',
				title: 'string|trim|optional',
			},
			paging: {
				$$type: 'object',
				start: 'number|integer|min:0|default:0',
				limit: 'number|integer|min:0'
			},
			sort: {
				$$type: 'object',
				createdAt: { type: 'enum', default: -1, values: [1, -1] }
			},
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		},
	}
};
