module.exports = {
	THEME: {
		DARK: 'DARK',
		LIGHT: 'LIGHT',
		OUTLINE: 'OUTLINE',
		BRAND_COLOR: 'BRAND_COLOR',
	},
};
