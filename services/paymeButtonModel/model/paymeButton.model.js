const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const generalConstant = require('../constants/general.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
	{
		buttonId: {
			type: String,
			required: true,
		},
		createdBy: {
			merchantId: {
				type: Number,
				required: true,
			},
			accountId: {
				type: Number,
				required: true,
			},
			email: {
				type: String,
				default: null,
			},
			username: {
				type: String,
				default: null,
			},
			storeId: {
				type: String,
				default: null,
			},
		},
		referencePageId: {
			type: String,
			required: true,
		},
		title: {
			type: String,
			required: true,
		},
		label: {
			type: String,
			required: true,
		},
		theme: {
			type: String,
			enum: Object.keys(generalConstant.THEME),
			required: true,
		},
		numOfClick: {
			type: Number,
			default: 0,
		},
		numOfPayment: {
			type: Number,
			default: 0,
		},
		numOfView: {
			type: Number,
			default: 0,
		},
	},
	{
		collection: 'PaymeButton',
		versionKey: false,
		timestamps: true,
	}
);

Schema.index({ id: 1 }, { unique: true });
Schema.index({ buttonId: 1 }, { unique: true });
Schema.index({ referencePageId: 1 });
Schema.index({ title: 'text' });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
