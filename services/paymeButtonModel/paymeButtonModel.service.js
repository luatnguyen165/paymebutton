const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const PaymeButtonModel = require('./model/paymeButton.model');

module.exports = {
	name: 'paymeButtonModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
	}),

	model: PaymeButtonModel,

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: MongooseAction({
		/**
		 * action count documentations
		 * @params {Object} query => Query object. Passes to adapter
		 */
		count: {
			rest: false,
			cache: false,
			params: {
				query: {
					$$type: 'object|optional|default:{}',
				},
			},
			handler: async (ctx) => {
				const { query } = ctx.params;
				const result = await PaymeButtonModel.countDocuments(query);
				return result;
			},
		},
		/**
		 * Action find with condition + paging + sort
		 * @params {object} body
		 * body = {
		 *   query: {object}, Query object. Passes to adapter.
		 *   paging: {object}, Pagination.
		 *   sort: {object}, Sorted fields.
		 *   fields: [{string}|{object}]} Fields filter.
		 */
		find: {
			rest: false,
			cache: false,
			params: {
				body: {
					$$type: 'object',
					query: 'object',
					paging: {
						$$type: 'object',
						start: 'number|optional|default:0',
						limit: 'number|optional|default:0',
					},
					sort: {
						$$type: 'object',
						createdAt: 'number|optional|default:-1',
					},
					fields: {
						type: 'multi',
						rules: [{ type: 'object' }, { type: 'string' }],
						default: '',
					},
				},
			},
			handler: async (ctx) => {
				const payload = ctx.params.body;
				const result = await PaymeButtonModel.find(payload.query)
					.select(payload.fields)
					.sort(payload.sort)
					.skip(payload.paging.start)
					.limit(payload.paging.limit)
					.lean();
				return result;
			},
		}
	}),

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {},

	/**
	 * Service started lifecycle event handler
	 */
	started() {},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},

	afterConnected() {},
};
