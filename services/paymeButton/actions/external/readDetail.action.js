/**
 * @notice get details external. Non access token
 */
const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');
const generalConstant = require('../../constants/general.constant');

const pageLinkUrl = process.env.PAGE_URL || 'https://payme.vn';

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: responseCode.GET_BUTTON_INFO_FAIL.code,
			message: this.__(responseCode.GET_BUTTON_INFO_FAIL.message),
		};

		const { buttonId } = ctx.params.params;
		const filter = { buttonId };

		const buttonInfo = await ctx.broker.call(
			'v1.paymeButtonModel.findOne',
			[filter]
		);
		if (!buttonInfo) {
			const { code, message } = responseCode.BUTTON_NOT_FOUND;
			return { code, message };
		}

		// const pageQuery = { pageId: buttonInfo.referencePageId };
		const pageQuery = {
			$or: [
				{ pageId: buttonInfo.referencePageId },
				{ 'shortLinkInfo.slug': buttonInfo.referencePageId },
			],
		};

		const paymePage = await this.broker.call('v1.paymePageModel.findOne', [
			pageQuery,
		]);
		if (!paymePage) {
			const { code, message } = responseCode.PAGE_NOT_FOUND;
			return { code, message };
		}

		let pageIdUuid = _.get(paymePage, 'pageId', '');
		if (_.get(paymePage, 'shortLinkInfo.slug', null) !== null) {
			pageIdUuid = paymePage.shortLinkInfo.slug;
		}
		const fullLink = `${process.env.PAGE_URL_SHORT}/${pageIdUuid}`;

		let colorCode; // payme color code
		if (buttonInfo.theme === generalConstant.THEME.BRAND_COLOR) {
			const merchantInfo = await this.broker.call(
				'v1.settingsDashboard.internalSettings',
				{ merchantId: buttonInfo.createdBy.merchantId }
			);
			colorCode = _.get(merchantInfo, 'data.themeColor', '#00be00');
		}

		this.broker.call('v1.paymeButtonModel.updateOne', [
			{
				buttonId,
			},
			{
				$inc: {
					numOfView: 1
				}
			}
		]);

		dataResponse.code = responseCode.GET_BUTTON_INFO_SUCCESS.code;
		dataResponse.message = this.__(
			responseCode.GET_BUTTON_INFO_SUCCESS.message
		);
		dataResponse.data = {
			title: buttonInfo.title,
			label: buttonInfo.label,
			theme: buttonInfo.theme,
			referencePageUrl: fullLink,
			colorCode
		};

		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(
			`PayME Button withdraw Error: ${err.message}`,
			99
		);
	}
};
