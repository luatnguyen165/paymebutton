/**
 * @notice get details external. Non access token
 */
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: responseCode.INCREASE_CLICK_FAIL.code,
			message: this.__(responseCode.INCREASE_CLICK_FAIL.message),
		};

		const { buttonId } = ctx.params.params;
		const filter = { buttonId };

		const buttonInfo = await ctx.broker.call(
			'v1.paymeButtonModel.findOne',
			[filter]
		);
		if (!buttonInfo) {
			const { code, message } = responseCode.BUTTON_NOT_FOUND;
			return { code, message };
		}

		await this.broker.call('v1.paymeButtonModel.updateOne', [
			{ id: buttonInfo.id },
			{
				numOfClick: buttonInfo.numOfClick + 1,
			},
		]);

		dataResponse.code = responseCode.INCREASE_CLICK_SUCCESS.code;
		dataResponse.message = this.__(
			responseCode.INCREASE_CLICK_SUCCESS.message
		);

		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(
			`PayME Button withdraw Error: ${err.message}`,
			99
		);
	}
};
