const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: responseCode.CREATE_BUTTON_FAIL.code,
			message: this.__(responseCode.CREATE_BUTTON_FAIL.message),
		};
		const payload = ctx.params;
		ctx.broker.logger.info(payload);
		const { body } = payload;
		const auth = _.get(ctx, 'meta.auth', {});

		let created = null;
		try {
			const buttonId = await this.customNanoId({ prefix: 'PAYME_BUTTONS', type: 'string' });
			if (!buttonId) {
				dataResponse.code = responseCode.GET_UUID_FAIL.code;
				dataResponse.message = this.__(responseCode.GET_UUID_FAIL.message);
				return dataResponse;
			}
			const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [
				// { pageId: body.referencePageId }
				{
					$or: [
						{ pageId: body.referencePageId },
						{ 'shortLinkInfo.slug': body.referencePageId }
					]
				}
			]);
			if (!pageInfo) {
				const { code, message } = responseCode.PAGE_NOT_FOUND;
				return { code, message: this.__(message) };
			}

			const buttonInfo = {
				buttonId,
				createdBy: {
					merchantId: auth.credentials.merchantId,
					accountId: auth.credentials.accountId,
					username: auth.data.username,
					email: auth.data.email,
				},
				title: body.title,
				theme: body.theme,
				referencePageId: body.referencePageId,
				label: body.label
			};
			created = await this.broker.call('v1.paymeButtonModel.create', [
				buttonInfo,
			]);
			if (_.get(created, 'id', null) === null) {
				return dataResponse;
			}
		} catch (e) {
			ctx.broker.logger.warn(e);
			dataResponse.message = e.message;
			return dataResponse;
		}

		dataResponse.code = responseCode.CREATE_BUTTON_SUCCESS.code;
		dataResponse.message = this.__(responseCode.CREATE_BUTTON_SUCCESS.message);
		dataResponse.data = {
			id: created.id,
			buttonId: created.buttonId
		};
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Button withdraw Error: ${err.message}`, 99);
	}
};
