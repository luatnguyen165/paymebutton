const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');

const pageLinkUrl = process.env.PAGE_URL_SHORT || 'https://dev.payme.net.vn/p';

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: responseCode.GET_LIST_BUTTON_FAIL.code,
			message: this.__(responseCode.GET_LIST_BUTTON_FAIL.message),
		};

		const payload = ctx.params.body;

		const { filter, paging = {}, sort = { id: -1 } } = payload;
		const auth = _.get(ctx, 'meta.auth', {});
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});

		const query = {
			...(filter.titleButton && { $text: { $search: filter.titleButton } }),
			...(filter.buttonId && { buttonId: filter.buttonId }),
			...(filter.referencePageId && { referencePageId: filter.referencePageId })
		};

		let total = 0;
		let result = [];

		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			query['createdBy.merchantId'] = auth.credentials.merchantId || merchantInfo.id;
		} else {
			query['createdBy.accountId'] = auth.credentials.accountId;
		}
		// result search payme button without reference page id
		const paymeButtons = await ctx.broker.call(
			'v1.paymeButtonModel.find',
			{
				body: {
					query,
					paging: {},
					sort,
					fields: 'referencePageId',
				},
			}
		);

		if (_.isEmpty(paymeButtons)) {
			return {
				code: responseCode.GET_LIST_BUTTON_SUCCESS.code,
				message: this.__(responseCode.GET_LIST_BUTTON_SUCCESS.message),
				data: { total, items: result }
			};
		}

		const referencePageIds = _.uniq(paymeButtons.map((item) => item.referencePageId));
		const pageQuery = {
			$or: [
				{ pageId: { $in: referencePageIds } },
				{ 'shortLinkInfo.slug': { $in: referencePageIds } }
			]
		};
		if (filter.titlePage) {
			pageQuery.$text = { $search: filter.titlePage };
		}

		const paymePages = await ctx.broker.call(
			'v1.paymePageModel.findMany', [pageQuery]
		);

		const arrRefPageId = [];
		const arrRefPageSlug = [];
		paymePages.forEach((page) => {
			arrRefPageId.push(page.pageId);
			if (_.get(page, 'shortLinkInfo.slug', false) !== false) {
				arrRefPageSlug.push(page.shortLinkInfo.slug);
			}
		});
		query.$or = [
			{ referencePageId: { $in: arrRefPageId } },
			{ referencePageId: { $in: arrRefPageSlug } }
		];
		total = await this.broker.call('v1.paymeButtonModel.count', {
			query,
		});
		if (total < 1) {
			dataResponse.code = responseCode.GET_LIST_BUTTON_SUCCESS.code;
			dataResponse.message = this.__(responseCode.GET_LIST_BUTTON_SUCCESS.message);
			dataResponse.data = { total, items: result };
			return dataResponse;
		}
		// result search page with all condition
		const paymeButtonList = await ctx.broker.call(
			'v1.paymeButtonModel.find',
			{
				body: {
					query,
					paging,
					sort,
					fields: '-_id buttonId createdBy referencePageId title label theme numOfClick numOfPayment',
				},
			}
		);

		const mappingPageIdToDetail = paymePages.reduce((acc, cur) => {
			if (_.get(cur, 'shortLinkInfo.slug', null) !== null) {
				acc[cur.shortLinkInfo.slug] = cur;
			} else {
				acc[cur.pageId] = cur;
			}
			return acc;
		}, {});

		result = paymeButtonList.map((paymeButton = {}) => {
			const pageInfo = mappingPageIdToDetail[paymeButton.referencePageId];
			let pageIdUuid = _.get(pageInfo, 'pageId', '');
			if (_.get(pageInfo, 'shortLinkInfo.slug', null) !== null) {
				pageIdUuid = pageInfo.shortLinkInfo.slug;
			}
			const fullLink = `${pageLinkUrl}/${pageIdUuid}`;
			return {
				createdBy: paymeButton.createdBy,
				buttonId: paymeButton.buttonId,
				titleButton: paymeButton.title,
				referencePageId: paymeButton.referencePageId,
				titlePage: _.get(pageInfo, 'businessDetail.title', ''),
				referencePageUrl: fullLink,
				theme: paymeButton.theme,
				label: paymeButton.label,
				numOfClick: paymeButton.numOfClick,
				numOfPayment: paymeButton.numOfPayment
			};
		});

		dataResponse.code = responseCode.GET_LIST_BUTTON_SUCCESS.code;
		dataResponse.message = this.__(
			responseCode.GET_LIST_BUTTON_SUCCESS.message
		);
		dataResponse.data = { total, items: result };
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(
			`PayME Button withdraw Error: ${err.message}`,
			99
		);
	}
};
