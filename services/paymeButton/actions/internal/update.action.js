const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const dataResponse = {
			code: responseCode.UPDATE_BUTTON_FAIL.code,
			message: this.__(responseCode.UPDATE_BUTTON_FAIL.message),
		};
		const payload = ctx.params;
		ctx.broker.logger.info(payload);
		const { body } = payload;
		const auth = _.get(ctx, 'meta.auth', {});

		try {
			const query = {
				buttonId: ctx.params.params.buttonId
			};
			const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
			if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
				query['createdBy.merchantId'] = auth.credentials.merchantId || merchantInfo.id;
			} else {
				query['createdBy.accountId'] = auth.credentials.accountId;
			}

			const paymeBtn = await this.broker.call('v1.paymeButtonModel.findOne', [
				query,
			]);
			if (_.get(paymeBtn, 'id', null) === null) {
				dataResponse.code = responseCode.BUTTON_NOT_FOUND.code;
				dataResponse.message = this.__(responseCode.BUTTON_NOT_FOUND.message);
				return dataResponse;
			}

			const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [
				{
					$or: [
						{ pageId: body.referencePageId },
						{ 'shortLinkInfo.slug': body.referencePageId }
					]
				}
			]);
			if (!pageInfo) {
				const { code, message } = responseCode.PAGE_NOT_FOUND;
				return { code, message: this.__(message) };
			}

			const payloadUpdate = _.pick(body, ['title', 'label', 'theme', 'referencePageId']);
			const updated = await this.broker.call('v1.paymeButtonModel.updateOne', [
				{ id: paymeBtn.id },
				payloadUpdate,
			]);

			if (!_.isObject(updated) || updated.nModified < 1) {
				dataResponse.message = this.__(responseCode.UPDATE_BUTTON_FAIL.message);
				return dataResponse;
			}
		} catch (e) {
			ctx.broker.logger.warn(e);
			dataResponse.message = this.__(e.message);
			return dataResponse;
		}
		dataResponse.code = responseCode.UPDATE_BUTTON_SUCCESS.code;
		dataResponse.message = this.__(responseCode.UPDATE_BUTTON_SUCCESS.message);
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`Payme Button withdraw Error: ${err.message}`, 99);
	}
};
