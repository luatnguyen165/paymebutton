const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const responseCode = require('../../constants/responseCode.constant');

module.exports = async function (ctx) {
	try {
		const { buttonId } = ctx.params.params;
		const filter = { buttonId };

		const auth = _.get(ctx, 'meta.auth', {});
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter['createdBy.merchantId'] = auth.credentials.merchantId;
		} else {
			filter['createdBy.accountId'] = auth.credentials.accountId;
		}

		const buttonInfo = await ctx.broker.call(
			'v1.paymeButtonModel.findOne',
			[filter]
		);
		if (!buttonInfo) {
			const { code, message } = responseCode.BUTTON_NOT_FOUND;
			return { code, message: this.__(message) };
		}

		const removeButton = await ctx.broker.call(
			'v1.paymeButtonModel.delete',
			[{ id: buttonInfo.id }]
		);
		if (removeButton.deletedCount < 1) {
			const { code, message } = responseCode.REMOVE_BUTTON_FAIL;
			return { code, message: this.__(message) };
		}

		const { code, message } = responseCode.REMOVE_BUTTON_SUCCESS;

		return {
			code,
			message: this.__(message),
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(
			`PayME Button withdraw Error: ${err.message}`,
			99
		);
	}
};
