const generate = (chars, length) => {
	let s = '';
	for (let i = 0; i < length; i += 1) {
		const pos = (Math.floor(Math.random() * Math.floor(chars.length)));
		s += chars[pos];
	}
	return s.toLowerCase().toString();
};

const generateChar = (length = 6) => {
	const chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
		'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'];
	return generate(chars, length);
};

module.exports = {
	generateChar,
};
