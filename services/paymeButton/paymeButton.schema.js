const generalConstant = require('./constants/general.constant');

/**
 * @notice Create payme button
 */
const create = {
	body: {
		$$type: 'object',
		// button details
		title: 'string|min:3',
		label: 'string|default:THANH TOÁN',
		theme: {
			type: 'enum',
			values: Object.values(generalConstant.THEME),
		},
		referencePageId: 'string',
		language: {
			type: 'enum',
			values: ['vi', 'en'],
			optional: true,
			default: 'vi',
		},
	},
};

/**
 * @notice update button info
 */
const update = {
	body: {
		$$type: 'object',
		title: 'string|optional|min:3',
		referencePageId: 'string|optional',
		label: 'string|optional',
		theme: {
			optional: true,
			type: 'enum',
			values: Object.values(generalConstant.THEME),
		},
		language: {
			type: 'enum',
			values: ['vi', 'en'],
			optional: true,
			default: 'vi',
		},
	},
	params: {
		$$type: 'object',
		buttonId: 'string',
	},
};

/**
 * @dev get list payme button
 */
const read = {
	body: {
		$$type: 'object',
		filter: {
			$$type: 'object',
			titlePage: 'string|optional',
			titleButton: 'string|optional',
			referencePageId: 'string|optional',
			buttonId: 'string|optional'
		},
		paging: {
			$$type: 'object',
			start: 'number|min:0|optional|default:0',
			limit: 'number|min:0|optional|default:0', // unlimited
		},
		sort: {
			$$type: 'object',
			createdAt: {
				type: 'enum',
				optional: true,
				default: -1,
				values: [1, -1],
			},
		},
		language: {
			type: 'enum',
			values: ['vi', 'en'],
			optional: true,
			default: 'vi',
		},
	},
};

/**
 * @notice get all data of specific payme button
 */
const readDetail = {
	// Internal & External
	params: {
		$$type: 'object',
		buttonId: 'string',
	},
};

const remove = {
	// Internal & External
	params: {
		$$type: 'object',
		buttonId: 'string',
	},
};

const increaseClick = {
	params: {
		$$type: 'object',
		buttonId: 'string',
	},
};

module.exports = {
	create,
	update,
	read,
	readDetail,
	remove,
	increaseClick,
};
