/**
 * @notice paymeButton service prefix response code: 123xxx
 * recommend code format: {SERVICE_CODE:3}{ACTION_CODE:1}{ERROR_CODE:2}
 * action exec success should be ERROR_CODE == 00 || 01. Else [02->99]
 */
module.exports = {
	// common error: {PREFIX}0xx
	INVALID_PARAMS: {
		code: 123002,
		message: 'Tham số đầu vào không hợp lệ',
	},
	GET_UUID_FAIL: {
		code: 123003,
		message: 'Lấy UUID thất bại',
	},
	SCOPE_CODE: {
		CHECK_ROLE_SUCCESS: 123010,
		ROLE_NOT_FOUND: 123011,
		SCOPE_NOT_FOUND: 123012,
	},
	BUTTON_NOT_FOUND: {
		code: 123004,
		message: 'Không tìm thấy PayME Button',
	},
	PAGE_NOT_FOUND: {
		code: 123005,
		message: 'Không tìm thấy trang liên kết',
	},
	// create button {PREFIX}1xx
	CREATE_BUTTON_SUCCESS: {
		code: 123100,
		message: 'Tạo nút thành công',
	},
	CREATE_BUTTON_FAIL: {
		code: 123102,
		message: 'Tạo nút thất bại',
	},
	// update button {PREFIX}2xx
	UPDATE_BUTTON_SUCCESS: {
		code: 123200,
		message: 'Cập nhật thành công',
	},
	UPDATE_BUTTON_FAIL: {
		code: 123202,
		message: 'Cập nhật thất bại',
	},
	// get button list code: {PREFIX}3xx
	GET_LIST_BUTTON_SUCCESS: {
		code: 123300,
		message: 'Lấy danh sách nút thành công',
	},
	GET_LIST_BUTTON_FAIL: {
		code: 123302,
		message: 'Lấy danh sách nút thất bại',
	},
	// delete button: {PREFIX}4xx
	REMOVE_BUTTON_SUCCESS: {
		code: 123400,
		message: 'Xóa nút thành công',
	},
	REMOVE_BUTTON_FAIL: {
		code: 123402,
		message: 'Xóa nút thất bại',
	},
	// read details a button: {PREFIX}5xx
	GET_BUTTON_INFO_SUCCESS: {
		code: 123500,
		message: 'Lấy thông tin nút thành công',
	},
	GET_BUTTON_INFO_FAIL: {
		code: 123502,
		message: 'Lấy thông tin nút thất bại',
	},
	// click a button: {PREFIX}6xx
	INCREASE_CLICK_SUCCESS: {
		code: 123600,
		message: 'Tăng số lượt click thành công',
	},
	INCREASE_CLICK_FAIL: {
		code: 123602,
		message: 'Tăng số lượt click thất bại',
	},
};
