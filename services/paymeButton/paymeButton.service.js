const _ = require('lodash');
const { I18n } = require('i18n');
const path = require('path');
const generalConstant = require('./constants/general.constant');
const paymeButtonSchema = require('./paymeButton.schema');
const ResponseConstants = require('./constants/responseCode.constant');

module.exports = {
	name: 'paymeButton',

	version: 1,
	/**
	 * Settings
	 */
	settings: {},

	hooks: {
		before: {
			// Assign it to a specified action or all actions (*) in service.
			'*': [
				function setLanguage(ctx) {
					const language = _.get(ctx.params, 'body.language', '') || _.get(ctx.params, 'language', '') || 'vi';
					this.setLocale(language);
				}
			],
			create: ['checkUserScope'],
			read: ['checkUserScope'],
			update: ['checkUserScope'],
			delete: ['checkUserScope'],
		},
		error: {
			'*': function (ctx, err) {
				return _.pick(err, ['message', 'code', 'data']);
			},
		},
	},

	dependencies: ['v1.paymeButtonModel', 'v1.paymePageModel'],

	/**
	 * Actions
	 */
	actions: {
		create: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/button',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			scope: {
				name: ['mc.paymeButton.create'],
				condition: generalConstant.SCOPE.CONDITION.AND,
			},
			params: paymeButtonSchema.create,
			handler: require('./actions/internal/create.action'),
		},
		update: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/button/:buttonId',
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			scope: {
				name: ['mc.paymeButton.update'],
				condition: generalConstant.SCOPE.CONDITION.AND,
			},
			params: paymeButtonSchema.update,
			handler: require('./actions/internal/update.action'),
		},
		read: {
			rest: {
				method: 'POST',
				security: true,
				fullPath: '/widgets/button/read',
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			scope: {
				name: ['mc.paymeButton.read'],
				condition: generalConstant.SCOPE.CONDITION.AND,
			},
			params: paymeButtonSchema.read,
			handler: require('./actions/internal/read.action'),
		},
		delete: {
			rest: {
				method: 'DELETE',
				fullPath: '/widgets/button/:buttonId',
				auth: {
					strategies: ['Default'],
					mode: 'required',
				},
			},
			scope: {
				name: ['mc.paymeButton.delete'],
				condition: generalConstant.SCOPE.CONDITION.AND,
			},
			params: paymeButtonSchema.remove,
			handler: require('./actions/internal/delete.action'),
		},
		readDetail: {
			rest: {
				method: 'GET',
				security: false,
				fullPath: '/widgets/button/external/:buttonId',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			// scope: {
			// 	name: ['mc.paymeButton.read'],
			// 	condition: generalConstant.SCOPE.CONDITION.AND,
			// },
			params: paymeButtonSchema.readDetail,
			handler: require('./actions/external/readDetail.action'),
		},
		increaseClick: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/button/:buttonId/click',
				auth: {
					strategies: ['Default'],
					mode: 'try',
				},
			},
			params: paymeButtonSchema.increaseClick,
			handler: require('./actions/external/increaseClick.action'),
		}
	},

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {
		/* eslint-disable no-throw-literal */
		async checkUserScope(ctx) {
			this.broker.logger.info(
				`PaymeButton: role check response: ${JSON.stringify(
					ctx.action.scope
				)}, ${
					ctx.action.scope.name && ctx.action.scope.name.length > 0
				}`
			);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				this.broker.logger.info(
					`PaymeButton: auth info: ${JSON.stringify(auth)}`
				);
				if (_.get(auth, 'credentials.accountId', null) === null) {
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!',
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call(
						'v1.kycAccount.checkKYC',
						{ accountId: _.toNumber(auth.credentials.accountId) }
					);
				} catch (error) {
					this.broker.logger.info(
						`PaymeButton: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(
							auth.credentials
						)}, scope: ${JSON.stringify(ctx.action.scope)}`
					);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`,
					};
				}
				const accountInfo = {
					accountId: _.toNumber(auth.credentials.accountId),
					merchantId: auth.credentials.merchantId || merchantInfo.id,
				};
				let mcRole = '';
				if (ctx.action.scope.name.includes('mc.paymeButton.read')) {
					mcRole = 'mc.paymeButton.read';
				} else if (
					ctx.action.scope.name.includes('mc.paymeButton.create')
				) {
					mcRole = 'mc.paymeButton.create';
				} else if (
					ctx.action.scope.name.includes('mc.paymeButton.update')
				) {
					mcRole = 'mc.paymeButton.update';
				}
				try {
					const resultCheckRole = await this.broker.call(
						'v1.role.get',
						{ target: accountInfo }
					);
					this.broker.logger.info(
						`PaymeButton: call v1.role.read response: ${JSON.stringify(
							resultCheckRole
						)}, input: ${JSON.stringify(
							accountInfo
						)}, scope: ${JSON.stringify(ctx.action.scope)}`
					);
					if (
						_.intersection(_.get(resultCheckRole, 'scope', []), [
							'mc.owner',
							'mc.manager',
							mcRole,
						]).length <= 0
					) {
						throw {
							code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden',
						};
					}
					if (
						_.intersection(_.get(resultCheckRole, 'scope', []), [
							'mc.owner',
							'mc.manager',
						]).length > 0
					) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
							merchantInfo,
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true, // creator
							merchantInfo,
						};
					}
				} catch (error) {
					this.broker.logger.warn(
						`PaymeButton: call v1.role.get response: ${error}, input: ${JSON.stringify(
							accountInfo
						)}, scope: ${JSON.stringify(
							ctx.action.scope
						)}, error: ${JSON.stringify(error)}`
					);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`,
					};
				}
			}
		},
		customNanoId: require('./methods/customNanoId.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(__dirname, '/locales'),
			defaultLocale: 'vi',
			register: this,
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
