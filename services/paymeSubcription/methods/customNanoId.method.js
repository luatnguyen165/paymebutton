/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-constant-condition */
const _ = require('lodash');
const { customAlphabet } = require('nanoid');

module.exports = async function (params = {}) {
	try {
		const { prefix, type } = params;
		const findCondition = {};
		if (!!prefix === false) return null;
		let customNanoId = null;
		let nanoId = null;
		let actionBroker = null;
		for (let index = 0; index < _.toNumber(process.env.UUID_RAMDOM_TIME) || 10; index++) {
			switch (prefix) {
				case 'PAYME_LINKS': {
					nanoId = customAlphabet('0123456789abcdefghiklmnopqrstuwxyz', 12);
					customNanoId = nanoId();
					actionBroker = 'v1.paymeSubcriptionModel.findOne';
					findCondition.subcriptionId = customNanoId;
					break;
				}
				case 'PAYMENT_REQUEST_ID': {
					nanoId = customAlphabet('012345678901234567890123456789', 12);
					customNanoId = nanoId();
					actionBroker = 'v1.paymentRequestModel.findOne';
					findCondition.transactionId = customNanoId;
					break;
				}
				default:
					break;
			}
			if (_.isEmpty(findCondition) === true) break;
			if (!_.isNil(customNanoId)) {
				const existPayment = await this.broker.call(actionBroker, [findCondition]);

				if (!_.isObject(existPayment) || _.get(existPayment, 'id', false) === false) {
					break;
				}
			}
		}
		this.logger.info(`[METHOD] => [CUSTOM_NANO_ID] => [${JSON.stringify({ actionBroker, prefix })}] => ${customNanoId}`);
		return type === 'number' ? _.toNumber(customNanoId) : customNanoId;
	} catch (error) {
		this.logger.info(`[METHOD] => [CUSTOM_NANO_ID]:>>  ${error}`);
		return null;
	}
};
