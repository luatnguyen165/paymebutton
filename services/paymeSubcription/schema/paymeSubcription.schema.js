const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');
const PageConst = require('../../paymePage/constants/paymePage.constant');

module.exports = {
	remindPayment: {
		body: {
			$$type: 'object',
			subcriptionId: 'string|trim|optional',
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		}
	},
	read: {
		body: {
			$$type: 'object',
			filter: {
				$$type: 'object',
				pageId: 'string|trim|optional',
				pageTitle: 'string|trim|optional',
				subcriptionId: 'string|trim|optional',
				isActive: 'boolean|optional',
				recurringPaymentPeriod: {
					type: 'enum',
					optional: true,
					values: _.values(PageConst.RECURRING_PAYMENT_PERIOD)
				},
				createdAt: {
					$$type: 'object|optional',
					from: { type: 'date', convert: true, optional: true },
					to: { type: 'date', convert: true, optional: true }
				}
			},
			paging: {
				$$type: 'object',
				start: 'number|integer|min:0|default:0',
				limit: 'number|integer|min:0'
			},
			sort: {
				$$type: 'object',
				createdAt: { type: 'enum', default: -1, values: [1, -1] }
			},
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		}
	},
	update: {
		params: {
			$$type: 'object',
			subcriptionId: 'string|trim',
		},
		body: {
			$$type: 'object',
			isActive: 'boolean|optional',
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			}
		}
	},
	getDetailExternal: {
		params: {
			$$type: 'object',
			subcriptionId: 'string|trim|required',
		}
	},
	createOrder: {
		body: {
			$$type: 'object',
			subcriptionId: 'string|trim|required',
			customerInfo: {
				$$type: 'object',
				id: 'number|optional',
				phone: 'string|trim|optional',
				fullname: 'string|trim|optional',
				email: 'email|trim|optional',
				shippingAddress: 'string|trim|optional'
			},
			paymentMethod: {
				$$type: 'object',
				methodId: 'number',
				payCode: 'string|trim'
			},
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
			voucherCode: 'string|optional|trim',
			note: 'string|optional|trim',
			redirectUrl: 'string|optional|trim',
			failedUrl: 'string|optional|trim'
		},
	},
	detailInternal: {
		params: {
			$$type: 'object',
			subcriptionId: 'string|trim|required'
		},
		query: {
			$$type: 'object',
			language: {
				type: 'enum',
				optional: true,
				values: _.values(GeneralConstants.LANGUAGE),
				default: GeneralConstants.LANGUAGE.VI
			},
		}
	},
	unsubscribe: {
		params: {
			$$type: 'object',
			token: 'string|trim|required',
		}
	},
};
