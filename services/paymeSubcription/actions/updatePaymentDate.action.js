const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');
const { getNextPaymentDate } = require('../helpers/date.helper');

module.exports = async function (ctx) {
	try {
		// update nextPaymentDate
		const paymeSubcriptionCond = {
			isActive: true,
			nextPaymentDate: {
				$lte: new Date() // tại thời điểm chạy cron
			}
		};
		const paymeSubcriptionInfo = await this.broker.call('v1.paymeSubcriptionModel.findOne', [paymeSubcriptionCond, '-id id nextPaymentDate recurringPaymentPeriod']);
		ctx.broker.logger.info(`[PAYME_SUBCRIPTION] paymeSubcriptionInfo = ${JSON.stringify(paymeSubcriptionInfo)}`);
		if (paymeSubcriptionInfo) {
			//  update getNextPaymentDate, countPayment+=1
			await this.broker.call('v1.paymeSubcriptionModel.updateOne', [{
				id: paymeSubcriptionInfo.id
			}, {
				previousPaymentDate: paymeSubcriptionInfo.nextPaymentDate,
				nextPaymentDate: getNextPaymentDate(paymeSubcriptionInfo.recurringPaymentPeriod, new Date())
			}]);
			return true;
		}
		return false;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
