const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');

module.exports = async function (ctx) {
	const { subcriptionId } = ctx.params.params;
	try {
		const subcription = await this.broker.call('v1.paymeSubcriptionModel.findOne', [{
			subcriptionId,
			isActive: true
		}]);
		if (!subcription) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin thanh toán định kì')
			};
		}
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{ pageId: subcription.pageId }]);
		return {
			code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
			message: this.__(ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.message),
			data: {
				paymentLink: `${process.env.PAGE_URL_SHORT}/${subcription.pageId}`,
				...subcription,
				pageTitle: pageInfo ? pageInfo.businessDetail.title : ''
			}
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
