const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');
const CryptoHelper = require('../helpers/crypto-js.helper');

module.exports = async function (ctx) {
	const { token } = ctx.params.params;
	const dataString = CryptoHelper.hex2string(token, GeneralConstant.UNSUBCRIBE_MAIL_SECRET_KEY);
	ctx.broker.logger.info(`[UNSUBCRIPTION] dataString: ${dataString}`);
	const mailData = JSON.parse(dataString);
	const query = {
		subcriptionId: mailData.subcriptionId,
		isSubscribe: true
	};
	if (mailData.customerId) {
		query['customerDetail.id'] = mailData.customerId;
	}
	if (mailData.customerEmail) {
		query['customerDetail.email'] = mailData.customerEmail;
	}
	ctx.broker.logger.info(`[UNSUBCRIPTION] filter by: ${JSON.stringify(query)}`);

	try {
		const subcriptionInfo = await this.broker.call('v1.paymeSubcriptionModel.findOne', [query]);
		if (!subcriptionInfo) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin thanh toán định kì')
			};
		}
		await ctx.broker.call('v1.paymeSubcriptionModel.updateOne', [
			{
				subcriptionId: subcriptionInfo.subcriptionId
			}, {
				$set: {
					isSubscribe: false
				}
			}
		]);
		return {
			code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
			message: this.__('Hủy nhắc thanh toán thành công'),
			data: {
				pageId: subcriptionInfo.pageId
			}
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
