const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');
const SendEmailService = require('../service/sendEmailRecurringPayment');

module.exports = async function (ctx) {
	try {
		const filter = {
			isActive: true,
			isSubscribe: true,
			recurringPaymentType: PaymentRequestConst.RECURRING_PAYMENT.REMIND, // đơn hàng cho phép nhắc thanh toán
			nextPaymentDate: {
				$gte: new Date(moment().tz('UTC').startOf('day')),
				$lte: new Date(moment().tz('UTC').endOf('day'))
			}
		};
		let paymeSubcriptions = await this.broker.call('v1.paymeSubcriptionModel.find', {
			body: {
				query: filter
			},
		});
		ctx.broker.logger.info(`[SUBCRIPTION] sendNotifyRecuringPayment filter by: ${JSON.stringify({
			filter,
			paymeSubcriptions
		})}`);

		paymeSubcriptions = paymeSubcriptions.map((v) => {
			const data = { ...v };
			data.broker = this.broker;
			return data;
		});
		SendEmailService.init(paymeSubcriptions);
		return true;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription sendEmailRecurringPayment Error: ${err.message}`, 99);
	}
};
