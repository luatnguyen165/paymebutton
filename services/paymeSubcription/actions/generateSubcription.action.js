const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const { customAlphabet } = require('nanoid');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');
const { getNextPaymentDate } = require('../helpers/date.helper');

const nanoid = customAlphabet('0123456789', 10);

// TODO: cron update next payment date
module.exports = async function (ctx) {
	try {
		// const auth = _.get(ctx, 'meta.auth', {});
		const dataResponse = {
			code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
			message: this.__(ResponseConstant.ACTION_SUBCRIPTION_FAIL.message)
		};
		const payload = ctx.params;
		payload.language = payload.language || 'vi';
		this.setLocale(payload.language);

		ctx.broker.logger.warn(`[PAYME_SUBCRIPTION] generate payload: ${JSON.stringify(payload)}`);

		let pageInfo;
		try {
			pageInfo = await this.broker.call('v1.paymePageModel.findOne', [{ pageId: payload.pageId }]);
		} catch (err) {
			ctx.broker.logger.warn(`[PAYME_SUBCRIPTION] paymePageModel.findOne error: ${err}`);
		}
		if (!pageInfo) {
			return {
				code: ResponseConstant.PAGE_NOT_FOUND.code,
				message: this.__(ResponseConstant.PAGE_NOT_FOUND.message)
			};
		}
		if (_.get(pageInfo, 'recurringPayment', 'NONE') === 'NONE') {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Page không cho phép thanh toán định kì')
			};
		}

		const query = {
			id: payload.paymentRequestId,
			referId: pageInfo.pageId,
			referType: 'PAYME_PAGE',
			status: 'SUCCEEDED',
			recurringPayment: { $in: ['AUTO', 'REMIND'] } // customer allow recurring payment
			// $or: [
			// 	{
			// 		$and: [
			// 			{ 'customerDetails.name': 'id' },
			// 			{ 'customerDetails.value': payload.customer.id }
			// 		]
			// 	},
			// 	{
			// 		$and: [
			// 			{ 'customerDetails.name': 'email' },
			// 			{ 'customerDetails.value': payload.customer.email }
			// 		]
			// 	}
			// ]
		};
		this.broker.logger.info(`[PAYME_SUBCRIPTION] find paymentRequest filter = ${JSON.stringify(query)}`);
		let paymentRequest;
		try {
			paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [query]);
		} catch (err) {
			ctx.broker.logger.warn(`[PAYME_SUBCRIPTION] paymentRequestModel.findOne error: ${err}`);
		}
		if (!paymentRequest) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__(ResponseConstant.ACTION_SUBCRIPTION_FAIL.message)
			};
		}
		// kiem tra da tao lenh subcription hay chưa

		const customerId = _.get(paymentRequest.customerDetails.find((v) => (v.name === 'id' ? v.value : undefined)), 'value', null);
		const customerEmail = _.get(paymentRequest.customerDetails.find((v) => (v.name === 'email' ? v.value : undefined)), 'value', null);
		if (!customerEmail && !customerId) {
			return {
				code: ResponseConstant.CUSTOMER_INVALID.code,
				message: this.__(ResponseConstant.CUSTOMER_INVALID.message)
			};
		}
		const paymeSubcriptionCond = {
			// paymentRequestId: paymentRequest.id,
			pageId: pageInfo.pageId,
			amount: paymentRequest.amount
		};
		if (customerId) paymeSubcriptionCond['customerDetail.id'] = customerId;
		if (customerEmail) paymeSubcriptionCond['customerDetail.email'] = customerEmail;

		ctx.broker.logger.info(`[PAYME_SUBCRIPTION] find paymeSubcriptionInfo by = ${JSON.stringify(paymeSubcriptionCond)}`);
		let paymeSubcriptionInfo = await this.broker.call('v1.paymeSubcriptionModel.findOne', [paymeSubcriptionCond]);
		ctx.broker.logger.info(`[PAYME_SUBCRIPTION] paymeSubcriptionInfo = ${JSON.stringify(paymeSubcriptionInfo)}`);
		if (paymeSubcriptionInfo) {
			//  update getNextPaymentDate, countPayment+=1
			await this.broker.call('v1.paymeSubcriptionModel.updateOne', [{
				id: paymeSubcriptionInfo.id
			}, {
				$set: {
					// previousPaymentDate: paymeSubcriptionInfo.nextPaymentDate,
					// nextPaymentDate: getNextPaymentDate(paymeSubcriptionInfo.recurringPaymentPeriod, paymeSubcriptionInfo.nextPaymentDate)
				},
				$inc: {
					countPayment: 1
				}
			}]);
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
				message: this.__(ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.message)
			};
		}
		//  else: create subcription
		let subcriptionId = nanoid();
		try {
			subcriptionId = await this.customNanoId({ prefix: 'PAYME_SUBCRIPTION', type: 'string' });
		} catch (e) {
			ctx.broker.logger.warn(`Call this.customNanoId get PAYME_SUBCRIPTION error: ${e}`);
			const { code, message } = ResponseConstant.ACTION_SUBCRIPTION_FAIL;
			return { code, message: this.__('Tạo đơn hàng thất bại') };
		}
		if (!subcriptionId) {
			const { code, message } = ResponseConstant.ACTION_SUBCRIPTION_FAIL;
			return { code, message: this.__('Tạo đơn hàng thất bại.') };
		}
		try {
			const customerInfoObj = {};
			// eslint-disable-next-line no-restricted-syntax
			for (const val of paymentRequest.customerDetails) {
				customerInfoObj[val.name] = val.value;
			}
			paymeSubcriptionInfo = await this.broker.call('v1.paymeSubcriptionModel.create', [{
				paymentRequestId: paymentRequest.id,
				pageId: paymentRequest.referId,
				pageTitle: pageInfo.businessDetail.title,
				merchantId: pageInfo.creator.merchantId,
				accountId: pageInfo.creator.accountId,
				subcriptionId,
				recurringPaymentPeriod: pageInfo.recurringPayment,
				previousPaymentDate: null,
				nextPaymentDate: getNextPaymentDate(pageInfo.recurringPayment, new Date()),
				recurringPaymentType: paymentRequest.recurringPayment,
				amount: paymentRequest.amount,
				paymentItems: paymentRequest.paymentItems,
				customerDetail: customerInfoObj,
				isActive: true,
				countPayment: 1
			}]);
		} catch (error) {
			ctx.broker.logger.info(`paymeSubcriptionModel.create error =====>: ${error.message}`);
			ctx.broker.logger.error(error);
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Tạo đơn hàng định kì thất bại')
			};
		}
		if (paymeSubcriptionInfo) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
				message: this.__('Tạo đơn hàng định kì thành công')
			};
		}
		return dataResponse;
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
