const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const {
			pageId, pageTitle, subcriptionId, isActive, recurringPaymentPeriod
		} = payload.filter;
		const auth = _.get(ctx, 'meta.auth', {});

		const filter = {};
		if (pageId) {
			filter.pageId = pageId;
		}
		if (subcriptionId) {
			filter.subcriptionId = subcriptionId;
		}
		if (pageTitle) {
			filter.$text = { $search: pageTitle };
		}
		if (_.isBoolean(isActive)) {
			filter.isActive = isActive;
		}
		if (recurringPaymentPeriod) {
			filter.recurringPaymentPeriod = recurringPaymentPeriod;
		}
		if (_.get(payload.filter, 'createdAt', false) !== false) {
			const queryByDate = {};
			if (payload.filter.createdAt.from) {
				queryByDate.$gte = new Date(payload.filter.createdAt.from);
			}
			if (payload.filter.createdAt.to) {
				queryByDate.$lte = new Date(payload.filter.createdAt.to);
			}
			if (_.isEmpty(queryByDate) === false) {
				filter.createdAt = queryByDate;
			}
		}
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			filter.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}

		this.broker.logger.info(`paymeSubcription read filter by > ${JSON.stringify(filter)}`);
		let result = [];
		try {
			let total = 0;
			total = await this.broker.call('v1.paymeSubcriptionModel.count', { query: filter });
			if (total > 0) {
				result = await this.broker.call('v1.paymeSubcriptionModel.find', {
					body: {
						query: filter,
						paging: payload.paging,
						sort: payload.sort,
						fields: '-_id pageId merchantId	accountId subcriptionId recurringPaymentPeriod previousPaymentDate nextPaymentDate recurringPaymentType amount paymentItems customerDetail isActive countPayment createdAt'
					},
				});
				const pages = await this.broker.call('v1.paymePageModel.find', {
					body: {
						query: { pageId: { $in: result.map((s) => s.pageId) } },
						fields: '-_id pageId businessDetail'
					},
				});
				result = result.map((s) => {
					const item = { ...s };
					item.pageTitle = _.get(pages.find((p) => p.pageId === s.pageId), 'businessDetail.title', '');
					return item;
				});
			}
			const { code, message } = RespConst.ACTION_SUBCRIPTION_SUCCEESS;
			return {
				code,
				message: this.__(message),
				data: {
					total,
					items: result
				}
			};
		} catch (e) {
			ctx.broker.logger.warn(e);
			const { code, message } = RespConst.ACTION_SUBCRIPTION_FAIL;
			return { code, message };
		}
	} catch (err) {
		ctx.broker.logger.error(`PaymepaymeSubcription Read Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
