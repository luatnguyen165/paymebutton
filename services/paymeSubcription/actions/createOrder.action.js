/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const slugify = require('slugify');
const moment = require('moment');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		payload.language = payload.language || 'vi';
		this.setLocale(payload.language);

		ctx.broker.logger.warn(`SUBCRIPTION create order payload...:  ${JSON.stringify(payload)}`);
		const { subcriptionId, customerInfo = {} } = payload;

		const subcriptionInfo = await ctx.broker.call('v1.paymeSubcriptionModel.findOne', [{
			subcriptionId,
			isActive: true
		}]);
		if (!subcriptionInfo) {
			const { code, message } = RespConst.ACTION_SUBCRIPTION_FAIL;
			return { code, message: this.__('Không tìm thấy thông tin thanh toán định kì') };
		}
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: subcriptionInfo.pageId },
				{ 'shortLinkInfo.slug': subcriptionInfo.pageId },
			],
			status: 'ACTIVE'
		}]);
		if (!pageInfo) {
			const { code, message } = RespConst.PAGE_NOT_FOUND;
			return { code, message: this.__(message) };
		}
		let merchantInfo = null;
		try {
			merchantInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: pageInfo.creator.merchantId });
		} catch (e) {
			const { code, message } = RespConst.CREATE_ORDER.MERCHANT_NOT_FOUND;
			return { code, message: this.__(message) };
		}
		if (merchantInfo.code !== GeneralConstants.GET_MC_INFO_SUCCESS_CODE) {
			ctx.broker.logger.warn(`Create order get MC INfo-- merchantId: ${pageInfo.creator.merchantId}, ${JSON.stringify(merchantInfo)}`);
			const { code, message } = RespConst.CREATE_ORDER.MERCHANT_NOT_FOUND;
			return { code, message: this.__(message) };
		}
		merchantInfo = merchantInfo.data;

		let paymentMethod = null;
		if (_.get(payload, 'paymentMethod.methodId', null) !== null) {
			paymentMethod = merchantInfo.paymentMethod.find(
				(method) => method.methodId === payload.paymentMethod.methodId
				// && method.payCode === payload.paymentMethod.payCode
			);
			// ctx.broker.logger.warn(`Get payment method Info ${ JSON.stringify(paymentMethod) }`);

			if (!paymentMethod || paymentMethod.isActive === false) {
				const { code, message } = RespConst.CREATE_ORDER.INVALID_PAYMENT_METHOD;
				return { code, message: this.__(message) };
			}
		}

		if (pageInfo.voucherAllowance === false && _.trim(payload.voucherCode)) {
			const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_ALLOW;
			return { code, message: this.__(message) };
		}
		let voucherInfo;
		if (_.trim(payload.voucherCode)) {
			if (_.isArray(pageInfo.vouchers) && pageInfo.vouchers.length > 0 && !_.includes(pageInfo.vouchers, _.trim(payload.voucherCode))) {
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_INVALID;
				return { code, message: this.__(message) };
			}
			try {
				voucherInfo = await this.broker.call('v1.voucherModel.findOne', [{
					voucherCode: _.trim(payload.voucherCode),
					merchantId: pageInfo.creator.merchantId,
					isActive: true,
					quantityRemaining: { $gt: 0 },
					'expiredIn.to': {
						$gt: new Date()
					},
				}]);
			} catch (error) {
				ctx.broker.logger.info(`[SUBCRIPTION] CreateOrder vouchersInfo ==>: ${JSON.stringify({ voucherInfo, voucherCode: payload.voucherCode })}`);
				ctx.broker.logger.error(error);
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_FOUND;
				return { code, message: this.__(message) };
			}
			if (!voucherInfo) {
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_FOUND;
				return { code, message: this.__('Voucher {{voucherNotFound}} không hợp lệ hoặc đã hết lượt sử dụng', { voucherNotFound: payload.voucherCode }) };
			}
		}
		const totalAmount = subcriptionInfo.amount;
		let totalDiscount = 0;
		let totalPayment = subcriptionInfo.amount;
		// eslint-disable-next-line no-plusplus
		if (voucherInfo) {
			if (voucherInfo.minAmount !== 0 && totalAmount < voucherInfo.minAmount) { //  tối thiếu
				const { code, message } = RespConst.CREATE_ORDER.ORDER_NOT_ENOUGH_AMOUNT;
				return { code, message: this.__(`Đơn hàng ${message.toLowerCase()} để sử dụng voucher ${voucherInfo.voucherCode}`) };
			}
			// tính toán số tiền giảm
			if (voucherInfo.unit === 'PERCENT') {
				// giảm theo %
				totalDiscount = Math.round((totalAmount * voucherInfo.amount) / 100);
				if (voucherInfo.maxDiscount !== 0 && totalDiscount > voucherInfo.maxDiscount) {
					totalDiscount = voucherInfo.maxDiscount; // giảm tối đa
				}
			} else {
				// giảm theo số tiền cụ thể
				totalDiscount = voucherInfo.amount;
			}
			if (totalDiscount > totalAmount) {
				totalDiscount = totalAmount;
			}
			// cập nhật số lượng remaining
			try {
				const voucherUpdated = await this.broker.call('v1.voucherModel.updateOne', [
					{
						id: voucherInfo.id
					},
					{
						$inc: {
							quantityRemaining: -1
						}
					}
				]);
				if (_.get(voucherUpdated, 'nModified', 0) === 0) {
					ctx.broker.logger.warn(`Create order update quantityRemaining FAILED!!!! voucherCode=${voucherInfo.voucherCode}`);
					const { code, message } = RespConst.CREATE_ORDER.FAIL;
					return { code, message: this.__(message) };
				}
			} catch (error) {
				ctx.broker.logger.warn(`Create order update quantityRemaining error!!: ${error}, voucherCode: ${voucherInfo.voucherCode}`);
				const { code, message } = RespConst.CREATE_ORDER.FAIL;
				return { code, message: this.__(message) };
			}
		}
		totalPayment = totalAmount - totalDiscount;
		if (totalPayment < 10000) {
			const { code, message } = RespConst.CREATE_ORDER.INVALID_AMOUNT;
			return { code, message: this.__(message, { amount: '10.000' }) };
		}
		// chưa tạo đơn hàng lần nào => tạo mới
		let transactionId = null;
		try {
			transactionId = await this.customNanoId({ prefix: 'PAYMENT_REQUEST_ID', type: 'string' });
		} catch (error) {
			const { code, message } = RespConst.CREATE_ORDER.GET_UUID_FAILED;
			return { code, message: this.__(message) };
		}
		if (!transactionId) {
			const { code, message } = RespConst.CREATE_ORDER.GET_UUID_FAILED;
			return { code, message: this.__(message) };
		}

		const requestPaymentData = {
			note: payload.note,
			transactionId,
			referType: PaymentRequestConst.REFER_TYPE.PAYME_SUBCRIPTION,
			referId: subcriptionInfo.subcriptionId,
			amount: totalAmount,
			discountAmount: totalDiscount,
			paymentAmount: totalPayment,
			customerDetails: customerInfo,
			paymentItems: subcriptionInfo.paymentItems,
			status: PaymentRequestConst.STATUS.FAILED,
			expiryAt: moment(new Date()).add(15, 'minutes').toISOString(),
			paymentMethod: paymentMethod || {},
			voucherInfo: {
				id: _.get(voucherInfo, 'id', null),
				code: _.get(voucherInfo, 'voucherCode', null)
			},
			transactionInfo: payload
		};

		const paymentRequest = await this.broker.call('v1.paymentRequestModel.create', [requestPaymentData]);
		if (_.get(paymentRequest, 'id', null) === null) {
			const { code } = RespConst.ACTION_SUBCRIPTION_FAIL;
			return { code, message: this.__('Tạo đơn hàng thất bại') };
		}
		const ipnUrl = `${process.env.HOST}/widgets/page/external/receive_callback`;
		const orderInfo = {
			title: subcriptionInfo.pageTitle,
			paymentItems: subcriptionInfo.paymentItems
		};
		const customerInfoObj = {};
		for (const val of customerInfo) {
			customerInfoObj[val.name] = val.value;
		}
		const orderData = {
			referId: subcriptionInfo.subcriptionId,
			referType: PaymentRequestConst.REFER_TYPE.PAYME_SUBCRIPTION,
			partnerTransaction: paymentRequest.transactionId,
			accountId: subcriptionInfo.accountId,
			amount: totalPayment,
			description: payload.note,
			referData: JSON.stringify({
				orderInfo,
				customerInfo: customerInfoObj,
				voucherInfo: {
					id: _.get(voucherInfo, 'id', null),
					code: _.get(voucherInfo, 'voucherCode', null)
				},
				note: payload.note
			}),
			ipnUrl,
			redirectUrl: `${payload.redirectUrl}&transid=${transactionId}`, // on success
			failedUrl: `${payload.failedUrl}&transid=${transactionId}`, // on fail
			payMethod: paymentMethod ? paymentMethod.payCode : undefined,
			lang: payload.language,
			note: payload.note,
		};
		let orderId = '';
		let paymentUrl = '';
		try {
			const orderCreated = await this.broker.call('v1.order.createWeb', orderData);
			ctx.broker.logger.info(`[SUBCRIPTION]Service 'v1.order.createWeb' result: ${JSON.stringify({ orderData, orderCreated })}`);
			if (orderCreated.code !== GeneralConstants.CREATE_ORDER_SUCCESS_CODE) {
				const { code } = RespConst.ACTION_SUBCRIPTION_FAIL;
				return { code, message: this.__('Tạo đơn hàng thất bại.') };
			}
			orderId = orderCreated.data.orderId;
			paymentUrl = orderCreated.data.url;
		} catch (e) {
			ctx.broker.logger.warn(`Service 'v1.order.createWeb' error: ${JSON.stringify({ message: e.message, input: orderData })}`);
			const { code, message } = RespConst.ACTION_SUBCRIPTION_FAIL;
			return { code, message: this.__(message) };
		}

		await this.broker.call('v1.paymentRequestModel.updateOne', [
			{
				id: paymentRequest.id
			},
			{
				orderId,
				paymentUrl,
				status: PaymentRequestConst.STATUS.PENDING
			}]);
		const { code, message } = RespConst.ACTION_SUBCRIPTION_SUCCEESS;
		return {
			code,
			message: this.__(message),
			data: {
				orderId,
				transaction: paymentRequest.transactionId,
				url: paymentUrl
			}
		};
	} catch (err) {
		ctx.broker.logger.error(`[SUBCRIPTION] Create Order Error: ${err}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
