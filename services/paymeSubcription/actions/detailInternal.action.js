const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');

module.exports = async function (ctx) {
	const { subcriptionId } = ctx.params.params;
	try {
		const auth = _.get(ctx, 'meta.auth', {});
		const filter = {
			subcriptionId
		};
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			filter.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}
		const subcription = await this.broker.call('v1.paymeSubcriptionModel.findOne', [filter]);
		if (!subcription) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin thanh toán định kì')
			};
		}
		// find payment history
		const paymentsHistory = await this.broker.call('v1.paymentRequestModel.find', {
			body: {
				query: {
					referType: 'PAYME_SUBCRIPTION',
					referId: subcription.subcriptionId
				},
				fields: '-_id finishedAt note paymentId transactionId amount discountAmount paymentAmount customerDetail paymentItems status paymentMethod voucherInfo',
				sort: { id: -1 },
			}
		});
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{ pageId: subcription.pageId }]);

		return {
			code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
			message: this.__(ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.message),
			data: {
				paymentLink: `${process.env.PAGE_URL_SHORT}/${subcription.pageId}`,
				...subcription,
				pageTitle: pageInfo ? pageInfo.businessDetail.title : '',
				paymentsHistory
			}
		};
	} catch (err) {
		console.log(err);
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
