const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');
const CryptoHelper = require('../helpers/crypto-js.helper');

module.exports = async function (ctx) {
	const payload = ctx.params.body;
	const auth = _.get(ctx, 'meta.auth', {});
	const filter = {
		subcriptionId: payload.subcriptionId,
		isActive: true
	};
	if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
		filter.merchantId = auth.credentials.merchantId; // lấy all theo MC id
	} else {
		filter.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
	}
	ctx.broker.logger.info(`[REMIND PAYMENT] filter by: ${JSON.stringify(filter)}`);

	try {
		const subcriptionInfo = await this.broker.call('v1.paymeSubcriptionModel.findOne', [filter]);
		if (!subcriptionInfo) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin thanh toán định kì')
			};
		}
		// if (subcriptionInfo.isSubscribe === false) {
		// 	return {
		// 		code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
		// 		message: this.__('Khách hành đã hủy nhắc thanh toán định kì')
		// 	};
		// }
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await ctx.broker.call('v1.settingsDashboard.internalSettings', { merchantId: subcriptionInfo.merchantId });
			ctx.broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND auto send email to KH -- merchantId:  ${subcriptionInfo.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
		} catch (e) {
			ctx.broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND auto send email to KH -- merchantId:  ${subcriptionInfo.merchantId}, Error: ${e}`);
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin  merchant')
			};
		}
		if (merchantFullInfo.code !== 114100) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin  merchant.')
			};
		}
		merchantFullInfo = merchantFullInfo.data;
		let chanel = 'INDIVIDUAL';
		let target = [subcriptionInfo.customerDetail.id];
		if (subcriptionInfo.customerDetail.email) {
			chanel = 'CUSTOM';
			target = [{ email: subcriptionInfo.customerDetail.email }];
		}
		const paymentLink = `${process.env.PAGE_URL_SHORT}/${subcriptionInfo.pageId}?subscriptionId=${subcriptionInfo.subcriptionId}`;
		const unsubscribeMailData = {
			subcriptionId: subcriptionInfo.subcriptionId,
			customerEmail: subcriptionInfo.customerDetail.email,
			customerId: subcriptionInfo.customerDetail.id,
			issuedAt: new Date()
		};
		const unsubscribeToken = CryptoHelper.string2hex(JSON.stringify(unsubscribeMailData), GeneralConstant.UNSUBCRIBE_MAIL_SECRET_KEY);
		const mailData = {
			meta: {
				merchantId: subcriptionInfo.merchantId,
				accountId: subcriptionInfo.accountId,
				chanel,
				target,
				method: ['EMAIL'],
				service: 'PAYME_SUBCRIPTION',
				extraData: { referId: subcriptionInfo.subcriptionId },
				template: {
					email: 'page-schedule'
				},
				content: {
					mail: {
						subject: `Thanh toán đơn hàng tại ${merchantFullInfo.brandName}`,
						content: {
							merchantLogo: _.get(merchantFullInfo, 'logo', ''),
							merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
							orderUrl: paymentLink,
							qrImage: `https://t.payme.vn/qr?t=${paymentLink}`,
							redirectUrl: `${process.env.UNSUBSCRIBE_EMAIL_URL}?token=${unsubscribeToken}`
						}
					}
				}
			}
		};
		try {
			const sendEmailRes = await ctx.broker.call('v1.customer.sendEmailAndSMS', {}, mailData);
			ctx.broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND send email to KH res: mailData = ${JSON.stringify(mailData)}, sendEmailRes = ${JSON.stringify(sendEmailRes)}`);
		} catch (err) {
			ctx.broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND send email to KH error: mailData = ${JSON.stringify(mailData)}, err = ${err}`);
			return false;
		}
		return {
			code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
			message: this.__('Nhắc thanh toán thành công')
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
