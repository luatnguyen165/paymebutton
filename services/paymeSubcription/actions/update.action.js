const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const GeneralConstant = require('../constants/general.constant');
const ResponseConstant = require('../constants/response.constant');
const { getNextPaymentDate } = require('../helpers/date.helper');

// cron update next payment date
module.exports = async function (ctx) {
	const { subcriptionId } = ctx.params.params;
	const payload = ctx.params.body;
	try {
		let filter = {
			subcriptionId
		};
		const auth = _.get(ctx, 'meta.auth', {});
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter.merchantId = auth.credentials.merchantId; // lấy all theo MC id
		} else {
			filter.accountId = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}
		filter = _.omitBy(filter, _.isNil);
		ctx.broker.logger.info(`[SUBCRIPTION] Update filter by: ${JSON.stringify(filter)}`);

		const subcription = await this.broker.call('v1.paymeSubcriptionModel.findOne', [filter]);
		ctx.broker.logger.info(`[SUBCRIPTION] Update subcription: ${JSON.stringify(subcription)}`);

		if (!subcription) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Không tìm thấy thông tin đơn hàng')
			};
		}
		const updated = await ctx.broker.call('v1.paymeSubcriptionModel.updateOne', [
			{
				subcriptionId: subcription.subcriptionId
			}, {
				$set: {
					isActive: _.isBoolean(payload.isActive) ? payload.isActive : subcription.isActive
				}
			}]);
		if (!updated || updated.nModified < 1) {
			return {
				code: ResponseConstant.ACTION_SUBCRIPTION_FAIL.code,
				message: this.__('Cập nhật đơn hàng thất bại')
			};
		}
		return {
			code: ResponseConstant.ACTION_SUBCRIPTION_SUCCEESS.code,
			message: this.__('Cập nhật thành công')
		};
	} catch (err) {
		if (err.name === 'MoleculerError') throw err;
		throw new MoleculerError(`PayME Subcription withdraw Error: ${err.message}`, 99);
	}
};
