const CryptoJS = require('crypto-js');

module.exports = {
	string2hex: (plaintText, prvKey) => {
		const key = CryptoJS.enc.Utf8.parse(prvKey);
		const encryptedData = CryptoJS.AES.encrypt(plaintText, key, {
			mode: CryptoJS.mode.ECB,
			padding: CryptoJS.pad.Pkcs7
		});
		const encryptedDataHexStr = encryptedData.toString(CryptoJS.format.Hex);
		return encryptedDataHexStr;
	},

	hex2string: (hexString, prvKey) => {
		const key = CryptoJS.enc.Utf8.parse(prvKey);
		const encryptedHex = CryptoJS.enc.Hex.parse(hexString);
		const encryptedBase64 = CryptoJS.enc.Base64.stringify(encryptedHex);
		const decryptedData = CryptoJS.AES.decrypt(encryptedBase64, key, {
			mode: CryptoJS.mode.ECB,
			padding: CryptoJS.pad.Pkcs7
		});
		const decryptText = decryptedData.toString(CryptoJS.enc.Utf8);
		return decryptText;
	}
};
