const moment = require('moment');

module.exports = {

	/**
	*	@param String period
		*	enum: [
		*	DAILY: 'DAILY',
		*	WEEKLY: 'WEEKLY',
		*	MONTHLY: 'MONTHLY',
		*	THREE_MONTHS: 'THREE_MONTHS',
		*	SIX_MONTHS: 'SIX_MONTHS',
		*	YEARLY: 'YEARLY'
		*]
	* @param Date fromDate
 */
	getNextPaymentDate: (period, fromDate = new Date()) => {
		let nextPaymentDate = null;
		switch (period) {
			case 'DAILY':
				nextPaymentDate = new Date(moment(fromDate).add(1, 'd').format('YYYY-MM-DD'));
				break;
			case 'WEEKLY':
				nextPaymentDate = new Date(moment(fromDate).add(1, 'w').format('YYYY-MM-DD'));
				break;
			case 'MONTHLY':
				nextPaymentDate = new Date(moment(fromDate).add(1, 'M').format('YYYY-MM-DD'));
				break;
			case 'THREE_MONTHS':
				nextPaymentDate = new Date(moment(fromDate).add(3, 'M').format('YYYY-MM-DD'));
				break;
			case 'SIX_MONTHS':
				nextPaymentDate = new Date(moment(fromDate).add(6, 'M').format('YYYY-MM-DD'));
				break;
			case 'YEARLY':
				nextPaymentDate = new Date(moment(fromDate).add(1, 'y').format('YYYY-MM-DD'));
				break;
			default:
				nextPaymentDate = new Date(moment().format('YYYY-MM-DD'));
				break;
		}
		return nextPaymentDate;
	}
};
