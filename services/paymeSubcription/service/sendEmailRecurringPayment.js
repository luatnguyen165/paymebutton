const Async = require('async');
const _ = require('lodash');
const { getNextPaymentDate } = require('../helpers/date.helper');
const CryptoHelper = require('../helpers/crypto-js.helper');
const GeneralConstant = require('../constants/general.constant');

const POOL_SEND_EMAIL = Async.queue(async (task, callback) => {
	const { broker } = task;
	const subcriptionInfo = _.clone(task);
	delete subcriptionInfo.broker;
	broker.logger.info(`SUBCRIPTION POOL_SEND_EMAIL TASK subcriptionInfo = ${JSON.stringify(subcriptionInfo)}`);

	const paymentLink = `${process.env.PAGE_URL_SHORT}/${subcriptionInfo.pageId}?subscriptionId=${subcriptionInfo.subcriptionId}`;
	const customerInfo = subcriptionInfo.customerDetail;
	if (customerInfo.email || customerInfo.id) {
		let chanel = 'INDIVIDUAL';
		let target = [customerInfo.id];
		if (customerInfo.email) {
			chanel = 'CUSTOM';
			target = [{ email: customerInfo.email }];
		}
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await broker.call('v1.settingsDashboard.internalSettings', { merchantId: subcriptionInfo.merchantId });
			broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND auto send email to KH -- merchantId:  ${subcriptionInfo.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
		} catch (e) {
			broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND auto send email to KH -- merchantId:  ${subcriptionInfo.merchantId}, Error: ${e}`);
			callback();
			return false;
		}
		if (merchantFullInfo.code !== 114100) {
			callback();
			return false;
		}
		merchantFullInfo = merchantFullInfo.data;
		const unsubscribeMailData = {
			subcriptionId: subcriptionInfo.subcriptionId,
			customerEmail: subcriptionInfo.customerDetail.email,
			customerId: subcriptionInfo.customerDetail.id
		};
		const unsubscribeToken = CryptoHelper.string2hex(JSON.stringify(unsubscribeMailData), GeneralConstant.UNSUBCRIBE_MAIL_SECRET_KEY);
		const mailData = {
			meta: {
				merchantId: subcriptionInfo.merchantId,
				accountId: subcriptionInfo.accountId,
				chanel,
				target,
				method: ['EMAIL'],
				service: 'PAYME_SUBCRIPTION',
				extraData: { referId: subcriptionInfo.subcriptionId },
				template: {
					email: 'page-schedule'
				},
				content: {
					mail: {
						subject: `Thanh toán đơn hàng tại ${merchantFullInfo.brandName}`,
						content: {
							merchantLogo: _.get(merchantFullInfo, 'logo', ''),
							merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
							orderUrl: paymentLink,
							qrImage: `https://t.payme.vn/qr?t=${paymentLink}`,
							redirectUrl: `${process.env.UNSUBSCRIBE_EMAIL_URL}?token=${unsubscribeToken}`
						}
					}
				}
			}
		};
		try {
			const sendEmailRes = await broker.call('v1.customer.sendEmailAndSMS', {}, mailData);
			broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND send email to KH res: mailData = ${JSON.stringify(mailData)}, sendEmailRes = ${JSON.stringify(sendEmailRes)}`);
		} catch (err) {
			broker.logger.warn(`SUBCRIPTION SEND EMAIL REMIND send email to KH error: mailData = ${JSON.stringify(mailData)}, err = ${err}`);
			callback();
			return false;
		}
		// sau khi gửi mail sẽ update nextPaymentDate
		broker.logger.info('SUBCRIPTION SEND EMAIL REMIND ==============update nextPaymentDate ================ ');

		await broker.call('v1.paymeSubcriptionModel.updateOne', [{
			id: subcriptionInfo.id
		}, {
			previousPaymentDate: subcriptionInfo.nextPaymentDate,
			nextPaymentDate: getNextPaymentDate(subcriptionInfo.recurringPaymentPeriod, new Date())
		}]);
	}
	callback();
	return true;
}, 10);
module.exports = {
	init(data) {
		POOL_SEND_EMAIL.push(data);
	}
};
