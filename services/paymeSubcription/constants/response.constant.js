module.exports = {
	ACTION_SUBCRIPTION_SUCCEESS: {
		code: 152100,
		message: 'Thành công'
	},
	ACTION_SUBCRIPTION_FAIL: {
		code: 152101,
		message: 'Thất bại'
	},
	CUSTOMER_INVALID: {
		code: 152102,
		message: 'Khách hàng không hợp lệ (thiếu thông tin mã khách hàng hoặc email)'
	},
	PAGE_NOT_FOUND: {
		code: 152103,
		message: 'Không tìm thấy thông tin page'
	}
};
