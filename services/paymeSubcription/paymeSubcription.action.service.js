/* eslint-disable object-shorthand */
/* eslint-disable no-throw-literal */
const _ = require('lodash');
const path = require('path');
const Cron = require('moleculer-cron');
const { I18n } = require('i18n');
const { MoleculerError } = require('moleculer').Errors;
const PaymeSubcriptionSchema = require('./schema/paymeSubcription.schema');
const GeneralConstants = require('./constants/general.constant');

const DIRNAME = __dirname;

module.exports = {
	name: 'paymeSubcription',
	mixins: [Cron],
	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	hooks: {
		before: {
			create: ['checkUserScope'],
			detail: ['checkUserScope'],
			read: ['checkUserScope'],
			updatePage: ['checkUserScope'],
			updateInputField: ['checkUserScope'],
			notify: ['checkUserScope'],
			// Assign it to a specified action or all actions (*) in service.
			'*': [
				function setLanguage(ctx) {
					const language = _.get(ctx.params, 'body.language', '') || _.get(ctx.params, 'query.language', '') || GeneralConstants.LANGUAGE.VI;
					ctx.service.setLocale(language);
				}

			]
		},
		error: {
			'*': function (ctx, err) {
				return _.pick(err, ['message', 'code', 'data']);
			}
		}
	},

	dependencies: ['v1.paymePageModel', 'v1.paymeSubcriptionModel', 'v1.paymentRequestModel'],
	crons: [
		{
			name: 'Subcription.updatePaymentDate',
			cronTime: '2 8 * * *', // At 08:02 every day
			onTick: async function () {
				await this.call('v1.paymeSubcription.updatePaymentDate');
			},
			runOnInit: () => {
				console.log('paymeSubcription.updatePaymentDate job is created');
			},
			timeZone: 'Asia/Ho_Chi_Minh'
		},
		{
			name: 'Subcription.sendNotifyRecuringPayment',
			cronTime: '0 8 * * *', // At 08:00 every day
			onTick: async function () {
				await this.call('v1.paymeSubcription.sendNotifyRecuringPayment');
			},
			runOnInit: () => {
				console.log('paymeSubcription.sendNotifyRecuringPayment job is created');
			},
			timeZone: 'Asia/Ho_Chi_Minh'
		},
	],
	/**
	 * Actions
	 */
	actions: {
		updatePaymentDate: {
			handler: require('./actions/updatePaymentDate.action')
			// call 'v1.paymeSubcription.updatePaymentDate'
		},
		sendNotifyRecuringPayment: {
			handler: require('./actions/sendNotifyRecuringPayment.action')
			// call 'v1.paymeSubcription.sendNotifyRecuringPayment'
		},
		generateSubcription: {
			params: {
				language: 'string|optional|default:vi',
				pageId: 'string|required',
				paymentRequestId: 'number|required',
				// customer: {
				// 	$$type: 'object',
				// 	id: 'string|optional',
				// 	email: 'string|optional',
				// }
			},
			handler: require('./actions/generateSubcription.action')
			// call 'v1.paymeSubcription.generateSubcription' '{"pageId":"v4c3lf","paymentRequestId":"2250"}'
		},
		read: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/subcription/read',
				auth: {
					strategies: ['Default'],
					mode: 'optional' // 'required', // 'required', 'optional', 'try'
				},
			},
			params: PaymeSubcriptionSchema.read,
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/read.action'),
		},
		remindPayment: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/subcription/remind_payment',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: PaymeSubcriptionSchema.remindPayment,
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/remindPayment.action'),
		},
		update: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/subcription/update/:subcriptionId',
				auth: {
					strategies: ['Default'],
					mode: 'optional' // 'required', // 'required', 'optional', 'try'
				},
			},
			params: PaymeSubcriptionSchema.update,
			scope: {
				name: ['mc.paymePage.update'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/update.action'),
		},
		getDetailExternal: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/subcription/external/:subcriptionId',
				auth: false,
			},
			params: PaymeSubcriptionSchema.getDetailExternal,
			handler: require('./actions/detailExternal.action'),
		},
		getDetailInternal: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/subcription/:subcriptionId',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			params: PaymeSubcriptionSchema.detailInternal,
			handler: require('./actions/detailInternal.action'),
		},
		createOrder: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/subcription/external/create_order',
				auth: false,
			},
			params: PaymeSubcriptionSchema.createOrder,
			handler: require('./actions/createOrder.action'),
		},
		verifyUnsubscribeMail: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/subcription/unsubscribe/:token',
				auth: false
			},
			params: PaymeSubcriptionSchema.unsubscribe,
			handler: require('./actions/verifyUnsubscribeMail.action'),
		},
	},
	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		// eslint-disable-next-line consistent-return
		async checkUserScope(ctx) {
			this.broker.logger.info(`PaymeSubcription: role check response: ${JSON.stringify(ctx.action.scope)}, ${ctx.action.scope.name && ctx.action.scope.name.length > 0}`);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				this.broker.logger.info(`PaymeSubcription: auth info: ${JSON.stringify(auth)}`);
				if (_.get(auth, 'credentials.accountId', null) === null) {
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!'
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: _.toNumber(auth.credentials.accountId) });
				} catch (error) {
					this.broker.logger.info(`PaymeSubcription: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(auth.credentials)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`
					};
				}
				const accountInfo = {
					accountId: _.toNumber(auth.credentials.accountId),
					merchantId: auth.credentials.merchantId || merchantInfo.id
				};
				let mcRole = '';
				if (ctx.action.scope.name.includes('mc.paymePage.read')) {
					mcRole = 'mc.paymePage.read';
				} else if (ctx.action.scope.name.includes('mc.paymePage.create')) {
					mcRole = 'mc.paymePage.create';
				} else if (ctx.action.scope.name.includes('mc.paymePage.update')) {
					mcRole = 'mc.paymePage.update';
				} else if (ctx.action.scope.name.includes('mc.paymePage.notify')) {
					mcRole = 'mc.paymePage.notify';
				}
				try {
					const resultCheckRole = await this.broker.call('v1.role.get', { target: accountInfo });
					this.broker.logger.info(`PaymeSubcription: call v1.role.read response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager', mcRole]).length <= 0) {
						throw {
							code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden'
						};
					}
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager']).length > 0) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
							merchantInfo
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true, // creator
							merchantInfo
						};
					}
				} catch (error) {
					this.broker.logger.warn(`PaymeSubcription: call v1.role.get response: ${error}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}, error: ${JSON.stringify(error)}`);
					throw {
						code: GeneralConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`
					};
				}
			}
		},
		customNanoId: require('./methods/customNanoId.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(DIRNAME, '/locales'),
			defaultLocale: 'vi',
			register: this
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		//
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
