/* eslint-disable object-shorthand */
/* eslint-disable no-throw-literal */
const _ = require('lodash');
const path = require('path');
const Cron = require('moleculer-cron');
const { I18n } = require('i18n');
const { MoleculerError } = require('moleculer').Errors;
const paymePageSchema = require('./schema/paymePage.schema');
const GeneralConstants = require('./constants/general.constant');
const ResponseConstants = require('./constants/response.constant');

const DIRNAME = __dirname;

module.exports = {
	name: 'paymePage',
	mixins: [Cron],
	version: 1,
	/**
	 * Settings
	 */
	settings: {

	},

	hooks: {
		before: {
			create: ['checkUserScope'],
			detail: ['checkUserScope'],
			read: ['checkUserScope'],
			updatePage: ['checkUserScope'],
			updateInputField: ['checkUserScope'],
			notify: ['checkUserScope'],
			// Assign it to a specified action or all actions (*) in service.
			'*': [
				// async function isValidScope(ctx) {
				// 	const mode = _.get(ctx, 'action.rest.auth.mode', '');
				// 	const scope = _.get(ctx, 'action.scope', null);
				// 	if ((mode === 'required' || mode === 'optional') && scope !== null) {
				// 		const accountInfo = {
				// 			accountId: _.get(ctx, 'meta.auth.credentials.accountId', ''),
				// 			merchantId: _.get(ctx, 'meta.auth.credentials.merchantId', ''),
				// 			// storeId: '' // TODO
				// 		};
				// 		const scopeInfo = {
				// 			name: scope.name || [],
				// 			condition: scope.condition || GeneralConstants.SCOPE.CONDITION.AND
				// 		};
				// 		const checkScopeResult = await this.checkScope(accountInfo, scopeInfo);
				// 		if (checkScopeResult.code !== GeneralConstants.SCOPE.CODE.CHECK_ROLE_SUCCESS) {
				// 			const errMsg = checkScopeResult.message || 'Tài khoản không được phép thực hiện thao tác này';
				// 			throw new MoleculerError(errMsg, checkScopeResult.code);
				// 		}
				// 	}
				// },

				function setLanguage(ctx) {
					const language = _.get(ctx.params, 'body.language', '') || _.get(ctx.params, 'query.language', '') || GeneralConstants.LANGUAGE.VI;
					ctx.service.setLocale(language);
				}

			]
		},
		error: {
			'*': function (ctx, err) {
				return _.pick(err, ['message', 'code', 'data']);
			}
		}
	},

	dependencies: ['v1.paymePageModel', 'v1.voucherModel', 'v1.productTagModel'],
	crons: [
		{
			name: 'Page.UpdatePaymentExpired',
			cronTime: '0/1 * * * *', // every minutes
			onTick: async function () {
				await this.call('v1.paymePage.updatePaymentExpired');
			},
			runOnInit: () => {
				console.log('Page.UpdatePaymentExpired job is created');
			},
			timeZone: 'Asia/Ho_Chi_Minh'
		},
		{
			name: 'Page.InquiredPaymentRequest',
			cronTime: '0/5 * * * *', // every 5 minutes
			onTick: async function () {
				await this.call('v1.paymePage.inquiredPaymentRequest');
			},
			runOnInit: () => {
				console.log('Page.InquiredPaymentRequest job is created');
			},
			timeZone: 'Asia/Ho_Chi_Minh'
		}
	],
	/**
	 * Actions
	 */
	actions: {
		updatePaymentExpired: {
			handler: require('./actions/updatePaymentExpired.action'),
			timeout: 30000
		},
		inquiredPaymentRequest: {
			timeout: 30000,
			handler: require('./actions/inquiryPaymentRequest')
		},
		create: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.create,
			scope: {
				name: ['mc.paymePage.create'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/create.action'),
		},
		read: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page/read',
				auth: {
					strategies: ['Default'],
					mode: 'optional' // 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.read,
			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/read.action'),
		},
		detail: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/page/:pageId',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.detail,

			scope: {
				name: ['mc.paymePage.read'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/readDetail.action').readInternal,
		},
		detailExternal: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page/external/detail',
				auth: {
					strategies: ['Default'],
					mode: 'try', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.detailExternal,
			handler: require('./actions/readDetail.action').readExternal,
		},
		updatePage: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/page/update/:pageId',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.updatePage,
			scope: {
				name: ['mc.paymePage.update'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/update.action').updatePage,
		},
		updateInputField: {
			rest: {
				method: 'PUT',
				fullPath: '/widgets/page/update_fields',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.updateInputField,
			scope: {
				name: ['mc.paymePage.update'],
				condition: GeneralConstants.SCOPE.CONDITION.AND
			},
			handler: require('./actions/update.action').updateInputField,
		},
		createOrder: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page/external/create_order',
				auth: {
					strategies: ['Default'],
					mode: 'try', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.createOrder,
			handler: require('./actions/createOrder.action'),
		},
		receiveCallback: {
			rest: {
				method: 'POST',
				security: false,
				fullPath: '/widgets/page/external/receive_callback',
				auth: {
					strategies: ['Default'],
					mode: 'try', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.receiveCallback,
			handler: require('./actions/receiveCallback.action'),
		},
		sendEmailAndSMS: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page/send_email_sms',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: paymePageSchema.sendEmailAndSMS,
			scope: {
				name: ['mc.paymePage.sendEmailAndSMS'],
				condition: 'AND'
			},
			handler: require('./actions/sendEmailAndSMS.action'),
		},
		receiveCallbackSendEmailAndSMS: {
			params: {
				pageId: 'string|trim|required',
				sms: {
					$$type: 'object|optional',
					totalSuccess: 'number|optional',
					totalFail: 'number|optional',
				},
				email: {
					$$type: 'object|optional',
					totalSuccess: 'number|optional',
					totalFail: 'number|optional',
				}
			},
			handler: require('./actions/receiveCallbackSendEmailAndSMS.action'),
		},
		paymentInformation: {
			rest: {
				method: 'GET',
				fullPath: '/widgets/page/payment/:transactionId',
				auth: false
			},
			params: {
				params: {
					$$type: 'object',
					transactionId: 'string|required|trim'
				},
				query: {
					$$type: 'object',
					language: {
						type: 'enum',
						optional: true,
						values: _.values(GeneralConstants.LANGUAGE),
						default: GeneralConstants.LANGUAGE.VI
					},
				}
			},
			handler: require('./actions/getPaymentInformation.action'),
		},
		updateAlias: {
			rest: {
				method: 'PUT',
				fullPath: 'page/alias/update',
				auth: {
					strategies: ['Default'],
					mode: 'required', // 'required', 'optional', 'try'
				},
			},
			params: {
				body: {
					$$type: 'object',
					pageId: 'string|required|trim',
					newPageId: 'string|required|trim'
				}
			},
			handler: require('./actions/updateAlias.action')
		},
		tuitionPayLink: {
			rest: {
				method: 'POST',
				fullPath: '/widgets/page/external/tuition_pay',
				auth: {
					strategies: ['Default'],
					mode: 'try'
				},
			},
			params: {
				body: {
					$$type: 'object',
					data: 'string|required'
				}
			},
			timeout: 180000, // 3 mins
			handler: require('./actions/tuitionPay.action')
		}
	},
	// call 'v1.paymePage.receiveCallbackSendEmailAndSMS' '{"pageId":"832188","sms":{"totalSuccess":10,"totalFail":2},"email":{"totalSuccess":20,"totalFail":0}}'
	// call 'v1.paymePage.receiveCallbackSendEmailAndSMS' '{"pageId":"832188","email":{"totalSuccess":20,"totalFail":0}}'

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {
		// eslint-disable-next-line consistent-return
		async checkUserScope(ctx) {
			this.broker.logger.info(`PaymePage: role check response: ${JSON.stringify(ctx.action.scope)}, ${ctx.action.scope.name && ctx.action.scope.name.length > 0}`);
			if (_.get(ctx.action, 'scope.name', null) !== null) {
				const auth = _.get(ctx, 'meta.auth', {});
				if (_.get(auth, 'credentials.accountId', null) === null) {
					this.broker.logger.info(`PaymePage: auth info: ${JSON.stringify(auth)}`);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[1]Forbidden!'
					};
				}
				let merchantInfo;
				try {
					merchantInfo = await this.broker.call('v1.kycAccount.checkKYC', { accountId: _.toNumber(auth.credentials.accountId) });
				} catch (error) {
					this.broker.logger.info(`PaymePage: call v1.kycAccount.checkKYC error: ${error}, input: ${JSON.stringify(auth.credentials)}, scope: ${JSON.stringify(ctx.action.scope)}`);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[2]${error.message}`
					};
				}
				if (_.get(merchantInfo, 'id', null) === null) {
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: '[3] Không tìm thấy thông tin merchant.'
					};
				}
				const accountInfo = {
					accountId: _.toNumber(auth.credentials.accountId),
					merchantId: auth.credentials.merchantId || merchantInfo.id
				};
				let mcRole = '';
				if (ctx.action.scope.name.includes('mc.paymePage.read')) {
					mcRole = 'mc.paymePage.read';
				} else if (ctx.action.scope.name.includes('mc.paymePage.create')) {
					mcRole = 'mc.paymePage.create';
				} else if (ctx.action.scope.name.includes('mc.paymePage.update')) {
					mcRole = 'mc.paymePage.update';
				} else if (ctx.action.scope.name.includes('mc.paymePage.notify')) {
					mcRole = 'mc.paymePage.notify';
				}
				try {
					const resultCheckRole = await this.broker.call('v1.role.get', { target: accountInfo });
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager', mcRole]).length <= 0) {
						this.broker.logger.info(`PaymePage: call v1.role.read response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}`);
						throw {
							code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
							message: 'Forbidden'
						};
					}
					if (_.intersection(_.get(resultCheckRole, 'scope', []), ['mc.owner', 'mc.manager']).length > 0) {
						ctx.prepareData = {
							isOwnerOrAdmin: true,
							isAuthor: false, // creator
							merchantInfo
						};
					} else {
						ctx.prepareData = {
							isOwnerOrAdmin: false,
							isAuthor: true, // creator
							merchantInfo
						};
					}
				} catch (error) {
					this.broker.logger.warn(`PaymePage: call v1.role.get response: ${error}, input: ${JSON.stringify(accountInfo)}, scope: ${JSON.stringify(ctx.action.scope)}, error: ${JSON.stringify(error)}`);
					throw {
						code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
						message: `[6]${error.message}`
					};
				}
				if (_.get(auth.credentials, 'merchantId', null) === null) {
					this.broker.logger.warn(`PaymePage: Auth INfo: MCID === null ==>${JSON.stringify(auth.credentials)}`);
					auth.credentials.merchantId = merchantInfo.id;
				}
				// }
				// else {
				// 	let checkData = {};
				// 	try {
				// 		checkData = {
				// 			target: accountInfo,
				// 			scope: ctx.action.scope.name,
				// 			// storeID: 1, // optional
				// 			options: { // optional
				// 				condition: ctx.action.scope.condition // AND & OR, default AND
				// 			}
				// 		};
				// 		const resultCheckRole = await this.broker.call('v1.role.check', checkData);
				// 		this.broker.logger.info(`PaymePage: call v1.role.check response: ${JSON.stringify(resultCheckRole)}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)}`);
				// 		if (_.get(resultCheckRole, 'isValid', false) === false) {
				// 			throw {
				// 				code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 				message: 'Forbidden'
				// 			};
				// 		}
				// 		return resultCheckRole;
				// 	} catch (error) {
				// 		this.broker.logger.warn(`PaymePage: call v1.role.check error: ${error}, input: ${JSON.stringify(checkData)}, scope: ${JSON.stringify(ctx.action.scope)} , error: ${JSON.stringify(error)}`);
				// 		throw {
				// 			code: ResponseConstants.SCOPE_CODE.SCOPE_NOT_FOUND,
				// 			message: `[3]${error.message}`
				// 		};
				// 	}
				// }
			}
		},
		customNanoId: require('./methods/customNanoId.method')
	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {
		this.$i18n = new I18n({
			locales: ['vi', 'en'],
			directory: path.join(DIRNAME, '/locales'),
			defaultLocale: 'vi',
			register: this
		});
	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},
};
