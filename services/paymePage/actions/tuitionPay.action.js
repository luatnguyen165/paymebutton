const _ = require('lodash');
const moment = require('moment');
const RespConst = require('../constants/response.constant');
const GeneralConstants = require('../constants/general.constant');
const poolCreateOrder = require('../services/poolCreateOrder');
const PageConstants = require('../constants/paymePage.constant');
const TuitionService = require('../services/tuitionService');

/**
 * @notice Get account information by call action `v1.account.getInformationInternal`
 * @param {any} broker broker of parent function
 * @param {Object} filter Payload data to get account info
 * @returns Returns an object with `isSuccess` = true and `data` as account info if the call was successful. Otherwise return `isSuccess` = false
 */
const getAccountInfo = async (broker, filter) => {
	const response = {
		isSuccess: false,
		data: null
	};
	try {
		const resGetAccountInfo = await broker.call('v1.account.getInformationInternal', filter);
		if (resGetAccountInfo.code !== GeneralConstants.SERVICES_CODE.GET_ACCOUNT_INFO_SUCCESS) {
			broker.logger.warn(`PayMEPage > tuitionPay > getAccountInfo with filter: ${JSON.stringify(filter)} and res = ${JSON.stringify(resGetAccountInfo)}`);
			return response;
		}
		response.isSuccess = true;
		response.data = _.get(resGetAccountInfo, 'data', {});
		return response;
	} catch (error) {
		broker.logger.error(`PayMEPage > tuitionPay > getAccountInfo > error = ${error}. requestBody = ${JSON.stringify(filter)}`);
		return response;
	}
};

/**
 * @notice Get merchant information by call action `v1.settingsDashboard.internalSettings`
 * @param {any} broker broker of parent function
 * @param {Object} filter Payload data to get merchant info
 * @returns Returns an object with `isSuccess` = true and `data` as merchant info if the call was successful. Otherwise return `isSuccess` = false
 */
const getMerchantInfo = async (broker, filter) => {
	const response = {
		isSuccess: false,
		data: null
	};
	try {
		const resGetMerchantInfo = await broker.call('v1.settingsDashboard.internalSettings', filter);
		if (resGetMerchantInfo.code !== GeneralConstants.SERVICES_CODE.GET_MERCHANT_INFO_SUCCESS) {
			broker.logger.warn(`PayMEPage > tuitionPay > getMerchantInfo with payload = ${JSON.stringify(filter)} and res = ${JSON.stringify(resGetMerchantInfo)}`);
			return response;
		}
		response.isSuccess = true;
		response.data = _.get(resGetMerchantInfo, 'data', {});
		return response;
	} catch (error) {
		broker.logger.error(`PayMEPage > tuitionPay > getMerchantInfo > error = ${error}. requestBody = ${JSON.stringify(filter)}}`);
		return response;
	}
};

/**
 * @notice Create many vouchers by call action `v1.voucher.createMany`
 * @param {any} ctx Context of parent function
 * @param {Object} voucherData Object data to create voucher
 * @returns Returns an object with `isSuccess` = true and `data` is the newly created voucher if the call was successful. Otherwise return `isSuccess` = false
 */
const createManyVoucher = async (ctx, listVoucherDataCreate = []) => {
	const response = {
		isSuccess: false,
		message: '',
		data: null
	};
	try {
		const resCreateVoucher = await ctx.broker.call('v1.voucher.createMany', {
			body: listVoucherDataCreate
		}, {
			parentCtx: ctx // @notice Parent Context instance. Passing parent's context to child
		});
		if (resCreateVoucher.code !== GeneralConstants.SERVICES_CODE.CREATE_VOUCHER_SUCCESS) {
			ctx.broker.logger.warn(`PayMEPage > tuitionPay > createManyVoucher with payload = ${JSON.stringify(listVoucherDataCreate)} and res = ${JSON.stringify(resCreateVoucher)}`);
			response.message = resCreateVoucher.message;
			return response;
		}
		response.isSuccess = true;
		response.data = _.get(resCreateVoucher, 'data', {});
		return response;
	} catch (error) {
		ctx.broker.logger.error(`PayMEPage > tuitionPay > createManyVoucher > error = ${error}. requestBody = ${JSON.stringify(listVoucherDataCreate)}}`);
		return response;
	}
};

/**
 * @notice update many vouchers by call action `v1.voucher.createMany`
 * @param {any} ctx Context of parent function
 * @param {Object} voucherData Object data to create voucher
 * @returns Returns an object with `isSuccess` = true and `data` is the newly created voucher if the call was successful. Otherwise return `isSuccess` = false
 */
const findManyAndUpdateVoucher = async (ctx, listVoucherDataUpdate = []) => {
	const response = {
		isSuccess: false,
		message: '',
		data: null
	};
	const bodyDataUpdate = listVoucherDataUpdate.reduce((acc, cur) => {
		acc.push({
			voucherCode: cur.voucherCode,
			dataUpdate: {
				expiredIn: cur.expiredIn
			}
		});
		return acc;
	}, []);
	try {
		const resFindManyAndUpdateVoucher = await ctx.broker.call('v1.voucher.findManyAndUpdate', {
			body: bodyDataUpdate
		}, {
			parentCtx: ctx // @notice Parent Context instance. Passing parent's context to child
		});
		if (resFindManyAndUpdateVoucher.code !== GeneralConstants.SERVICES_CODE.UPDATE_VOUCHER_SUCCESS) {
			ctx.broker.logger.warn(`PayMEPage > tuitionPay > findManyAndUpdateVoucher with payload = ${JSON.stringify(bodyDataUpdate)} and res = ${JSON.stringify(resFindManyAndUpdateVoucher)}`);
			response.message = resFindManyAndUpdateVoucher.message;
			return response;
		}
		response.isSuccess = true;
		response.data = _.get(resFindManyAndUpdateVoucher, 'data', {});
		return response;
	} catch (error) {
		ctx.broker.logger.error(`PayMEPage > tuitionPay > findManyAndUpdateVoucher > error = ${error}. requestBody = ${JSON.stringify(bodyDataUpdate)}}`);
		return response;
	}
};

/**
 * @notice Create or update voucher with listVoucherData
 * if vouchers[i].voucherCode is exist => update `expiredIn`
 * else voucherCode is not exist => create new one
 * @returns the list voucher has just been created and updated
 */
const createOrUpdateManyVoucher = async (ctx, listVoucherData = []) => {
	const response = {
		isSuccess: false,
		message: '', // @dev: Lấy thông báo lỗi từ service được gọi (v1.voucher.create) nếu có.
		data: null
	};
	try {
		const listVoucherCode = listVoucherData.map((voucher) => voucher.voucherCode);
		const listVoucherExists = await ctx.broker.call('v1.voucherModel.findMany', [{
			merchantId: _.get(ctx, 'meta.auth.credentials.merchantId', null),
			voucherCode: {
				$in: listVoucherCode,
			}
		}]);
		const listVoucherCodeExists = listVoucherExists.map((voucher) => voucher.voucherCode);
		const listVoucherDataCreate = [];
		const listVoucherDataUpdate = [];
		// Update if voucherCode is exists. Else, create new one.
		listVoucherData.forEach((voucherData) => {
			if (listVoucherCodeExists.includes(voucherData.voucherCode)) {
				listVoucherDataUpdate.push(voucherData);
			} else {
				listVoucherDataCreate.push(voucherData);
			}
		});

		const resultCreateManyVoucher = await createManyVoucher(ctx, listVoucherDataCreate);
		if (resultCreateManyVoucher.isSuccess === false) {
			response.message = resultCreateManyVoucher.message || 'Tạo mã giảm giá thất bại';
			return response;
		}

		const resultUpdateManyVoucher = await findManyAndUpdateVoucher(ctx, listVoucherDataUpdate);
		if (resultUpdateManyVoucher.isSuccess === false) {
			response.message = resultUpdateManyVoucher.message || 'Cập nhật mã giảm giá thất bại';
			return response;
		}

		response.isSuccess = true;
		response.data = {
			items: [..._.get(resultCreateManyVoucher, 'data.items', []), ..._.get(resultUpdateManyVoucher, 'data.items', [])]
		};
		return response;
	} catch (error) {
		ctx.broker.logger.error(`PayMEPage > tuitionPay.js > createOrUpdateManyVoucher > error ${error}`);
		return response;
	}
};

/**
 * @notice Create PayME Page by call action `v1.paymePage.create`
 * @param {any} ctx Context of parent function
 * @param {Object} dataCreate Object data to create PayME Page
 * @returns Return an object with `isSuccess` = true and `data` is the newly created PayME page. Otherwise return `isSuccess` = false
 */
const createPayMEPage = async (ctx, dataCreate) => {
	const response = {
		isSuccess: false,
		message: '', // @dev: Lấy thông báo lỗi từ service được gọi (v1.voucher.create) nếu có.
		data: null
	};
	try {
		const resCreatePayMEPage = await ctx.broker.call('v1.paymePage.create', {
			body: dataCreate
		}, {
			parentCtx: ctx
		});
		if (resCreatePayMEPage.code !== GeneralConstants.SERVICES_CODE.CREATE_PAGE_SUCCESS) {
			ctx.broker.logger.warn(`PayMEPage > tuitionPay > createPayMEPage with payload = ${JSON.stringify(dataCreate)} and res = ${JSON.stringify(resCreatePayMEPage)}`);
			response.message = resCreatePayMEPage.message;
			return response;
		}
		response.isSuccess = true;
		response.data = _.get(resCreatePayMEPage, 'data', {});
		return response;
	} catch (error) {
		ctx.broker.logger.error(`PayMEPage > tuitionPay > createPayMEPage > error = ${error}. requestBody = ${JSON.stringify(dataCreate)}`);
		return response;
	}
};

/** @Flow
 * 1. Create voucher if have config
 * 2. Create payMEPage
 * 3. Get customerId -> create order with customerId => createOrder with paymentMethod is MANUAL_BANK
 * 4. Generate payment url for each student and send to Email/SMS
 */
module.exports = async function (ctx) {
	const response = {
		code: RespConst.TUITION_PAY.FAIL.code,
		message: RespConst.TUITION_PAY.FAIL.message
	};
	try {
		this.logger.info(`PayMEPage > tuitionPay > meta = ${JSON.stringify(ctx.meta)}`);
		/** Kiểm tra thông tin xác thực tài khoản */
		const securityCredentials = _.get(ctx, 'meta.security.credentials', {});
		const { accountId = null } = securityCredentials;
		const { merchantId = null } = securityCredentials;

		if (accountId === null || merchantId === null) {
			response.message = this.__('Vui lòng đăng nhập hoặc xác thực tài khoản');
			return response;
		}

		let payload = ctx.params.body;
		payload = JSON.parse(Buffer.from(payload.data, 'base64').toString());
		ctx.broker.logger.info(`PayMEPage > tuitionPay > payload = ${(JSON.stringify(payload))}`);

		/** validate input */
		if (_.isEmpty(payload.items)) {
			response.message = this.__('Không tìm thấy thiết lập trang');
			return response;
		}
		if (_.isEmpty(payload.students)) {
			response.message = this.__('Không tìm thấy thông tin học sinh');
			return response;
		}

		/** Get account information */
		const resultGetAccountInfo = await getAccountInfo(ctx.broker, {
			username: _.get(payload, 'info.merchantAccount', '')
		});
		if (resultGetAccountInfo.isSuccess === false) {
			response.code = RespConst.TUITION_PAY.ACCOUNT_NOT_FOUND.code;
			response.message = RespConst.TUITION_PAY.ACCOUNT_NOT_FOUND.message;
			return response;
		}
		const accountInfo = resultGetAccountInfo.data;

		/** Get merchant information */
		const resultGetMerchantInfo = await getMerchantInfo(ctx.broker, { merchantId });
		if (resultGetMerchantInfo.isSuccess === false) {
			response.code = RespConst.TUITION_PAY.MERCHANT_NOT_FOUND.code;
			response.message = RespConst.TUITION_PAY.MERCHANT_NOT_FOUND.message;
			return response;
		}
		const merchantInfo = resultGetMerchantInfo.data;

		/** @dev check the action of user is `preview` or `send` */
		if (!!payload.info.sendEmailSms === false) { // convert to boolean and compare
			const tuitionService = new TuitionService();
			const data = payload.students.map((item) => ({
				...item,
				broker: this.broker,
				info: payload.info,
			}));
			tuitionService.calcPayment(data);
			const previewData = await tuitionService.previewData(this.broker, payload.students.length);
			ctx.broker.logger.info(`PayMEPage > tuitionPay > previewData = ${JSON.stringify(previewData)}`);
			return {
				code: RespConst.TUITION_PAY.SUCCESS.code,
				message: this.__('Hệ thống đang xử lý. Vui lòng đợi trong giây lát'),
				data: previewData
			};
		}

		const ctxWithAuthData = { ...ctx };
		ctxWithAuthData.meta.auth.data = accountInfo; // attach account info to `ctx.meta.auth.data`
		ctxWithAuthData.meta.auth.credentials = securityCredentials; // assign auth.credentials equal security.credentials

		/** Nếu action không phải preview -> thực thực hiện các thao tác tiếp theo để tạo link */
		/** Tạo mới hoặc cập nhật voucher nếu voucherCode & merchantId đã tồn tại */
		let listVoucherCode = [];
		if (_.isEmpty(payload.voucher) === false) {
			const listVoucherDataCreate = payload.voucher.map((item) => ({
				name: item.name,
				voucherCode: item.code,
				unit: _.upperCase(item.type) === 'PERCENTAGE' ? 'PERCENT' : 'VND',
				amount: item.value,
				minAmount: item.minAmount,
				maxDiscount: item.maxDiscount,
				expiredIn: {
					fromDate: moment(item.validFrom, 'DD/MM/YYYY').format('YYYY-MM-DD'), // convert date format DD/MM/YYYY to YYYY-MM-DD
					toDate: moment(item.validTo, 'DD/MM/YYYY').format('YYYY-MM-DD')
				}
			}));
			const resultCreateVoucher = await createOrUpdateManyVoucher(ctxWithAuthData, listVoucherDataCreate);
			if (resultCreateVoucher.isSuccess === false) {
				response.message = resultCreateVoucher.message || 'Thiết lập mã giảm giá thất bại';
				return response;
			}
			listVoucherCode = _.get(resultCreateVoucher, 'data.items', []).map((voucher) => voucher.voucherCode);
		}

		/** Tạo page */
		const pageDataCreate = {
			businessDetail: {
				title: payload.info.title
			},
			customerRequiredField: {
				email: true,
				fullname: true,
				phone: true,
			},
			paymentDetail: {
				inputFields: payload.items.map((item) => ({
					productId: item.id,
					label: item.name,
					description: item.name,
					isRequired: !!item.mandatory, // convert to boolean
					inputType: item.chooseQuantity ? PageConstants.PRICE_FIELD_TYPE.ITEM_WITH_QUANTITY : PageConstants.PRICE_FIELD_TYPE.FIXED_AMOUNT,
					options: {
						fixedAmount: item.price
					}
				}))
			},
			voucherAllowance: true,
			vouchers: listVoucherCode
		};
		const resultCreatePage = await createPayMEPage(ctxWithAuthData, pageDataCreate);
		if (resultCreatePage.isSuccess === false) {
			response.code = RespConst.TUITION_PAY.CREATE_PAGE_FAIL.code;
			response.message = resultCreatePage.message || RespConst.TUITION_PAY.CREATE_PAGE_FAIL.message;
			return response;
		}
		const payMEPageInfo = resultCreatePage.data;

		/** Tạo order cho mỗi học sinh với paymentMethod = MANUAL_BANK */
		const dataCreateOrder = payload.students.map((item) => ({
			...item,
			broker: this.broker,
			info: payload.info,
			accountId,
			merchantId,
			merchantName: 'tueduc',
			merchantLogo: merchantInfo.logo || '',
			mcPaymentMethod: _.get(merchantInfo, 'paymentMethod', {}), // @dev Get paymentMethod to create paymentRequest with payment method is MANUAL_BANK
			payMEPageInfo
		}));

		poolCreateOrder.init(dataCreateOrder);
		const countSMS = payload.students.map((v) => _.get(v.studentInfo, 'parentPhone', null)).filter((v) => v !== null).length;
		const countEmail = payload.students.map((v) => _.get(v.studentInfo, 'parentEmail', null)).filter((v) => v !== null).length;

		response.code = RespConst.TUITION_PAY.SUCCESS.code;
		response.message = this.__('Ghi nhận thành công {{countSMS}} sms, {{countEmail}} email. Hệ thống đang xử lý.', { countSMS, countEmail });
		return response;
	} catch (err) {
		ctx.broker.logger.error(`PayMEPage > tuitionPay > error = ${err}`);
		return response;
	}
};
