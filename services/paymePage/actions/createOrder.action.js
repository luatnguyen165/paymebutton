/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const slugify = require('slugify');
const moment = require('moment');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const PageConst = require('../constants/paymePage.constant');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;

		ctx.broker.logger.warn(`PAGE create order payload...:  ${JSON.stringify(payload)}`);
		const { pageId, customerInfos = {} } = payload;
		const paymentItems = _.get(payload, 'paymentItems', []);
		if (!pageId) {
			const { code, message } = RespConst.CREATE_ORDER.MISS_PAGEID;
			return { code, message: this.__(message) };
		}
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId },
				{ 'shortLinkInfo.slug': pageId },
			],
			status: PageConst.STATUS.ACTIVE
		}]);
		if (!pageInfo) {
			const { code, message } = RespConst.CREATE_ORDER.NOT_FOUND;
			return { code, message: this.__(message) };
		}
		if (pageInfo.expireIn) {
			if (moment(pageInfo.expireIn).isBefore(moment())) {
				const { code, message } = RespConst.CREATE_ORDER.EXPIRE;
				return { code, message: this.__(message) };
			}
		}
		let merchantInfo = null;
		try {
			merchantInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: pageInfo.creator.merchantId });
		} catch (e) {
			const { code, message } = RespConst.CREATE_ORDER.MERCHANT_NOT_FOUND;
			return { code, message: this.__(message) };
		}
		ctx.broker.logger.warn(`Create order get MC INfo-- merchantId: ${pageInfo.creator.merchantId}, ${JSON.stringify(merchantInfo)}`);
		if (merchantInfo.code !== GeneralConstants.GET_MC_INFO_SUCCESS_CODE) {
			const { code, message } = RespConst.CREATE_ORDER.MERCHANT_NOT_FOUND;
			return { code, message: this.__(message) };
		}
		merchantInfo = merchantInfo.data;
		payload.language = payload.language || merchantInfo.language || 'vi';
		this.setLocale(payload.language);

		let paymentMethod = null;
		if (_.get(payload, 'paymentMethod.methodId', null) !== null) {
			paymentMethod = merchantInfo.paymentMethod.find(
				(method) => method.methodId === payload.paymentMethod.methodId
				// && method.payCode === payload.paymentMethod.payCode
			);
			ctx.broker.logger.warn(`Get payment method Info ${JSON.stringify(paymentMethod)}`);

			if (!paymentMethod || paymentMethod.isActive === false) {
				const { code, message } = RespConst.CREATE_ORDER.INVALID_PAYMENT_METHOD;
				return { code, message: this.__(message) };
			}
		}

		if (pageInfo.voucherAllowance === false && _.trim(payload.voucherCode)) {
			const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_ALLOW;
			return { code, message: this.__(message) };
		}
		let voucherInfo;
		if (_.trim(payload.voucherCode)) {
			if (_.isArray(pageInfo.vouchers) && pageInfo.vouchers.length > 0 && !_.includes(pageInfo.vouchers, _.trim(payload.voucherCode))) {
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_INVALID;
				return { code, message: this.__(message) };
			}
			try {
				const now = new Date();
				voucherInfo = await this.broker.call('v1.voucherModel.findOne', [{
					voucherCode: _.trim(payload.voucherCode),
					merchantId: pageInfo.creator.merchantId,
					isActive: true,
					quantityRemaining: { $gt: 0 },
					'expiredIn.from': {
						$lte: now
					},
					'expiredIn.to': {
						$gte: now
					},
				}]);
			} catch (error) {
				ctx.broker.logger.info(`[PAGE] CreateOrder vouchersInfo ==>: ${JSON.stringify({ voucherInfo, voucherCode: payload.voucherCode })}`);
				ctx.broker.logger.error(error);
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_FOUND;
				return { code, message: this.__(message) };
			}
			if (!voucherInfo) {
				const { code, message } = RespConst.CREATE_ORDER.VOUCHER_NOT_FOUND;
				return { code, message: this.__('Voucher {{voucherNotFound}} không hợp lệ hoặc đã hết lượt sử dụng', { voucherNotFound: payload.voucherCode }) };
			}
		}

		const inputFields = _.get(pageInfo, 'paymentDetail.inputFields', []).filter((v) => v.isActive);
		console.log('create order PAGE=> ', JSON.stringify({ inputFields, paymentItems }));
		ctx.broker.logger.info(`create order PAGE => ${JSON.stringify({ inputFields, paymentItems })}`);
		const missedPriceField = inputFields.filter(
			(field) => field.isRequired === true
				&& _.get(field, 'options.unitInStock', 0) > _.get(field, 'options.itemSold', 0)
				&& !paymentItems.find((item) => item.id === field.id)
		);
		if (_.isEmpty(missedPriceField) === false) {
			const { code, message } = RespConst.CREATE_ORDER.MISS_REQUIRED_FIELD;
			return { code, message: this.__(message, { key: missedPriceField[0].label }) };
		}

		const fieldPrefix = 'paymentDetail.inputFields';
		const inOrderData = {};
		const paymentData = [];
		let totalAmount = 0;
		let totalDiscount = 0;
		let totalPayment = 0;
		// eslint-disable-next-line no-plusplus
		for (let i = 0; i < paymentItems.length; i++) {
			const item = paymentItems[i];
			const itemId = item.id;
			const foundItem = inputFields.find((input) => input.id === itemId);
			const foundIndex = inputFields.findIndex((input) => input.id === itemId);
			if (!foundItem) {
				const { code, message } = RespConst.CREATE_ORDER.FIELD_NOT_FOUND;
				return { code, message: this.__(_.toString(code), { key: itemId }) };
			}
			const inOrder = _.get(foundItem, 'options.itemInOrder', 0);
			let quantity;
			if (foundItem.inputType === PageConst.PRICE_FIELD_TYPE.ITEM_WITH_QUANTITY) {
				if (!item.quantity) {
					const { code, message } = RespConst.CREATE_ORDER.MISS_QUANTITY;
					return { code, message: this.__(message, { key: foundItem.label }) };
				}
				quantity = item.quantity;
			} else {
				quantity = 1;
				// const { code, message } = RespConst.CREATE_ORDER.INVALID_QUANTITY;
				// return { code, message: this.__(message) };
			}
			const name = _.camelCase(slugify(foundItem.label, { replacement: ' ', lower: true, locale: 'vi' }));
			let unitAmount;
			if (foundItem.inputType === PageConst.PRICE_FIELD_TYPE.CUSTOMER_DECIDE_AMOUNT) {
				if (!item.amount) {
					const { code, message } = RespConst.CREATE_ORDER.MISS_AMOUNT;
					return { code, message: this.__(message, { key: foundItem.label }) };
				}
				// if (item.amount < 10000) {
				// 	const { code, message } = RespConst.CREATE_ORDER.INVALID_AMOUNT;
				// 	return { code, message: this.__(message, { amount: '10.000' }) };
				// }
				unitAmount = item.amount;
			} else {
				unitAmount = _.get(foundItem, 'options.fixedAmount', 0);
			}
			if (_.get(foundItem, 'options.unitsAvailable', '') === PageConst.UNITSAVAILABLE.LIMITED) {
				const stock = _.get(foundItem, 'options.unitInStock', 0);
				const sold = _.get(foundItem, 'options.itemSold', 0);
				if (quantity > stock - sold) {
					const { code, message } = RespConst.CREATE_ORDER.OUT_OF_STOCK;
					return { code, message: this.__(message, { key: foundItem.label }) };
				}
			}
			inOrderData[`${fieldPrefix}.${foundIndex}.options.itemInOrder`] = inOrder + quantity;

			const amount = quantity * unitAmount;
			paymentData.push({
				id: itemId,
				label: foundItem.label,
				name,
				unitAmount,
				amount,
				quantity,
				image: foundItem.image
			});
			totalAmount += amount;
			totalPayment = totalAmount - totalDiscount;
		}
		if (voucherInfo) {
			if (voucherInfo.minAmount !== 0 && totalAmount < voucherInfo.minAmount) { //  tối thiếu
				const { code, message } = RespConst.CREATE_ORDER.ORDER_NOT_ENOUGH_AMOUNT;
				return { code, message: this.__(`Đơn hàng ${message.toLowerCase()} để sử dụng voucher ${voucherInfo.voucherCode}`) };
			}
			// tính toán số tiền giảm
			if (voucherInfo.unit === 'PERCENT') {
				// giảm theo %
				totalDiscount = Math.round((totalAmount * voucherInfo.amount) / 100);
				if (voucherInfo.maxDiscount !== 0 && totalDiscount > voucherInfo.maxDiscount) {
					totalDiscount = voucherInfo.maxDiscount; // giảm tối đa
				}
			} else {
				// giảm theo số tiền cụ thể
				totalDiscount = voucherInfo.amount;
			}
			if (totalDiscount > totalAmount) {
				totalDiscount = totalAmount;
			}
			// cập nhật số lượng remaining
			try {
				const voucherUpdated = await this.broker.call('v1.voucherModel.updateOne', [
					{
						id: voucherInfo.id
					},
					{
						$inc: {
							quantityRemaining: -1
						}
					}
				]);
				if (_.get(voucherUpdated, 'nModified', 0) === 0) {
					ctx.broker.logger.warn(`Create order update quantityRemaining FAILED!!!! voucherCode=${voucherInfo.voucherCode}`);
					const { code, message } = RespConst.CREATE_ORDER.FAIL;
					return { code, message: this.__(message) };
				}
			} catch (error) {
				ctx.broker.logger.warn(`Create order update quantityRemaining error!!: ${error}, voucherCode: ${voucherInfo.voucherCode}`);
				const { code, message } = RespConst.CREATE_ORDER.FAIL;
				return { code, message: this.__(message) };
			}
		}
		totalPayment = totalAmount - totalDiscount;
		if (totalPayment < 10000) {
			const { code, message } = RespConst.CREATE_ORDER.INVALID_AMOUNT;
			return { code, message: this.__(message, { amount: '10.000' }) };
		}

		const customerKeys = ['email', 'phone', 'fullname', 'shippingAddress'];
		const customerData = [
			{
				label: 'id',
				name: 'id',
				value: customerInfos.id || _.get(pageInfo, 'customerDetail.id', '')
			}];
		_.forEach(customerKeys, (key) => {
			if (_.get(pageInfo, `customerRequiredField.${key}`, false) && !customerInfos[key]) {
				const { code, message } = RespConst.CREATE_ORDER.MISS_REQUIRED_FIELD;
				return { code, message: this.__(message, { key }) };
			}
			customerData.push({
				label: key,
				name: key,
				value: customerInfos[key] || _.get(pageInfo, `customerDetail.${key}`, '')
			});
			return true;
		});
		const customerInfo = customerData.filter((i) => i.value);
		ctx.broker.logger.info(`[PAGE]Service  customerInfo: ${JSON.stringify({ customerInfo, customerInfos, customerData })}`);

		// chưa tạo đơn hàng lần nào => tạo mới
		let transactionId = null;
		try {
			transactionId = await this.customNanoId({ prefix: 'PAYMENT_REQUEST_ID', type: 'string' });
		} catch (error) {
			const { code, message } = RespConst.CREATE_ORDER.GET_UUID_FAILED;
			return { code, message: this.__(message) };
		}
		if (!transactionId) {
			const { code, message } = RespConst.CREATE_ORDER.GET_UUID_FAILED;
			return { code, message: this.__(message) };
		}
		const requestPaymentData = {
			note: payload.note,
			transactionId,
			referType: PaymentRequestConst.REFER_TYPE.PAYME_PAGE,
			referId: pageInfo.pageId,
			referButtonId: payload.referButtonId,
			amount: totalAmount,
			discountAmount: totalDiscount,
			paymentAmount: totalPayment,
			customerDetails: customerInfo,
			paymentItems: paymentData,
			status: PaymentRequestConst.STATUS.FAILED,
			expiryAt: moment(new Date()).add(15, 'minutes').toISOString(),
			paymentMethod: paymentMethod || {},
			voucherInfo: {
				id: _.get(voucherInfo, 'id', null),
				code: _.get(voucherInfo, 'voucherCode', null)
			},
			transactionInfo: payload,
			recurringPayment: payload.recurringPayment
		};

		const paymentRequest = await this.broker.call('v1.paymentRequestModel.create', [requestPaymentData]);
		if (_.get(paymentRequest, 'id', null) === null) {
			const { code, message } = RespConst.CREATE_ORDER.FAIL;
			return { code, message: this.__(message) };
		}
		const ipnUrl = `${process.env.HOST}/widgets/page/external/receive_callback`;
		const orderInfo = {
			title: _.get(pageInfo, 'businessDetail.title', ''),
			paymentItems: paymentData
		};
		const customerInfoObj = {};
		for (const val of customerInfo) {
			customerInfoObj[val.name] = val.value;
		}
		const orderData = {
			referId: pageInfo.pageId,
			referType: PaymentRequestConst.REFER_TYPE.PAYME_PAGE,
			partnerTransaction: paymentRequest.transactionId,
			accountId: _.get(pageInfo, 'creator.accountId', ''),
			amount: totalPayment,
			description: _.get(pageInfo, 'businessDetail.title', ''),
			referData: JSON.stringify({
				orderInfo,
				customerInfo: customerInfoObj,
				voucherInfo: {
					id: _.get(voucherInfo, 'id', null),
					code: _.get(voucherInfo, 'voucherCode', null)
				},
				note: payload.note
			}),
			ipnUrl,
			redirectUrl: `${payload.redirectUrl}&transid=${transactionId}`, // on success
			failedUrl: `${payload.failedUrl}&transid=${transactionId}`, // on fail
			payMethod: paymentMethod ? paymentMethod.payCode : undefined,
			lang: payload.language,
			note: payload.note,
		};
		let orderId = '';
		let paymentUrl = '';
		try {
			const orderCreated = await this.broker.call('v1.order.createWeb', orderData);
			ctx.broker.logger.info(`[PAGE]Service 'v1.order.createWeb' result: ${JSON.stringify({ orderData, orderCreated })}`);
			if (orderCreated.code !== GeneralConstants.CREATE_ORDER_SUCCESS_CODE) {
				return {
					code: RespConst.CREATE_ORDER.FAIL_CREATE_WEB.code,
					message: this.__(orderCreated.message)
				};
			}
			orderId = orderCreated.data.orderId;
			paymentUrl = orderCreated.data.url;
		} catch (e) {
			ctx.broker.logger.warn(`Service 'v1.order.createWeb' error: ${JSON.stringify({ message: e.message, input: orderData })}`);
			const { code, message } = RespConst.CREATE_ORDER.FAIL_CREATE_WEB;
			return { code, message: this.__(message) };
		}

		await this.broker.call('v1.paymentRequestModel.updateOne', [
			{
				id: paymentRequest.id
			},
			{
				orderId,
				paymentUrl,
				status: PaymentRequestConst.STATUS.PENDING
			}]);
		await this.broker.call('v1.paymePageModel.updateOne', [
			{
				_id: pageInfo._id
			},
			{
				...inOrderData
			}]);

		const { code, message } = RespConst.CREATE_ORDER.SUCCESS;
		return {
			code,
			message: this.__(message),
			data: {
				orderId,
				transaction: paymentRequest.transactionId,
				url: paymentUrl
			}
		};
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Create Order Error: ${err}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
