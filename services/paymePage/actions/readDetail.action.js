/* eslint-disable no-underscore-dangle */
const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const PageConst = require('../constants/paymePage.constant');
const RespConst = require('../constants/response.constant');

const pageLinkUrl = process.env.PAGE_URL || 'https://page.payme.net.vn';

const getPageInfo = async function (ctx, pageId, isAuthRequired = true) {
	const result = {
		isSucceed: false,
		code: null,
		message: ctx.service.__('Thất bại'),
		data: null
	};
	try {
		const filter = {
			$or: [
				{ pageId },
				{ 'shortLinkInfo.slug': pageId }
			]
		};
		ctx.broker.logger.info(`PAYME_PAGE: isAuthRequired = ${isAuthRequired}, filter by: ${JSON.stringify(filter)}`);
		if (isAuthRequired) {
			const auth = _.get(ctx, 'meta.auth', {});
			if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
				filter['creator.merchantId'] = auth.credentials.merchantId; // lấy all theo MC id
			} else {
				filter['creator.accountId'] = auth.credentials.accountId; // chỉ lấy theo account tạo link
			}
			ctx.broker.logger.info(`Page detail filter by > ${JSON.stringify(filter)}`);
		}
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [filter]);
		if (!pageInfo) {
			const { code, message } = RespConst.DETAIL.NOT_FOUND;
			result.code = code;
			result.message = ctx.service.__(message, { key: pageId });
			return result;
		}
		delete pageInfo._id;
		let inputFields = _.get(pageInfo, 'paymentDetail.inputFields', []);
		if (!isAuthRequired) { // không có auth => xem external => ko hiển thị các sp bị ẩn, chỉ show isActive = true
			inputFields = inputFields.filter((v) => v.isActive);
		}
		const tags = [];
		const inputFieldsReturned = inputFields.map((field) => {
			const newField = { ...field }; // avoid side effect
			if ((newField.group === PageConst.FIELD_GROUP.PRICE || !newField.group) && field.options) {
				newField.options.itemSold = field.options.itemSold || 0;
				newField.options.itemInOrder = field.options.itemInOrder || 0;
				newField.options.revenue = field.options.revenue || 0;
			}
			if (_.isArray(newField.tags)) tags.push(...newField.tags);
			return newField;
		});
		let accountCreate = {};
		try {
			accountCreate = await ctx.broker.call('v1.account.getInformationInternal', {
				accountId: _.get(pageInfo, 'creator.accountId', '')
			});
			ctx.broker.logger.info(`PAYME PAGE call v1.account.getInformationInternal res ===> ${JSON.stringify(accountCreate)} filter: ${_.get(pageInfo, 'creator.accountId', '')}`);
			if (!accountCreate || _.get(accountCreate, 'data.id', false) === false) {
				return {
					isSucceed: false,
					message: ctx.service.__('Không tìm thấy thông tin tài khoản ví'),
				};
			}
		} catch (error) {
			ctx.broker.logger.warn(`PAYME PAGE Error ===> ${error}, filter: ${_.get(pageInfo, 'creator.accountId', '')}`);
			return {
				isSucceed: false,
				message: ctx.service.__('Không tìm thấy thông tin tài khoản ví!')
			};
		}
		let pageIdUuid = _.get(pageInfo, 'pageId', '');
		if (_.get(pageInfo, 'shortLinkInfo.slug', null) !== null) {
			pageIdUuid = pageInfo.shortLinkInfo.slug;
		}
		const fullLink = `${process.env.PAGE_URL_SHORT}/${pageIdUuid}`;
		const pageDataReturned = {
			id: _.get(pageInfo, 'id', ''),
			pageUrl: fullLink, // `${pageLinkUrl}/${pageInfo.pageId}`,
			shortLink: fullLink, // _.get(pageInfo, 'shortLinkInfo.url', `${pageLinkUrl}/${pageId}`),
			creator: {
				...pageInfo.creator,
				displayname: accountCreate.data.fullname,
				phone: accountCreate.data.phone,
				email: accountCreate.data.email
			},
			businessDetail: _.get(pageInfo, 'businessDetail', {}),
			paymentDetail: {
				currency: _.get(pageInfo, 'paymentDetail.currency', ''),
				labelPay: _.get(pageInfo, 'paymentDetail.labelPay', ''),
				inputFields: inputFieldsReturned
			},
			customerRequiredField: {
				email: _.get(pageInfo, 'customerRequiredField.email', false),
				fullname: _.get(pageInfo, 'customerRequiredField.fullname', false),
				phone: _.get(pageInfo, 'customerRequiredField.phone', false),
				shippingAddress: _.get(pageInfo, 'customerRequiredField.shippingAddress', false),
			},
			type: _.get(pageInfo, 'type', ''),
			totalRevenue: _.get(pageInfo, 'actualRevenue', _.get(pageInfo, 'totalRevenue', 0)),
			theme: _.get(pageInfo, 'theme', ''),
			expireIn: pageInfo.expireIn,
			aliasPageName: _.get(pageInfo, 'aliasPageName', ''),
			successMsg: _.get(pageInfo, 'successMsg', ''),
			successRedirect: _.get(pageInfo, 'successRedirect', ''),
			status: _.get(pageInfo, 'status', ''),
			pageId: _.get(pageInfo, 'pageId', ''),
			createdAt: pageInfo.createdAt,
			updatedAt: pageInfo.updatedAt,
			customerDetail: _.get(pageInfo, 'customerDetail', {}),
			voucherAllowance: _.get(pageInfo, 'voucherAllowance', false),
			isNotified: pageInfo.isNotified,
			vouchers: _.get(pageInfo, 'vouchers', null),
			tags: _.isArray(pageInfo.tags) && pageInfo.tags.length > 0 ? pageInfo.tags : _.uniq(tags),
			recurringPayment: pageInfo.recurringPayment,
			alias: _.get(pageInfo, 'alias', null)
		};
		return {
			isSucceed: true,
			message: ctx.service.__('Thành công'),
			data: pageDataReturned
		};
	} catch (e) {
		ctx.broker.logger.error(`Get Page Info Error: ${e.message}`);
		return result;
	}
};

module.exports = {
	async readInternal(ctx) {
		try {
			const { pageId } = ctx.params.params;
			if (!pageId) {
				const { code, message } = RespConst.DETAIL.MISS_PAGEID;
				return { code, message: this.__(message) };
			}
			const pageInfoResult = await getPageInfo(ctx, pageId, true);
			if (pageInfoResult.isSucceed === false) {
				const { code, message } = pageInfoResult;
				return { code, message };
			}
			// if (_.get(pageInfoResult.data, 'creator.accountId', null) !== _.get(ctx, 'meta.auth.credentials.accountId', null)) {
			// 	const { code, message } = RespConst.READ.NO_PERMISSION;
			// 	return { code, message: this.__(message) };
			// }
			const { code, message } = RespConst.DETAIL.SUCCESS;
			return {
				code,
				message: this.__(message),
				data: {
					pageUrl: `${pageLinkUrl}/${pageId}`,
					...pageInfoResult.data,
				}
			};
		} catch (err) {
			ctx.broker.logger.error(`PaymePage Detail Error: ${err.message}`);
			return {
				code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
				message: this.__('Internal Server Error')
			};
		}
	},
	async readExternal(ctx) {
		try {
			const payload = ctx.params.body;
			const { filter, referData } = payload;
			const { pageId } = filter;
			if (!pageId) {
				const { code, message } = RespConst.DETAIL.MISS_PAGEID;
				return { code, message: this.__(message) };
			}
			const pageInfoResult = await getPageInfo(ctx, pageId, false);
			if (pageInfoResult.isSucceed === false) {
				const { code, message } = pageInfoResult;
				return { code, message };
			}
			const pageInfo = pageInfoResult.data;
			if (pageInfo.expireIn) {
				if (moment(pageInfo.expireIn, 'DD/MM/YYYY HH:mm:ss').isBefore(moment())) {
					const { code, message } = RespConst.DETAIL.EXPIRE;
					return { code, message: this.__(message) };
				}
			}
			if (pageInfo.status === PageConst.STATUS.INACTIVE) {
				const { code, message } = RespConst.DETAIL.INACTIVE;
				return { code, message: this.__(message) };
			}
			delete pageInfo.notes;
			let merchantInfo = null;
			try {
				merchantInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: pageInfo.creator.merchantId });
			} catch (e) {
				const { code, message } = RespConst.DETAIL.MERCHANT_NOT_FOUND;
				return { code, message: this.__(message) };
			}
			ctx.broker.logger.warn(`PageDetails External get MC INfo merchantId:  ${pageInfo.creator.merchantId},  ${JSON.stringify(merchantInfo)}`);
			if (_.isEmpty(_.get(merchantInfo, 'data', {}))) {
				const { code, message } = RespConst.DETAIL.MERCHANT_NOT_FOUND;
				return { code, message: this.__(message) };
			}
			if (referData && referData.buttonId) { // if page is opening from button -> increase click button
				ctx.broker.call('v1.paymeButtonModel.updateOne', [
					{
						buttonId: referData.buttonId,
					},
					{
						$inc: {
							numOfClick: 1
						}
					}
				]);
			}
			const { code, message } = RespConst.DETAIL.SUCCESS;
			return {
				code,
				message: this.__(message),
				data: {
					...pageInfo,
					merchantInfo: {
						brandName: _.get(merchantInfo, 'data.brandName', ''),
						language: _.get(merchantInfo, 'data.language', ''),
						storeName: _.get(merchantInfo, 'data.storeName', ''),
						themeColor: _.get(merchantInfo, 'data.themeColor', ''),
						logo: _.get(merchantInfo, 'data.logo', ''),
						url: _.get(merchantInfo, 'data.url', ''),
						introduce: _.get(merchantInfo, 'data.introduce', ''),
						contactInfo: {
							email: _.get(merchantInfo, 'data.contactInfo.email', ''),
							phone: _.get(merchantInfo, 'data.contactInfo.phone', ''),
						}
					}
				}
			};
		} catch (err) {
			ctx.broker.logger.error(`PaymePage Detail Error: ${err.message}`);
			return {
				code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
				message: this.__('Internal Server Error')
			};
		}
	}
};
