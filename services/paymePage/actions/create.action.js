const _ = require('lodash');
const slugify = require('slugify');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const QRCode = require('qrcode');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const PageConst = require('../constants/paymePage.constant');
const PhoneHelper = require('../helpers/PhoneHelper');
const GenIdHelper = require('../helpers/GenIdHelper');

const pageLinkUrl = process.env.PAGE_URL || 'https://page.payme.vn';

const validateBusinessDetail = function (businessDetail, ctx) {
	const result = {
		isValid: false,
		code: null,
		message: 'Thông tin không hợp lệ',
		data: null
	};
	let formatedPhone;
	if (businessDetail.phone !== undefined) {
		const phoneHelper = new PhoneHelper();
		const checkPhoneResult = phoneHelper.validatePhone(businessDetail.phone);
		if (!checkPhoneResult.isValid) {
			const { code, message } = RespConst.CREATE.INVALID_PHONE;
			result.code = code;
			result.message = ctx.service.__(message);
			return result;
		}
		formatedPhone = checkPhoneResult.phone;
	}
	const formated = {
		title: _.get(businessDetail, 'title', ''),
		description: _.get(businessDetail, 'description', ''),
		socialMediaShareIcon: _.get(businessDetail, 'socialMediaShareIcon', false),
		email: _.get(businessDetail, 'email', ''),
		phone: formatedPhone || '',
		termAndCondition: _.get(businessDetail, 'termAndCondition', '')
	};
	return {
		isValid: true,
		message: 'Thông tin hợp lệ',
		data: formated
	};
};

const validatePaymentDetail = async function (paymentDetail, ctx) {
	const result = {
		isValid: false,
		code: null,
		message: 'Thông tin không hợp lệ',
		data: null
	};
	const { inputFields } = paymentDetail;
	let countPriceField = 0;
	const formartedFields = [];
	const productIds = [];
	inputFields.forEach((field) => {
		if (!_.trim(field.productId)) {
			// eslint-disable-next-line no-param-reassign
			field.productId = GenIdHelper.generateChar(6).toString();
		}
		productIds.push(field.productId);
	});
	const uniqueProductId = [...new Set(productIds)];
	if (uniqueProductId.length !== productIds.length) {
		const { code, message } = RespConst.CREATE.INVALID_PRODUCTID;
		result.code = code;
		result.message = ctx.service.__(message);
		return result;
	}
	const { credentials = {} } = _.get(ctx, 'meta.auth', {});
	const tagsName = _.uniq(_.concat(...(inputFields.map((field) => field.tags).filter((tag) => tag))));
	const existedTagsName = await ctx.broker.call('v1.productTagModel.find', {
		body: {
			query: {
				merchantId: credentials.merchantId,
			},
			fields: '-_id id title',
		},
	});
	const notExistTagsName = _.difference(tagsName.map((field) => field.toLowerCase()), existedTagsName.map((v) => v.title.toLowerCase()));
	// ctx.broker.logger.info(JSON.stringify({ tagsName, existedTagsName, notExistTagsName }));
	if (notExistTagsName.length > 0) {
		const tagCreate = [];
		notExistTagsName.forEach((element) => {
			const tag = ctx.broker.call('v1.productTagModel.create', [{
				merchantId: credentials.merchantId,
				accountId: credentials.accountId,
				title: element,
			}]);
			tagCreate.push(tag);
		});
		if (tagCreate.length > 0) await Promise.all(tagCreate);
	}

	for (let i = 0; i < inputFields.length; i += 1) {
		const field = inputFields[i];
		let options = {};
		if (_.get(field, 'options.unitsAvailable') === PageConst.UNITSAVAILABLE.LIMITED
			&& _.get(field, 'options.unitInStock', 0) <= 0) {
			const { code, message } = RespConst.CREATE.INVALID_STOCK;
			result.code = code;
			result.message = ctx.service.__(message, { key: field.label });
			return result;
		}
		if (field.inputType === PageConst.PRICE_FIELD_TYPE.FIXED_AMOUNT) {
			if (_.get(field, 'options.fixedAmount', 0) === 0) {
				const { code, message } = RespConst.CREATE.INVALID_AMOUNT;
				result.code = code;
				result.message = ctx.service.__(message, { amount: '10.000' });
				return result;
			}
			options = {
				fixedAmount: _.get(field, 'options.fixedAmount', 0),
				unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
				unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
					? field.options.unitInStock : null,
				quantity: {},
			};
		} else if (field.inputType === PageConst.PRICE_FIELD_TYPE.CUSTOMER_DECIDE_AMOUNT) {
			options = {
				fixedAmount: null,
				unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
				unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
					? field.options.unitInStock : null,
				quantity: {
					min: _.get(field, 'options.quantity.min', null),
					max: _.get(field, 'options.quantity.max', null)
				}
			};
		} else if (field.inputType === PageConst.PRICE_FIELD_TYPE.ITEM_WITH_QUANTITY) {
			if (!_.get(field, 'options.fixedAmount', null)) {
				const { code, message } = RespConst.CREATE.MISS_AMOUNT;
				result.code = code;
				result.message = ctx.service.__(message, { key: field.label });
				return result;
			}
			options = {
				fixedAmount: _.get(field, 'options.fixedAmount', 0),
				unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
				unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
					? field.options.unitInStock : null,
				quantity: {
					min: _.get(field, 'options.quantity.min', null),
					max: _.get(field, 'options.quantity.max', null)
				}
			};
		} else {
			const { code, message } = RespConst.CREATE.INVALID_FIELD_TYPE;
			result.code = code;
			result.message = ctx.service.__(message, { key: field.label });
			return result;
		}
		const formatedPriceField = {
			id: field.productId,
			group: _.get(field, 'group', ''),
			inputType: _.get(field, 'inputType', ''),
			label: _.get(field, 'label', ''),
			image: _.get(field, 'image', null),
			description: _.get(field, 'description', ''),
			isRequired: _.get(field, 'isRequired', false),
			options: {
				...options,
				itemSold: 0,
				itemInOrder: 0,
				revenue: 0
			},
			isActive: _.isBoolean(field.isActive) ? field.isActive : true,
			tags: field.tags
		};
		// ctx.broker.logger.info(JSON.stringify({ formatedPriceField }));

		countPriceField += 1;
		formartedFields.push(formatedPriceField);
	}

	if (countPriceField === 0) {
		const { code, message } = RespConst.CREATE.MISS_PRICE_FIELD;
		result.code = code;
		result.message = ctx.service.__(message);
		return result;
	}
	const formated = {
		currency: _.get(paymentDetail, 'currency', PageConst.CURRENCY.VND),
		inputFields: formartedFields,
		labelPay: _.get(paymentDetail, 'labelPay', 'Pay')
	};

	return {
		isValid: true,
		message: ctx.service.__('Thông tin hợp lệ'),
		data: formated
	};
};

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		const auth = _.get(ctx, 'meta.auth', {});

		if (_.get(merchantInfo, 'id', null) === null) {
			if (_.get(auth.credentials, 'merchantId', null) === null) {
				return {
					code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
					message: this.__('Vui lòng cung cấp thông tin merchant')
				};
			}
		}
		ctx.broker.logger.info(`creae PAGE payload: ${JSON.stringify(payload)}`);
		const { credentials = {}, data = {} } = _.get(ctx, 'meta.auth', {});
		const { businessDetail, paymentDetail, expireIn } = payload;

		const aliasPageName = slugify(_.get(businessDetail, 'title', ''), { lower: true, }) || ' ';
		const creator = {
			merchantId: credentials.merchantId || merchantInfo.id,
			accountId: credentials.accountId,
			email: data.email,
			username: data.username,
			// storeId: ''
		};
		const templateType = _.get(payload, 'type', PageConst.TEMPLATE.OTHER);

		const checkBusinessResult = validateBusinessDetail(businessDetail, ctx);
		if (checkBusinessResult.isValid === false) {
			const { code, message } = checkBusinessResult;
			return { code, message };
		}

		const checkPaymentResult = await validatePaymentDetail(paymentDetail, ctx);

		if (checkPaymentResult.isValid === false) {
			ctx.broker.logger.info(`create PAGE checkPaymentResult: ${JSON.stringify(checkPaymentResult)}`);
			const { code, message } = checkPaymentResult;
			return { code, message };
		}
		if (expireIn && moment(expireIn).isBefore(moment().add(15, 'minutes'))) {
			const { code, message } = RespConst.CREATE.INVALID_EXPIRE_TIME;
			return { code, message: this.__(message) };
		}
		const dataToCreate = {
			aliasPageName,
			creator,
			type: templateType,
			businessDetail: checkBusinessResult.data,
			paymentDetail: checkPaymentResult.data,
			customerRequiredField: {
				email: _.get(payload, 'customerRequiredField.email', false),
				fullname: _.get(payload, 'customerRequiredField.fullname', false),
				phone: _.get(payload, 'customerRequiredField.phone', false),
				shippingAddress: _.get(payload, 'customerRequiredField.shippingAddress', false),
			},
			theme: _.get(payload, 'theme', PageConst.THEME.LIGHT),
			expireIn: _.get(payload, 'expireIn', null),
			successMsg: _.get(payload, 'successMsg', null),
			successRedirect: _.get(payload, 'successRedirect', null),
			status: PageConst.STATUS.ACTIVE,
			customerDetail: {
				id: _.get(payload, 'customerDetail.id', null),
				email: _.get(payload, 'customerDetail.email', null),
				fullname: _.get(payload, 'customerDetail.fullname', null),
				phone: _.get(payload, 'customerDetail.phone', null),
				shippingAddress: _.get(payload, 'customerDetail.shippingAddress', null)
			},
			voucherAllowance: _.isBoolean(payload.voucherAllowance) ? payload.voucherAllowance : false,
			vouchers: payload.vouchers || null,
			tags: payload.tags || [],
			recurringPayment: payload.recurringPayment
		};
		let pageId = Math.floor(Math.random() * 10000000000000).toString();
		try {
			pageId = await this.customNanoId({ prefix: 'PAYME_PAGES_SHORT', type: 'string' });
		} catch (e) {
			ctx.broker.logger.warn(`Call this.customNanoId error: ${e}`);
			const { code, message } = RespConst.CREATE.GET_UUID_FAILED;
			return { code, message: this.__(message) };
		}
		if (!pageId) {
			const { code, message } = RespConst.CREATE.FAIL;
			return { code, message: this.__(message) };
		}
		dataToCreate.pageId = pageId;
		const fullLinkPage = `${pageLinkUrl}/${pageId}`;
		const created = await this.broker.call('v1.paymePageModel.create', [dataToCreate]);
		if (_.get(created, 'id', null) === null) {
			const { code, message } = RespConst.CREATE.FAIL;
			return { code, message: this.__(message) };
		}
		// let generateQR;
		// try {
		// 	generateQR = await QRCode.toDataURL(fullLinkPage);
		// } catch (error) {
		// 	ctx.broker.logger.warn(`tạo QRCode thất bại + ${error}`);
		// }
		// #region SEND EMAIL OR SMS
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: credentials.merchantId });
		} catch (e) {
			const { code } = RespConst.CREATE.FAIL;
			return { code, message: this.__('Không tìm thấy thông tin merchant!') };
		}
		if (merchantFullInfo.code !== 114100) {
			ctx.broker.logger.warn(`[CREATE PAGE] auto send email to KH -- merchantId:  ${credentials.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			const { code } = RespConst.CREATE.FAIL;
			return { code, message: this.__('Không tìm thấy thông tin merchant') };
		}
		merchantFullInfo = merchantFullInfo.data;
		// nếu có setting tự động gửi mail thì gửi cho KH nếu có chọn KH có sẵn lúc tạo page
		if (_.get(merchantFullInfo, 'isAutoEmail', false) === true && _.get(payload, 'customerDetail.id', null) !== null) {
			const dataSendMail = {
				meta: {
					merchantId: credentials.merchantId,
					accountId: credentials.accountId,
					chanel: ['INDIVIDUAL'],
					target: [payload.customerDetail.id],
					method: ['EMAIL', 'SMS'],
					service: 'PAYME_PAGE',
					extraData: { referId: created.pageId },
					brokerCall: 'v1.paymePage.receiveCallbackSendEmailAndSMS',
					template: {
						email: 'merchant-payme-page',
						sms: 'default-sms'
					},
					content: {
						mail: {
							subject: 'PayME - Đơn hàng mới',
							content: {
								merchantLogo: _.get(merchantFullInfo, 'logo', ''),
								merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
								orderUrl: `${process.env.PAGE_URL_SHORT}/${created.pageId}`,
								qrImage: `https://t.payme.vn/qr?t=${process.env.PAGE_URL_SHORT}/${created.pageId}`,
							}
						},
						sms: {
							content: {
								data: `${merchantFullInfo.brandName || merchantFullInfo.fullName} gui quy khach thong tin dat hang, vui long truy cap ${process.env.PAGE_URL_SHORT}/${created.pageId} de hoan tat dat hang va thanh toan. Xin cam on!`
							}
						}
					}
				}
			};
			try {
				const sendEmailAndSMSRes = await this.broker.call('v1.customer.sendEmailAndSMS', {}, dataSendMail);
				this.broker.logger.info(`PayMEPage > CreatePage > auto send mail/sms > broker v1.customer.sendEmailAndSMS > ${JSON.stringify({ sendEmailAndSMSRes, dataSendMail })}`);
			} catch (e) {
				ctx.broker.logger.warn(`PayMEPage > CreatePage > auto send mail/sms > broker v1.customer.sendEmailAndSMS > Error > ${e.message} data: ${JSON.stringify(dataSendMail)} `);
			}
		}
		// #endregion
		const { code, message } = RespConst.CREATE.SUCCESS;
		return {
			code,
			message: this.__(message),
			data: {
				pageId,
				shortLink: `${process.env.PAGE_URL_SHORT}/${created.pageId}` // _.get(created, 'shortLinkInfo.url', '')
			}
		};
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Create Error: ${err.message} `);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
