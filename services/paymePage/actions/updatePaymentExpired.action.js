const _ = require('lodash');
const { MoleculerError } = require('moleculer').Errors;
const moment = require('moment');
const Async = require('async');
const AsyncForEach = require('await-async-foreach');
const generalConstant = require('../constants/general.constant');
const PaymentRequestConstant = require('../../paymentRequestModel/constant/paymentRequest.constant');

module.exports = async function (ctx) {
	try {
		let paymentRequests = [];
		try {
			paymentRequests = await this.broker.call('v1.paymentRequestModel.findMany', [
				{
					referType: 'PAYME_PAGE',
					status: 'PENDING',
					$and: [
						{ expiryAt: { $lt: new Date() } },
						{ expiryAt: { $ne: null } }
					]
				}, '-_id referId'
			]);
		} catch (err) {
			this.logger.info('[CRON] -> [PAYME_PAGE] -> [UPDATE_PAYMENT] -> [EXCEPTION] -> ', err);
		// ctx.broker.logger.warn(`[PAYME_PAGE]Update expired PaymentRequest >> Get pending order error: ${err}`);
		}
		// eslint-disable-next-line consistent-return

		if (paymentRequests.length > 0) {
			await AsyncForEach(paymentRequests, async (task) => {
				try {
				// hủy payment request nếu có
					const paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [
						{
							referId: task.referId,
							referType: 'PAYME_PAGE',
							status: 'PENDING',
						}
					]);
					if (paymentRequest) {
						await this.broker.call('v1.paymentRequestModel.updateOne', [{
							id: paymentRequest.id,
							status: { $ne: 'SUCCEEDED' }
						}, {
							status: PaymentRequestConstant.STATUS.CANCELED,
							reason: 'Payment đã hết hạn'
						}]);
					}
				} catch (error) {
					ctx.broker.logger.warn(`Update expired PaymentRequest error:${error}`);
					return {
						message: error.message
					};
				}

				return null;
			}, '', 3);
		}

		return null;
	} catch (e) {
		ctx.broker.logger.info('[CRON] -> [PAYME_PAGE] -> [UPDATE_PAYMENT] -> [EXCEPTION] FAILED');
	}
};
