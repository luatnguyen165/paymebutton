/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');
const { updatePaymentRequest } = require('../services/prepareData');
const paymentRequestConstant = require('../../paymentRequestModel/constant/paymentRequest.constant');

const validPreStatus = {
	SUCCEEDED: [PaymentRequestConst.STATUS.PENDING, PaymentRequestConst.STATUS.CANCELED],
	EXPIRED: [PaymentRequestConst.STATUS.PENDING],
	REFUNDED: [PaymentRequestConst.STATUS.SUCCEEDED, PaymentRequestConst.STATUS.REFUNDED],
	CANCELED_SUCCEEDED: [PaymentRequestConst.STATUS.SUCCEEDED]
};

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		ctx.broker.logger.info(`IPN PaymePage at ${new Date()}, data: ${JSON.stringify(payload)}`);
		const {
			accountId,
			transaction,
			partnerTransaction,
			amount,
			fee,
			total,
			state,
			desc,
			extraData,
			createdAt,
			updatedAt
		} = payload;

		const paymentRequestFound = await this.broker.call('v1.paymentRequestModel.findOne', [{
			orderId: transaction,
			// transactionId: partnerTransaction,
			referType: PaymentRequestConst.REFER_TYPE.PAYME_PAGE
		}]);

		if (!paymentRequestFound) {
			const { code, message } = RespConst.CALLBACK.NOT_FOUND;
			return { code, message: this.__(message) };
		}
		const validStatus = validPreStatus[state];
		if (_.isArray(validStatus) && !validStatus.includes(paymentRequestFound.status)) {
			ctx.broker.logger.warn(`IPN status: ${state}. Valid status ${validStatus}, but found ${paymentRequestFound.status}`);
			const { code, message } = RespConst.CALLBACK.INVALID_STATUS;
			return { code, message: this.__(message) };
		}
		const pageId = paymentRequestFound.referId;
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId },
				{ 'shortLinkInfo.slug': pageId }
			]
		}]);
		if (accountId !== _.get(pageInfo, 'creator.accountId', null)) {
			const { code, message } = RespConst.CALLBACK.INVALID_ACCOUNT;
			return { code, message: this.__(message) };
		}
		const resultUpdate = await updatePaymentRequest(payload, pageInfo, paymentRequestFound, ctx);
		ctx.broker.logger.info(`PaymePage Receive Callback updatePaymentRequest res = ${JSON.stringify(resultUpdate)}`);
		if (!resultUpdate.isSucceed) {
			const { code } = RespConst.CALLBACK.FAIL;
			return { code, message: this.__(resultUpdate.message) };
		}
		const { code, message } = RespConst.CALLBACK.SUCCESS;
		return { code, message: this.__(message) };
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Receive Callback Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
