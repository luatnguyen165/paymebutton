const _ = require('lodash');
const Async = require('async');
const AwaitAsyncForEach = require('await-async-foreach');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');
const GeneralConstants = require('../constants/general.constant');
const { updatePaymentRequest } = require('../services/prepareData');

module.exports = async function (ctx) {
	try {
		let paymentPending = [];
		try {
			paymentPending = await this.broker.call('v1.paymentRequestModel.findMany', [
				{
					referType: 'PAYME_PAGE',
					status: 'PENDING'
				}, 'id referId transactionId paymentItems customerDetails status paymentAmount totalRefund',
				{ sort: { _id: -1 }, skip: 0, limit: 20 }]);
		} catch (err) {
			this.logger.info('[CRON] -> [PAYME_PAGE] -> [INQUIRY_PAYMENT] -> [EXCEPTION] -> ', err);
		// ctx.broker.logger.warn(`[PAYME_PAGE] Cron inquiredPaymentRequest >> Get pending order error: ${err}`);
		}

		if (paymentPending.length > 0) {
			await AwaitAsyncForEach(paymentPending, async (task) => {
				try {
					// ctx.broker.logger.info(`Cron inquiredPaymentRequest >> Data: ${JSON.stringify(task)}`);
					const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{ pageId: task.referId }, 'creator pageId paymentDetail']);
					const security = {
						credentials: {
							accountId: pageInfo.creator.accountId
						}
					};
					const orderQueryResult = await this.broker.call('v1.order.query', {
						body: {
							partnerTransaction: _.get(task, 'transactionId', ''),
						}
					}, { meta: { security } });
					ctx.broker.logger.info(`[PAYME_PAGE] Cron inquiredPaymentRequest >> orderQueryResult: ${JSON.stringify(orderQueryResult)}`);
					if (_.get(orderQueryResult, 'code', 0) === 105002) {
						const resultUpdate = await updatePaymentRequest(orderQueryResult, pageInfo, task, ctx);
						ctx.broker.logger.info(`[PAYME_PAGE] Cron inquiredPaymentRequest >> resultUpdate: ${JSON.stringify(resultUpdate)}`);
					} else {
						this.logger.info('[CRON] -> [PAYME_PAGE] -> [INQUIRY_PAYMENT] -> [UPDATE_FAILED] ->', orderQueryResult);
						ctx.broker.logger.info('[PAYME_PAGE] Cron inquiredPaymentRequest CTT response FAILED');
					}
				} catch (error) {
					ctx.broker.logger.warn(`[PAYME_PAGE] Cron inquiredPaymentRequest error: ${error}`);
					return {
						message: error.message
					};
				}

				return null;
			}, 'parallel', 3);
		}

		// if (paymentPending.length > 0) {
		// 	POOL.push(paymentPending);
		// }
		// POOL.drain = function () {
		// 	console.log('[PAYME_PAGE] UpdateLinkStatus all items have been processed');
		// };
		// return null;
		return null;
	} catch (e) {
		ctx.broker.logger.info('[CRON] -> [PAYME_PAGE] -> [inquiredPaymentRequest] FAILED');
	}
};
