const _ = require('lodash');
const moment = require('moment');
const { MoleculerError } = require('moleculer').Errors;
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const PageConst = require('../constants/paymePage.constant');

const pageLinkUrl = process.env.PAGE_URL || 'https://page.payme.net.vn';

module.exports = async function (ctx) {
	try {
		const payload = ctx.params.body;
		const { pageId, title, status } = payload.filter;
		const auth = _.get(ctx, 'meta.auth', {});

		const filter = {
			...status && { status },
		};
		if (pageId) {
			filter.$or = [
				{ pageId },
				{ 'shortLinkInfo.slug': pageId }
			];
		}
		if (title) {
			filter.$text = { $search: title };
		}
		if (_.get(payload.filter, 'createdAt', false) !== false) {
			const queryByDate = {};
			if (payload.filter.createdAt.from) {
				queryByDate.$gte = new Date(payload.filter.createdAt.from);
			}
			if (payload.filter.createdAt.to) {
				queryByDate.$lte = new Date(payload.filter.createdAt.to);
			}
			if (_.isEmpty(queryByDate) === false) {
				filter.createdAt = queryByDate;
			}
		}
		const merchantInfo = _.get(ctx, 'prepareData.merchantInfo', {});
		if (_.get(merchantInfo, 'id', null) === null) {
			if (_.get(auth.credentials, 'merchantId', null) === null) {
				return {
					code: GeneralConstants.ResponseCode.CREATE_LINK_FAILED,
					message: this.__('Vui lòng cung cấp thông tin merchant')
				};
			}
		}
		if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
			filter['creator.merchantId'] = merchantInfo.id; // lấy all theo MC id
		} else {
			filter['creator.accountId'] = auth.credentials.accountId; // chỉ lấy theo account tạo link
		}

		this.broker.logger.info(`Page read filter by > ${JSON.stringify(filter)}`);

		try {
			let total = 0;
			const pageList = [];

			total = await this.broker.call('v1.paymePageModel.count', { query: filter });
			if (total > 0) {
				const result = await this.broker.call('v1.paymePageModel.find', {
					body: {
						query: filter,
						paging: payload.paging,
						sort: payload.sort,
						fields: '-_id pageId aliasPageName shortLinkInfo businessDetail customerDetail paymentDetail createdAt status totalRevenue actualRevenue voucherAllowance updatedAt isNotified totalSending vouchers tags recurringPayment'
					},
				});
				const tags = [];
				for (let i = 0; i < result.length; i += 1) {
					const page = result[i];
					const fields = _.get(page, 'paymentDetail.inputFields', []);
					let pageIdUuid = _.get(page, 'pageId', '');
					if (_.get(page, 'shortLinkInfo.slug', null) !== null) {
						pageIdUuid = page.shortLinkInfo.slug;
					}
					const fullLink = `${process.env.PAGE_URL_SHORT}/${pageIdUuid}`;
					const obj = {
						// id: _.get(page, 'id', ''),
						pageId: pageIdUuid,
						customerDetail: _.get(page, 'customerDetail', {}),
						pageUrl: fullLink,
						shortLink: fullLink,
						title: _.get(page, 'businessDetail.title', ''),
						currency: _.get(page, 'paymentDetail.currency', ''),
						priceFields: fields.map((field) => {
							const newfield = { ...field };
							newfield.group = PageConst.FIELD_GROUP.PRICE;
							newfield.isActive = !![undefined, '', null].includes(field.isActive);
							if (_.isArray(newfield.tags)) tags.push(...newfield.tags);
							return newfield;
						}),
						totalRevenue: _.get(page, 'actualRevenue', _.get(page, 'totalRevenue', 0)),
						status: _.get(page, 'status', ''),
						createdAt: page.createdAt,
						updatedAt: page.updatedAt,
						voucherAllowance: page.voucherAllowance || false,
						isNotified: page.isNotified,
						totalSending: {
							email: {
								success: _.get(page.totalSending, 'email.success', 0),
								totalSend: _.get(page.totalSending, 'email.totalSend', 0)
							},
							sms: {
								success: _.get(page.totalSending, 'sms.success', 0),
								totalSend: _.get(page.totalSending, 'sms.totalSend', 0)
							}
						},
						vouchers: _.get(page, 'vouchers', null),
						tags: _.isArray(page.tags) && page.tags.length > 0 ? page.tags : _.uniq(tags),
						recurringPayment: page.recurringPayment
					};
					pageList.push(obj);
				}
			}
			const { code, message } = RespConst.READ.SUCCESS;
			return {
				code,
				message: this.__(message),
				data: {
					total,
					items: pageList
				}
			};
		} catch (e) {
			ctx.broker.logger.warn(e);
			const { code, message } = RespConst.READ.FAIL;
			return { code, message };
		}
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Read Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
