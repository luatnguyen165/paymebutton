const _ = require('lodash');
const slugify = require('slugify');
const errorCodeConstant = require('../constants/errorCode.constant');

// eslint-disable-next-line consistent-return
module.exports = async function (ctx) {
	const payload = ctx.params.body;
	const Logger = this.logger.info;
	const response = {
		code: errorCodeConstant.SYSTEM_ERROR,
		message: this.__(_.toString(errorCodeConstant.SYSTEM_ERROR))
	};

	try {
		if (_.get(payload, 'pageId', null) === null) {
			response.message = 'pageId không được để trống. Vui lòng kiểm tra lại';
			return response;
		}
		// if (_.get(payload, 'alias', null) === null) {
		// 	response.message = 'alias còn trống vui lòng kiểm tra lại';
		// 	return response;
		// }
		if (_.get(payload, 'newPageId', null) === null) {
			response.message = 'newPageId còn trống vui lòng kiểm tra lại';
			return response;
		}
		const pageId = _.get(payload, 'pageId', null);
		const page = await this.broker.call('v1.paymePageModel.findOne', [{ pageId }]);

		Logger('[updateAlias] page ', JSON.stringify(page));
		if (_.isNull(page) === true) {
			response.message = 'pageId không tồn tại không thể update vui lòng kiểm tra lại';
			return response;
		}
		if (_.get(page, 'pageId', null) === null) {
			response.message = 'pageId không tìm thấy không thể update vui lòng kiểm tra lại';
			return response;
		}
		const alias = slugify(_.get(payload, 'newPageId', null) || ' ');
		const newPageId = _.get(payload, 'newPageId', '');
		// const existAlias = await this.broker.call('v1.paymePageModel.findOne', [{ 'shortLinkInfo.slug': `${alias}` }]);
		const existAlias = await this.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ 'shortLinkInfo.slug': `${alias}` },
				{ pageId: `${alias}` },
				{ alias: `${alias}` },
			]
		}]);
		if (existAlias) {
			response.message = 'alias đã tồn tại vui lòng chọn alias khác';
			return response;
		}
		const update = await this.broker.call('v1.paymePageModel.updateOne', [{ id: page.id }, {
			alias, 'shortLinkInfo.url': `${process.env.PAGE_URL_SHORT}/${newPageId}`, 'shortLinkInfo.slug': `${newPageId}`
		}]);
		Logger('[updateAlias] page ', JSON.stringify(update));
		if (update.nModified < 1 || !_.isObject(update) || !update) {
			return {
				code: errorCodeConstant.UPDATE_PAGE_ALIAS_FAILED,
				message: this.__(_.toString(errorCodeConstant.UPDATE_PAGE_ALIAS_FAILED))
			};
		}
		return {
			code: errorCodeConstant.UPDATE_PAGE_ALIAS_SUCCEDD,
			message: this.__(_.toString(errorCodeConstant.UPDATE_PAGE_ALIAS_SUCCEDD))
		};
	} catch (error) {
		Logger('[updateAlias]', error);
		if (error === 'MoleculerError') {
			response.code = error.code;
			response.message = error.message;
			return response;
		}
		return response;
	}
};
