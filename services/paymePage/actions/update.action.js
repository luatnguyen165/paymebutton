/* eslint-disable no-param-reassign */
const _ = require('lodash');
const moment = require('moment');
const GeneralConstants = require('../constants/general.constant');
const PageConst = require('../constants/paymePage.constant');
const RespConst = require('../constants/response.constant');
const PhoneHelper = require('../helpers/PhoneHelper');
const GenIdHelper = require('../helpers/GenIdHelper');

const validateBusinessDetail = (businessDetail, ctx) => {
	const result = {
		isValid: false,
		code: null,
		message: ctx.service.__('Thông tin không hợp lệ'),
		data: null
	};
	const {
		title,
		description,
		socialMediaShareIcon,
		phone,
		termAndCondition
	} = businessDetail;
	let formatedPhone;
	if (businessDetail.phone) {
		const phoneHelper = new PhoneHelper();
		const checkPhoneResult = phoneHelper.validatePhone(businessDetail.phone);
		if (!checkPhoneResult.isValid) {
			const { code, message } = RespConst.UPDATE.INVALID_PHONE;
			result.code = code;
			result.message = ctx.service.__(message);
			return result;
		}
		formatedPhone = checkPhoneResult.phone;
	}
	const formated = {
		...title != null && { 'businessDetail.title': title },
		...description != null && { 'businessDetail.description': description },
		...socialMediaShareIcon != null && { 'businessDetail.socialMediaShareIcon': socialMediaShareIcon },
		...phone != null && { 'businessDetail.phone': formatedPhone },
		...termAndCondition != null && { 'businessDetail.termAndCondition': termAndCondition },
	};
	return {
		isValid: true,
		message: ctx.service.__('Thông tin hợp lệ'),
		data: formated
	};
};

const validatePaymentDetail = async function (paymentDetail, dbPaymentDetail, ctx) {
	const result = {
		isValid: false,
		code: null,
		message: ctx.service.__('Thông tin không hợp lệ'),
		data: null
	};
	const { inputFields, currency, labelPay } = paymentDetail;
	let countPriceField = 0;
	const formartedFields = [];
	const dbFields = dbPaymentDetail.inputFields;
	const productIds = [];
	inputFields.forEach((field) => {
		if (!field.productId) {
			field.productId = GenIdHelper.generateChar(6).toString();
		}
		productIds.push(field.productId);
	});
	const uniqueProductId = [...new Set(productIds)];
	if (uniqueProductId.length !== productIds.length) {
		const { code, message } = RespConst.CREATE.INVALID_PRODUCTID;
		result.code = code;
		result.message = ctx.service.__(message);
		return result;
	}
	if (_.isEmpty(inputFields) === false) {
		const { credentials = {} } = _.get(ctx, 'meta.auth', {});
		const tagsName = _.uniq(...inputFields.map((field) => field.tags).filter((v) => v));
		const existedTagsName = await ctx.broker.call('v1.productTagModel.find', {
			body: {
				query: {
					merchantId: credentials.merchantId,
				},
				fields: '-_id id title',
			},
		});
		const notExistTagsName = _.difference(tagsName.map((field) => field.toLowerCase()), existedTagsName.map((v) => v.title.toLowerCase()));
		if (notExistTagsName.length > 0) {
			const tagCreate = [];
			notExistTagsName.forEach((element) => {
				const tag = ctx.broker.call('v1.productTagModel.create', [{
					merchantId: credentials.merchantId,
					accountId: credentials.accountId,
					title: element,
				}]);
				tagCreate.push(tag);
			});
			if (tagCreate.length > 0) await Promise.all(tagCreate);
		}
		for (let i = 0; i < inputFields.length; i += 1) {
			const field = inputFields[i];
			const id = _.get(field, 'productId', GenIdHelper.generateChar(6).toString());
			const foundField = dbFields.find((dbField) => dbField.id === id);
			let isNewField = true;
			if (_.isEmpty(foundField) === false) {
				isNewField = false;
			}
			let options = {};
			if (!field.group || field.group === PageConst.FIELD_GROUP.PRICE) {
				if (_.get(field, 'options.unitsAvailable') === PageConst.UNITSAVAILABLE.LIMITED) {
					if (_.get(field, 'options.unitInStock', 0) <= 0) {
						const { code, message } = RespConst.UPDATE.INVALID_STOCK;
						result.code = code;
						result.message = ctx.service.__(message, { key: field.label });
						return result;
					}
					// @rule uintInStock > 0. do not care itemSold
					// const itemSold = _.get(foundField, 'options.itemSold', 0);
					// if (!isNewField && _.get(field, 'options.unitInStock', 0) < itemSold) {
					// 	const { code, message } = RespConst.UPDATE.INVALID_MIN_STOCK;
					// 	result.code = code;
					// 	result.message = ctx.service.__(message, { key: field.label });
					// 	return result;
					// }
				}
				if (field.inputType === PageConst.PRICE_FIELD_TYPE.FIXED_AMOUNT) {
					if (!_.get(field, 'options.fixedAmount', null)) {
						const { code, message } = RespConst.UPDATE.MISS_AMOUNT;
						result.code = code;
						result.message = ctx.service.__(message, { key: field.label });
						return result;
					}
					options = {
						fixedAmount: _.get(field, 'options.fixedAmount', 0),
						unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
						unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
							? field.options.unitInStock : null,
						quantity: {}
					};
				} else if (field.inputType === PageConst.PRICE_FIELD_TYPE.CUSTOMER_DECIDE_AMOUNT) {
					options = {
						fixedAmount: null,
						unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
						unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
							? field.options.unitInStock : null,
						quantity: {
							min: _.get(field, 'options.quantity.min', null),
							max: _.get(field, 'options.quantity.max', null)
						}
					};
				} else if (field.inputType === PageConst.PRICE_FIELD_TYPE.ITEM_WITH_QUANTITY) {
					if (!_.get(field, 'options.fixedAmount', null)) {
						const { code, message } = RespConst.UPDATE.MISS_AMOUNT;
						result.code = code;
						result.message = ctx.service.__(message, { key: field.label });
						return result;
					}
					options = {
						fixedAmount: _.get(field, 'options.fixedAmount', 0),
						unitsAvailable: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED),
						unitInStock: _.get(field, 'options.unitsAvailable', PageConst.UNITSAVAILABLE.UNLIMITED) === PageConst.UNITSAVAILABLE.LIMITED
							? field.options.unitInStock : null,
						quantity: {
							min: _.get(field, 'options.quantity.min', null),
							max: _.get(field, 'options.quantity.max', null)
						}
					};
				} else {
					const { code, message } = RespConst.UPDATE.INVALID_FIELD_TYPE;
					result.code = code;
					result.message = ctx.service.__(message, { key: field.label });
					return result;
				}
				options = {
					...options,
					itemSold: isNewField ? 0 : _.get(foundField, 'options.itemSold', 0),
					itemInOrder: isNewField ? 0 : _.get(foundField, 'options.itemInOrder', 0),
					revenue: isNewField ? 0 : _.get(foundField, 'options.revenue', 0)
				};
				const formatedPriceField = {
					id,
					...field.group && { group: _.get(field, 'group', '') },
					inputType: _.get(field, 'inputType', ''),
					label: _.get(field, 'label', ''),
					image: _.get(field, 'image', null),
					description: _.get(field, 'description', ''),
					isRequired: _.get(field, 'isRequired', false),
					options,
					isActive: _.isBoolean(field.isActive) ? field.isActive : foundField.isActive,
					tags: field.tags
				};
				countPriceField += 1;
				formartedFields.push(formatedPriceField);
			}
			// else {
			// 	if (!_.values(PageConst.INPUT_FIELD_TYPE).includes(field.inputType)) {
			// 		const { code, message } = RespConst.UPDATE.INVALID_FIELD_TYPE;
			// 		result.code = code;
			// 		result.message = RespConst.replaceMsg(message, [field.label]);
			// 		return result;
			// 	}
			// 	const formatedInputField = {
			// 		id,
			// 		group: _.get(field, 'group', ''),
			// 		inputType: _.get(field, 'inputType', ''),
			// 		label: _.get(field, 'label', ''),
			// 		image: _.get(field, 'image', null),
			// 		description: _.get(field, 'description', ''),
			// 		isRequired: _.get(field, 'isRequired', false),
			// 		options
			// 	};
			// 	formartedFields.push(formatedInputField);
			// }
		}

		if (countPriceField === 0) {
			const { code, message } = RespConst.UPDATE.MISS_PRICE_FIELD;
			result.code = code;
			result.message = ctx.service.__(message);
			return result;
		}
	}

	const formated = {
		...currency != null && { 'paymentDetail.currency': currency },
		...labelPay != null && { 'paymentDetail.labelPay': labelPay },
		...inputFields != null && { 'paymentDetail.inputFields': formartedFields },
	};
	return {
		isValid: true,
		message: ctx.service.__('Thông tin hợp lệ'),
		data: formated
	};
};

const validateInputField = async (fieldFound, fieldIndex, data, ctx) => {
	const result = {
		isValid: false,
		code: null,
		message: ctx.service.__('Thông tin không hợp lệ'),
		data: null
	};
	fieldFound.options = fieldFound.options || {};
	const prefix = `paymentDetail.inputFields.${fieldIndex}.`;

	const {
		label, inputType, image, description, isRequired, isActive, options, tags
	} = data;

	const dataToUpdate = {};

	if (tags) {
		const { credentials = {} } = _.get(ctx, 'meta.auth', {});
		const existedTagsName = await ctx.broker.call('v1.productTagModel.find', {
			body: {
				query: {
					merchantId: credentials.merchantId,
				},
				fields: '-_id id title',
			},
		});
		const notExistTagsName = _.difference(tags.map((field) => field.toLowerCase()), existedTagsName.map((v) => v.title.toLowerCase()));
		if (notExistTagsName.length > 0) {
			const tagCreate = [];
			notExistTagsName.forEach((element) => {
				const tag = ctx.broker.call('v1.productTagModel.create', [{
					merchantId: credentials.merchantId,
					accountId: credentials.accountId,
					title: element,
				}]);
				tagCreate.push(tag);
			});
			if (tagCreate.length > 0) await Promise.all(tagCreate);
		}
		dataToUpdate.tags = tags;
	}

	if (_.isBoolean(isActive)) dataToUpdate[`${prefix}isActive`] = isActive;
	if (label != null) { dataToUpdate[`${prefix}label`] = label; fieldFound.label = label; }
	if (image != null) dataToUpdate[`${prefix}image`] = image;
	if (description != null) dataToUpdate[`${prefix}description`] = description;
	if (isRequired != null) dataToUpdate[`${prefix}isRequired`] = isRequired;
	if (inputType != null) {
		fieldFound.inputType = inputType;
		dataToUpdate[`${prefix}inputType`] = inputType;
	}
	if (_.isEmpty(options) === false) {
		// if (options.itemSold !== undefined) {
		// 	fieldFound.options.itemSold = options.itemSold;
		// 	dataToUpdate[`${prefix}options.itemSold`] = options.itemSold;
		// }
		// if (options.itemInOrder !== undefined) {
		// 	fieldFound.options.itemInOrder = options.itemInOrder;
		// 	dataToUpdate[`${prefix}options.itemInOrder`] = options.itemInOrder;
		// }
		if (options.unitsAvailable) {
			fieldFound.options.unitsAvailable = options.unitsAvailable;
			dataToUpdate[`${prefix}options.unitsAvailable`] = options.unitsAvailable;
		}
		if (options.unitInStock !== undefined) {
			fieldFound.options.unitInStock = options.unitInStock;
			dataToUpdate[`${prefix}options.unitInStock`] = options.unitInStock;
		}
		if (fieldFound.options.unitsAvailable === PageConst.UNITSAVAILABLE.LIMITED) {
			if (fieldFound.options.unitInStock <= 0) {
				const { code, message } = RespConst.UPDATE.INVALID_STOCK;
				result.code = code;
				result.message = ctx.service.__(message, { key: fieldFound.label });
				return result;
			}
			// @rule uintInStock > 0. do not care itemSold
			// const itemSold = _.get(fieldFound, 'options.itemSold', 0);
			// if (_.get(fieldFound, 'options.unitInStock', 0) < itemSold) {
			// 	const { code, message } = RespConst.UPDATE.INVALID_MIN_STOCK;
			// 	result.code = code;
			// 	result.message = ctx.service.__(message);
			// 	return result;
			// }
		}
		if (fieldFound.inputType === PageConst.PRICE_FIELD_TYPE.FIXED_AMOUNT) {
			if (options.fixedAmount !== undefined) {
				dataToUpdate[`${prefix}options.fixedAmount`] = options.fixedAmount;
			}
		} else if (fieldFound.inputType === PageConst.PRICE_FIELD_TYPE.CUSTOMER_DECIDE_AMOUNT) {
			if (options.quantity && options.quantity.min !== undefined) {
				dataToUpdate[`${prefix}options.quantity.min`] = options.quantity.min;
			}
			if (options.quantity && options.quantity.max !== undefined) {
				dataToUpdate[`${prefix}options.quantity.max`] = options.quantity.max;
			}
		} else if (fieldFound.inputType === PageConst.PRICE_FIELD_TYPE.ITEM_WITH_QUANTITY) {
			if (options.fixedAmount !== undefined) {
				dataToUpdate[`${prefix}options.fixedAmount`] = options.fixedAmount;
			}
			if (options.quantity && options.quantity.min !== undefined) {
				dataToUpdate[`${prefix}options.quantity.min`] = options.quantity.min;
			}
			if (options.quantity && options.quantity.max !== undefined) {
				dataToUpdate[`${prefix}options.quantity.max`] = options.quantity.max;
			}
		}
	}
	return {
		isValid: true,
		message: ctx.service.__('Thông tin hợp lệ'),
		data: _.omitBy(dataToUpdate, _.isNil)
	};
};

module.exports = {
	// TODO: update pageTitle in paymeSubcriptionModel when user update title page
	async updatePage(ctx) {
		try {
			const payload = ctx.params.body;
			const { pageId } = ctx.params.params;
			const filter = {
				$or: [
					{ pageId },
					{ 'shortLinkInfo.slug': pageId }
				]
			};
			const auth = _.get(ctx, 'meta.auth', {});
			if (_.get(ctx, 'prepareData.isOwnerOrAdmin', false) === true) {
				filter['creator.merchantId'] = auth.credentials.merchantId; // lấy all theo MC id
			} else {
				filter['creator.accountId'] = auth.credentials.accountId; // chỉ lấy theo account tạo link
			}
			const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [filter]);
			if (!pageInfo) {
				const { code, message } = RespConst.UPDATE.NOT_FOUND;
				return { code, message: this.__(message) };
			}

			const {
				businessDetail,
				paymentDetail,
				status,
				theme,
				sucessMsg,
				successRedirect,
				customerRequiredField,
				customerDetail,
			} = payload;

			let dataToUpdate = {
				...status != null && { status },
				...theme != null && { theme },
				...sucessMsg != null && { sucessMsg },
				...successRedirect != null && { successRedirect }
			};

			if (businessDetail) {
				const checkBusinessResult = validateBusinessDetail(businessDetail, ctx);
				if (checkBusinessResult.isValid === false) {
					const { code, message } = checkBusinessResult;
					return { code, message };
				}
				dataToUpdate = { ...dataToUpdate, ...checkBusinessResult.data };
			}
			if (paymentDetail) {
				const checkPaymentResult = await validatePaymentDetail(paymentDetail, pageInfo.paymentDetail, ctx);
				if (checkPaymentResult.isValid === false) {
					const { code, message } = checkPaymentResult;
					return { code, message };
				}
				dataToUpdate = { ...dataToUpdate, ...checkPaymentResult.data };
			}
			if (customerRequiredField) {
				const {
					email, fullname, phone, shippingAddress
				} = customerRequiredField;
				const formated = {
					...email != null && { 'customerRequiredField.email': email },
					...fullname != null && { 'customerRequiredField.fullname': fullname },
					...phone != null && { 'customerRequiredField.phone': phone },
					...shippingAddress != null && { 'customerRequiredField.shippingAddress': shippingAddress },
				};
				dataToUpdate = { ...dataToUpdate, ...formated };
			}
			if (payload.expireIn !== undefined) { // if update expireIn
				if (payload.expireIn !== null && moment(payload.expireIn).isBefore(moment().add(15, 'minutes'))) { // update to time not null
					const { code, message } = RespConst.UPDATE.INVALID_EXPIRE_TIME;
					return { code, message: this.__(message) };
				}
				dataToUpdate.expireIn = payload.expireIn;
			}
			if (payload.notes) {
				const notes = payload.notes.map((note) => ({
					key: note.key,
					value: note.value
				}));
				dataToUpdate.notes = notes;
			}
			dataToUpdate.customerDetail = customerDetail;
			if (_.isBoolean(payload.voucherAllowance)) dataToUpdate.voucherAllowance = payload.voucherAllowance;
			dataToUpdate.vouchers = payload.vouchers || [];
			if (payload.tags) dataToUpdate.tags = payload.tags;

			ctx.broker.logger.info(`PaymePage Update dataToUpdate: ${JSON.stringify(dataToUpdate)}`);
			const updated = await ctx.broker.call('v1.paymePageModel.updateOne', [
				{
					_id: pageInfo._id
				}, {
					...dataToUpdate
				}]);
			const { code, message } = RespConst.UPDATE.SUCCESS;
			return { code, message: this.__(message) };
		} catch (err) {
			ctx.broker.logger.error(`PaymePage Update Error: ${err.message}`);
			return {
				code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
				message: this.__('Internal Server Error')
			};
		}
	},
	async updateInputField(ctx) {
		try {
			const payload = ctx.params.body;
			const { pageId, fieldId, data } = payload;

			const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
				$or: [
					{ pageId },
					{ 'shortLinkInfo.slug': pageId }
				]
			}]);
			if (!pageInfo) {
				const { code, message } = RespConst.UPDATE.NOT_FOUND;
				return { code, message: this.__(message, { key: pageId }) };
			}
			if (_.get(pageInfo, 'creator.accountId', null) !== _.get(ctx, 'meta.auth.credentials.accountId', null)) {
				const { code, message } = RespConst.UPDATE.NO_PERMISSION;
				return { code, message: this.__(message) };
			}
			const inputFields = _.get(pageInfo, 'paymentDetail.inputFields', []);
			const fieldFound = inputFields.find((field) => field.id === fieldId);
			if (!fieldFound) {
				const { code, message } = RespConst.UPDATE.FIELD_NOT_FOUND;
				return { code, message: this.__(message, { key: fieldId }) };
			}
			const fieldIndex = inputFields.findIndex((field) => field.id === fieldId);
			let dataToUpdate = {};
			const checkInputField = await validateInputField(fieldFound, fieldIndex, data, ctx);
			if (checkInputField.isValid === false) {
				const { code, message } = checkInputField;
				return { code, message };
			}
			dataToUpdate = checkInputField.data;
			console.log(checkInputField);

			const updated = await ctx.broker.call('v1.paymePageModel.updateOne',
				[{
					_id: pageInfo._id
				},
				{
					...dataToUpdate
				}]);
			const { code, message } = RespConst.UPDATE.SUCCESS;
			return { code, message: this.__(message) };
		} catch (err) {
			ctx.broker.logger.error(`PaymePage Update InputFields Error: ${err.message}`);
			return {
				code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
				message: this.__('Internal Server Error')
			};
		}
	}
};
