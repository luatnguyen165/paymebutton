const QRCode = require('qrcode');
const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');

module.exports = async function (ctx) {
	try {
		ctx.broker.logger.info(ctx.params);
		const payload = ctx.params.body;
		let resMessage = 'Email và SMS';
		if (payload.method.length === 1 && payload.method[0] === 'SMS') resMessage = 'SMS';
		if (payload.method.length === 1 && payload.method[0] === 'EMAIL') resMessage = 'Email';

		const result = {
			code: RespConst.NOTIFY.FAIL.code,
			message: this.__(`Gửi ${resMessage} thất bại`)
		};

		const paymePage = await this.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: payload.pageId },
				{ 'shortLinkInfo.slug': payload.pageId }
			]
		}]);

		if (!paymePage && _.get(paymePage, 'id', false) === false) {
			result.code = RespConst.NOTIFY.PAGE_NOT_FOUND.code;
			result.message = this.__(RespConst.NOTIFY.PAGE_NOT_FOUND.message);
			return result;
		}

		// if (_.get(paymePage, 'creator.accountId', null) !== _.get(ctx, 'meta.auth.credentials.accountId', null)) {
		// 	result.code = RespConst.NOTIFY.NO_PERMISSION.code;
		// 	result.message = this.__(RespConst.NOTIFY.NO_PERMISSION.message);
		// 	return result;
		// }
		const { credentials = {}, data = {} } = _.get(ctx, 'meta.auth', {});
		// let generateQR;
		// try {
		// 	generateQR = await QRCode.toDataURL(_.get(paymePage, 'shortLinkInfo.url', ''));
		// } catch (error) {
		// 	ctx.broker.logger.warn(`[SendEmail] tạo QRCode thất bại + ${error}`);
		// }
		let merchantFullInfo = null;
		try {
			merchantFullInfo = await this.broker.call('v1.settingsDashboard.internalSettings', { merchantId: credentials.merchantId });
		} catch (e) {
			return {
				code: RespConst.NOTIFY.FAIL.code,
				message: this.__('Không tìm thấy thông tin merchant!')
			};
		}
		if (merchantFullInfo.code !== 114100) {
			ctx.broker.logger.warn(`send email to MC-- merchantId:  ${credentials.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			return {
				code: RespConst.NOTIFY.FAIL.code,
				message: this.__('Không tìm thấy thông tin merchant')
			};
		}
		merchantFullInfo = merchantFullInfo.data;
		const dataSendMail = {
			meta: {
				merchantId: credentials.merchantId,
				accountId: credentials.accountId,
				chanel: payload.chanel,
				target: payload.target,
				method: payload.method,
				service: 'PAYME_PAGE',
				extraData: { referId: paymePage.pageId },
				brokerCall: 'v1.paymePage.receiveCallbackSendEmailAndSMS',
				template: {
					email: 'merchant-payme-page'
				},
				content: {
					mail: {
						subject: 'PayME - Đơn hàng mới',
						content: {
							merchantLogo: _.get(merchantFullInfo, 'logo', ''),
							merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
							// username: _.get(data, 'username', ''),
							orderUrl: `${process.env.PAGE_URL_SHORT}/${paymePage.pageId}`,
							qrImage: `https://t.payme.vn/qr?t=${process.env.PAGE_URL_SHORT}/${paymePage.pageId}`,
						}
					}
				}
			}
		};
		if (_.includes(payload.method, 'SMS')) {
			dataSendMail.meta.template.sms = 'default-sms';
			dataSendMail.meta.content.sms = {
				content: {
					data: `${merchantFullInfo.brandName || merchantFullInfo.fullName} gui quy khach thong tin dat hang, vui long truy cap ${process.env.PAGE_URL_SHORT}/${paymePage.pageId} de hoan tat dat hang va thanh toan. Xin cam on!`
				}
			};
		}
		try {
			const sendEmailAndSMSRes = await this.broker.call('v1.customer.sendEmailAndSMS', {}, dataSendMail);
			this.broker.logger.info(`PayMEPage > sendEmailAndSMS > broker v1.customer.sendEmailAndSMS > ${JSON.stringify({ sendEmailAndSMSRes, dataSendMail })}`);
		} catch (e) {
			ctx.broker.logger.warn(`PayMEPage > sendEmailAndSMS > broker v1.customer.sendEmailAndSMS > Error > ${e.message}, data: ${JSON.stringify(dataSendMail)}`);
			return result;
		}
		await this.broker.call('v1.paymePageModel.updateOne', [
			{ id: paymePage.id },
			{
				isNotified: {
					email: _.get(paymePage, 'isNotified.email', false) || payload.method.includes('EMAIL'),
					sms: _.get(paymePage, 'isNotified.sms', false) || payload.method.includes('SMS')
				}
			}
		]);
		result.code = RespConst.NOTIFY.SUCCESS.code;
		result.message = this.__(`Đã gửi ${resMessage}`);
		return result;
	} catch (err) {
		ctx.broker.logger.error(`PayMEPage > sendEmailAndSMS > Error > ${err.message}`);
		return {
			code: GeneralConstants.ResponseCode.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
