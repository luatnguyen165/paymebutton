/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');

module.exports = async function (ctx) {
	try {
		const payload = ctx.params;
		ctx.broker.logger.info(`EMAIL/SMS IPN PaymePage, data: ${JSON.stringify(payload)}`);

		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: payload.pageId },
				{ 'shortLinkInfo.slug': payload.pageId }
			]
		}]);
		if (!pageInfo) {
			const { code } = RespConst.CALLBACK.FAIL;
			return { code, message: this.__('Không tìm thấy pageId') };
		}
		await this.broker.call('v1.paymePageModel.updateOne', [
			{
				_id: pageInfo._id,
			},
			{
				$inc: {
					'totalSending.email.success': _.get(payload, 'email.totalSuccess', 0),
					'totalSending.email.totalSend': _.toNumber(_.get(payload, 'email.totalSuccess', 0)) + _.toNumber(_.get(payload, 'email.totalFail', 0)),
					'totalSending.sms.success': _.get(payload, 'sms.totalSuccess', 0),
					'totalSending.sms.totalSend': _.toNumber(_.get(payload, 'sms.totalSuccess', 0)) + _.toNumber(_.get(payload, 'sms.totalFail', 0))
				}
			}]);

		const { code, message } = RespConst.CALLBACK.SUCCESS;
		return { code, message: this.__(message) };
	} catch (err) {
		ctx.broker.logger.error(`PaymePage Receive Callback EMAIL/SMS Error: ${err.message}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
