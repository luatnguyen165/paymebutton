/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-syntax */
const _ = require('lodash');
const GeneralConstants = require('../constants/general.constant');
const RespConst = require('../constants/response.constant');
const { updatePaymentRequest } = require('../services/prepareData');

module.exports = async function (ctx) {
	try {
		const language = ctx.params.query.language || 'vi';
		this.setLocale(language);

		ctx.broker.logger.info(`PAGE get payment result payload...:  ${JSON.stringify(ctx.params.params)}`);

		let paymentRequest = await this.broker.call('v1.paymentRequestModel.findOne', [{ transactionId: ctx.params.params.transactionId }]);
		if (_.get(paymentRequest, 'id', null) === null) {
			const { code, message } = RespConst.CREATE_ORDER.GET_PAYMENT_INFORMATION_FAILED;
			return { code, message: this.__(message) };
		}
		const pageInfo = await ctx.broker.call('v1.paymePageModel.findOne', [{
			$or: [
				{ pageId: paymentRequest.referId },
				{ 'shortLinkInfo.slug': paymentRequest.referId },
			],
		}]);
		if (paymentRequest.status === 'PENDING') {
			const security = {
				credentials: {
					accountId: pageInfo.creator.accountId
				}
			};
			const orderQueryResult = await this.broker.call('v1.order.query', {
				body: {
					partnerTransaction: _.get(paymentRequest, 'transactionId', ''),
				}
			}, { meta: { security } });
			ctx.broker.logger.info(`[PAYME_PAGE]   get payment result >> orderQueryResult: ${JSON.stringify(orderQueryResult)}`);

			if (_.get(orderQueryResult, 'code', 0) === 105002) {
				const resultUpdate = await updatePaymentRequest(orderQueryResult, pageInfo, paymentRequest, ctx);
				ctx.broker.logger.info(`[PAYME_PAGE]   get payment result >> resultUpdate: ${JSON.stringify(resultUpdate)}`);
				if (_.get(resultUpdate, 'isSucceed', false) === true) {
					paymentRequest = resultUpdate.paymentRequest;
				}
			} else {
				ctx.broker.logger.info('[PAYME_PAGE]  get payment result CTT response FAILED');
			}
		}

		const getMethodParams = {
			accountId: pageInfo.creator.accountId,
			language: _.get(paymentRequest.transactionInfo, 'language', 'vi')
		};
		let paymentMethods = [];
		try {
			const resultGetPaymentMethod = await this.broker.call('v1.payment.getMethod', {
				body: getMethodParams
			});
			ctx.broker.logger.info(`[PAYME_PAGE]  get payment result  call v1.payment.getMethod, payload = ${JSON.stringify(getMethodParams)} --- response: ${JSON.stringify(paymentMethods)}`);
			paymentMethods = _.get(resultGetPaymentMethod, 'data', []);
		} catch (error) {
			ctx.broker.logger.info(`[PAYME_PAGE]  get payment result  call v1.payment.getMethod FAIL, payload = ${JSON.stringify(getMethodParams)} --- error: ${error.message}`);
		}
		const paymentMethod = _.isEmpty(paymentMethods) === false && paymentMethods.find(
			(method) => method.methodId === paymentRequest.paymentMethod.methodId
				&& method.payCode === paymentRequest.paymentMethod.payCode
		);
		ctx.broker.logger.info(`[PAYME_PAGE]  get payment result > GET PAYMENT METHOD INFO ${JSON.stringify({ paymentMethods, paymentMethod })}`);
		const { code, message } = RespConst.CREATE_ORDER.GET_PAYMENT_INFORMATION_SUCCEEDED;
		return {
			code,
			message: this.__(message),
			data: {
				status: paymentRequest.status,
				orderId: paymentRequest.orderId,
				finishedAt: paymentRequest.finishedAt || paymentRequest.updatedAt,
				paymentMethod: paymentMethod || paymentRequest.paymentMethod,
				customerDetail: paymentRequest.customerDetails.filter((v) => v.value),
				note: paymentRequest.note,
				amount: paymentRequest.amount,
				discountAmount: paymentRequest.discountAmount,
				paymentAmount: paymentRequest.paymentAmount,
				paymentItems: paymentRequest.paymentItems,
				voucherInfo: paymentRequest.voucherInfo,
				id: paymentRequest.id,
				transactionId: paymentRequest.transactionId,
				paymentId: paymentRequest.ipnInfo && _.isObject(JSON.parse(paymentRequest.ipnInfo)) ? _.get(JSON.parse(paymentRequest.ipnInfo), 'paymentId', paymentRequest.paymentId) : paymentRequest.paymentId,
				language: _.get(paymentRequest.transactionInfo, 'language', 'vi')
			}
		};
	} catch (err) {
		ctx.broker.logger.error(`PaymePage get payment result Error: ${err}`);
		return {
			code: GeneralConstants.RESPONSE_CODE.SYSTEM_ERROR,
			message: this.__('Internal Server Error')
		};
	}
};
