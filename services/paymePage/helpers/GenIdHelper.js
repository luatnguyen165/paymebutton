// const _ = require('lodash');

const genegate = (chars, length) => {
	let s = '';
	for (let i = 0; i < length; i += 1) {
		const pos = (Math.floor(Math.random() * Math.floor(chars.length)));
		s += chars[pos];
	}
	return s.toLowerCase().toString();
};

const generateChar = (length) => {
	const chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
		'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z'];
	return genegate(chars, length);
};
const genReferenceIdNumber = (length = 6) => {
	const chars = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
	return genegate(chars, length);
};
module.exports = {
	generateChar,
	genReferenceIdNumber
};
