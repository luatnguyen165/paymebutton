module.exports = {
	RESPONSE_CODE: {
		SYSTEM_ERROR: 500,
		INVALID_PARAMS: 400,
		INVALID_TOKEN: 401,
		REQUEST_SUCCESS: 1000,
		REQUEST_FAIL: 1001,
	},
	CREATE_ORDER_SUCCESS_CODE: 105000,
	GET_MC_INFO_SUCCESS_CODE: 114100,
	SCOPE: {
		CONDITION: {
			AND: 'AND',
			OR: 'OR'
		},
		CODE: {
			CHECK_ROLE_SUCCESS: 131000,
			ROLE_NOT_FOUND: 131001,
			SCOPE_NOT_FOUND: 131002,
		}
	},
	LANGUAGE: {
		VI: 'vi',
		EN: 'en'
	},
	SERVICES_CODE: { // service code của các service khác
		GET_ACCOUNT_INFO_SUCCESS: 100000,
		GET_MERCHANT_INFO_SUCCESS: 114100,
		CREATE_VOUCHER_SUCCESS: 134100,
		UPDATE_VOUCHER_SUCCESS: 134110,
		CREATE_PAGE_SUCCESS: 122000
	}
};
