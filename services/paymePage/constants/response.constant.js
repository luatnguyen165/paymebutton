const _ = require('lodash');

const SERVICE_CODE = '122';
const ACTION_CODE = {
	CREATE: '0',
	READ: '1',
	DETAIL: '2',
	UPDATE: '3',
	CREATE_ORDER: '4',
	CALLBACK: '5',
	NOTIFY: '6', // send Email/SMS
	CREATE_TAG: '7',
	READ_TAG: '8',
	REMOVE_TAG: '9',
	TUITION_PAY: '10'
};

module.exports = { // 00, 01: success, 02-99: fail
	CREATE: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}00`),
			key: 'CREATE_SUCCESS',
			message: 'Tạo trang thành công'
		},

		INVALID_INPUT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}02`),
			key: 'CREATE_INVALID_INPUT',
			message: 'Thông tin không hợp lệ'
		},
		INVALID_PHONE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}03`),
			key: 'CREATE_INVALID_PHONE',
			message: 'Số điện thoại không hợp lệ'
		},
		INVALID_STOCK: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}04`),
			key: 'CREATE_INVALID_STOCK',
			message: 'Số lượng giới hạn cho sản phẩm {{key}} không hợp lệ'
		},
		MISS_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}05`),
			key: 'CREATE_MISS_AMOUNT',
			message: 'Vui lòng cung cấp thông tin số tiền cho sản phẩm {{key}}'
		},
		INVALID_FIELD_TYPE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}06`),
			key: 'CREATE_INVALID_FIELD_TYPE',
			message: 'Loại của trường {{key}} không hợp lệ'
		},
		MISS_DROPDOWN_OPTIONS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}07`),
			key: 'CREATE_MISS_OPTION_DROPDOWN',
			message: 'Chưa có tùy chọn cho sản phẩm {{key}}'
		},
		MISS_PRICE_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}08`),
			key: 'CREATE_MISS_PRICE_FIELD',
			message: 'Phải có ít nhất một sản phẩm'
		},
		MISS_EMAIL_OR_PHONE_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}09`),
			key: 'CREATE_MISS_EMAIL_OR_PHONE_FIELD',
			message: 'Bắt buộc phải có trường email và số điện thoại'
		},
		INVALID_EXPIRE_TIME: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}10`),
			key: 'CREATE_INVALID_EXPIRE_TIME',
			message: 'Thời gian hết hạn phải sau 15 phút khi tạo trang'
		},
		GET_UUID_FAILED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}11`),
			key: 'CREATE_GET_UUID_FAILED',
			message: 'Tạo UUID thất bại'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}12`),
			key: 'CREATE_FAIL',
			message: 'Tạo trang thất bại'
		},
		INVALID_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}13`),
			key: 'CREATE_INVALID_AMOUNT',
			message: 'Số tiền thanh toán không được nhỏ hơn {{amount}} đồng'
		},
		INVALID_PRODUCTID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}14`),
			key: 'CREATE_INVALID_PRODUCTID',
			message: 'Mã sản phẩm không hợp lệ hoặc trùng lặp'
		},
		INVALID_TAGID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE}16`),
			key: 'CREATE_INVALID_TAGID',
			message: 'Tag không hợp lệ'
		},
	},
	READ: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ}00`),
			key: 'READ_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ}02`),
			key: 'READ_FAIL',
			message: 'Thất bại'
		},
		NO_PERMISSION: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ}03`),
			key: 'READ_NO_PERMISSION',
			message: 'Chỉ được xem trang bạn tạo'
		},
	},
	DETAIL: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}00`),
			key: 'DETAIL_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}02`),
			key: 'DETAIL_FAIL',
			message: 'Thất bại'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}03`),
			key: 'DETAIL_NOT_FOUND',
			message: 'Không tìm thấy thông tin page {{key}}'
		},
		EXPIRE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}04`),
			key: 'DETAIL_EXPIRE',
			message: 'Trang đã hết hạn'
		},
		INACTIVE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}05`),
			key: 'DETAIL_INACTIVE',
			message: 'Trang không tồn tại, vui lòng liên hệ cửa hàng của bạn'
		},
		MISS_PAGEID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}06`),
			key: 'DETAIL_MISS_PAGEID',
			message: 'Vui lòng cung cấp ID của trang'
		},
		MERCHANT_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.DETAIL}07`),
			key: 'DETAIL_MERCHANT_NOT_FOUND',
			message: 'Không tìm thấy thông tin merchant'
		}
	},
	UPDATE: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}00`),
			key: 'UPDATE_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}02`),
			key: 'UPDATE_FAIL',
			message: 'Thất bại'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}03`),
			key: 'UPDATE_NOT_FOUND',
			message: 'Không tìm thấy thông tin page'
		},
		NO_PERMISSION: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}04`),
			key: 'UPDATE_NO_PERMISSION',
			message: 'Chỉ được chỉnh sửa trang bạn tạo'
		},
		INVALID_PHONE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}05`),
			key: 'UPDATE_INVALID_PHONE',
			message: 'Số điện thoại không hợp lệ'
		},
		INVALID_STOCK: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}06`),
			key: 'UPDATE_INVALID_STOCK',
			message: 'Số lượng hàng cho sản phẩm {{key}} không hợp lệ'
		},
		INVALID_MIN_STOCK: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}06`),
			key: 'UPDATE_INVALID_STOCK',
			message: 'Số lượng kho phải lớn hơn số lượng đã bán'
		},
		MISS_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}07`),
			key: 'UPDATE_MISS_AMOUNT',
			message: 'Vui lòng cung cấp thông tin số tiền cho sản phẩm {{key}}'
		},
		INVALID_FIELD_TYPE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}08`),
			key: 'UPDATE_INVALID_FIELD_TYPE',
			message: 'Loại của trường {{key}} không hợp lệ'
		},
		MISS_DROPDOWN_OPTIONS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}09`),
			key: 'UPDATE_MISS_OPTION_DROPDOWN',
			message: 'Chưa có tùy chọn cho sản phẩm {{key}}'
		},
		MISS_PRICE_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}10`),
			key: 'UPDATE_MISS_PRICE_FIELD',
			message: 'Phải có ít nhất một sản phẩm'
		},
		MISS_EMAIL_OR_PHONE_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}11`),
			key: 'UPDATE_MISS_EMAIL_OR_PHONE_FIELD',
			message: 'Bắt buộc phải có trường email và số điện thoại'
		},
		INVALID_EXPIRE_TIME: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}12`),
			key: 'UPDATE_INVALID_EXPIRE_TIME',
			message: 'Thời gian hết hạn phải sau 15 phút khi cập nhật trang'
		},
		FIELD_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.UPDATE}13`),
			key: 'UPDATE_FIELD_NOT_FOUND',
			message: 'Không tìm thấy field {{key}}'
		}

	},
	CREATE_ORDER: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}00`),
			key: 'CREATE_ORDER_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}02`),
			key: 'CREATE_ORDER_FAIL',
			message: 'Thất bại'
		},
		MISS_PAGEID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}03`),
			key: 'CREATE_ORDER_MISS_PAGEID',
			message: 'Vui lòng cung cấp ID của trang'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}04`),
			key: 'CREATE_ORDER_NOT_FOUND',
			message: 'Không tìm thấy thông tin page'
		},
		FIELD_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}05`),
			key: 'CREATE_ORDER_FIELD_NOT_FOUND',
			// message: 'Trường ({{key}}) không hợp lệ'
			message: 'Sản phẩm bạn chọn đã bị xoá. Vui lòng chọn lại sản phẩm khác.'
		},
		GET_UUID_FAILED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}06`),
			key: 'CREATE_ORDER_GET_UUID_FAILED',
			message: 'Tạo UUID thất bại'
		},
		MISS_PRICE_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}07`),
			key: 'CREATE_ORDER_MISS_PRICE_FIELD',
			message: 'Phải có ít nhất một sản phẩm'
		},
		FAIL_CREATE_WEB: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}08`),
			key: 'CREATE_ORDER_FAIL_CREATE_WEB',
			message: 'Tạo order thất bại'
		},
		EXPIRE: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}09`),
			key: 'CREATE_ORDER_EXPIRE',
			message: 'Trang đã hết hạn'
		},
		MISS_REQUIRED_FIELD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}10`),
			key: 'CREATE_ORDER_MISS_REQUIRED_FIELD',
			message: 'Thiếu thông tin cho sản phẩm {{key}}'
		},
		MISS_QUANTITY: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}12`),
			key: 'CREATE_ORDER_MISS_QUANTITY',
			message: 'Chưa chọn số lượng cho sản phẩm {{key}}'
		},
		MISS_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}13`),
			key: 'CREATE_ORDER_MISS_AMOUNT',
			message: 'Thiếu số tiền cho sản phẩm {{key}}'
		},
		OUT_OF_STOCK: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}14`),
			key: 'CREATE_ORDER_OUT_OF_STOCK',
			message: 'Sản phẩm {{key}} không đủ'
		},
		MERCHANT_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}15`),
			key: 'CREATE_ORDER_MERCHANT_NOT_FOUND',
			message: 'Không tìm thấy thông tin merchant'
		},
		INVALID_PAYMENT_METHOD: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}16`),
			key: 'CREATE_ORDER_INVALID_PAYMENT_METHOD',
			message: 'Phương thức thanh toán không hợp lệ'
		},
		VOUCHER_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}17`),
			key: 'CREATE_ORDER_VOUCHER_NOT_FOUND',
			message: 'Không tìm thấy thông tin mã giảm giá'
		},
		VOUCHER_INVALID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}18`),
			key: 'CREATE_ORDER_VOUCHER_INVALID',
			message: 'Mã giảm giá không hợp lệ'
		},
		VOUCHER_EXPIRED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}18`),
			key: 'CREATE_ORDER_VOUCHER_EXPIRED',
			message: 'Mã giảm giá đã hết hạn sử dụng'
		},
		VOUCHER_ENDOFUSED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}19`),
			key: 'CREATE_ORDER_VOUCHER_ENDOFUSED',
			message: 'Mã giảm giá đã hết lượt sử dụng'
		},
		ORDER_NOT_ENOUGH_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}20`),
			key: 'CREATE_ORDER_ORDER_NOT_ENOUGH_AMOUNT',
			message: 'Chưa đạt số tiền thanh toán tối thiểu'
		},
		VOUCHER_NOT_ALLOW: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}21`),
			key: 'CREATE_ORDER_ORDER_VOUCHER_NOT_ALLOW',
			message: 'Không cho phép sử dụng mã giảm giá'
		},
		INVALID_AMOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}22`),
			key: 'CREATE_ORDER_INVALID_AMOUNT',
			message: 'Số tiền thanh toán không được nhỏ hơn {{amount}} đồng'
		},
		INVALID_QUANTITY: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}23`),
			key: 'CREATE_ORDER_INVALID_QUANTITY',
			message: 'Vui lòng chọn số lượng > 0'
		},
		VOUCHER_OUT_OF_STOCK: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}24`),
			key: 'CREATE_ORDER_VOUCHER_OUT_OF_STOCK',
			message: 'Số lượt sử dụng mã giảm giá lớn hơn số lượng khả dụng'
		},
		GET_PAYMENT_INFORMATION_FAILED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}25`),
			key: 'GET_PAYMENT_INFORMATION_FAILED',
			message: 'Không tìm thấy thông tin đơn hàng'
		},
		GET_PAYMENT_INFORMATION_SUCCEEDED: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_ORDER}26`),
			key: 'GET_PAYMENT_INFORMATION_SUCCEEDED',
			message: 'Lấy thông tin đơn hàng thành công'
		},
	},
	CALLBACK: {
		SUCCESS: {
			code: 1000, // update for ipn page/link/button
			key: 'CALLBACK_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CALLBACK}02`),
			key: 'CALLBACK_FAIL',
			message: 'Ghi nhận thất bại'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CALLBACK}03`),
			key: 'CALLBACK_NOT_FOUND',
			message: 'Không tìm thấy thông tin giao dịch'
		},
		INVALID_ACCOUNT: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CALLBACK}04`),
			key: 'CALLBACK_INVALID_ACCOUNT',
			message: 'Thông tin doanh nghiệp không hợp lệ'
		},
		INVALID_STATUS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CALLBACK}05`),
			key: 'CALLBACK_INVALID_STATUS',
			message: 'Trạng thái đơn hàng không hợp lệ'
		}
	},
	NOTIFY: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.NOTIFY}00`),
			key: 'NOTIFY_SUCCESS',
			message: 'Gửi Email/SMS thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.NOTIFY}02`),
			key: 'NOTIFY_FAIL',
			message: 'Gửi Email/SMS thất bại'
		},
		MISS_PAGEID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.NOTIFY}03`),
			key: 'NOTIFY_MISS_PAGEID',
			message: 'Vui lòng cung cấp ID của trang'
		},
		PAGE_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.NOTIFY}04`),
			key: 'NOTIFY_PAGE_NOT_FOUND',
			message: 'Không tìm thấy trang'
		},
		NO_PERMISSION: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.NOTIFY}05`),
			key: 'NOTIFY_NO_PERMISSION',
			message: 'Chỉ được gửi thông báo cho trang của bạn'
		}
	},
	CREATE_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_TAG}00`),
			key: 'CREATE_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.CREATE_TAG}02`),
			key: 'CREATE_TAG_FAIL',
			message: 'Tạo tag thất bại'
		},
	},
	READ_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ_TAG}00`),
			key: 'READ_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.READ_TAG}02`),
			key: 'READ_TAG_FAIL',
			message: 'Lấy danh sách tag thất bại'
		},
	},
	REMOVE_TAG: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}00`),
			key: 'REMOVE_TAG_SUCCESS',
			message: 'Thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}02`),
			key: 'REMOVE_TAG_FAIL',
			message: 'Xóa tag thất bại'
		},
		MISS_TAGID: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}03`),
			key: 'REMOVE_TAG_MISS_PAGEID',
			message: 'Vui lòng cung cấp tagId'
		},
		NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.REMOVE_TAG}04`),
			key: 'REMOVE_TAG_NOT_FOUND',
			message: 'Không tìm thấy thông tin tag'
		}
	},
	TUITION_PAY: {
		SUCCESS: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}0`),
			key: 'TUITION_PAY_SUCCESS',
			message: 'Ghi nhận thành công'
		},
		FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}1`),
			key: 'TUITION_PAY_FAIL',
			message: 'Ghi nhận thất bại. Vui lòng thử lại sau'
		},
		PAGE_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}2`),
			key: 'PAGE_NOT_FOUND',
			message: 'Không tìm thấy thông tin page'
		},
		ACCOUNT_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}3`),
			key: 'ACCOUNT_NOT_FOUND',
			message: 'Không tìm thấy thông tin tài khoản'
		},
		MERCHANT_NOT_FOUND: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}4`),
			key: 'MERCHANT_NOT_FOUND',
			message: 'Không tìm thấy thông tin doanh nghiệp'
		},
		CREATE_VOUCHER_FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}5`),
			key: 'CREATE_VOUCHER_FAIL',
			message: 'Tạo mã giảm giá không thành công'
		},
		CREATE_PAGE_FAIL: {
			code: Number(`${SERVICE_CODE}${ACTION_CODE.TUITION_PAY}6`),
			key: 'CREATE_PAGE_FAIL',
			message: 'Tạo PayME Page thất bại'
		}
	},

	replaceMsg: (str, replacements) => {
		let newStr = str;
		for (let i = 0; i < replacements.length; i += 1) {
			const replacement = replacements[i];
			newStr = _.replace(newStr, `{{${i}}}`, replacement);
		}
		return newStr;
	},
	SCOPE_CODE: {
		CHECK_ROLE_SUCCESS: 131000,
		ROLE_NOT_FOUND: 131001,
		SCOPE_NOT_FOUND: 131002
	}
};
