const _ = require('lodash');
const moment = require('moment');
const numeral = require('numeral');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');

const createDataToUpdate = (pageInfo, paymentItems, state, paymentReceived) => {
	const inputFields = _.get(pageInfo, 'paymentDetail.inputFields', []);
	let totalAmountChanged = 0;
	const inputFieldToUpdate = [];
	for (let i = 0; i < inputFields.length; i += 1) {
		const field = inputFields[i];
		const itemSoldFound = paymentItems.find((item) => item.id === field.id);
		if (itemSoldFound && field.options) { // input field and not relative price field
			const quantitySold = _.get(itemSoldFound, 'quantity', 0);
			const amountSold = _.get(itemSoldFound, 'amount', 0);
			if (state === PaymentRequestConst.IPN_STATUS.SUCCEEDED) {
				field.options.itemInOrder = _.get(field, 'options.itemInOrder', 0) - quantitySold;
				field.options.itemSold = _.get(field, 'options.itemSold', 0) + quantitySold;
				field.options.revenue = _.get(field, 'options.revenue', 0) + amountSold;
				totalAmountChanged += amountSold;
			} else if (state === PaymentRequestConst.IPN_STATUS.EXPIRED) {
				field.options.itemInOrder = _.get(field, 'options.itemInOrder', 0) - quantitySold;
			} else if (state === PaymentRequestConst.IPN_STATUS.REFUNDED) { // refund a partial of order
				// remove sold
			} else if (state === PaymentRequestConst.IPN_STATUS.CANCELED_SUCCEEDED) { // refund all order
				// field.options.itemSold = _.get(field, 'options.itemSold', 0) - quantitySold;
				// field.options.revenue = _.get(field, 'options.revenue', 0) - amountSold;
				// totalAmountChanged -= amountSold;
			}
		}
		inputFieldToUpdate.push(field);
	}
	const pageDataToUpdate = {
		// totalRevenue: _.get(pageInfo, 'totalRevenue', 0) + totalAmountChanged,
		'paymentDetail.inputFields': inputFieldToUpdate,
		paymentAt: new Date()
	};
	const paymentRequestDataToUpdate = {
		status: state,
		finishedAt: state === 'SUCCEEDED' ? new Date() : null,
		paymentId: paymentReceived.paymentId || null,
		ipnInfo: JSON.stringify(paymentReceived)
	};
	return { pageDataToUpdate, paymentRequestDataToUpdate };
};
const updatePaymentRequest = async (orderQueryResult, pageInfo, paymentRequest, ctx) => {
	const queriedOrder = _.get(orderQueryResult, 'data', orderQueryResult);
	const queriedOrderStatus = _.get(queriedOrder, 'state', '');
	// REFUNDED& SUCCEEDED & EXPIRED & CANCELED
	const { pageDataToUpdate, paymentRequestDataToUpdate } = createDataToUpdate(pageInfo, paymentRequest.paymentItems, queriedOrderStatus, queriedOrder);
	ctx.broker.logger.info(`[PAYME_PAGE] updatePaymentRequest >> createDataToUpdate: ${JSON.stringify({ pageDataToUpdate, paymentRequestDataToUpdate })}`);
	// nếu đơn hàng thành công => thu thập thông tin KH, kiểm tra payment và page có cho phép thanh toán định kì
	let updatedPayment = paymentRequest;
	if (queriedOrderStatus === 'SUCCEEDED') {
		updatedPayment = await ctx.broker.call('v1.paymentRequestModel.findOneAndUpdate', [
			{
				id: paymentRequest.id,
				status: 'PENDING'
			},
			{
				...paymentRequestDataToUpdate,
			},
			{ new: true }
		]);
		if (!updatedPayment || updatedPayment.status === paymentRequest.status) {
			ctx.broker.logger.warn(`[PAYME_PAGE] updatePaymentRequest  Failed >> updatedPayment: ${JSON.stringify(updatedPayment)}`);
			return {
				isSucceed: false,
				message: 'Cập nhật paymentRequest thất bại'
			};
		}
		await ctx.broker.call('v1.paymePageModel.updateOne', [
			{
				_id: pageInfo._id,
			},
			{
				$set: {
					...pageDataToUpdate
				},
				$inc: {
					actualRevenue: queriedOrder.amount,
					totalRevenue: queriedOrder.amount // update by amount received from gateway (đã trừ voucher/giảm giá nếu có)
				}
			}
		]);
		const customerInfo = {};
		// eslint-disable-next-line array-callback-return
		paymentRequest.customerDetails.map((v) => {
			customerInfo[v.name] = v.value || undefined;
		});
		const newCustomer = {
			name: customerInfo.fullname || undefined,
			email: customerInfo.email || undefined,
			number: customerInfo.phone ? customerInfo.phone : undefined,
			location: customerInfo.shippingAddress ? customerInfo.shippingAddress : undefined,
			code: customerInfo.id ? customerInfo.id : undefined
		};
		ctx.broker.logger.info(`[PAYME_PAGE] Create customer in updatePaymentRequest customerInfo: ${JSON.stringify(newCustomer)} - customerDetail in payment req: ${JSON.stringify(customerInfo)}`);
		if (Object.values(newCustomer).filter((v) => v === undefined).length !== 5) {
			try {
				const createCustomerRes = await ctx.broker.call(
					'v1.customer.CreateCustomer',
					{ body: newCustomer },
					{
						meta: {
							auth: {
								data: {
									accountId: _.get(pageInfo, 'creator.accountId')
								}
							}
						}
					}
				);
				ctx.broker.logger.info(`[PAYME_PAGE] Create customer in updatePaymentRequest response: ${JSON.stringify(createCustomerRes)}`);
			} catch (error) {
				ctx.broker.logger.info(`[PAYME_PAGE] Create customer in updatePaymentRequest error!: ${error}`);
			}
		}
		// check payment và page có cho phép thanh toán định kì
		if (['AUTO', 'REMIND'].includes(updatedPayment.recurringPayment)) {
			const dataGenSub = {
				pageId: pageInfo.pageId,
				paymentRequestId: updatedPayment.id
			};
			const generateSubcription = await ctx.broker.call('v1.paymeSubcription.generateSubcription', dataGenSub);
			ctx.broker.logger.warn(`PAGE [PAYMENT_SUCCEEDED] generateSubcription: data = ${JSON.stringify(dataGenSub)}, response = ${JSON.stringify(generateSubcription)}`);
		}
		// update number of payment in payme button
		if (updatedPayment.referType === PaymentRequestConst.REFER_TYPE.PAYME_PAGE) {
			const referButtonId = _.get(updatedPayment, 'referButtonId', null);
			if (referButtonId) {
				const updatedButton = await ctx.broker.call('v1.paymeButtonModel.updateOne', [
					{
						buttonId: referButtonId,
					},
					{
						$inc: {
							numOfPayment: 1
						}
					}
				]);
			}
		}
		// gui mail cho KH
		if (customerInfo.email || customerInfo.id) {
			let chanel = 'INDIVIDUAL';
			let target = [customerInfo.id];
			if (customerInfo.email) {
				chanel = 'CUSTOM';
				target = [{ email: customerInfo.email }];
			}
			let merchantFullInfo = null;
			try {
				merchantFullInfo = await ctx.broker.call('v1.settingsDashboard.internalSettings', { merchantId: pageInfo.creator.merchantId });
				ctx.broker.logger.warn(`PAGE [PAYMENT_SUCCEEDED] auto send email to KH -- merchantId:  ${pageInfo.creator.merchantId},  ${JSON.stringify(merchantFullInfo)}`);
			} catch (e) {
				ctx.broker.logger.warn(`PAGE [PAYMENT_SUCCEEDED] auto send email to KH -- merchantId:  ${pageInfo.creator.merchantId}, Error: ${e}`);
				return {
					isSucceed: false,
					message: 'Không tìm thấy thông tin merchant!'
				};
			}
			if (merchantFullInfo.code !== 114100) {
				return {
					isSucceed: false,
					message: 'Không tìm thấy thông tin merchant.'
				};
			}
			merchantFullInfo = merchantFullInfo.data;
			const mailData = {
				meta: {
					merchantId: _.get(pageInfo, 'creator.merchantId'),
					accountId: _.get(pageInfo, 'creator.accountId'),
					chanel,
					target,
					method: ['EMAIL'],
					service: 'PAYME_PAGE',
					extraData: {
						referId: pageInfo.pageId
					},
					brokerCall: 'v1.paymePage.receiveCallbackSendEmailAndSMS',
					template: {
						email: 'page-succeeded'
					},
					content: {
						mail: {
							subject: 'Đặt hàng thành công',
							content: {
								logo: _.get(merchantFullInfo, 'logo', ''),
								merchantTitle: merchantFullInfo.brandName || merchantFullInfo.fullName,
								customerName: customerInfo.fullname || customerInfo.email,
								customerEmail: customerInfo.email,
								orderId: updatedPayment.orderId,
								paymentId: updatedPayment.paymentId,
								paidAt: moment(updatedPayment.finishedAt).format('HH:mm DD/MM/YYYY'),
								method: updatedPayment.paymentMethod.name || updatedPayment.paymentMethod.description,
								customerNumber: customerInfo.phone,
								customerAddress: customerInfo.shippingAddress,
								note: updatedPayment.note,
								products: updatedPayment.paymentItems.map((item) => {
									const product = {
										productImage: !item.image ? '' : process.env.STATIC_URL + item.image,
										productTitle: item.label,
										productAmount: `${numeral(item.unitAmount).format('0,0')} đ`,
										productQuantity: item.quantity
									};
									return product;
								}),
								amount: `${numeral(updatedPayment.amount).format('0,0')} đ`,
								discount: `-${numeral(updatedPayment.discountAmount).format('0,0')} đ`,
								total: `${numeral(updatedPayment.paymentAmount).format('0,0')} đ`
							}
						}
					}
				}
			};
			try {
				const sendEmailRes = await ctx.broker.call('v1.customer.sendEmailAndSMS', {}, mailData);
				ctx.broker.logger.warn(`PAGE [PAYMENT_SUCCEEDED] send email to KH res: mailData = ${JSON.stringify(mailData)}, sendEmailRes = ${JSON.stringify(sendEmailRes)}`);
			} catch (err) {
				ctx.broker.logger.warn(`PAGE [PAYMENT_SUCCEEDED] send email to KH error: mailData = ${JSON.stringify(mailData)}, err = ${err}`);
				return {
					isSucceed: false,
					messages: `Gửi email cho khách hàng thất bại. ${err.message}`
				};
			}
		}
	} else if (queriedOrderStatus === PaymentRequestConst.IPN_STATUS.REFUNDED || queriedOrderStatus === PaymentRequestConst.IPN_STATUS.CANCELED_SUCCEEDED) {
		if (queriedOrderStatus === PaymentRequestConst.IPN_STATUS.REFUNDED) {
			if (queriedOrder.amount + _.get(paymentRequest, 'totalRefund', 0) !== paymentRequest.paymentAmount) {
				delete paymentRequestDataToUpdate.status;// nếu là GD refund thì vẫn giữ trạng thái SUCCEEDED nếu chưa hoàn all
			}
		}
		updatedPayment = await ctx.broker.call('v1.paymentRequestModel.findOneAndUpdate', [
			{
				id: paymentRequest.id
			},
			{
				$set: { ...paymentRequestDataToUpdate },
				$addToSet: { refunds: JSON.stringify(orderQueryResult) },
				$inc: {
					totalRefund: queriedOrder.amount
				}
			},
			{ new: true }
		]);
		// cập nhật lại tổng doanh thu sau refund
		await ctx.broker.call('v1.paymePageModel.updateOne', [
			{
				_id: pageInfo._id,
			},
			{
				$inc: {
					actualRevenue: -queriedOrder.amount
				}
			}
		]);
	}
	return {
		isSucceed: true,
		messages: 'Cập nhật thành công',
		paymentRequest: updatedPayment
	};
};
module.exports = {
	updatePaymentRequest,
	createDataToUpdate
};
