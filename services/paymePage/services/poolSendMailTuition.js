const _ = require('lodash');
const Async = require('async');
const ejs = require('ejs');
const path = require('path');
const UtilityHelper = require('../helpers/UtilityHelper');
const PageConstants = require('../constants/paymePage.constant');

/**
 * @dev Create payment link for each student depend on their tuition info
 * @notice Convert tuitionInfo to format like => [{id: 'mjpdyl',n: 5}, {id: 'rnvzse',m: 15000}]; -> convert to querystring
 */
const createPaymentLink = ({ baseUrl = '', tuitionInfo = [], studentInfo = {} }) => {
	const tuitionConverted = tuitionInfo.reduce((acc, cur) => {
		const item = {
			id: cur.code,
			r: Boolean(cur.quantity) // r aka require. if quantity !== 0 => require
		};
		if (_.upperCase(cur.type) === PageConstants.TUITION_TYPE.VOLUME) {
			item.n = cur.quantity;
		} else {
			item.m = cur.price;
		}
		return [...acc, item];
	}, []);
	const paramsObj = {
		p: Buffer.from(JSON.stringify(tuitionConverted)).toString('base64'),
		cn: studentInfo.name,
		cm: studentInfo.parentEmail,
		cp: studentInfo.parentPhone
	};
	const searchParams = new URLSearchParams(paramsObj);
	return `${baseUrl}?${searchParams}`;
};

const POOL_SEND_EMAIL = Async.queue(async (task, callback) => {
	const { broker, ...payload } = task;
	broker.logger.info(`PayMEPage > poolSendMailTuition.js > payload = ${JSON.stringify(payload)}`);

	const paymentLink = createPaymentLink({
		baseUrl: payload.payMEPageInfo.shortLink,
		tuitionInfo: payload.tuitionInfo,
		studentInfo: payload.studentInfo
	});
	const dataSendMail = {
		logo: payload.merchantLogo,
		mcname: payload.merchantName,
		merchantId: payload.merchantId,
		accountId: payload.accountId,
		studentInfo: {
			studentId: payload.studentInfo.id,
			fullName: payload.studentInfo.name,
			className: payload.studentInfo.className,
			schoolYear: payload.info.scholastic,
			email: payload.studentInfo.parentEmail,
			schoolBranch: payload.studentInfo.schoolName,
			phoneNumber: payload.studentInfo.parentPhone,
			parentName: payload.studentInfo.parentName,
		},
		tuition: {
			items: payload.tuitionInfo,
			total: payload.tuitionInfo.reduce((acc, cur) => acc + cur.quantity * cur.price, 0)
		},
		detail: {
			title: payload.info.title,
			bankTransferInfo: {
				fullName: payload.customerBankAccount.fullName,
				number: payload.customerBankAccount.number,
				bankName: payload.customerBankAccount.bankName,
				branch: payload.customerBankAccount.branch,
			},
			submissionDeadline: payload.info.deadline,
			contact: payload.info.contact
		},
		paymentLink,
		helpers: {
			formatCurrency: UtilityHelper.formatCurrency
		}
	};
	broker.logger.info(`PayMEPage > poolSendMailTuition.js > dataSendMail > ${JSON.stringify(dataSendMail)}`);

	const paymentLinkInSecure = dataSendMail.paymentLink.replace('https://', 'http://');
	const template = {
		tueduc: {
			sms: `Truong Tue Duc thong bao hoc phi cua ${UtilityHelper.removeUnicode(dataSendMail.studentInfo.fullName)} la ${UtilityHelper.formatCurrency(dataSendMail.tuition.total, '.')}d, quy PH vui long thanh toan truc tuyen tai ${(paymentLinkInSecure)}. Tran trong.`,
			email: '../template/tuition-tueduc-mailcontent.ejs',
			method: ['EMAIL', 'SMS']
		},
		tesla: {
			sms: `HT truong Tesla thong bao hoc phi cua ${UtilityHelper.removeUnicode(dataSendMail.studentInfo.fullName)} la ${UtilityHelper.formatCurrency(dataSendMail.tuition.total, '.')}d, quy khach thanh toan truc tuyen tai ${(paymentLinkInSecure)}. Tran trong.`,
			email: '../template/tuition-tesla-mailcontent.ejs',
			method: ['EMAIL', 'SMS']
		}
	};
	const smsData = template[`${dataSendMail.mcname}`].sms; // `HT truong Tue Duc thong bao hoc phi cua ${utilityHelper.removeUnicode(payload.studentInfo.fullName)} la ${data.helpers.formatCurrency(payload.tuition.total, '.')}d, quy khach thanh toan truc tuyen tai ${(paymentLink)}. Tran trong.`;
	broker.logger.info(`PayMEPage > poolSendMailTuition > smsData = ${JSON.stringify(smsData)}`);
	const metaData = {
		merchantId: dataSendMail.merchantId,
		accountId: dataSendMail.accountId,
		target: [
			{
				name: dataSendMail.studentInfo.parentName,
				email: dataSendMail.studentInfo.email,
				number: dataSendMail.studentInfo.phoneNumber
			}
		],
		chanel: 'CUSTOM',
		method: template[`${dataSendMail.mcname}`].method,
		service: 'PAYME_PAGE',
		template: {
			email: 'default-mail',
			sms: 'default-sms'
		}
	};
	broker.logger.info(`PayMEPage > poolSendMailTuition.js > call 'v1.customer.sendEmailAndSMS' with metaData: ${JSON.stringify(metaData)}`);

	try {
		ejs.renderFile(path.join(__dirname, template[`${dataSendMail.mcname}`].email), { data: dataSendMail }, async (err, htmlData) => {
			if (err) {
				throw err;
			}
			metaData.content = {
				mail: {
					subject: `THÔNG BÁO ${dataSendMail.detail.title}`.toUpperCase(),
					content: {
						data: htmlData
					}
				},
				sms: {
					content: {
						data: smsData
					}
				}
			};
			const sendEmailRes = broker.call('v1.customer.sendEmailAndSMS', '', // don't need `await`
				{
					meta: metaData
				});
			broker.logger.info(`PayMEPage > poolSendMailTuition > metaData: ${JSON.stringify(metaData)}`);
			broker.logger.info(`PayMEPage > poolSendMailTuition > sendEmailRes: ${JSON.stringify(sendEmailRes)}`);
		});
	} catch (error) {
		broker.logger.error(`PayMEPage > poolSendMailTuition.js > error > ${error}`);
	}
	callback();
}, 10);
module.exports = {
	init(data) {
		POOL_SEND_EMAIL.push(data);
	}
};
