const _ = require('lodash');
const { customAlphabet } = require('nanoid');
const PageConstants = require('../constants/paymePage.constant');

const SERVICE_CODE = {
	GET_CUSTOMER_SUCCESS: 1010000,
	CREATE_ORDER_SUCCESS: 105000
};

module.exports = {
	/**
	 * Get customerId from customer info.
	 * If customer info (phone, email) is exist -> return old customerId
	 * Otherwise, create new customer and return this customerId
	 * @param {Object} customerInfo, accountId, broker
	 * customerInfo.name: required
	 * customerInfo.email: required
	 * customerInfo.phone: optional
	 */
	async getCustomerId({ customerInfo = {}, accountId = '', broker = {} }) {
		let customerId = null;
		const customerInfoCleanData = {
			name: customerInfo.name,
			email: customerInfo.email,
			number: customerInfo.phone,
		};
		try {
			broker.logger.info(`PayMEPage > commonService > getCustomerId > call 'v1.customer.CreateCustomer' with requestBody = ${JSON.stringify(customerInfoCleanData)}}`);
			const createCustomerRes = await broker.call(
				'v1.customer.CreateCustomer',
				{ body: customerInfoCleanData },
				{
					meta: {
						auth: {
							data: { accountId },
						},
					},
				}
			);
			broker.logger.info(`PayMEPage > commonService > getCustomerId > call 'v1.customer.CreateCustomer' with responseData = ${JSON.stringify(createCustomerRes)}}`);
			const filterCustomer = {
				phone: customerInfo.phone,
				email: customerInfo.email,
			};
			broker.logger.info(`PayMEPage > commonService > getCustomerId > call 'v1.customer.GetCustomerInfos' with filter = ${JSON.stringify(filterCustomer)}}`);
			const customers = await broker.call(
				'v1.customer.GetCustomerInfos',
				{
					body: { filter: filterCustomer },
				},
				{
					meta: {
						auth: {
							data: { id: accountId },
						},
					},
				}
			);
			broker.logger.info(`PayMEPage > commonService > getCustomerId > call 'v1.customer.GetCustomerInfos' with responseData = ${JSON.stringify(customers)}}`);
			if (_.get(customers, 'code', null) !== SERVICE_CODE.GET_CUSTOMER_SUCCESS || _.get(customers, 'totalRow', null) === 0) {
				return customerId; // fail -> return null
			}
			customerId = customers.data[0].id;
			return customerId;
		} catch (error) {
			broker.logger.error(`PayMEPage > commonService > getCustomerId > error ${error}`);
			return customerId;
		}
	},

	/**
 * @notice createOrder with paymentMethod is MANUAL_BANK. generate virtual account for customer
 * @param {*} param0 { broker, pageId, tuitionInfo, studentInfo, customerInfo, mcPaymentMethod }
 */
	async createOrder(payload) {
		const returnData = {
			success: false,
			message: 'Thất bại',
			data: null
		};
		const PAY_CODE_MANUAL_BANK = 'MANUAL_BANK';
		const {
			broker, pageId, tuitionInfo, studentInfo, customerInfo, mcPaymentMethod
		} = payload;
		try {
			const payMEPageInfo = await broker.call('v1.paymePageModel.findOne', [{ pageId }]);
			if (!payMEPageInfo) {
				returnData.message = 'Tạo đơn thanh toán thất bại. Không tìm thấy trang thanh toán';
				return returnData;
			}
			const paymentMethod = mcPaymentMethod.find(
				(method) => method.payCode === PAY_CODE_MANUAL_BANK
			);
			if (!paymentMethod || paymentMethod.isActive === false) {
				returnData.message = 'Phương thức thanh toán không hợp lệ';
				return returnData;
			}
			let transactionId = null;
			try {
				const nanoId = customAlphabet('012345678901234567890123456789', 12);
				transactionId = nanoId();
			} catch (error) {
				broker.logger.error(`PayMEPage > commonService > call 'customAlphabet' with error: ${error}`);
			}
			if (!transactionId) {
				returnData.message = 'Khởi tạo đơn hàng thất bại';
				return returnData;
			}

			// create paymentItems and calculate total payment amount
			let totalPayment = 0;
			const paymentItems = [];
			tuitionInfo.forEach((tuition) => {
				paymentItems.push({
					id: tuition.code,
					label: tuition.title,
					name: tuition.code,
					unitAmount: tuition.price,
					quantity: tuition.quantity,
					amount: tuition.price * tuition.quantity
				});
				totalPayment += tuition.price * tuition.quantity;
			});
			const paymentRequestData = {
				transactionId,
				paymentItems,
				paymentMethod,
				referId: payMEPageInfo.pageId,
				referType: PageConstants.REFER_TYPE.PAYME_PAGE,
				amount: totalPayment,
				paymentAmount: totalPayment,
				customerDetails: [
					{ label: 'fullname', name: 'fullname', value: studentInfo.name },
					{ label: 'phone', name: 'phone', value: studentInfo.parentPhone },
					{ label: 'email', name: 'email', value: studentInfo.parentEmail }],
				status: PageConstants.PAYMENT_REQUEST_STATUS.FAILED,
			};

			const paymentRequest = await broker.call('v1.paymentRequestModel.create', [paymentRequestData]);
			if (!paymentRequest) {
				returnData.message = 'Khởi tạo đơn hàng thất bại';
				return returnData;
			}

			const orderData = {
				partnerTransaction: paymentRequest.transactionId,
				referType: PageConstants.REFER_TYPE.PAYME_PAGE,
				referId: payMEPageInfo.pageId,
				accountId: payMEPageInfo.creator.accountId,
				amount: totalPayment,
				lang: payMEPageInfo.language,
				redirectUrl: _.get(payMEPageInfo, 'redirectURL', ''), // redirect if success or fail...
				failedUrl: payMEPageInfo.paymentLink, // user cancel payment and rechoose payment method
				ipnUrl: `${process.env.HOST}/widgets/page/external/receive_callback`,
				description: payMEPageInfo.description || payMEPageInfo.note || '-',
				referData: JSON.stringify({
					orderInfo: {
						title: payMEPageInfo.businessDetail.title,
						paymentItems: paymentRequest.paymentItems
					},
					customerInfo,
					voucherInfo: {},
					note: ''
				}),
				payMethod: PAY_CODE_MANUAL_BANK, // hardcode. transfer to bank (virtual account)
				customerId: customerInfo.id
			};

			let createOrder;
			try {
				broker.logger.info(`payMEPage > commonService > createOrder > call 'v1.order.createWeb' with requestBody = ${JSON.stringify(orderData)}`);
				createOrder = await broker.call('v1.order.createWeb', orderData);
				broker.logger.info(`payMEPage > commonService > createOrder > call 'v1.order.createWeb' with responseData = ${JSON.stringify(createOrder)}`);
			} catch (error) {
				broker.logger.error(`payMEPage > commonService > createOrder > call 'v1.order.createWeb' with error = ${error}`);
			}
			if (_.get(createOrder, 'code', null) !== SERVICE_CODE.CREATE_ORDER_SUCCESS) { // 105000: code thành công
				returnData.message = 'Khởi tạo đơn hàng thất bại';
				return returnData;
			}

			const dataUpdate = {
				orderId: createOrder.data.orderId,
				status: PageConstants.PAYMENT_REQUEST_STATUS.PENDING,
			};
			await broker.call('v1.paymentRequestModel.updateOne', [
				{ id: paymentRequest.id },
				dataUpdate,
			]);

			returnData.data = {
				orderId: createOrder.data.orderId,
				bankInfo: _.get(createOrder, 'data.bankInfo', {})
			};
			returnData.message = 'Khởi tạo đơn hàng thành công';
			returnData.success = true;
			return returnData;
		} catch (error) {
			broker.logger.error(`PayMEPage > commonService > createOrder > error ${error}`);
			return returnData;
		}
	}
};
