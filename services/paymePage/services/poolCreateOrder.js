const _ = require('lodash');
const Async = require('async');
const CommonService = require('./commonService');
const poolSendMailTuition = require('./poolSendMailTuition');

/** @dev Create order with paymentMethod is MANUAL_BANK (require customerId) */
const POOL_CREATE_ORDER = Async.queue(async (task, callback) => {
	const { broker, ...payload } = task;
	broker.logger.info(`PayMEPage > poolCreateOrder.js > payload = ${JSON.stringify(payload)}`);

	/** Get customer id */
	const customerInfo = {
		name: payload.studentInfo.name,
		email: payload.studentInfo.parentEmail,
		phone: payload.studentInfo.parentPhone
	};
	const customerId = await CommonService.getCustomerId({ customerInfo, accountId: task.accountId, broker });

	/** Create order */
	const createOrderRes = await CommonService.createOrder({
		broker,
		customerInfo: { ...payload.customerInfo, id: customerId },
		pageId: payload.payMEPageInfo.pageId,
		..._.pick(payload, ['tuitionInfo', 'studentInfo', 'mcPaymentMethod'])
	});
	if (createOrderRes.success === false) {
		broker.logger.info(`PayMEPage > poolCreateOrder.js > error > ${JSON.stringify(createOrderRes)}`);
	}
	const customerBankAccount = _.get(createOrderRes, 'data.bankInfo', { fullName: '', number: '', bankName: '' }); // virtual account system generate for customer

	poolSendMailTuition.init({
		broker,
		customerBankAccount,
		..._.pick(payload, [
			'merchantLogo',
			'merchantName',
			'merchantId',
			'accountId',
			'payMEPageInfo',
			'tuitionInfo',
			'studentInfo',
			'info'
		]),
	});

	callback();
}, 1);

module.exports = {
	init(data) {
		POOL_CREATE_ORDER.push(data);
	}
};
