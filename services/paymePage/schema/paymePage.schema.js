const _ = require('lodash');
const PageConst = require('../constants/paymePage.constant');
const GeneralConstants = require('../constants/general.constant');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');

const create = {
	body: {
		$$type: 'object',
		type: {
			type: 'enum',
			values: _.values(PageConst.TEMPLATE),
			optional: true,
			default: PageConst.TEMPLATE.OTHER
		},
		businessDetail: {
			$$type: 'object',
			title: 'string|trim|optional',
			description: 'string|optional|trim',
			templateDetail: 'object|optional',
			socialMediaShareIcon: 'boolean|optional',
			email: 'email|optional',
			phone: 'string|trim|empty:false|optional',
			termAndCondition: 'string|optional|trim',
		},
		customerDetail: {
			$$type: 'object|optional',
			id: 'number|optional',
			email: 'string|optional|trim',
			phone: 'string|optional|trim',
			fullname: 'string|optional|trim',
			shippingAddress: 'string|optional|trim'
		},
		paymentDetail: {
			$$type: 'object',
			currency: {
				type: 'enum',
				values: _.values(PageConst.CURRENCY),
				optional: true,
				default: PageConst.CURRENCY.VND
			},
			inputFields: {
				type: 'array',
				items: {
					$$type: 'object',
					productId: 'string|optional',
					label: 'string|trim|empty:false',
					inputType: {
						type: 'enum',
						values: _.values(PageConst.PRICE_FIELD_TYPE)
					},
					image: 'string|trim|optional',
					description: 'string|trim|optional',
					isRequired: 'boolean|default:true',
					options: {
						$$type: 'object|optional',
						fixedAmount: 'number|optional|min:0', // positive mean >0
						unitInStock: 'number|integer|optional|min:0',
						unitsAvailable: {
							type: 'enum',
							optional: true,
							values: _.values(PageConst.UNITSAVAILABLE),
							default: PageConst.UNITSAVAILABLE.UNLIMITED
						},
						quantity: {
							$$type: 'object|optional',
							min: 'number|integer|optional|min:0',
							max: 'number|integer|optional|min:0'
						}
					},
					tags: {
						$$type: 'array|optional',
						items: {
							$$type: 'string|optional',
						}
					}
				}
			},
			labelPay: 'string|optional|trim',
		},
		customerRequiredField: {
			$$type: 'object|optional',
			email: 'boolean|optional|default:false',
			fullname: 'boolean|optional|default:false',
			phone: 'boolean|optional|default:false',
			shippingAddress: 'boolean|optional|default:false'
		},
		theme: {
			type: 'enum',
			values: _.values(PageConst.THEME),
			optional: true,
			default: PageConst.THEME.LIGHT
		},
		expireIn: {
			type: 'date', optional: true, default: null, convert: true // 2021/07/30 14:28:05 or 2021-07-30 14:28:05
		},
		sucessMsg: 'string|optional|trim',
		successRedirect: 'string|optional|trim',
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
		voucherAllowance: 'boolean|default:false',
		vouchers: {
			type: 'array',
			optional: true
		},
		recurringPayment: {
			type: 'enum',
			optional: true,
			values: _.values(PageConst.RECURRING_PAYMENT_PERIOD),
			default: PageConst.RECURRING_PAYMENT_PERIOD.NONE
		}
	},
};

const read = {
	body: {
		$$type: 'object',
		filter: {
			$$type: 'object',
			pageId: 'string|trim|optional',
			title: 'string|trim|optional',
			status: { type: 'enum', optional: true, values: _.values(PageConst.STATUS) },
			createdAt: {
				$$type: 'object|optional',
				from: { type: 'date', convert: true, optional: true },
				to: { type: 'date', convert: true, optional: true }
			}
		},
		paging: {
			$$type: 'object',
			start: 'number|integer|min:0|default:0',
			limit: 'number|integer|min:0'
		},
		sort: {
			$$type: 'object',
			createdAt: { type: 'enum', default: -1, values: [1, -1] }
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	},
};

const detail = {
	params: {
		$$type: 'object',
		pageId: 'string|trim'
	},
	query: {
		$$type: 'object',
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	}
};

const detailExternal = {
	body: {
		$$type: 'object',
		filter: {
			$$type: 'object',
			pageId: 'string'
		},
		referData: {
			$$type: 'object|optional',
			buttonId: 'string|optional'
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	}
};

const updatePage = {
	params: {
		$$type: 'object',
		pageId: 'string|trim',
	},
	body: {
		$$type: 'object',
		// type: { type: 'enum', values: _.values(PageConst.TEMPLATE) }, // can't change template
		status: { type: 'enum', optional: true, values: _.values(PageConst.STATUS) },
		businessDetail: {
			$$type: 'object|optional',
			title: 'string|trim|optional',
			description: 'string|optional|trim',
			templateDetail: 'object|optional',
			socialMediaShareIcon: 'boolean|optional',
			email: 'email|optional',
			phone: 'string|trim|empty:false|optional',
			termAndCondition: 'string|optional|trim',
		},
		paymentDetail: {
			$$type: 'object|optional',
			currency: { type: 'enum', optional: true, values: _.values(PageConst.CURRENCY) },
			inputFields: {
				type: 'array',
				optional: true,
				items: {
					$$type: 'object',
					productId: 'string|optional|trim', // if have id: update, no id: add new (gen new id)
					label: 'string|trim|empty:false',
					inputType: {
						type: 'enum',
						values: _.values(PageConst.PRICE_FIELD_TYPE)
					},
					image: 'string|trim|optional',
					description: 'string|trim|optional',
					isRequired: 'boolean|default:false',
					options: {
						$$type: 'object|optional',
						fixedAmount: 'number|optional|positive',
						unitInStock: 'number|integer|optional|positive',
						unitsAvailable: {
							type: 'enum',
							optional: true,
							values: _.values(PageConst.UNITSAVAILABLE),
							default: PageConst.UNITSAVAILABLE.UNLIMITED
						},
						quantity: {
							$$type: 'object|optional',
							min: 'number|integer|optional|min:0',
							max: 'number|integer|optional|min:0'
						},
						// itemSold: 'number|optional'
					},
					tags: {
						$$type: 'array|optional',
						items: {
							$$type: 'string|optional',
						}
					}
				}
			},
			labelPay: 'string|optional|trim',
		},
		customerRequiredField: {
			$$type: 'object|optional',
			email: 'boolean|optional',
			fullname: 'boolean|optional',
			phone: 'boolean|optional',
			shippingAddress: 'boolean|optional'
		},
		theme: { type: 'enum', optional: true, values: _.values(PageConst.THEME) },
		expireIn: {
			type: 'date', optional: true, convert: true // 2021/07/30 14:28:05 or 2021-07-30 14:28:05
		},
		sucessMsg: 'string|optional|trim',
		successRedirect: 'string|optional|trim',
		notes: {
			type: 'array',
			optional: true,
			items: {
				$$type: 'object',
				key: 'string|empty:false|optional',
				value: 'string|empty:false|optional'
			}
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
		voucherAllowance: 'boolean|optional',
	}
};

const updateInputField = {
	body: {
		$$type: 'object',
		pageId: 'string|trim',
		fieldId: 'string|trim',
		data: {
			$$type: 'object',
			label: 'string|trim|empty:false|optional',
			inputType: {
				type: 'enum',
				optional: true,
				values: _.values(PageConst.PRICE_FIELD_TYPE)
			},
			image: 'string|trim|optional',
			description: 'string|trim|optional',
			isRequired: 'boolean|optional',
			options: {
				$$type: 'object|optional',
				fixedAmount: 'number|optional|positive',
				unitInStock: 'number|integer|optional|positive',
				unitsAvailable: { type: 'enum', optional: true, values: _.values(PageConst.UNITSAVAILABLE) },
				quantity: {
					$$type: 'object|optional',
					min: 'number|integer|optional|min:0',
					max: 'number|integer|optional|min:0'
				},
				voucherAllowance: 'boolean|optional',
				tags: {
					$$type: 'array|optional',
					items: {
						$$type: 'string|optional',
					}
				}
				// itemSold: 'number|optional'
			}
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	}
};

const createOrder = {
	body: {
		$$type: 'object',
		pageId: 'string|trim',
		note: 'string|trim|optional', // ghi chú cho người bán
		paymentItems: {
			type: 'array',
			min: 1,
			items: {
				type: 'object',
				props: {
					id: 'string|trim|empty:false',
					amount: 'number',
					quantity: 'number|integer|optional',
				}
			}
		},
		customerInfos: {
			$$type: 'object',
			id: 'number|optional',
			phone: 'string|trim|optional',
			fullname: 'string|trim|optional',
			email: 'email|trim|optional',
			shippingAddress: 'string|trim|optional'
		},
		paymentMethod: {
			$$type: 'object',
			methodId: 'number',
			payCode: 'string|trim'
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
		voucherCode: 'string|optional|trim',
		note: 'string|optional|trim',
		redirectUrl: 'string|optional|trim',
		failedUrl: 'string|optional|trim',
		referButtonId: 'string|trim|optional',
		recurringPayment: {
			type: 'enum',
			optional: true,
			values: _.values(PaymentRequestConst.RECURRING_PAYMENT),
			default: PaymentRequestConst.RECURRING_PAYMENT.NULL
		}
	},
};

const receiveCallback = {
	body: {
		$$type: 'object',
		transaction: 'string|trim',
		partnerTransaction: 'string',
		amount: 'number',
		state: 'string'
		// $$type: 'object',
		// accountId: 'number',
		// transaction: 'string|trim',
		// partnerTransaction: 'string|trim',
		// amount: 'number|positive',
		// fee: 'number|min:0',
		// total: 'number|positive', // amount and fee?
		// state: {
		// 	type: 'enum',
		// 	optional: true,
		// 	values: _.values(PaymentRequestConst.IPN_STATUS)
		// },
		// desc: 'string|optional',
		// extraData: {
		// 	type: 'object',
		// 	optional: true,
		// 	props: {
		// 		supplier: 'string',
		// 		issuer: 'string',
		// 		method: 'string',
		// 		partnerTransaction: 'string'
		// 	}
		// },
		// createdAt: 'string',
		// updatedAt: 'string',
		// language: {
		// 	type: 'enum',
		// 	optional: true,
		// 	values: _.values(GeneralConstants.LANGUAGE),
		// 	default: GeneralConstants.LANGUAGE.VI
		// },
	},
};

const notify = {
	params: {
		$$type: 'object',
		pageId: 'string|trim'
	},
	body: {
		$$type: 'object',
		email: 'email',
		// contact: 'string' // phone
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	},
};

const sendEmailAndSMS = {
	body: {
		$$type: 'object',
		pageId: 'string|trim',
		method: {
			type: 'array',
			items: { type: 'string', enum: ['EMAIL', 'SMS'] },
			min: 1
		},
		chanel: {
			type: 'string',
			enum: ['INDIVIDUAL', 'GROUP'] // 'INDIVIDUAL', 'GROUP', 'CUSTOM'
		},
		target: {
			type: 'array',
			items: { type: 'number' },
			min: 1
		},
		language: {
			type: 'enum',
			optional: true,
			values: _.values(GeneralConstants.LANGUAGE),
			default: GeneralConstants.LANGUAGE.VI
		},
	}
};

module.exports = {
	create,
	read,
	detail,
	detailExternal,
	updatePage,
	updateInputField,
	createOrder,
	receiveCallback,
	notify,
	sendEmailAndSMS
};
