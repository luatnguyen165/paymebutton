const DbService = require('moleculer-db');
const _ = require('lodash');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const PaymentRequestModel = require('./model/paymentRequest.model');

module.exports = {
	name: 'paymentRequestModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
	}),

	model: PaymentRequestModel,

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: MongooseAction({
		/**
		 * action count documentations
		 * @params {Object} query => Query object. Passes to adapter
		 */
		find: {
			rest: false,
			cache: false,
			params: {
				body: {
					$$type: 'object',
					query: 'object',
					paging: {
						$$type: 'object|optional',
						start: 'number|optional|default:0',
						limit: 'number|optional|default:0',
					},
					sort: {
						$$type: 'object|optional',
						createdAt: 'number|optional|default:-1',
					},
					fields: {
						type: 'multi',
						rules: [{ type: 'object' }, { type: 'string' }],
						default: '',
					}
				}
			},
			handler: async (ctx) => {
				const payload = ctx.params.body;
				let cond = {};
				if (_.isObject(payload.query)) cond = payload.query;
				const aggregate = [{ $match: cond }];
				if (payload.fields) {
					if (_.isString(payload.fields)) {
						let fieldsName = payload.fields;
						fieldsName = `{ ${fieldsName.split(',').map((field) => `"${field.trim()}": 1`)} }`;
						aggregate.push({ $project: JSON.parse(fieldsName) });
					} else {
						aggregate.push({ $project: payload.fields });
					}
				}
				if (_.isObject(payload.sort)) aggregate.push({ $sort: payload.sort });
				if (_.isObject(payload.paging)) aggregate.push(...[{ $skip: payload.paging.start }, { $limit: payload.paging.limit }]);
				const result = await PaymentRequestModel.aggregate(aggregate);
				return result;
			},
		},
		aggregate: {
			rest: false,
			cache: false,
			params: {
				pipeline: {
					type: 'array',
					items: 'object'
				},
			},
			handler: async (ctx) => {
				const result = await PaymentRequestModel.aggregate(ctx.params.pipeline);
				return result;
			}
		}
	}),

	/**
	 * Events
	 */
	events: {},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() { },

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		//
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},

	async afterConnected() {
		//
	},
};
