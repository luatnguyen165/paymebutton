const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const paymentRequestConstant = require('../constant/paymentRequest.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	transactionId: { // field mình gửi qua ctt
		type: String,
		required: true,
	},
	orderId: { // transaction của ctt trả về
		type: String,
		// required: true
	},
	paymentId: {
		type: String,
	},
	paymentUrl: {
		type: String,
	},
	referType: {
		type: String,
		required: true,
		enum: Object.values(paymentRequestConstant.REFER_TYPE)
	},
	referId: { // pageId, linkId, buttonId
		type: String,
		required: true
	},
	referButtonId: { // buttonId
		type: String,
		default: null
	},
	amount: { // số tiền đơn hàng, chưa discount
		type: Number,
		default: 0,
	},
	discountAmount: {
		type: Number,
		default: 0
	},
	paymentAmount: { // số tiền KH phải thanh toán, đã trừ discount = amount - discountAmount
		type: Number,
		default: 0
	},
	customerDetails: [{
		_id: false,
		label: {
			type: String,
			required: true,
		},
		name: {
			type: String,
			required: true,
		},
		value: {
			type: mongoose.Schema.Types.Mixed,
			default: '',
		},
	}],
	paymentItems: [{
		_id: false,
		id: String, // id của payment item trong link/ page/ button
		label: {
			type: String,
			required: true,
		},
		name: {
			type: String,
			required: true,
		},
		unitAmount: {
			type: Number,
			// default: 0
		}, // total
		amount: { // = unitAmount * quantity
			type: Number,
			default: 0,
		}, // total
		quantity: {
			type: Number,
			default: 1,
		},
		image: {
			type: String,
			default: null
		}
	}],
	status: {
		type: String,
		enum: Object.values(paymentRequestConstant.STATUS),
		default: paymentRequestConstant.STATUS.FAILED,
	},
	expiryAt: {
		type: Date,
		default: null, // () => new Date(+new Date() + 15 * 60 * 1000), // 15 minutes
	},
	paymentMethod: Object,
	transactionInfo: Object,
	ipnInfo: String,
	reason: String,
	finishedAt: {
		type: Date,
		default: null
	},
	voucherInfo: {
		id: Number,
		code: String,
	},
	note: String,
	recurringPayment: { // hinh thuc thanh toan dinh ki (auto | remind)
		type: String,
		enum: Object.values(paymentRequestConstant.RECURRING_PAYMENT),
		default: null
	},
	crossChecking: {
		type: Boolean,
		default: false
	},
	totalRefund: { type: Number, default: 0 },
	refunds: [{ type: String }]
}, {
	collection: 'PaymentRequest',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
Schema.index({ transactionId: 1 }, { unique: true });
Schema.index({ referType: 1, status: 1 });
Schema.index({ referId: 1, status: 1 });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
