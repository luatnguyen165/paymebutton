const DbService = require('moleculer-db');
const _ = require('lodash');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const PaymePageModel = require('./model/paymePage.model');

module.exports = {
	name: 'paymePageModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
	}),

	model: PaymePageModel,

	/**
	 * Settings
	 */
	settings: {
	},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: MongooseAction({
		count: {
			rest: false,
			cache: false,
			params: {
				query: {
					$$type: 'object|optional|default:{}',
				},
			},
			handler: async (ctx) => {
				const { query } = ctx.params;
				const result = await PaymePageModel.countDocuments(query);
				return result;
			},
		},
		find: {
			rest: false,
			cache: false,
			params: {
				body: {
					$$type: 'object',
					query: 'object',
					paging: {
						$$type: 'object|optional',
						start: 'number|optional|default:0',
						limit: 'number|optional|default:0',
					},
					sort: {
						$$type: 'object|optional',
					},
					fields: {
						type: 'multi',
						rules: [{ type: 'object' }, { type: 'string' }],
						default: '',
					},
				},
			},
			handler: async (ctx) => {
				const payload = ctx.params.body;
				const result = await PaymePageModel.find(payload.query)
					.select(payload.fields)
					.sort(payload.sort)
					.skip(_.get(payload, 'paging.start', undefined))
					.limit(_.get(payload, 'paging.limit', undefined))
					.lean();
				return result;
			},
		},
		findAll: {
			handler: async (ctx) => {
				const result = await PaymePageModel.find().lean();

				return result;
			}
		}
	}),

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {
		//
	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},

	async afterConnected() {
		//
	},
};
