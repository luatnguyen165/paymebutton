const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const PageConst = require('../../paymePage/constants/paymePage.constant');

autoIncrement.initialize(mongoose);

const statusOrder = {
	PROCESSING: 'PROCESSING',
	SUCCESS: 'SUCCESS',
	NULL: null
};

const Schema = mongoose.Schema({
	pageId: {
		type: String,
		required: true
	},
	aliasPageName: {
		type: String,
		required: true
	},
	alias: {
		type: String,
		default: null,
		unique: true
	},
	creator: {
		merchantId: {
			type: Number,
			require: true
		},
		accountId: {
			type: Number,
			require: true
		},
		email: {
			type: String,
			require: true
		},
		username: {
			type: String,
			require: true,
		},
		storeId: {
			type: String,
		}
	},
	type: { // 4 template
		type: String,
		enum: _.values(PageConst.TEMPLATE),
		default: PageConst.TEMPLATE.OTHER
	},
	businessDetail: {
		title: {
			type: String,
			default: null
		},
		description: { // type == OTHER
			type: String,
			default: null
		},
		templateDetail: { // Description for 4 templates
			type: Object
		},
		socialMediaShareIcon: {
			type: Boolean
		},
		email: {
			type: String,
			// required: true
		},
		phone: {
			type: String,
			// required: true
		},
		termAndCondition: {
			type: String
		},
	},
	paymentDetail: {
		currency: {
			type: String
		},
		inputFields: [
			{
				_id: false,
				id: String,
				label: String,
				inputType: {
					type: String,
					enum: _.values(PageConst.PRICE_FIELD_TYPE)
				},
				image: String,
				description: String,
				isRequired: { // cho phép user chọn thanh toán hoặc không, do user thanh toán chọn hoặc bỏ chọn sp
					type: Boolean,
					default: false
				},
				isActive: { // cho phép show ở trang thanh toán của user hay ko,  do MC setup on/off
					type: Boolean,
					default: true
				},
				options: { // optional
					fixedAmount: Number,
					unitInStock: Number, // stock cannot be lesser than the quantity sold
					unitsAvailable: {
						type: String,
						enum: ['LIMITED', 'UNLIMITED']
					},
					quantity: { min: Number, max: Number },
					itemSold: {
						type: Number,
						// default: 0
					},
					itemInOrder: {
						type: Number,
						// default: 0
					},
					revenue: {
						type: Number,
						// default: 0
					}
				},
				tags: [String]
			},
		],
		labelPay: {
			type: String
		},
	},
	customerRequiredField: {
		email: {
			type: Boolean,
			default: false
		},
		shippingAddress: {
			type: Boolean,
			default: false
		},
		fullname: {
			type: Boolean,
			default: false
		},
		phone: {
			type: Boolean,
			default: false
		}
	},
	customerDetail: { // Chi tiết khách hàng
		id: {
			type: Number
		},
		email: {
			type: String
		},
		phone: {
			type: String
		},
		fullname: {
			type: String
		},
		shippingAddress: String
	},
	totalRevenue: { // doanhthu theo số lượng sp đã bán, đã trừ giảm giá, vouchers
		type: Number,
		default: 0
	},
	actualRevenue: { // doanh thu sau khi trừ refund, cancel
		type: Number,
		default: 0
	},
	shortLinkInfo: {
		url: String,
		slug: String,
		transaction: String
	},
	theme: {
		type: String,
		default: PageConst.THEME.LIGHT,
		enum: _.values(PageConst.THEME)
	},
	expireIn: {
		type: Date,
		default: null
	},
	successMsg: {
		type: String
	},
	successRedirect: {
		type: String
	},
	status: {
		type: String,
		enum: _.values(PageConst.STATUS),
	},
	notes: {
		key: String,
		value: String
	},
	hyperlinkButton: {
		type: Object
	},
	receiptsSetting: {
		type: Object
	},
	statusOrder: {
		type: String,
		enum: _.values(statusOrder),
		default: statusOrder.NULL
	},
	isNotified: {
		email: {
			type: Boolean,
			default: false
		},
		sms: {
			type: Boolean,
			default: false
		}
	},
	totalSending: {
		email: {
			success: {
				type: Number,
				default: 0
			},
			totalSend: {
				type: Number,
				default: 0
			}
		},
		sms: {
			success: {
				type: Number,
				default: 0
			},
			totalSend: {
				type: Number,
				default: 0
			}
		}
	},
	voucherAllowance: {
		type: Boolean,
		default: false
	},
	vouchers: {
		type: Array,
		default: null
	},
	tags: {
		type: Array,
		default: null
	},
	recurringPayment: {
		type: String,
		enum: _.values(PageConst.RECURRING_PAYMENT_PERIOD),
		default: PageConst.RECURRING_PAYMENT_PERIOD.NONE
	}
}, {
	collection: 'PaymePage',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
Schema.index({ 'businessDetail.title': 'text' });
Schema.index({ pageId: 1 });
/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
