const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const PageConst = require('../../paymePage/constants/paymePage.constant');
const PaymentRequestConst = require('../../paymentRequestModel/constant/paymentRequest.constant');
const PaymeSubcriptionConst = require('../constants/paymeSubcription.constant');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	paymentRequestId: {
		type: Number,
		required: true
	},
	pageId: {
		type: String,
		required: true
	},
	pageTitle: {
		type: String,
		required: true
	},
	merchantId: {
		type: Number,
		require: true
	},
	accountId: {
		type: Number,
		require: true
	},
	subcriptionId: {
		type: String,
		required: true
	},
	recurringPaymentPeriod: {
		type: String,
		enum: _.values(PageConst.RECURRING_PAYMENT_PERIOD),
		required: true
	},
	previousPaymentDate: {
		type: Date,
		defautl: null
	},
	nextPaymentDate: {
		type: Date,
		required: true
	},
	recurringPaymentType: { // hình thức thanh toán đinh kì (Auto|remind)
		type: String,
		enum: _.values(PaymentRequestConst.RECURRING_PAYMENT),
		required: true
	},
	amount: {
		type: Number,
		default: 0
	},
	paymentItems: [{
		_id: false,
		id: String, // id của payment item trong link/ page/ button
		label: {
			type: String,
			required: true,
		},
		name: {
			type: String,
			required: true,
		},
		unitAmount: {
			type: Number,
			// default: 0
		}, // total
		amount: { // = unitAmount * quantity
			type: Number,
			default: 0,
		}, // total
		quantity: {
			type: Number,
			default: 1,
		},
		image: {
			type: String,
			default: null
		}
	}],
	customerDetail: { // Chi tiết khách hàng
		id: {
			type: Number
		},
		email: {
			type: String
		},
		phone: {
			type: String
		},
		fullname: {
			type: String
		},
		shippingAddress: String
	},
	// status: {
	// 	type: String,
	// 	enum: _.values(PaymeSubcriptionConst.STATUS),
	// 	default: PaymeSubcriptionConst.STATUS.PENDING
	// },
	isActive: { // MC on/off
		type: Boolean,
		default: true
	},
	countPayment: {
		type: Number,
		default: 0
	},
	isSubscribe: { // KH hủy nhắc tt
		type: Boolean,
		default: true
	},
}, {
	collection: 'PaymeSubcription',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
Schema.index({ pageId: 1, accountId: 1, merchantId: 1 });
Schema.index({ subcriptionId: 1, accountId: 1, merchantId: 1 });
Schema.index({ pageId: 1, subcriptionId: 1, });
Schema.index({ pageTitle: 'text' });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
