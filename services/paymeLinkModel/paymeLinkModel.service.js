const DbService = require('moleculer-db');
const MongooseAdapter = require('moleculer-db-adapter-mongoose');
const MongooseAction = require('moleculer-db-adapter-mongoose-action');
const PaymeLinkModel = require('./model/paymeLink.model');

module.exports = {
	name: 'paymeLinkModel',

	version: 1,

	mixins: [DbService],

	adapter: new MongooseAdapter(process.env.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
	}),

	model: PaymeLinkModel,

	/**
	 * Settings
	 */
	settings: {},

	/**
	 * Dependencies
	 */
	dependencies: [],

	/**
	 * Actions
	 */
	actions: MongooseAction({
		count: {
			rest: false,
			cache: false,
			params: {
				query: {
					$$type: 'object|optional|default:{}',
				},
			},
			handler: async (ctx) => {
				const { query } = ctx.params;
				const result = await PaymeLinkModel.countDocuments(query);
				return result;
			},
		},
		// findOne: {
		// 	rest: false,
		// 	cache: false,
		// 	params: [{
		// 		$$type: 'object|optional|default:{}',
		// 	}],
		// 	handler: async (ctx) => {
		// 		const { query } = ctx.params;
		// 		const result = await PaymeLinkModel.findOne(query).lean();
		// 		return result;
		// 	},
		// },
		find: {
			rest: false,
			cache: false,
			params: {
				body: {
					$$type: 'object',
					query: 'object',
					paging: {
						$$type: 'object',
						start: 'number|optional|default:0',
						limit: 'number|optional|default:0',
					},
					sort: {
						$$type: 'object',
					},
					fields: {
						type: 'multi',
						rules: [{ type: 'object' }, { type: 'string' }],
						default: '',
					},
				},
			},
			handler: async (ctx) => {
				const payload = ctx.params.body;
				const result = await PaymeLinkModel.find(payload.query)
					.select(payload.fields)
					.sort(payload.sort)
					.skip(payload.paging.start)
					.limit(payload.paging.limit)
					.lean();
				return result;
			},
		},
		findAll: {
			handler: async (ctx) => {
				const result = await PaymeLinkModel.find().lean()

				return result;
			}
		},
		aggregate: {
			rest: false,
			cache: false,
			params: {
				pipeline: {
					type: 'array',
					items: 'object'
				},
			},
			handler: async (ctx) => {
				const result = await PaymeLinkModel.aggregate(ctx.params.pipeline);
				return result;
			}
		}

	}),

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	async started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */

	// async stopped() {},

	async afterConnected() {

	},
};