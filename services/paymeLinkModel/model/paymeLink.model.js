const _ = require('lodash');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const statusPaymeLink = require('../../paymeLink/constants/status');
const typePaymeLink = require('../../paymeLink/constants/type');
const statusOrder = require('../../paymeLink/constants/statusOrder');

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema({
	customerDetail: { // Chi tiết khách hàng
		id: {
			type: Number
		},
		email: {
			type: String
		},
		phone: {
			type: String
		},
		fullname: {
			type: String
		},
		shippingAddress: String
	},
	createdBy: { // Thông tin người khởi tạo link
		merchantId: {
			type: Number,
			required: true
		},
		accountId: {
			type: Number,
			required: true
		},
		email: {
			type: String,
			default: null
		},
		phone: {
			type: String,
			default: null
		},
		storedId: {
			type: String
		}
	},
	description: { // Chú thích link
		type: String,
		default: ''
	},
	referenceId: { //
		type: String,
		default: ''
	},
	expiredAt: { // Ngày hết hạn sử dụng link (Nếu không có truyền null)
		type: Date,
		default: null
	},
	status: { // Trạng thái link thanh toán
		type: String,
		enum: _.values(statusPaymeLink)
	},
	// link: { // link thanh toán
	//     type: String,
	//     required: true
	// },
	paymentLink: {
		type: String,
		required: true,
		unique: true
	},
	linkId: { // Id link thanh toán
		type: String,
		required: true,
		unique: true
	},
	statusOrder: {
		type: String,
		enum: _.values(statusOrder),
		default: statusOrder.NULL
	},
	amount: { // Số tiền ban đầu cần thanh toán
		type: Number,
		default: 0
		// min: 1
	},
	currency: { // đơn vị tiền
		type: String,
		required: true
	},
	amountPaid: { // Số tiền đã thanh toán (Trong trường hợp thanh toán nhiều lần)
		type: Number,
		default: 0
	},
	isAcceptedPartialPayment: { // Chấp nhận thanh toán từng phần hay không
		type: Boolean,
		default: false
	},
	minPaidPartial: { // Giới hạn min trong những lần thanh toán ( TH thanh toán nhiều lần)
		type: Number,
		default: 0
	},
	notify: { // Gửi thông báo qua email hoặc sms
		email: {
			type: Boolean,
			default: false,
			required: true
		},
		sms: {
			type: Boolean,
			default: false,
			required: true
		}
	},
	isNotified: {
		email: {
			type: Boolean,
			default: false,
		},
		sms: {
			type: Boolean,
			default: false,
		}
	},
	note: {
		type: String,
		default: ''
	},
	type: { // Loại link khởi tạo
		type: String,
		enum: _.values(typePaymeLink),
		required: true
	},
	orderRequiredField: {
		email: {
			type: Boolean,
			default: false
		},
		shippingAddress: {
			type: Boolean,
			default: false
		},
		fullname: {
			type: Boolean,
			default: false
		},
		phone: {
			type: Boolean,
			default: false
		}
	},
	shortLinkInfo: {
		url: String,
		slug: String,
		transaction: String
	},
	finishedAt: {
		type: Date,
		default: null
	},
	attachedFiles: [{
		_id: false,
		fileName: String,
		url: String
	}],
	extraData: mongoose.Schema.Types.Mixed,
	totalSending: {
		email: {
			success: {
				type: Number,
				default: 0
			},
			totalSend: {
				type: Number,
				default: 0
			}
		},
		sms: {
			success: {
				type: Number,
				default: 0
			},
			totalSend: {
				type: Number,
				default: 0
			}
		}
	},
}, {
	collection: 'PaymeLink',
	versionKey: false,
	timestamps: true,
});

Schema.index({ id: 1 }, { unique: true });
Schema.index({ linkId: 1 });
Schema.index({ status: 1 });
Schema.index({ linkId: 1, status: 1 });
Schema.index({ status: 1, expiredAt: 1 });
Schema.index({ note: 'text', description: 'text' });

/*
| ==========================================================
| Plugins
| ==========================================================
*/

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}_id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

/*
| ==========================================================
| Methods
| ==========================================================
*/

/*
| ==========================================================
| HOOKS
| ==========================================================
*/

module.exports = mongoose.model(Schema.options.collection, Schema);
