const _ = require('lodash');

const arr = [{ tags: [1, 2, 3] }, { tags: [2, 4, 5] }];
const tagsName = arr.map((field) => field.tags).filter((v) => v);
console.log(_.concat(...tagsName));
